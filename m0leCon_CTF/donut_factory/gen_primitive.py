from ptrlib import *
import pickle
import hashlib

sock = Socket("localhost", 9999)

def md5donut(roundness):
    sock.sendlineafter("factory\n", "c")
    sock.recvline()
    sock.sendline(str(roundness))
    sock.recvuntil("I'm making it....\n")
    hashval = hashlib.md5(sock.recvuntil("(y/n)\n")[:-23]).hexdigest()
    sock.sendline("n")
    return hashval

try:
    with open("revtable", "rb") as f:
        table = pickle.load(f)
    n = max(table.keys()) + 1
except:
    table = {}
    n = 0

for i in range(n, 0x100):
    hashval = md5donut(i)
    print(i, hashval)
    table[i] = hashval
    with open("revtable", "wb") as f:
        pickle.dump(table, f)

sock.interactive()
