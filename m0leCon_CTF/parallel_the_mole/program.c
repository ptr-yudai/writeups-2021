typedef struct {
  pthread_t *th;
  int n;
} X;

void func1() {
}

void wrapper1(X* x) {
  int r = rand();
  usleep(r % 1000);
  pthread_mutex_lock(mutex);
  int a = ord_ind;
  if ( ord_ind > 6 ) {
    ++ord_ind;
    order[20 - a] = x->n;
  } else {
    ++ord_ind;
    order[a] = x->n;
  }
  funcs[x->n](s_input, 0x10);
  pthread_mutex_unlock(mutex);
  pthread_exit(0);
}

void wrapper2(X* x) {
  pthread_mutex_lock(mutex);
  while (order[ord_ind] != x->n)
    pthread_cond_wait(cv, mutex);
  funcs[x->n])(s_input, 0x10);
  ++ord_ind;
  pthread_cond_broadcast(cv);
  pthread_mutex_unlock(mutex);
  pthread_exit(0);
}

int main(int argc, const char **argv, const char **envp)
{
  assert (strlen(argv[1]) == 0x10);
  strcpy(s_input, argv[1]);
  mutex = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
  cv = (pthread_cond_t *)malloc(sizeof(pthread_cond_t));
  pthread_mutex_init(mutex, 0LL);
  pthread_cond_init(cv, 0LL);

  pthread_t **threads = (pthread_t**)malloc(sizeof(pthread_t*) * 14);
  X *xs = (X*)malloc(sizeof(X) * 14);

  srand(time(NULL));

  /* Round 1 */
  for (int i = 0; i < 14; i++) {
    xs[i].n  = i;
    xs[i].th = threads[i];
    pthread_create(&threads[i], NULL, wrapper1, &xs[i]);
  }
  for (int i = 0; i < 14; i++)
    pthread_join(threads[i], NULL);
  print_str(s_input, 0x10, 0);

  /* Round 2 */
  ord_ind = 0;
  strcpy(s_input, argv[1]);
  for (int i = 0; i < 14; i++) {
    xs[i].n  = i;
    xs[i].th = threads[i];
    pthread_create(&threads[i], NULL, wrapper2, &xs[i]);
  }
  for (int i = 0; i < 14; i++)
    pthread_join(threads[i], 0LL);

  pthread_mutex_destroy(mutex);
  pthread_cond_destroy(cv);
  print_str(s_input, 0x10, 1);
  putchar('\n');
  return 0;
}
