import numpy as np

D = np.matrix([
    [0xac0, 0x779, 0xa43, 0x859, 0x982],
    [0xcda, 0x92c, 0xc10, 0x9d5, 0xb5e],
    [0xfc9, 0xba1, 0xe65, 0xbc2, 0xdfc],
    [0x1465, 0xf9e, 0x120c, 0xf08, 0x1200],
    [0x1b6e, 0x15c2, 0x17de, 0x13d5, 0x183b]
])
C = [
    [0 for i in range(5)]
    for j in range(5)
]
x = 1
for i in range(5):
    for j in range(i+1):
        C[j][i] = x
        C[i][j] = x
        x += 1
C = np.matrix(C)
B = C**-1 * D

# shift
B = np.roll(B, 2, axis=1)
print(B)

# sub
for i in range(5):
    for j in range(5):
        B[j,i] = round(B[j,i]) - round((i+1)*(j-2))

flag = ""
for i in range(5):
    for j in range(5):
        flag += chr(round(B[i,j]))

print(flag)
