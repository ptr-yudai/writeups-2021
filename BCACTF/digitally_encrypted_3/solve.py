from z3 import *

with open("encrypted.txt", "r") as f:
    r = list(map(lambda x: int(x,16), f.read().split()))

flag = b""
for b in r:
    p = BitVec("p", 64)
    r2 = 0
    r1 = p
    counter = 1
    for i in range(64):
        r2 = (r1 << counter) + (r1 ^ r2)
        r1 = r1 + r1
        counter += 1
    s = Solver()
    s.add(r2 == b)
    r = s.check()
    if r == sat:
        m = s.model()
        rp = m[p].as_long()
        flag += int.to_bytes(int(rp), 8, 'big')
    else:
        print(r)
        exit(1)

print(flag)
