from z3 import *

def encrypt_block(p, k):
    # p: 64-bit / k: 40-bit
    k1 = k & 0xffffffff # 32-bit
    k2 = k >> 8 # 32-bit
    p1 = p & 0xffffffff # 32-bit
    p2 = p >> 32 # 32-bit
    sp1 = ((p1 ^ k1) ^ 0xffffffff) ^ p2
    sp2 = p1
    c1 = sp1
    c2 = ((sp1 ^ k2) ^ 0xffffffff) ^ sp2
    return c1 | (c2 << 32)

def z3_encrypt_block(p, k):
    # p: 64-bit / k: 40-bit
    k1 = Extract(31, 0, k)
    k2 = Extract(39, 8, k)
    p1 = Extract(31, 0, p)
    p2 = Extract(63, 32, p)
    sp1 = ((p1 ^ k1) ^ 0xffffffff) ^ p2
    sp2 = p1
    c1 = sp1
    c2 = ((sp1 ^ k2) ^ 0xffffffff) ^ sp2
    return ZeroExt(32, c1) | (ZeroExt(32, c2) << 32)

with open("encrypted.txt", "r") as f:
    cipher = list(map(lambda x: int(x,16), f.read().split()))

# Leak the key
c = cipher[0]
p = BitVec('p', 64)
k = BitVec('k', 40)
s = Solver()
s.add(p >> 8 == int(b'bcactf{'.hex(), 16))
s.add(z3_encrypt_block(p, k) == c)

r = s.check()
if r == sat:
    m = s.model()
    rp = int(m[p].as_long())
    rk = int(m[k].as_long())
    print(hex(rp))
    print(hex(rk))
else:
    print(r)
    exit(1)

# Find the plaintext
flag = b""
for c in cipher:
    p = BitVec('p', 64)
    k = BitVec('k', 40)
    s = Solver()
    s.add(k == rk)
    s.add(z3_encrypt_block(p, k) == c)

    r = s.check()
    if r == sat:
        m = s.model()
        rp = m[p].as_long()
        flag += int.to_bytes(rp, 8, 'big')
    else:
        print(r)
        exit(1)

print(flag)
