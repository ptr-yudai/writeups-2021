from ptrlib import *

with open("trailblazer", "rb") as f:
    prog = f.read()

#ge52y9}
key = b'bcactf{now_thats_how_you_blaze_a_trail_8'
print(len(prog))
prog = prog[:0x1262] + xor(prog[0x1262:0x1262+len(key)], key) + prog[0x1262+len(key):]
print(len(prog))

with open("dump", "wb") as f:
    f.write(prog)

