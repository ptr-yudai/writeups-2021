import angr
import claripy
from logging import getLogger, WARN

getLogger("angr").setLevel(WARN + 1)

p = angr.Project("./story3", load_options={"auto_load_libs": False})
for minutes in range(60):
    state = p.factory.blank_state(addr=0x400000 + 0x1ad2)
    simgr = p.factory.simulation_manager(state)

    @p.hook(0x40191c, length=0)
    def pokemon(state):
        global minutes
        state.regs.eax = minutes

    simgr.explore(find=0x401991)
    print(minutes, simgr.found[0].posix.dumps(1))
