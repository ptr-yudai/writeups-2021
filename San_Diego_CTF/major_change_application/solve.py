from ptrlib import *
from z3 import *
import ctypes

### S3v3ru5 solver
from Crypto.Util.number import inverse, GCD
import math
import random

modulus = 0xffffffffffffffff + 1
base = 31

assert GCD(base, modulus) == 1

def gen_hash(name):
    hval = 0xC0DE5BAD13375EED
    for c in name:
        hval = (hval * 31 + c) & 0xffffffffffffffff
    return hval

def inv_gen_hash(target):
    hval = 0xC0DE5BAD13375EED
    approx_chars = math.ceil(math.log(modulus, base))
    # while True:
    #     m = 2*modulus
    c = []
    value = (target - hval * base**approx_chars) % modulus
    # value += modulus
    while value > 0:
        c.append(value % 31)
        value = (value - (value % base)) // base
    name = c[::-1]
    if len(c) == (approx_chars + 1):
        if (name[0] * 31 + name[1]) < 256:
            name = [name[0]*31 + name[1]] + name[2:]
        else:
            return
    if 0 in name or 10 in name:

        return
    assert gen_hash(name) == target
    return name
###

glibc = ctypes.cdll.LoadLibrary('/lib/x86_64-linux-gnu/libc-2.27.so')
text = b"ONCE UPON A TIME THERE USED TO BE A FAMILY OF DRAGONS. ONE OF THEM IS CALLED GHIDRA, AND THE OTHER IS CALLED..."

def gen_id(last, first):
    seed = 0
    for c in first:
        seed = seed * 31 + ord(c)
        seed &= 0xffffffffffffffff
    for c in last:
        seed = seed * 31 + ord(c)
        seed &= 0xffffffffffffffff
    glibc.srand(seed)
    return "H{:08d}".format(glibc.rand() % 100000000)

def randgen64(seed):
    def randgen32(seed):
        glibc.srand(seed)
        return glibc.rand()
    return randgen32(seed >> 32) + (randgen32(seed & 0xffffffff) << 32)

def gen_hash_2(last, first):
    hval = 0xC0DE5BAD13375EED
    for c in first:
        hval = (hval * 31 + ord(c)) & 0xffffffffffffffff
    for c in last:
        hval = (hval * 31 + ord(c)) & 0xffffffffffffffff
    return hval

time_seed = glibc.time(0) >> 2
logger.info("<attempt> time_seed = " + hex(time_seed))

for i in range(0, 10000):
    name = inv_gen_hash(time_seed + i)
    if name is not None:
        go = time_seed + i
        break

print(name, time_seed, go)
import time
while True:
    if glibc.time(0) >> 2 == go:
        break
    time.sleep(1)
#sock = Socket("localhost", 9999)
sock = Socket("nc major.sdc.tf 1337")
time_seed = glibc.time(0) >> 2
logger.info("time_seed = " + hex(time_seed))

name = ''.join(map(chr, name))
last  = name[1:]
first = name[:1]
sock.sendlineafter("): ", last + " " + first)
sock.sendlineafter("ID: ", gen_id(last, first))

print(hex(gen_hash_2(last, first)))
delta = randgen64(time_seed) - randgen64(gen_hash_2(last, first))
delta %= 0x10000000000000000
print(delta)

result = b"6)bc"
response = ""
for i in range(4):
    response += chr((result[i] - i*i*i) ^ ((text[i] - i*i) % 0x100))
sock.sendlineafter("?\n", response)

sock.interactive()
