import gdb

gdb.execute("break *0x400f5f")
gdb.execute("run")

flag = ""
while True:
    al = gdb.execute("p/x $dl", to_string=True).strip().split("= ")[1]
    flag += chr(int(al, 16))
    print(flag)
    gdb.execute("set $al=$dl")
    gdb.execute("continue")
