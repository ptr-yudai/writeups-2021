import ctypes
import zipfile
import os

libc = ctypes.CDLL("../../libc-2.31.so")

#
s = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
for i in range(-60, 60):
    print(i)
    libc.srand(1637532720 + i)
    password = ""
    for j in range(15):
        password += s[libc.rand() % len(s)]

    os.system(f"7za x -aoa -p{password} flag.zip")
    with open("flag.txt", "rb") as f:
        buf = f.read()
    if buf:
        print(buf)
        exit()
