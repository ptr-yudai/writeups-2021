import ctypes

with open("../distfiles/beanstalk", "rb") as f:
    f.seek(0x24D8)
    table = f.read(0x100)
    f.seek(0x25FB)
    hashval = f.read(10)
    f.seek(0x262B)
    cipher = f.read(0x30)

"""
1. Calculate key by hash
The program checks the "hash" value of the license key.
This hash value is derived from the encryption table:

  _hash[i] = _table[i][0x77];

This table is generated like this:

  for (int c = 0; c < 256; c++) {
    _table[i][c] = t()[c ^ key[i]];
  }

So, we can easily find the original key, which is necessary
to decrypt the block cipher. (Modified version of skipjack)
"""
key = []
for i in range(10):
    key.append(table.index(hashval[i]) ^ 0x77)

lic = 'BEAN-{:02x}{:02x}-{:02x}{:02x}{:02x}{:02x}{:02x}-{:02x}{:02x}{:02x}'\
.format(
    key[1], key[0], key[6], key[5], key[4],
    key[3], key[2], key[9], key[8], key[7]
)
print("Key: " + str(list(map(hex, key))))
print("License: " + lic)

"""
2. Decrypt ciphertext
As we have the symmetric key, we can decrypt the ciphertext.
Basically we only need to perform the reverse operation of the
encryption function. Be aware it uses libc's rand function.
Also, the internal state (w) of the skipjack cryptosystem is shifted.
"""
libc = ctypes.CDLL("/lib/x86_64-linux-gnu/libc-2.31.so")

def decrypt(w, t):
    global cnt
    libc.srand(t[1][14] | (t[5][14]<<8) | (t[8][10]<<16) | (t[9][31]<<24))
    randlist = [libc.rand() & 0xffff for i in range(0x20)][::-1]
    cnt = 0

    def h(w, a, i, j, k, l):
        w[a] ^= t[i][w[a] >> 8]
        w[a] ^= t[j][w[a] & 0xff] << 8
        w[a] ^= t[k][w[a] >> 8]
        w[a] ^= t[l][w[a] & 0xff] << 8
    def h0(w, a): h(w, a, 0, 1, 2, 3)
    def h1(w, a): h(w, a, 4, 5, 6, 7)
    def h2(w, a): h(w, a, 8, 9, 0, 1)
    def h3(w, a): h(w, a, 2, 3, 4, 5)
    def h4(w, a): h(w, a, 6, 7, 8, 9)
    def z(w, a, b):
        global cnt
        w[a] ^= w[b] ^ randlist[cnt]
        cnt += 1

    h1(w, 0); z(w, 1, 0);
    h0(w, 1); z(w, 2, 1);
    h4(w, 2); z(w, 3, 2);
    h3(w, 3); z(w, 0, 3);
    h2(w, 0); z(w, 1, 0);
    h1(w, 1); z(w, 2, 1);
    h0(w, 2); z(w, 3, 2);
    h4(w, 3); z(w, 0, 3);

    z(w, 3, 0); h3(w, 0);
    z(w, 0, 1); h2(w, 1);
    z(w, 1, 2); h1(w, 2);
    z(w, 2, 3); h0(w, 3);
    z(w, 3, 0); h4(w, 0);
    z(w, 0, 1); h3(w, 1);
    z(w, 1, 2); h2(w, 2);
    z(w, 2, 3); h1(w, 3);

    h0(w, 0); z(w, 1, 0);
    h4(w, 1); z(w, 2, 1);
    h3(w, 2); z(w, 3, 2);
    h2(w, 3); z(w, 0, 3);
    h1(w, 0); z(w, 1, 0);
    h0(w, 1); z(w, 2, 1);
    h4(w, 2); z(w, 3, 2);
    h3(w, 3); z(w, 0, 3);

    z(w, 3, 0); h2(w, 0);
    z(w, 0, 1); h1(w, 1);
    z(w, 1, 2); h0(w, 2);
    z(w, 2, 3); h4(w, 3);
    z(w, 3, 0); h3(w, 0);
    z(w, 0, 1); h2(w, 1);
    z(w, 1, 2); h1(w, 2);
    z(w, 2, 3); h0(w, 3);

    plain = []
    for i in range(4):
        plain.append(w[i] >> 8)
        plain.append(w[i] & 0xff)
    return plain

t = [[0 for j in range(256)] for i in range(10)]
for i in range(10):
    for c in range(256):
        t[i][c] = table[c ^ key[i]]

flag = ''
for ofs in range(0, len(cipher), 8):
    blk = cipher[ofs:ofs+8]
    w = [blk[1]|(blk[0]<<8), blk[3]|(blk[2]<<8), blk[5]|(blk[4]<<8), blk[7]|(blk[6]<<8)]
    plain = decrypt(w, t)
    for c in plain:
        flag += chr(c)

print("Flag: " +flag)
