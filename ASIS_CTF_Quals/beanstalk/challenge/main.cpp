#include <iostream>
#include <sstream>

using namespace std;

uint8_t* __attribute__ ((noinline)) t() {
  uint8_t *p;
  __asm__ volatile(".intel_syntax noprefix;"
                   "xor eax, eax;"
                   ".byte 0xE8,0x00,0x01,0x00,0x00;"
                   ".byte 0xa3,0xd7,0x09,0x83,0xf8,0x48,0xf6,0xf4,0xb3,0x21,0x15,0x78,0x99,0xb1,0xaf,0xf9;"
                   ".byte 0xe7,0x2d,0x4d,0x8a,0xce,0x4c,0xca,0x2e,0x52,0x95,0xd9,0x1e,0x4e,0x38,0x44,0x28;"
                   ".byte 0x0a,0xdf,0x02,0xa0,0x17,0xf1,0x60,0x68,0x12,0xb7,0x7a,0xc3,0xe9,0xfa,0x3d,0x53;"
                   ".byte 0x96,0x84,0x6b,0xba,0xf2,0x63,0x9a,0x19,0x7c,0xae,0xe5,0xf5,0xf7,0x16,0x6a,0xa2;"
                   ".byte 0x39,0xb6,0x7b,0x0f,0xc1,0x93,0x81,0x1b,0xee,0xb4,0x1a,0xea,0xd0,0x91,0x2f,0xb8;"
                   ".byte 0x55,0xb9,0xda,0x85,0x3f,0x41,0xbf,0xe0,0x5a,0x58,0x80,0x5f,0x66,0x0b,0xd8,0x90;"
                   ".byte 0x35,0xd5,0xc0,0xa7,0x33,0x06,0x65,0x69,0x45,0x00,0x94,0x56,0x6d,0x98,0x9b,0x76;"
                   ".byte 0x97,0xfc,0xb2,0xc2,0xb0,0xfe,0xdb,0x20,0xe1,0xeb,0xd6,0xe4,0xdd,0x47,0x4a,0x1d;"
                   ".byte 0x42,0xed,0x9e,0x6e,0x49,0x3c,0xcd,0x43,0x27,0xd2,0x07,0xd4,0xde,0xc7,0x67,0x18;"
                   ".byte 0x89,0xcb,0x30,0x1f,0x8d,0xc6,0x8f,0xaa,0xc8,0x74,0xdc,0xc9,0x5d,0x5c,0x31,0xa4;"
                   ".byte 0x70,0x88,0x61,0x2c,0x9f,0x0d,0x2b,0x87,0x50,0x82,0x54,0x64,0x26,0x7d,0x03,0x40;"
                   ".byte 0x34,0x4b,0x1c,0x73,0xd1,0xc4,0xfd,0x3b,0xcc,0xfb,0x7f,0xab,0xe6,0x3e,0x5b,0xa5;"
                   ".byte 0xad,0x04,0x23,0x9c,0x14,0x51,0x22,0xf0,0x29,0x79,0x71,0x7e,0xff,0x8c,0x0e,0xe2;"
                   ".byte 0x0c,0xef,0xbc,0x72,0x75,0x6f,0x37,0xa1,0xec,0xd3,0x8e,0x62,0x8b,0x86,0x10,0xe8;"
                   ".byte 0x08,0x77,0x11,0xbe,0x92,0x4f,0x24,0xc5,0x32,0x36,0x9d,0xcf,0xf3,0xa6,0xbb,0xac;"
                   ".byte 0x5e,0x6c,0xa9,0x13,0x57,0x25,0xb5,0xe3,0xbd,0xa8,0x3a,0x01,0x05,0x59,0x2a,0x46;"
                   "pop rax;"
                   "mov %0, rax;"
                   : "=r"(p)
                   :
                   : "rax");
  return p;
}

uint8_t* __attribute__ ((noinline)) k(int sw) {
  uint8_t *p;
  __asm__ volatile(".intel_syntax noprefix;"
                   "xor eax, eax;"
                   ".byte 0xE8,0x0a,0x00,0x00,0x00;"
                   ".byte 0xbf,0x0b,0x0f,0xa2,0xa5,0x94,0x0e,0xd7,0xcb,0x85;"
                   "pop rax;"
                   "cmp %1, 0;"
                   "jz _%=;"
                   "add rax, 10;"
                   "_%=:"
                   "mov %0, rax;"
                   : "=r"(p)
                   : "r"(sw)
                   : "rax");
  return p;
}

uint8_t* __attribute__ ((noinline)) f() {
  uint8_t *p;
  __asm__ volatile(".intel_syntax noprefix;"
                   "xor eax, eax;"
                   ".byte 0xE8,0x30,0x00,0x00,0x00;"
                   ".byte 0x46,0x9d,0xfa,0x32,0x51,0xe2,0x65,0xf4;"
                   ".byte 0x80,0xc6,0xbe,0xb3,0xc6,0x6e,0x7e,0x3c;"
                   ".byte 0x65,0xc1,0x35,0xe0,0x11,0x19,0x0d,0x86;"
                   ".byte 0x2e,0x93,0xfe,0xea,0xd6,0x67,0xd7,0xb1;"
                   ".byte 0xcd,0xec,0x52,0xe4,0x53,0x3e,0x3b,0xe1;"
                   ".byte 0x0a,0xfd,0x50,0x7e,0xb4,0xf8,0xd0,0x43;"
                   "pop rax;"
                   "mov %0, rax;"
                   : "=r"(p)
                   :
                   : "rax");
  return p;
}

class Beanstalk {
private:
  uint16_t _w[4];
  uint8_t **_table;
  uint8_t _hash[10];

  void g(int a, int i, int j, int k, int l) {
    _w[a] ^= (uint16_t)_table[l][_w[a] & 0xff] << 8;
    _w[a] ^= (uint16_t)_table[k][_w[a] >> 8];
    _w[a] ^= (uint16_t)_table[j][_w[a] & 0xff] << 8;
    _w[a] ^= (uint16_t)_table[i][_w[a] >> 8];
  }
  void g0(int a) { g(a, 0, 1, 2, 3); }
  void g1(int a) { g(a, 4, 5, 6, 7); }
  void g2(int a) { g(a, 8, 9, 0, 1); }
  void g3(int a) { g(a, 2, 3, 4, 5); }
  void g4(int a) { g(a, 6, 7, 8, 9); }
  void z(int a, int b) { _w[a] ^= _w[b] ^ (rand() & 0xffff); }

public:
  Beanstalk(const uint8_t *key) {
    _table = new uint8_t*[10];
    for (int i = 0; i < 10; i++) {
      _table[i] = new uint8_t[256];
      for (int c = 0; c < 256; c++) {
        _table[i][c] = t()[c ^ key[i]];
      }
      _hash[i] = _table[i][0x77];
    }
  }

  const uint8_t* hash() {
    return _hash;
  }

  void encrypt(const uint8_t *in, uint8_t *out) {
    for (int i = 0; i < 4; i++) {
      _w[i] = (in[i*2] << 8) | in[i*2+1];
    }

    srand(_table[1][14] |
          (_table[5][14] << 8) |
          (_table[8][10] << 16) |
          (_table[9][31] << 24));

    /* Rule A */
    g0(3); z(2, 3);
    g1(2); z(1, 2);
    g2(1); z(0, 1);
    g3(0); z(3, 0);
    g4(3); z(2, 3);
    g0(2); z(1, 2);
    g1(1); z(0, 1);
    g2(0); z(3, 0);

    /* Rule B */
    z(0, 3); g3(3);
    z(3, 2); g4(2);
    z(2, 1); g0(1);
    z(1, 0); g1(0);
    z(0, 3); g2(3);
    z(3, 2); g3(2);
    z(2, 1); g4(1);
    z(1, 0); g0(0);

    /* Rule A */
    g1(3); z(2, 3);
    g2(2); z(1, 2);
    g3(1); z(0, 1);
    g4(0); z(3, 0);
    g0(3); z(2, 3);
    g1(2); z(1, 2);
    g2(1); z(0, 1);
    g3(0); z(3, 0);

    /* Rule B */
    z(0, 3); g4(3);
    z(3, 2); g0(2);
    z(2, 1); g1(1);
    z(1, 0); g2(0);
    z(0, 3); g3(3);
    z(3, 2); g4(2);
    z(2, 1); g0(1);
    z(1, 0); g1(0);

    for (int i = 0; i < 4; i++) {
      out[i*2] = _w[i] >> 8;
      out[i*2+1] = _w[i] & 0xff;
    }
  }

  ~Beanstalk() {
    for (int i = 0; i < 10; i++) {
      delete _table[i];
    }
    delete _table;
  }
};

bool license_to_key(string license, uint8_t *key) {
  if (license.size() != 27 || license.find("BEAN-") != 0)
    return false;
  if (!(license[9] == '-' && license[20] == '-'))
    return false;

  string t1 = license.substr(5,4);
  string t2 = license.substr(10,10);
  string t3 = license.substr(21,6);
  if ((t1.find_first_not_of("0123456789abcdefABCDEF") != string::npos)
      || (t2.find_first_not_of("0123456789abcdefABCDEF") != string::npos)
      || (t3.find_first_not_of("0123456789abcdefABCDEF") != string::npos))
    return false;

  uint64_t t;
  stringstream ss;
  ss << hex << t1; ss >> t; ss.clear();
  for (int i = 0; i < 2; i++) {
    key[i] = static_cast<uint8_t>((t >> (i*8)) & 0xff);
  }  
  ss << hex << t2; ss >> t; ss.clear();
  for (int i = 0; i < 5; i++) {
    key[2+i] = static_cast<uint8_t>((t >> (i*8)) & 0xff);
  }
  ss << hex << t3; ss >> t; ss.clear();
  for (int i = 0; i < 3; i++) {
    key[7+i] = static_cast<uint8_t>((t >> (i*8)) & 0xff);
  }

  return true;
}

int main() {
  /* 2021年9月10日8時48分34秒気温24度6分湿度76%
     BEAN-2A21-B91DC84834-24E676 */
  //uint8_t key[] = {0x2A,0x21,0xB9,0x1D,0xC8,0x48,0x34,0x24,0xE6,0x76};
  uint8_t key[10] = { 0 };

  /* Enter license key */
  string license;
  cout << "License Key: ";
  cin >> license;
  if (!license_to_key(license, key)) {
    cout << "[-] Invalid license format" << endl;
    return 1;
  }

  /* Check hash */
  Beanstalk cipher(key);
  if (!equal(k(0), k(1), cipher.hash())) {
    cout << "[-] Invalid license key" << endl;
    return 1;
  }

  /* Check flag */
  string flag;
  cout << "Flag: ";
  cin >> flag;

  if (flag.size() >= 6*8) {
    cout << "[-] Invalid flag" << endl;
    return 1;
  }

  uint8_t plain_block[8];
  uint8_t cipher_block[8];
  uint8_t padding = 8 - (flag.size() % 8);
  for (int i = 0; i < flag.size(); i += 8) {
    for (int j = 0; j < 8; j++) {
      try {
        plain_block[j] = flag.at(i+j);
      } catch(out_of_range& oor) {
        plain_block[j] = padding;
      }
    }
    cipher.encrypt(plain_block, cipher_block);
    for (int j = 0; j < 8; j++) {
      if (cipher_block[j] != f()[i+j]) {
        cout << "[-] Invalid flag" << endl;
        return 1;
      }
    }
  }

  cout << "[+] Correct!" << endl;
  return 0;
}
