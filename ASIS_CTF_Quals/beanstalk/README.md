# beanstalk - ASIS CTF 2021 Quals

## Challenge
Description: `<p>You need to enter the license key before validating the flag.</p>`
Difficulty: hard
Tag: `reversing`
Author: `ptr-yudai`
Flag: `ASIS{DATA_1n_TEXT_1s_4n_34sy_0bfusc4t10n}`

## Keywords
- Obfuscation
- Block cipher (ECB mode)
- Skipjack
