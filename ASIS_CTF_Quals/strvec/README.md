# strvec - ASIS CTF 2021 Quals

## Challenge
Description: `<p>How to implement secure array in C?</p><code>nc {host} 9003</code>`
Difficulty: medium
Tag: `pwn`
Author: `ptr-yudai`
Flag: `ASIS{n0_1d34_4_g00d_fl4g_t3xt_59723644e687a5c5e2fe80eae0b4f4b8}`

## Config
You can change the port number by modifying `docker-compose.yml`.

## Keywords
- Integer Overflow
- Heap Exploitation
- ROP
