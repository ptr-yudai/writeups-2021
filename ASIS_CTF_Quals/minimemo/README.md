# minimemo - ASIS CTF 2021 Quals

## Challenge
Description: `<p>Little notes for some little ideas.</p><code>nc {host} 9011</code>`
Difficulty: hard
Tag: `pwn`
Author: `ptr-yudai`
Flag: `ASIS{unl1nk_4tt4ck_1n_k3rn3l-l4nd_1s_5tr0ng!}`

## Config
You can change the port number by modifying `docker-compose.yml`.

## Keywords
- Kernel Exploit
- Unlink Attack
- ret2usr
