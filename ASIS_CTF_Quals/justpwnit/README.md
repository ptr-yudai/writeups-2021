# JustPwnIt - ASIS CTF 2021 Quals

## Challenge
Description: `<p>Just Pwn It!</p><code>nc {host} 9001</code>`
Difficulty: warmup
Tag: `pwn`, `warmup`
Author: `ptr-yudai`

## Config
You can change the port number by modifying `docker-compose.yml`.

## Keywords
- Out-of-bound write
- Stack pivot
- ROP
