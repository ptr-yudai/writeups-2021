# abbr - ASIS CTF 2021 Quals

## Challenge
Description: `<p>Abbreviations in English are complicated...</p><code>nc {host} 9002</code>`
Difficulty: easy
Tag: `pwn`
Author: `ptr-yudai`
Flag: `ASIS{d1d_u_kn0w_ASIS_1s_n0t_4n_4bbr3v14t10n}`

## Config
You can change the port number by modifying `docker-compose.yml`.

## Keywords
- Heap Overflow
- Stack Pivot
- ROP
