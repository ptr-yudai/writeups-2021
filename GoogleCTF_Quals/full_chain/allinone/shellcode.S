_start:
  xor edx, edx
  push rdx
  lea rdi, [rel s_argv2]
  push rdi
  lea rdi, [rel s_argv1]
  push rdi
  lea rdi, [rel s_argv0]
  push rdi
  mov rsi, rsp
  mov eax, 59
  syscall
  xor edi, edi
  mov eax, 60
  syscall

s_argv0:
  db "/bin/sh", 0
s_argv1:
  db "-c", 0
s_argv2:
  db "/bin/bash", 0
