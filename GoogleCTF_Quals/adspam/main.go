package main

import (
	"bufio"
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rsa"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"math/big"
	"net"
	"regexp"
	"strings"
)

func GetPublicKey() *rsa.PublicKey {
	num := "123596194752125979169492805898717739603063620825567453811699517077883880522042319510992511000584675998759365763564255380979966851592871801417286328813040909357944964680643572428136557634939634304256208583286122710159831699682677545905872084440840115646364525758632063374092374969271133111500901869640594407799"
	n := new(big.Int)
	n.SetString(num, 10)
	return &rsa.PublicKey{
		N: n,
		E: 0x10001,
	}
}

func RsaDecrypt(s string) []byte {
	buf, _ := base64.StdEncoding.DecodeString(s)
	pk := GetPublicKey()
	cip := new(big.Int)
	cip.SetBytes(buf)
	r := cip.Exp(cip, big.NewInt(int64(pk.E)), pk.N)
	return r.Bytes()
}

func GetLicenses() []string {
	lic := []string{
		"QIknTsIjeUEF9yJjeZ/kPPfTlSm8vzMU4LWjzfSXvN+OSqBu3iNgZJgeW7fc8oltH9MprO9nI8vxgsjO/VA4t7YuNm16a7elPVAHqD4dXtzngnZPpsbek3Rc/We/WQ5YxXHgUt7YJ6tcd4wH3fhduC9tl/E5elwJL/YAcbD4mT8=",
		"o9kjqYWCBKMgodl1JvDiscUeRjh9Ip9HcC7tHskoYqNQfAPE0XvSAKBSOFgleNHzVY9BVkfxmutgn/kVXUs3yl/qAurc4jokg0eA/v3flnnkWxqTOh4vv0yfr7PGXqwHk4qUFK1SldZ4VsLhd8PAb0aHj22E5b4U5jeJ16z187E=",
		"gpDbCb0BmUZfdKVIZgF08lQ80K9SeUsRadZG+UUjE7wI1NRZ1evLk2GQ3sqskGHFKlPg8cTR2Xy69WedNu4QLboOWm/w13ocOvHwCoiQ1ZdmibgnhMQBznqpjpBnL083YMRYskcUX68R2PFaXY3taV7MoG1DyQWFRfdr/CnLyS8=",
		"ZBLhwMu0DbgpUANm2ukYldrppJERiH1Tgp02CRB5I4dDP8n4+ZCv33ScspELtgAKHhiwIVksQVsnwDLsQRi6nqq9nrIwqSHMR0TwOe6UKTpAegbH53FXtriopPHfLuI2M45SzJ88GFjXy7wfOOjwDYe4KKO9KU8+LGD15Au73EM=",
		"Hygv+bTtsnI9IBf44GkvoF38r3g5zBB7uyYT7PTlbjhCdgYRwRayutI3vY+n66xM7GOFgUFVIBI5+OBDnvazLNttjGomPED/OXlImndWvrZxYcaKaE3vYGPezorV0xwPahGGq/DWafPKdYxLxwICq1GXKYNAckCZIqfpGbJRRwg=",
		"GARMZAX7fQN7i7Wnp4J6HxMTLe9+VM/wGJs+zN6b9IOmynh2gIkGjmssfOA9KdYydqBLEOJymayH8HeyrtInhhQNR3el8A5n8GMEMkyF1gUFAiSEPyhNeWWOj2IAHGNNwccmF7QywdfOUGjsTNFbrW6Yl5QLLAmMbA95qF0IERk=",
		"YWlx8Cok1x/3ZsW9JKIsKj9UpBaCNkXSPiVXUrNX1IDZE0B8iNr3iliOr90TW0BvsIaFEwvDTlcESXJ8kLc3iZq0fm1lgujfM7Z156VdxEPjr9LplcEZ9ZVhYGNtVyGIRcouUDJHu3FVfXQ1XesaNlNHOb50hADprsw3RnTAGbU=",
		"I3dsx2vSfXxZ1/QlMbwYPRFEZBtOuB8qLEY8cqFVtYjMluNWSkbHAYB+kwCBEv3yuoOjkdQEfqq4pS+K0ka1+pFDyss8sSbV3OiZdpRf40SS/pZxw2duJr9uDd1DdX8mST7fdjqj0V1a2ZBMpqaEI2gFlCwzXlfZBC47LKNiM+8=",
		"ow7r5VJMGfSf0odNKxzBpUtSJdj8gHdt+Z7Xu54MAdsnUParSjrtRI4yJYzcW4toOFmDdSs5SERR289yohYI5hHSWLElv/44O+g4M08F5qpwCmOp5otW32qRG1RnhqR95evH44nOyK24UnpvWlebNwVhniSu4A7znjluGRrao/U=",
		"TeGqGWv8ZmsY/rFq1puW9N+01TWTKJm8qzUuY/7JUCPDJ1AR6Y3XsPb73FuSVHPL63sjiuCTiKTRSUDzBE0VBfo59rtOKI05k64Jrz88nODD7BiK7ssacsOr2dAFGQKgBaWV2jitSAdxtCmh9sDpYsfs0/vXBBfVLqfVZDfAVGQ=",
		"Al3QWY+nNFoLezt+rSdbWmqp7iZ+rR9pnM35IJNZ63bLQeM3CUvULVczhrM3toXNLCY7xmAT4jg+u0uDAjanaKMB+T1Tmym7aaCqwCfHYVFn5nw+tw54e13CLxj7OO+e847+XH8DtK/BiA+n03vPnt/cEDPvIM59sPsjHThJvpk=",
		"VOGr60qxiO1r0YlKnrIWbQu7UhBmtBeNw2NDQnoNU3H1mjVEs/ji3AYuEGc2HGKINByq7Mpb4mWKD2oH5ii/UZDpxbzCFlJrjvjEG25c9Hhf2fiQHvRXmJd8iA8YdffBii3csCjaydLFSX6Vn7XPg+/PF/TdM1zUiLTJZX4LXRw=",
		"ELL9maLDpdmmEgaT76qtw9IugtaQX2r7V7QVqMKXQcbwq7o0dvaO3+yMt6m5K5Milm4JSNwX/810YUaoAsHNuaIavuLRsxbP3b6KnKxaKz3EDgyhye2en3U1EZouiLljBB0bKz8rAtyGdolWDdNoKjvLhv7x2edc05HQZOt3aiA=",
	}
	return lic
}

func JavaEncrypt(buf []byte) []byte {
	key := []byte("eaW~IFhnvlIoneLl")
	ans, _ := AES_Encrypt(buf, key)
	return []byte(base64.StdEncoding.EncodeToString(ans))
}

func JavaDecrypt(buf []byte) []byte {
	buf, _ = base64.StdEncoding.DecodeString(string(buf))
	key := []byte("eaW~IFhnvlIoneLl")
	ans, _ := AES_Decrypt(buf, key)
	return ans
}

func DecryptLicenses() []byte {
	var ans []byte
	for _, x := range GetLicenses() {
		tt := RsaDecrypt(x)
		ans = append(ans, tt...)
	}
	return ans
}

type JoinedStrings struct {
	buf []byte
}

func (self *JoinedStrings) GetString(pos int) string {
	l := int(self.buf[pos])
	return string(self.buf[pos+1 : pos+1+l])
}

type Info struct {
	OsVersion string `json:"os_version"`
	ApiLevel  int    `json:"api_level"`
	Device    string `json:"device"`
}

type Payload struct {
	Name       string `json:"name"`
	IsAdmin    int    `json:"is_admin"`
	DeviceInfo Info   `json:"device_info"`
	License   string   `json:"license"`
}

func ReadUntil(rdr *bufio.Reader, text string) (string, bool) {
	var buf []byte
	tmp := make([]byte, 1024)
	f := true
	for {
		n, err := rdr.Read(tmp)
		if err != nil {
			break
		}
		buf = append(buf, tmp[:n]...)
		if strings.Contains(string(tmp[:n]), text) {
			break
		}
		if strings.Contains(string(tmp[:n]), "disabled") {
			f = false
			break
		}
	}
	return string(buf), f
}

func main() {
	buf := DecryptLicenses()
	fmt.Println(hex.Dump(buf))

	js := &JoinedStrings{buf: buf}
	name := js.GetString(0)
	fmt.Printf("name: %s\n", name)
	l := len(name) + 1
	tmp := js.GetString(l)
	l += len(tmp) + 1
	tmp2 := js.GetString(l)
	fmt.Printf("tmp: %s\n", tmp)
	fmt.Printf("is_admin: %s\n", tmp2)

	pp := &Payload{
		Name:    "1337_admin",
		IsAdmin: 1,
		DeviceInfo: Info{
			OsVersion: "8.0",
			ApiLevel:  28,
			Device:    "Pixel",
		},
		License:   strings.Join(GetLicenses(), "::") + "::",
	}

	mbuf, err := json.Marshal(pp)
	if err != nil {
		fmt.Println(err)
		return
	}
	//space := make([]byte, 1024)
	conn, err := net.Dial("tcp4", "adspam.2021.ctfcompetition.com:1337")
	if err != nil {
		fmt.Println(err)
		return
	}

	writer := bufio.NewWriter(conn)
	reader := bufio.NewReader(conn)

	text, pow := ReadUntil(reader, "Solution? ")
	fmt.Printf("[+] Got: %s", text)

	if pow {
		reg := regexp.MustCompile("solve (s\\..+)$")
		quest := reg.FindStringSubmatch(text)[0]
		fmt.Printf("[+] Need to solve %q\n", quest)
	}

	fmt.Printf("[+] Payload:%s\n", string(mbuf))
	rrr := JavaEncrypt(mbuf)
	mbuf[0] += 0
	//fmt.Println(string(rrr))
	_, err = writer.Write(rrr)
	writer.WriteRune('\n')
	writer.Flush()

	line, _ := reader.ReadString('\n')

	fmt.Println("[+] Got:")
	fmt.Println(string(JavaDecrypt([]byte(line))))
}


func AES_Encrypt(content, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	ecb := NewECBEncrypter(block)
	content = pKCS5Padding(content, block.BlockSize())
	crypted := make([]byte, len(content))
	ecb.CryptBlocks(crypted, content)
	return crypted, nil
}

// 3DES解密
func AES_Decrypt(crypted, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	blockMode := NewECBDecrypter(block)
	origData := make([]byte, len([]byte(crypted)))
	blockMode.CryptBlocks(origData, []byte(crypted))
	origData = pKCS5UnPadding(origData)
	return origData, nil
}

func pKCS5Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func pKCS5UnPadding(origData []byte) []byte {
	length := len(origData)
	// 去掉最后一个字节 unpadding 次
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

type ecb struct {
	b         cipher.Block
	blockSize int
}

func newECB(b cipher.Block) *ecb {
	return &ecb{
		b:         b,
		blockSize: b.BlockSize(),
	}
}

// 引入golang官方 https://codereview.appspot.com/7860047/
type ecbEncrypter ecb

// NewECBEncrypter returns a BlockMode which encrypts in electronic code book
// mode, using the given Block.
func NewECBEncrypter(b cipher.Block) cipher.BlockMode {
	return (*ecbEncrypter)(newECB(b))
}
func (x *ecbEncrypter) BlockSize() int { return x.blockSize }
func (x *ecbEncrypter) CryptBlocks(dst, src []byte) {
	if len(src)%x.blockSize != 0 {
		panic("crypto/cipher: input not full blocks")
	}
	if len(dst) < len(src) {
		panic("crypto/cipher: output smaller than input")
	}
	for len(src) > 0 {
		x.b.Encrypt(dst, src[:x.blockSize])
		src = src[x.blockSize:]
		dst = dst[x.blockSize:]
	}
}

type ecbDecrypter ecb

// NewECBDecrypter returns a BlockMode which decrypts in electronic code book
// mode, using the given Block.
func NewECBDecrypter(b cipher.Block) cipher.BlockMode {
	return (*ecbDecrypter)(newECB(b))
}
func (x *ecbDecrypter) BlockSize() int { return x.blockSize }
func (x *ecbDecrypter) CryptBlocks(dst, src []byte) {
	if len(src)%x.blockSize != 0 {
		panic("crypto/cipher: input not full blocks")
	}
	if len(dst) < len(src) {
		panic("crypto/cipher: output smaller than input")
	}
	for len(src) > 0 {
		x.b.Decrypt(dst, src[:x.blockSize])
		src = src[x.blockSize:]
		dst = dst[x.blockSize:]
	}
}
