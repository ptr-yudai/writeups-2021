from ptrlib import *
import csv

with open("./ask-over-the-air.csv") as f:
    reader = csv.reader(f)
    next(reader)
    l = [row for row in reader]

bits = ''
for (t, iv, qv) in l:
    a = (float(iv)**2 + float(qv)**2)**0.5 * 120
    bits += '0' if a < 0.5 else '1'

v = 0
for block in chunks(bits, 16):
    if block.count("1") > 8:
        v = (v << 1) | 1
    else:
        v <<= 1

print(int.to_bytes(v, 1024, 'big'))
