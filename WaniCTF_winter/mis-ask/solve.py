import csv

with open("./digital_ask.csv") as f:
    reader = csv.reader(f)
    next(reader)
    l = [row for row in reader]

output = b''
i, c = 0, 0
for (t, b) in l:
    c = (c << 1) | int(b)
    i += 1
    if i == 8:
        output += bytes([c])
        i, c = 0, 0

output = output.replace(b'\x00\x00', b'0').replace(b'\xff\xff', b'1')
print(hex(int(output, 2)))
print(int.to_bytes(int(output, 2), 1024, 'big'))
