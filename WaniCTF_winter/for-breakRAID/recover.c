#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

int main() {
  int disk1 = open("./disk01", O_RDONLY);
  int disk2 = open("./disk02", O_RDONLY);
  int recv = open("./output", O_RDWR | O_CREAT | O_TRUNC, S_IREAD | S_IWRITE);
  if (disk1 == -1 || disk2 == -1 || recv == -1) {
    perror("open");
    return 1;
  }
  ftruncate(recv, 102400000);
  unsigned long *buf1 = mmap(NULL, 102400000, PROT_READ, MAP_SHARED, disk1, 0);
  unsigned long *buf2 = mmap(NULL, 102400000, PROT_READ, MAP_SHARED, disk2, 0);
  unsigned long *buf3 = mmap(NULL, 102400000, PROT_WRITE, MAP_SHARED, recv, 0);
  if (buf1 == MAP_FAILED || buf2 == MAP_FAILED || buf3 == MAP_FAILED) {
    perror("mmap");
    return 1;
  }

  for (unsigned long i = 0; i < 102400000 / 8; i++) {
    if (i % 0x100000 == 0) {
      printf("[DEBUG] 0x%lx\n", i);
    }
    buf3[i] = buf1[i] ^ buf2[i];
  }

  close(disk1);
  close(disk2);
  close(recv);
  munmap(buf1, 102400000);
  munmap(buf2, 102400000);
  munmap(buf3, 102400000);
}
