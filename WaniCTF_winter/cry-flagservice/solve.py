from ptrlib import xor
import requests
import base64

#URL = "http://127.0.0.1:5000"
URL = "https://service.crypto.wanictf.org"

r = requests.post(f"{URL}/login",
                  data={"username": "taro"},
                  allow_redirects=False)
c = base64.b64decode(r.cookies["token"])
iv, ciphertext = c[:16], c[16:]
iv = iv[:10] + xor(iv[10:], xor(b"false,", "true, "))

c = iv + ciphertext
r = requests.get(f"{URL}/",
                 cookies = {"token": base64.b64encode(c).decode()},
                 allow_redirects=False)
print(r.text)
