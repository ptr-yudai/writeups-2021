import requests
with open("wani.png", "rb") as f:
    buf = f.read()

URL = "https://post.web.wanictf.org"

# FLAG{y0u_ar3_http_p0st_m@ster!}

r = requests.post(f"{URL}/chal/4", data='{"hoge":1, "fuga":null}',
                  headers={"Content-Type": "application/json"})
print(r.text)

#r = requests.post(f"{URL}/chal/5", files={"data": ("data", buf)})

#r = requests.post(f"{URL}/chal/3", data={"data[hoge]":"fuga"},
#                  headers={"user-agent": "Mozilla/5.0"})

#r = requests.post(f"{URL}/chal/2", data={"data":"hoge"},
#                  headers={"user-agent": "Mozilla/5.0"})

#r = requests.post(f"{URL}/chal/1", data={"data":"hoge"})
