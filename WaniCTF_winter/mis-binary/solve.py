import csv

with open("./binary.csv") as f:
    reader = csv.reader(f)
    next(reader)
    l = [row for row in reader]

output = b''
i, c = 0, 0
for (t, b) in l:
    c = (c << 1) | int(b)
    i += 1
    if i == 8:
        output += bytes([c])
        i, c = 0, 0

print(output)
