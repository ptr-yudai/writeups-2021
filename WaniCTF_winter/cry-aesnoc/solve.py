from ptrlib import *

def get_aesed(block):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", block.hex())
    c = bytes.fromhex(sock.recvregex(" = '([0-9a-f]+)'")[0].decode())
    return xor(c, iv)[:16]

#sock = Process(["python", "chall.py"])
sock = Socket("nc aesnoc.crypto.wanictf.org 50000")

iv = bytes.fromhex(sock.recvregex(" = '([0-9a-f]+)'")[0].decode())
logger.info("iv = " + iv.hex())

sock.sendlineafter("> ", "1")
encflag = bytes.fromhex(sock.recvregex(" = '([0-9a-f]+)'")[0].decode())
logger.info("encflag = " + encflag.hex())
C = [
    encflag[i:i+16]
    for i in range(0, len(encflag), 16)
]

known = b'}' + bytes([15] * 15)
flag = known
for i in range(3):
    aesed = get_aesed(known)
    known = xor(aesed, C[3-i])
    flag = known + flag
    print(flag)

sock.interactive()
