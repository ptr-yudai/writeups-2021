from ptrlib import chunks, u64
from z3 import *

mapping = {0: 23, 1: 16, 2: 19, 3: 12, 4: 31, 5: 24, 6: 17, 7: 22, 8: 13, 9: 18, 10: 25, 11: 30, 12: 9, 13: 2, 14: 11, 15: 4, 16: 33, 17: 26, 18: 3, 19: 8, 20: 5, 21: 10, 22: 27, 23: 32, 24: 21, 25: 14, 26: 35, 27: 28, 28: 7, 29: 0, 30: 15, 31: 20, 32: 29, 33: 34, 34: 1, 35: 6}

def get_models(solver, num_models):
    n = 0
    while n < num_models and solver.check() == sat:
        try:
            model = solver.model()
            n += 1
            yield model
            block = []
            for declaration in model:
                c = declaration()
                block.append(c != model[declaration])
            solver.add(Or(block))
            solver.push() # save some work, dont redo the work done so far
        except KeyboardInterrupt: # got bored waiting?
            print("interrupted")
            break

def F(v):
    return (v << 1) ^ (27 * LShR(v, 7))

def part_unscramble(data, v):
    solver = Solver()
    s = [BitVec(f'input_{i}', 8) for i in range(0x24)]
    solver.add(s[v[0]] ^ F(s[v[0]]) ^ s[v[2]] ^ F(s[v[2]]) ^ F(s[v[4]]) == data[v[0]])
    solver.add(s[v[1]] ^ F(s[v[1]]) ^ s[v[3]] ^ F(s[v[3]]) ^ F(s[v[5]]) == data[v[1]])
    solver.add(s[v[4]] ^ F(s[v[0]]) == data[v[2]])
    solver.add(s[v[5]] ^ F(s[v[1]]) == data[v[3]])
    solver.add(s[v[0]] ^ F(s[v[0]]) ^ F(s[v[2]]) == data[v[4]])
    solver.add(s[v[1]] ^ F(s[v[1]]) ^ F(s[v[3]]) == data[v[5]])
    for i in range(0x24):
        if i not in v:
            solver.add(s[i] == data[i])
    if solver.check() == sat:
        m = solver.model()
        output = b''
        for c in s:
            output += bytes([m[c].as_long()])
    else:
        print("[-] Not found :(")
        exit(1)
    return output

def flag_unscramble(data):
    shuffler = [
        [0x00, 0x01, 0x02, 0x06, 0x0C, 0x12],
        [0x03, 0x04, 0x05, 0x0B, 0x11, 0x17],
        [0x07, 0x08, 0x09, 0x0D, 0x0E, 0x0F],
        [0x0A, 0x10, 0x16, 0x1C, 0x1D, 0x23],
        [0x13, 0x14, 0x18, 0x19, 0x1A, 0x1E],
        [0x15, 0x1B, 0x1F, 0x20, 0x21, 0x22]
    ][::-1]
    for i in range(6):
        data = part_unscramble(data, shuffler[i])
        print(list(map(lambda x: hex(u64(x)), chunks(data, 8))))
    return data

def inverse_map(mapping, data):
    output = [None for c in data]
    for i in range(len(data)):
        output[mapping[i]] = bytes([data[i]])
    return b''.join(output)

with open("flag_checker", "rb") as f:
    f.seek(0x3080)
    flag = f.read(0x24)

for i in range(0x10):
    flag = inverse_map(mapping, flag)
    print(flag)
    flag = flag_unscramble(flag)
    print(flag)
