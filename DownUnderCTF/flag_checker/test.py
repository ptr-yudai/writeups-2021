seed = 0x1337
def lcg_next():
    global seed
    seed = (seed * 0x9157131 + 0x44799011) & 0xffffffff
    return seed
def set_seed(v):
    global seed
    seed = v

# cpLsT{]wajx>[3d5([y,D.i'^8XAndie9B=|
def gen_matrix(flag):
    set_seed(0x1337)
    array = [None for i in range(0x500)]
    p = [0, 0]
    array[0] = p
    cnt = 1
    for i in range(0x100):
        n = lcg_next() % 5
        if n:
            nazo = [None for i in range(n)]
            for j in range(n):
                v12 = [0, 0]
                nazo[j] = v12
                array[cnt] = v12
                cnt += 1
            array[i][1] = nazo
            array[i] = None

    for i in range(cnt):
        if array[i] is not None:
            array[i][0] = flag[lcg_next() % 0x24]

    return p

p = gen_matrix(b'A'*0x24)
print(p[1][1])
