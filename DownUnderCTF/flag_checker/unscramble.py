from z3 import *

def get_models(solver, num_models):
    n = 0
    while n < num_models and solver.check() == sat:
        try:
            model = solver.model()
            n += 1
            yield model
            block = []
            for declaration in model:
                c = declaration()
                block.append(c != model[declaration])
            solver.add(Or(block))
            solver.push() # save some work, dont redo the work done so far
        except KeyboardInterrupt: # got bored waiting?
            print("interrupted")
            break

def F(v):
    return ((v<<1) ^ (27*(v>>7))) & 0xff

def part_unscramble(data, v):
    solver = Solver()
    s = [BitVec(f'input_{i}', 8) for i in range(0x24)]
    solver.add(s[v[0]] ^ F(s[v[0]]) ^ s[v[2]] ^ F(s[v[2]]) ^ F(s[v[4]]) == data[v[0]])
    solver.add(s[v[1]] ^ F(s[v[1]]) ^ s[v[3]] ^ F(s[v[3]]) ^ F(s[v[5]]) == data[v[1]])
    solver.add(s[v[4]] ^ F(s[v[0]]) == data[v[2]])
    solver.add(s[v[5]] ^ F(s[v[1]]) == data[v[3]])
    solver.add(s[v[0]] ^ F(s[v[0]]) ^ F(s[v[2]]) == data[v[4]])
    solver.add(s[v[1]] ^ F(s[v[1]]) ^ F(s[v[3]]) == data[v[5]])
    for i in range(0x24):
        if i not in v:
            solver.add(s[i] == data[i])
    if solver.check() == sat:
        m = solver.model()
        output = b''
        for c in s:
            output += bytes([m[c].as_long()])
    else:
        print("[-] Not found :(")
        exit(1)
    return output

def flag_unscramble(data):
    shuffler = [
        [0x00, 0x01, 0x02, 0x06, 0x0C, 0x12],
        [0x03, 0x04, 0x05, 0x0B, 0x11, 0x17],
        [0x07, 0x08, 0x09, 0x0D, 0x0E, 0x0F],
        [0x0A, 0x10, 0x16, 0x1C, 0x1D, 0x23],
        [0x13, 0x14, 0x18, 0x19, 0x1A, 0x1E],
        [0x15, 0x1B, 0x1F, 0x20, 0x21, 0x22]
    ][::-1]
    for i in range(6):
        data = part_unscramble(data, shuffler[i])
    return data

# flag{AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA}
scrambled = bytes([
    0x4d, 0x6a, 0x20, 0xa5, 0x0b, 0x84, 0xb5, 0xbf,
    0x5e, 0x55, 0x5c, 0xbc, 0xe9, 0x01, 0x27, 0xbe,
    0xd8, 0x42, 0xd5, 0x27, 0xe3, 0x71, 0xc5, 0xa9,
    0x36, 0x97, 0xb6, 0xa1, 0xa5, 0xc8, 0x3e, 0xf1,
    0x9c, 0x46, 0x90, 0x7f
])
print(flag_unscramble(scrambled))
