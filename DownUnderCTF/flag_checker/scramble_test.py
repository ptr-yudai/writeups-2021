from ptrlib import *

inp = flat([
    0x82998fcce88df58b,
    0x4141c368b782c382,
    0x0fc3418282362bfa,
    0x414141ff4141c3c3,
    0x41414141
],map=p64)

def F(v):
    return ((v<<1) ^ (27*(v>>7))) & 0xff

def part_scramble(s, v):
    x = s[v[0]] ^ F(s[v[0]]) ^ s[v[2]] ^ F(s[v[2]]) ^ F(s[v[4]])
    y = s[v[1]] ^ F(s[v[1]]) ^ s[v[3]] ^ F(s[v[3]]) ^ F(s[v[5]])
    z = s[v[4]] ^ F(s[v[0]])
    w = s[v[5]] ^ F(s[v[1]])
    p = s[v[0]] ^ F(s[v[0]]) ^ F(s[v[2]])
    q = s[v[1]] ^ F(s[v[1]]) ^ F(s[v[3]])
    output = list(s)
    output[v[0]] = x
    output[v[1]] = y
    output[v[2]] = z
    output[v[3]] = w
    output[v[4]] = p
    output[v[5]] = q
    return bytes(output)

def flag_scramble(data):
    shuffler = [
        [0x00, 0x01, 0x02, 0x06, 0x0C, 0x12],
        [0x03, 0x04, 0x05, 0x0B, 0x11, 0x17],
        [0x07, 0x08, 0x09, 0x0D, 0x0E, 0x0F],
        [0x0A, 0x10, 0x16, 0x1C, 0x1D, 0x23],
        [0x13, 0x14, 0x18, 0x19, 0x1A, 0x1E],
        [0x15, 0x1B, 0x1F, 0x20, 0x21, 0x22]
    ]
    for i in range(6):
        data = part_scramble(data, shuffler[i])
        print(list(map(lambda x: hex(u64(x)), chunks(data, 8))))
    return data

mapping = {0: 23, 1: 16, 2: 19, 3: 12, 4: 31, 5: 24, 6: 17, 7: 22, 8: 13, 9: 18, 10: 25, 11: 30, 12: 9, 13: 2, 14: 11, 15: 4, 16: 33, 17: 26, 18: 3, 19: 8, 20: 5, 21: 10, 22: 27, 23: 32, 24: 21, 25: 14, 26: 35, 27: 28, 28: 7, 29: 0, 30: 15, 31: 20, 32: 29, 33: 34, 34: 1, 35: 6}
def inverse_map(mapping, data):
    output = [None for c in data]
    for i in range(len(data)):
        for v in mapping:
            if mapping[v] == i:
                output[v] = bytes([data[i]])
    return b''.join(output)

inp = b'flag{AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA}'
flag = flag_scramble(inp)
flag = inverse_map(mapping, flag)
print(flag)
print("----")
flag = flag_scramble(flag)
flag = inverse_map(mapping, flag)
print(flag)
