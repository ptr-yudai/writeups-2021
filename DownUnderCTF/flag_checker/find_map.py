import gdb

gdb.execute("break *(0x555555554000 + 0x1C7E)")
gdb.execute("break *(0x555555554000 + 0x1C9A)")
gdb.execute("run < flag")

addr_flag = gdb.parse_and_eval('$rdi')

for rnd in range(0x10):
    for i in range(0x24):
        gdb.execute(f"set {{char}}{addr_flag + i} = {i}")

    gdb.execute("conti")

    mapping = {}
    for i in range(0x24):
        c = gdb.parse_and_eval(f"{{char}}{addr_flag + i}")
        mapping[i] = int(c)

    print(mapping)

    gdb.execute("conti")

