char answer[] = {0x0f, 0x4f, ...}; // byte_4080

u8 potate(u8 v) { // sub_17C3
  return (v<<1) ^ (27*(v>>7));
}

void part_scramble(u8 *buf, u32 *v) { // sub_17F0
  u8 x, y, z, w, p, q;
  x = buf[v[0]] ^ potate(buf[v[0]]) ^             \
    buf[v[2]] ^ potate(buf[v[2]]) ^               \
    potate(buf[v[4]]);
  y = buf[v[1]] ^ potate(buf[v[1]]) ^         \
    buf[v[3]] ^ potate(buf[v[3]]) ^           \
    potate(buf[v[5]]);
  z = buf[v[4]] ^ potate(buf[v[0]]);
  w = buf[v[5]] ^ potate(buf[v[1]]);
  p = buf[v[0]] ^ potate(buf[v[0]]) ^ potate(buf[v[2]]);
  q = buf[v[1]] ^ potate(buf[v[1]]) ^ potate(buf[v[3]]);
  buf[0] = x;
  buf[1] = y;
  buf[2] = z;
  buf[3] = w;
  buf[4] = p;
  buf[5] = q;
}

void flag_scramble(u8 *flag) { // sub_19FA
  u32 arr[][] = {
                 {0x00, 0x01, 0x02, 0x06, 0x0C, 0x12},
                 {0x03, 0x04, 0x05, 0x0B, 0x11, 0x17},
                 {0x07, 0x08, 0x09, 0x0D, 0x0E, 0x0F},
                 {0x0A, 0x10, 0x16, 0x1C, 0x1D, 0x23},
                 {0x13, 0x14, 0x18, 0x19, 0x1A, 0x1E},
                 {0x15, 0x1B, 0x1F, 0x20, 0x21, 0x22}
  };
  part_scramble(flag, arr0);
  part_scramble(flag, arr1);
  part_scramble(flag, arr2);
  part_scramble(flag, arr3);
  part_scramble(flag, arr4);
  part_scramble(flag, arr5);
}

u32 seed;
void set_seed(u32 _seed) {
  seed = _seed;
}
u32 lcg_next() {
  return (seed = seed * 0x9157131 + 0x44799011);
}

void sub_1232(u8 *flag) {
  set_seed(0x1337);
  u64 *array = calloc(0x500, 8);
  u8 *p = malloc(0x10);
  array[0] = p;

  int v = 1;
  for (int i = 0; i < 0x100; i++) {
    array[i];
    int n = lcg_next() % 5;
    if (n) {
      u64 *nazo = calloc(n, 8);
      for (int j = 0; j < n; j++) {
        char *q = malloc(0x10);
        nazo[j] = q;
        array[v++] = q;
      }
      array[i][1] = nazo;
    }
  }
}

int main() {
  char flag[0x28];
  fgets(flag, 0x25, stdin);
  if (strlen(flag) != 0x24) wrong_flag();

  for (int i = 0; i < 0x10; i++) {
    flag_scramble(flag);
    sub_1413(flag, sub_1232(flag));
  }

  for (int j = 0; j < 0x24; j++) {
    if (flag[j] != answer[j]) wrong_flag();
  }

  puts("Corret! :)");
}
