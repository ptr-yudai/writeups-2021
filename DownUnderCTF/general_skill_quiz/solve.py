from ptrlib import *
import urllib.parse
import base64
import codecs

sock = Socket("pwn-2021.duc.tf", 31905)

sock.sendline("")
sock.sendlineafter("1+1=?", "2")
sock.sendline(str(int(sock.recvlineafter(": "), 16)))
sock.sendline(chr(int(sock.recvlineafter(": "), 16)))
sock.sendline(urllib.parse.unquote(sock.recvlineafter(": ").decode()))
sock.sendline(base64.b64decode(sock.recvlineafter(": ")))
sock.sendline(base64.b64encode(sock.recvlineafter(": ")))
sock.sendline(codecs.encode(sock.recvlineafter(": ").decode(), "rot13"))
sock.sendline(codecs.encode(sock.recvlineafter(": ").decode(), "rot13"))
sock.sendline(str(int(sock.recvlineafter(": "), 2)))
sock.sendline(bin(int(sock.recvlineafter(": "))))
#sock.sendlineafter("?", "zer0pts CTF")  # why doesn't this work? :thinking:
sock.sendlineafter("?", "DUCTF")

sock.interactive()
