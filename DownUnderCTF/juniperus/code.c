char answer[] = {0x21, 0xdf, ...};

bool auth() {
  my_printf("[I] New context...\n");
  EVP_MD_CTX *sha512 = EVP_MD_CTX_new();
  my_printf("[I] Init context...\n");
  EVP_DigestInit_ex(sha512, EVP_sha3_512());
  my_printf("[I] Prompting user for password...\n");

  char password[0x20];
  printf("Root password (32 chars. max): ");
  fgets(password, 0x20, stdin);
  password[strcspn(password, "\n")] = '\0';
  my_printf("[I] Password attempt: %s (%d chars long)\n", password, strlen(password));

  my_printf("[I] Updating context...\n");
  EVP_DigestUpdate(sha512, password, 0x20);
  my_printf("[I] Allocating digest...\n");
  char *hash = CRYPTO_malloc(EVP_MD_size(EVP_sha256()), "shell.c", 0x58);
  my_printf("[I] Finalising digest...\n");
  EVP_DigestFinal_ex(sha512, hash, &x);

  EVP_MD_CTX_free(sha512);
  my_printf("[I] Comparing hashes...\n");

  bool result = CRYPTO_memcmp(hash, answer, 0x40) == 0;
  CRYPTO_free(hash);
  return result;
}

int main() {
  devnull_fp = fopen("/dev/null", "a");

  /* read flag */
  FILE *fp = fopen("flag.txt", "r");
  char flag[0x20];
  for (char *p = flag; (*p = fgetc(fp)) != -1; p++);

  if (auth()) {
    printf("Authenticated. Here's your flag: %s\n", flag);
  } else {
    puts("Authentication failure.");
  }
  return 0;
}
