import sqlite3
from math import gcd
from Crypto.PublicKey import RSA
from Crypto.Util.number import inverse
from Crypto.Cipher import PKCS1_OAEP

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

con = sqlite3.connect("secuchat.db")
con.row_factory = dict_factory
cur = con.cursor()

# {"username": "key", ...} dictionary
users = {}
for user in cur.execute("SELECT * FROM User"):
    N = RSA.importKey(user['rsa_key']).n
    p, q = None, None
    for others in users:
        if gcd(users[others]['N'], N) != 1:
            common = gcd(users[others]['N'], N)
            p = common
            q = N // p
            users[others]['p'] = p
            users[others]['q'] = users[others]['N'] // p
    users[user['username']] = {'N': N, 'p': p, 'q': q}

# all conversation list
conversations = []
for conversation in cur.execute("SELECT * FROM Conversation"):
    conversations.append(conversation)

for conversation in conversations:
    # Initial key
    key_id = conversation['initial_parameters']
    src = conversation['initiator']
    dst = conversation['peer']

    # Try only when we know the private key
    if users[src]['p'] is None and users[dst]['p'] is None:
        continue

    # Get all messages in this conversation
    query = "SELECT * FROM Message WHERE conversation=? ORDER BY timestamp"
    for message in cur.execute(query, (conversation["id"],)):
        query = "SELECT * FROM Parameters WHERE id=?"
        cur.execute(query, (key_id,))
        enckey = cur.fetchone()

        C1 = enckey['encrypted_aes_key_for_initiator'] # C1 = K^e mod N1
        C1 = int.from_bytes(C1, 'big')
        C2 = enckey['encrypted_aes_key_for_peer']      # C2 = K^e mod N2
        C2 = int.from_bytes(C2, 'big')

        if users[src]['p'] is not None:
            d = inverse(0x10001, (users[src]['p']-1)*(users[src]['q']-1))
            key = pow(C1, d, users[src]['N'])
            print("A", hex(key))
        if users[dst]['p'] is not None:
            d = inverse(0x10001, (users[dst]['p']-1)*(users[dst]['q']-1))
            key = pow(C2, d, users[dst]['N'])
            print("B", hex(key))
        exit()

        key_id = message['next_parameters']
