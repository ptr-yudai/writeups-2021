from scapy.all import *
import re

result = b''

cnt = 0
def analyse(pkt):
    global result, cnt
    if DNS not in pkt:
        return
    if pkt[DNS].opcode != 0 or pkt[DNS].qr != 0:
        return
    packet = bytes(pkt[DNS].qd.qname).split(b'.')
    for sd in packet:
        if sd == b'qawesrdtfgyhuj':
            break
        result += bytes.fromhex(sd.decode())

sniff(offline="./extracted.pcap", store=0, prn=analyse)

with open("output.bin", "wb") as f:
    f.write(result)
