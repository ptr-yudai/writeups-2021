from PIL import Image, ImageSequence

out = Image.new('RGB', (300*10, 300), (255, 255, 255))
img = Image.open("challenge.gif")
i = 0
for frame in ImageSequence.Iterator(img):
    x = (i % 10) * 300
    y = (i // 10) * 22
    out.paste(frame.copy(), (x, y))
    i += 1

out.save("output.png")
