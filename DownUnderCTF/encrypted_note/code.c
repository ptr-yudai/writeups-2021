long X, A, B; // read from /dev/random

long lcg_next() {
  return X = X * A + B;
}

void encrypt(char *data) {
  int n = strlen(data) / 8;
  if (n % 8) n++;
  if (n > 11) n = 11;

  for (int i = 0; i < n; i++) {
    (long*)&data[8*i] ^= lcg_next();
  }
}

void append_to_note(char *note) {
  if (note[0x57] > 0) // [!] vuln?
    return; // Note is full!

  buf = malloc(8);
  read(0, buf, 8);
  char *p = &note[strlen(note)];
  strncpy(p, buf, 8);
  p[7] = 0;
  encrypt(p);
}
