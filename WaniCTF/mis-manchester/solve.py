bits = 0
with open("manchester.csv", "r") as f:
    clk = 0
    w = 0
    for line in f:
        if w % 31 == 0:
            bits <<= 1
            bits |= int(line) ^ clk
            clk ^= 1
        w += 1

out = 0
while bits > 0:
    out <<= 1
    out |= bits & 1
    bits >>= 2
out = int(bin(out)[2:][::-1], 2)

print(int.to_bytes(out, 512, 'big'))
