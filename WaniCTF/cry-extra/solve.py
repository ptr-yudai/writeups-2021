from sage.all import *

with open("output.txt", "r") as f:
    exec(f.read())

p = (M + sqrt(M**2 - 8*N)) / 4
q = N / p
d = inverse_mod(e, (p-1)*(q-1))
m = power_mod(c, d, N)

print(int.to_bytes(int(m), 128, 'big'))
