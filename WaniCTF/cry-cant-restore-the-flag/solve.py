from ptrlib import *
from sage.all import *

#sock = Process(["python", "server.py"])
sock = Socket("nc crt.cry.wanictf.org 50000")

p = 1
rlist = []
mlist = []
while p < 300:
    sock.sendlineafter("> ", str(p))
    r = int(sock.recvline())
    rlist.append(r)
    mlist.append(p)
    p = next_prime(p)

print(rlist, mlist)
m = crt(rlist, mlist)
print(int.to_bytes(int(m), 64, 'big'))
