from ptrlib import *
import re

sock = Socket("auto-pwn.chal.csaw.io", 11002)

sock.sendlineafter("> ", "4a47f4618ce3e7b567cce92b48f41e61")
sock.recvuntil("this box: \n")
sock.recvline()
binary = b''
while True:
    l = sock.recvline()
    r = re.findall(b": ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4})", l)
    if r:
        for block in r[0]:
            binary += bytes([int(block[0:2], 16), int(block[2:4], 16)])
    else:
        l = l[l.index(b':'):]
        r = re.findall(b"[0-9a-f]{4}", l)
        for block in r:
            binary += bytes([int(block[0:2], 16), int(block[2:4], 16)])
        break

with open("02.bin", "wb") as f:
    f.write(binary)

sock.interactive()
