#include <string_view>

template<class T>
class BitStream {
    string_view     buf;
    size_t          index;
    void set_bit(size_t index, int b);
public:
    BitStream(string_view s) :
        buf(s), index(0) {
        // all done
    }
    int get_bit(int n_bits);
    void append_bit(int b);
    void append_bits(size_t count, T b);
    T get_bits<T>(int n_bits);
};

int BitStream::get_bit(int n)
{
    int t = n >= 0 ? n : n+7;
    return (buf[t >> 3] >> (n & 7)) & 1;
}

template <class T>
T BitStream<T>::get_bits(int n_bits)
{
    T val = 0;
    for (int i = 0; i < n_bits; ++i)
    {
        val = (val << 1) | get_bit(i+index);
    }
    index += n_bits;
    return val;
}

template <class T>
void BitStream<T>::append_bits(size_t count, T val)
{
    for (size_t i = 0; i < count; ++i)
    {
        append_bit(val >> i & 1);
    }
}

uint8_t rotate_left8(uint8_t val, int r)
{
    r %= 8;
    return (val << r | val >> 8-r) & 0xff;
}

void BitStream::set_bit(size_t index, int b)
{
    uint8_t v5 = buf[index>>3] & rotate_left8(0xFE, index);
    uint8_t v6 = buf[index>>3] | (1 << (index & 7));
    if ( !b )
        v6 = v5;
    buf[index>>3] = v6;
}

void BitStream::append_bit(int b)
{
    set_bit(index++, b);
}

void
decompress(BitStream src, BitStream dest)
{
    int64_t result; // rax
    uint8_t v3; // al
    int64_t v4; // r15
    int64_t v5; // rax
    int64_t v6; // rbx
    uint8_t v7; // al
    uint64_t v8; // rbx
    uint64_t v9; // rbx
    while ( 1 )
    {
        result = src.get_bits<char>();
        switch ( (char)result )
        {
          case 0:
            return result;
          case 1:
            v3 = src.get_bit();
            dest.append_bit(v3);
            break;
          case 2:
            v4 = src.get_bits<char>();
            dest.append_bits<char>(8, v4);
            break;
          case 3:
            v4 = src.get_bits<long>(0xALL);
            v5 = src.get_bits<long>(0xALL);
            if ( v5 )
            {
              v6 = v5;
              do
              {
                v7 = dest.get_bit(dest.size() - v4);
                dest.append_bit(v7);
                --v6;
              }
              while ( v6 );
            }
            break;
          case 4:
            src.index += src.get_bits<short>();
            break;
          default:
            printf("Invalid opcode: %#x\n", (__int8)result);
            abort();
        }
        v8 = dest.index;
        if ( v8 < 8 * dest.buf.size() )
        {
          v9 = src.index;
          if ( v9 < 8 * src.size() )
            continue;
        }
        puts("Went out of bounds");
        abort();
    }
}

int main()
{
    puts("Ready for compressed buffer:");
    char buf[0x400];
    ssize_t n = read(0, buf, sizeof buf);
    if ( n < 0 )
    {
        puts("Read failure");
        abort();
    }
    string s(buf, n);
    BitStream src_stream(s);

    size_t size = src_stream.get_bits<size_t>(0x14);

    string target;
    BitStream dest_stream(target);

    printf("Decompressing %zu bytes of data\n", size);
    decompress(src_stream, dest_stream);

    printf("Output: %s\n", target.c_str());
}