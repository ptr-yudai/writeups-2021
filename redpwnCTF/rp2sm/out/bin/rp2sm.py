a, b, b0, t, x0, x1, div, mod = range(8)

bcode = [
    ['LDARG', 0],
    ['STLOC_POP', a],

    ['LDARG', 1],
    ['STLOC_POP', b],

    ['LDLOC', b],
    ['STLOC_POP', b0],

    ['PUSH32', 0],
    ['STLOC_POP', x0],

    ['PUSH32', 1],
    ['STLOC_POP', x1],

    ['PUSH32', 0],
    ['STLOC_POP', div],

    ['MAKE_LABEL', 'main_loop_entry'],
    ['LDLOC', a],
    ['PUSH32', 1],
    ['CMP_ULE'],
    ['JNZ', 'main_loop_exit'],

        ['LDLOC', a],
        ['STLOC_POP', t],

        ['MAKE_LABEL', 'divmod_loop_entry'],
        ['LDLOC', t],
        ['LDLOC', b],
        ['CMP_ULT'],
        ['JNZ', 'divmod_loop_exit'],

            ['LDLOC', div],
            ['PUSH32', 1],
            ['ADD'],
            ['STLOC_POP', div],

            ['LDLOC', t],
            ['LDLOC', b],
            ['SUB'],
            ['STLOC_POP', t],

        ['JMP', 'divmod_loop_entry'],
        ['MAKE_LABEL', 'divmod_loop_exit'],

        ['LDLOC', t],
        ['STLOC_POP', mod],

        ['LDLOC', b],
        ['STLOC_POP', t],

        ['LDLOC', mod],
        ['STLOC_POP', b],

        ['LDLOC', t],
        ['STLOC_POP', a],

        ['LDLOC', x0],
        ['STLOC_POP', t],

        ['LDLOC', x1],
        ['STLOC_POP', x0],

        ['MAKE_LABEL', 'mul_loop_entry'],
        ['LDLOC', div],
        ['IS_EQ_ZERO'],
        ['JNZ', 'mul_loop_exit'],

            ['LDLOC', div],
            ['PUSH32', 1],
            ['SUB'],
            ['STLOC_POP', div],

            ['LDLOC', x0],
            ['LDLOC', t],
            ['SUB'],
            ['STLOC_POP', x0],

        ['JMP', 'mul_loop_entry'],
        ['MAKE_LABEL', 'mul_loop_exit'],

        ['LDLOC', t],
        ['STLOC_POP', x1],

    ['JMP', 'main_loop_entry'],
    ['MAKE_LABEL', 'main_loop_exit'],

    ['LDLOC', x1],
    ['PUSH32', 0],
    ['CMP_GE'],
    ['JNZ', 'ret'],

        ['LDLOC', x1],
        ['LDLOC', b0],
        ['ADD'],
        ['STLOC_POP', x1],

    ['MAKE_LABEL', 'ret'],

    ['LDLOC', x1],
]

def mk_jump_table(bcode):
    jump_table = {}
    for i, (op, *args) in enumerate(bcode):
        if op == 'MAKE_LABEL':
            label, = args
            if label in jump_table:
                raise RuntimeError("duplicate labels")
            jump_table[label] = i
    return jump_table

def as_uint32(x):
    return x % 2 ** 32

def as_int32(x):
    x = as_uint32(x)
    return -(2 ** 31) + x % (2 ** 31) if x >= 2 ** 31 else x

assert as_int32(0xffffffff) == -1
assert as_int32(100) == 100
assert as_int32(0xfffffff0) == -16

def run(bcode, args):
    assert all(type(x) == list for x in bcode)
    jump_table = mk_jump_table(bcode)
    stack = []
    loc = {}
    pc = 0
    while pc < len(bcode):
        assert pc >= 0
        try:
            op, = bcode[pc]
        except ValueError:
            op, param = bcode[pc]
        if op == 'LDARG':
            stack.append(args[param])
        elif op == 'STLOC_POP':
            loc[param] = stack.pop()
        elif op == 'LDLOC':
            stack.append(loc[param])
        elif op == 'PUSH32':
            stack.append(param)
        elif op == 'MAKE_LABEL':
            pass
        elif op == 'CMP_ULE':
            stack[-2:] = [as_uint32(as_uint32(stack[-2]) <= as_uint32(stack[-1]))]
        elif op == 'CMP_ULT':
            stack[-2:] = [as_uint32(as_uint32(stack[-2]) < as_uint32(stack[-1]))]
        elif op == 'CMP_GE':
            stack[-2:] = [as_uint32(as_int32(stack[-2]) >= as_int32(stack[-1]))]
        elif op == 'ADD':
            stack[-2:] = [as_uint32(as_uint32(stack[-2]) + as_uint32(stack[-1]))]
        elif op == 'SUB':
            stack[-2:] = [as_uint32(as_uint32(stack[-2]) - as_uint32(stack[-1]))]
        elif op == 'IS_EQ_ZERO':
            stack[-1:] = [as_uint32(as_uint32(stack[-1]) == 0)]
        elif op == 'JNZ':
            if stack.pop() != 0:
                pc = jump_table[param]
                continue
        elif op == 'JMP':
            pc = jump_table[param]
            continue
        else:
            raise RuntimeError(f"unknown opcode: {op}")
        pc += 1
    return jump_table, stack, loc, pc

opname2opnum = {
    'PUSH32': 8,
    'PUSH16': 9,
    'PUSH8': 10,
    'DUP': 2,
    'POP': 1,
    'CALL': 16,
    'JMP_END': 17,
    'MAKE_LABEL': 18,
    'JMP': 19,
    'JNZ': 20,
    'AND': 80,
    'OR': 81,
    'XOR': 82,
    'ADD': 83,
    'SUB': 84,
    'SHL': 92,
    'SHR': 93,
    'SAR': 94,
    'IS_EQ_ZERO': 64,
    'IS_NE_ZERO': 65,
    'CMP_EQ': 66,
    'CMP_NE': 67,
    'CMP_LT': 68,
    'CMP_GT': 69,
    'CMP_LE': 70,
    'CMP_GE': 71,
    'CMP_ULT': 72,
    'CMP_UGT': 73,
    'CMP_ULE': 74,
    'CMP_UGE': 75,
    'LDLOC': 32,
    'LDARG': 35,
    'STLOC_POP': 33,
    'STLOC': 34,
}

def dump(bcode):
    jump_table = mk_jump_table(bcode)
    jumps = [label for _, label in sorted((i, label) for label, i in jump_table.items())]
    ret = b''
    for pc in range(len(bcode)):
        try:
            op, = bcode[pc]
            ret += bytes([opname2opnum[op]])
        except ValueError:
            op, param = bcode[pc]
            ret += bytes([opname2opnum[op]])
            if op in ('PUSH8', 'LDLOC', 'LDARG', 'STLOC_POP', 'STLOC'):
                ret += bytes([param])
            elif op in ('PUSH16', 'CALL'):
                ret += param.to_bytes(2, 'little')
            elif op in ('PUSH32',):
                ret += param.to_bytes(4, 'little')
            elif op == 'MAKE_LABEL':
                pass
            elif op in ('JMP', 'JNZ'):
                ret += jumps.index(param).to_bytes(2, 'little')
            else:
                raise RuntimeError(f"unknown opcode: {op}")
    return ret

if __name__ == '__main__':
    print(run(bcode, (42, 2017)))
    print(run(bcode, (13371337, 68879)))
    print(run(bcode, (0x0000000000006FAD, 0x00000000AB0D82B1)))
    with open('rp2sm.bin', 'wb') as f:
        f.write(dump(bcode))
