#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#define MKTYPE(n) typedef int##n##_t i##n; typedef uint##n##_t u##n;
MKTYPE(8) MKTYPE(16) MKTYPE(32) MKTYPE(64)

#define BYTE(n)       ((n) & 0xff)
#define WORD(n)       BYTE(n), BYTE((n)>>8)
#define DWORD(n)      WORD(n), WORD((n)>>16)

#define PUSH32(n)     8, DWORD(n)
#define PUSH16(n)     9, WORD(n)
#define PUSH8(n)      10, BYTE(n)
#define DUP           2
#define POP           1
#define CALL(n)       16, WORD(n)
#define JMP_END       17
#define MAKE_LABEL    18
#define JMP(n)        19, WORD(n)
// pop rax; test eax, eax; jnz jmp_vec2[index]
#define JNZ(n)        20, WORD(n)
#define AND           80
#define OR            81
#define XOR           82
#define ADD           83
#define SUB           84
#define SHL           92
#define SHR           93
#define SAR           94
#define IS_EQ_ZERO    64
#define IS_NE_ZERO    65
#define CMP_EQ        66
#define CMP_NE        67
#define CMP_LT        68
#define CMP_GT        69
#define CMP_LE        70
#define CMP_GE        71
#define CMP_ULT       72
#define CMP_UGT       73
#define CMP_ULE       74
#define CMP_UGE       75
#define LDLOC(n)      32, BYTE(n)
#define LDARG(n)      35, BYTE(n)
#define STLOC_POP(n)  33, BYTE(n)
#define STLOC(n)      34, BYTE(n)

#define ADD_FUNC(name, arg, rv, loc) {.code=name, .size=sizeof name, .n_args=arg, .n_ret_vals=rv, .n_locals=loc}
#define ADD_FUNC_EX(name, xsize, arg, rv, loc) {.code=name, .n_args=arg, .n_ret_vals=rv, .n_locals=loc, .size = xsize}

#pragma pack(push, 1)
struct func_t {
  u32 ptr_to_raw;  // +00
  u32 size_of_raw; // +04
  i8 n_args;     // +08
  u8 n_return_vals; // +09
  i16 n_locals;   // +0A
};

struct code_t {
  u64 magic;       // +00
  u32 ptr_to_r14;      // +08
  u32 size_r14;     // +0C
  u32 vsize_r14;    // +10
  u32 ptr_to_r13;      // +14
  u32 size_r13;     // +18
  u32 vsize_r13;    // +1C
  u32 n_functions;      // +20
};
#pragma pack(pop)

// u8 buffer[0x4000];

struct scratch_t {
  u8* buf;
  size_t size;
};

struct jmp_table_t {
  char name[128];
  int index;
} g_CallTable[0x1000];

int g_LastCallIndex;

struct function_t {
  u8* code;
  char name[100];
  size_t size;
  int index;
  int n_args;
  int n_ret_vals;
  int n_locals;
  struct jmp_table_t g_JumpTable[0x1000], g_Locals[0x1000], g_Args[0x1000];
  int last_jump, last_local, last_arg, g_LastCallIndex;
};

struct compiled_code_t {
  int n_functions;
  int entry_point_index;
  struct scratch_t r13;
  struct scratch_t r14;
  struct function_t* functions;
};


void func_add(char* name) {
  strcpy(g_CallTable[g_LastCallIndex].name, name);
  g_CallTable[g_LastCallIndex].index = g_LastCallIndex;
  g_LastCallIndex += 1;
}

int func_get(char* name) {
  int i;
  const int len = sizeof(g_CallTable) / sizeof(g_CallTable[0]);
  for (i = 0; i < len; i++) {
    if (strcmp(name, g_CallTable[i].name) == 0) {
      return g_CallTable[i].index;
    }
  }
  return -1;
}

void jmp_table_add(struct function_t* self, char* name) {
  strcpy(self->g_JumpTable[self->last_jump].name, name);
  self->g_JumpTable[self->last_jump].index = self->last_jump;
  self->last_jump += 1;
}

int jmp_table_get(struct function_t* self, char* name) {
  int i;
  const int len = sizeof(self->g_JumpTable) / sizeof(self->g_JumpTable[0]);
  for (i = 0; i < len; i++) {
    if (strcmp(name, self->g_JumpTable[i].name) == 0) {
      return self->g_JumpTable[i].index;
    }
  }
  return -1;
}

void argv_add(struct function_t* self, char* name) {
  strcpy(self->g_Args[self->last_arg].name, name);
  self->g_Args[self->last_arg].index = self->last_arg;
  self->last_arg += 1;
}

int argv_get(struct function_t* self, char* name) {
  int i;
  const int len = sizeof(self->g_Args) / sizeof(self->g_Args[0]);
  for (i = 0; i < len; i++) {
    if (strcmp(name, self->g_Args[i].name) == 0) {
      return self->g_Args[i].index;
    }
  }
  return -1;
}

int locals_get(struct function_t* self, char* name) {
  int i;
  const int len = sizeof(self->g_Locals) / sizeof(self->g_Locals[0]);
  for (i = 0; i < len; i++) {
    if (strcmp(name, self->g_Locals[i].name) == 0) {
      return self->g_Locals[i].index;
    }
  }
  return -1;
}

void locals_add(struct function_t* self, char* name) {
  strcpy(self->g_Locals[self->last_local].name, name);
  self->g_Locals[self->last_local].index = self->last_local;
  self->last_local += 1;
}

struct insn_t {
  char const* name;
  int opcode;
  int args_size;
  int n_args;
} g_Instructions[] = {
"PUSH32",        8, 4, 1,
"PUSH16",        9, 2, 1,
"PUSH8",         10, 1, 1,
"DUP",           2, 0, 0,
"POP",           1, 0, 0,
"CALL",          16, 2, 1,
"JMP_END",       17, 0, 0,
"MAKE_LABEL",    18, 0, 0,
"JMP",           19, 2, 1,
"JNZ",           20, 2, 1,
"AND",           80, 0, 0,
"OR",            81, 0, 0,
"XOR",           82, 0, 0,
"ADD",           83, 0, 0,
"SUB",           84, 0, 0,
"SHL",           92, 0, 0,
"SHR",           93, 0, 0,
"SAR",           94, 0, 0,
"IS_EQ_ZERO",    64, 0, 0,
"IS_NE_ZERO",    65, 0, 0,
"CMP_EQ",        66, 0, 0,
"CMP_NE",        67, 0, 0,
"CMP_LT",        68, 0, 0,
"CMP_GT",        69, 0, 0,
"CMP_LE",        70, 0, 0,
"CMP_GE",        71, 0, 0,
"CMP_ULT",       72, 0, 0,
"CMP_UGT",       73, 0, 0,
"CMP_ULE",       74, 0, 0,
"CMP_UGE",       75, 0, 0,
"LDLOC",         32, 1, 1,
"LDARG",         35, 1, 1,
"STLOC_POP",     33, 1, 1,
"STLOC",         34, 1, 1,
// load arb
"LOAD_32_R13",         0x30, 0, 0,   // pop rsi ; mov eax, dword ptr [rsi + r13]; push rax
"SIGN_EXT_LOAD_16_R13",   0x31, 0, 0,   // pop rsi ; movsx eax, word ptr [rsi + r13]; push rax
"SIGN_EXT_LOAD_8_R13",    0x32, 0, 0,   // pop rsi ; movsx eax, byte ptr [rsi + r13]; push rax
"ZERO_EXT_LOAD_16_R13",   0x33, 0, 0,   // pop rsi ; movzx eax, word ptr [rsi + r13]; push rax
"ZERO_EXT_LOAD_8_R13",    0x34, 0, 0,   // pop rsi ; movzx eax, byte ptr [rsi + r13]; push rax
"STORE_32_R13", 0x35, 0, 0,   // pop rdi; pop rax; mov [r13+rdi], eax;
"STORE_16_R13", 0x36, 0, 0,   // pop rdi; pop rax; mov [r13+rdi], ax
"STORE_8_R13",  0x37, 0, 0,   // pop rdi; pop rax; mov [r13+rdi], al
"LOAD_32_R14",         0x38, 0, 0,   // pop rsi ; mov eax, dword ptr [rsi + r14]; push rax
"SIGN_EXT_LOAD_16_R14",   0x39, 0, 0,   // pop rsi ; movsx eax, word ptr [rsi + r14]; push rax
"SIGN_EXT_LOAD_8_R14",    0x3A, 0, 0,   // pop rsi ; movsx eax, byte ptr [rsi + r14]; push rax
"ZERO_EXT_LOAD_16_R14",   0x3B, 0, 0,   // pop rsi ; movzx eax, word ptr [rsi + r14]; push rax
"ZERO_EXT_LOAD_8_R14",    0x3C, 0, 0,   // pop rsi ; movzx eax, byte ptr [rsi + r14]; push rax
};

struct insn_t* find_instruction(char* name) {
  int index = 0;
  while (index < sizeof(g_Instructions) / sizeof(g_Instructions[0])) {
    if (strcmp(g_Instructions[index].name, name) == 0) {
      return &g_Instructions[index];
    }
    index++;
  }
  printf("Panik - %s!!\n", name);
  exit(1337);
}

char backup[0x1000], backup2[0x1000];

struct scratch_t load_scratch(char* filename) {
  FILE* f = fopen(filename, "rb");
  if (f == NULL) {
    return (struct scratch_t) {0, 0};
  }
  fseek(f, 0, SEEK_END);
  int size = ftell(f);
  fseek(f, 0, SEEK_SET);
  char* data = malloc(size);
  fread(data, 1, size, f);
  fclose(f);
  return (struct scratch_t) {
    .buf = data,
    .size = size
  };
}

struct compiled_code_t parse_code(char* code) {
  struct compiled_code_t answer;
  strcpy(backup, code);
  strcpy(backup2, code);
  char const* delims = " \n\t\r";
  struct insn_t* mk_tbl = find_instruction("MAKE_LABEL");
  struct insn_t* mk_call = find_instruction("CALL");
  size_t size = 0;
  struct insn_t* t;
  char save[100];
  int n_args = 0, n_locs = 0, n_ret = 0;
  // count procs
  char* token = strtok(code, delims);
  int n_funcs = 0;
  int entry_point_index = 0;
  char entry_name[100];
  
  do {
    strcpy(save, token);
    if (!strcmp(save, "ENTRY")) {
      token = strtok(NULL, delims);
      strcpy(entry_name, token);
      continue;
    }
    if (!strcmp(save, "PROC")) {
      n_funcs += 1;
    }
  } while (token = strtok(NULL, delims));

  struct function_t* func_list = calloc(sizeof(struct function_t), n_funcs);
  strcpy(code, backup);

  int tmp_fuck = 0;

  token = strtok(code, delims);
  do {
    strcpy(save, token);
    if (!strcmp(save, "PROC")) {
      token = strtok(NULL, delims);
      func_add(token);
    }
  } while (token = strtok(NULL, delims));

  strcpy(code, backup);
  token = strtok(code, delims);

  do {
    strcpy(save, token);
    if (!strcmp(save, "ENTRY")) {
      token = strtok(NULL, delims);
      int pos = func_get(token);
      if (pos == -1) {
        printf("No EntryPoint!!!\n");
        exit(1337);
      }
      entry_point_index = pos;
    }
  } while (token = strtok(NULL, delims));

  answer.entry_point_index = entry_point_index;
  // move entry point to the first
  struct jmp_table_t tpp;
  tpp.index = g_CallTable[0].index;
  strcpy(tpp.name, g_CallTable[0].name);
  g_CallTable[0].index = 0;
  strcpy(g_CallTable[0].name, g_CallTable[entry_point_index].name);
  strcpy(g_CallTable[entry_point_index].name, tpp.name);
  g_CallTable[entry_point_index].index = entry_point_index;

  strcpy(code, backup);
  token = strtok(code, delims);
  struct function_t* curr_func_ptr = NULL;
  size = 0;

  do {
    // printf("token: %s, ", token);
    memset(save, 0, sizeof save);
    strcpy(save, token);

    if (strstr(save, "HALT")) break;

    if (!strcmp(save, "SCRATCH_R13:")) {
      token = strtok(NULL, delims);
      printf("r13 file: %s\n", token);
      answer.r13 = load_scratch(token);
      continue;
    }
    if (!strcmp(save, "ENTRY")) {
      token = strtok(NULL, delims);
      continue;
    }
    if (!strcmp(save, "SCRATCH_R14:")) {
      token = strtok(NULL, delims);
      printf("r14 file: %s\n", token);
      answer.r14 = load_scratch(token);
      continue;
    }
    if (!strcmp(save, "PROC")) {
      token = strtok(NULL, delims);
      int pos = func_get(token);
      if (pos == -1) {
        printf("No Func %s\n", token);
        exit(1337);
      }
      curr_func_ptr = &func_list[pos];
      strcpy(curr_func_ptr->name, token);
      size = 0;
      continue;
    }
    if (!strcmp(save, "ENDP")) {
      continue;
    }
    char* p;
    if (p = strstr(save, ":")) {
      *p = 0;
      jmp_table_add(curr_func_ptr, save);
      t = mk_tbl;
    }
    else {
      if (!strcmp(save, "CALL")) {
        token = strtok(NULL, delims);
        int pos = func_get(token);
        if (pos == -1) {
          printf("No such call!!!\n");
          exit(4);
        }
        t = mk_call;
        curr_func_ptr->size += t->args_size + 1 + 2;
        continue;
      }
      else if (!strcmp(save, "RETURN")) {
        t = find_instruction("LDLOC");
      }
      else {
        t = find_instruction(save);
      }
    }
    curr_func_ptr->size += t->args_size + 1;
    if (t->n_args) {
      token = strtok(NULL, delims);
      if (!strcmp(save, "RETURN")) {
        curr_func_ptr->n_ret_vals += 1;
        int pos = locals_get(curr_func_ptr, token);
        if (pos == -1) {
          locals_add(curr_func_ptr, token);
          curr_func_ptr->n_locals += 1;
        }
      }
      if (!strcmp(save, "LDLOC") || !strcmp(save, "STLOC") || !strcmp(save, "STLOC_POP")) {
        int pos = locals_get(curr_func_ptr, token);
        if (pos == -1) {
          locals_add(curr_func_ptr, token);
          curr_func_ptr->n_locals += 1;
        }
      }
      if (!strcmp(save, "LDARG")) {
        int pos = argv_get(curr_func_ptr, token);
        if (pos == -1) {
          argv_add(curr_func_ptr, token);
          curr_func_ptr->n_args += 1;
        }
      }
    }
  } while (token = strtok(NULL, delims));

  printf("[parser] size of code: %zd bytes\n", size);
  // u8* buffer = (u8*)calloc(1, size);
  u8* ptr = NULL;
  curr_func_ptr = NULL;

  token = strtok(backup, delims);
  do {
    strcpy(save, token);
    if (strstr(save, "HALT")) break;
    if (!strcmp(save, "SCRATCH_R13:")) {
      token = strtok(NULL, delims);
      // printf("r13 file: %s\n", token);
      // answer.r13 = load_scratch(token);
      continue;
    }
    if (!strcmp(save, "SCRATCH_R14:")) {
      token = strtok(NULL, delims);
      // printf("r14 file: %s\n", token);
      // answer.r14 = load_scratch(token);
      continue;
    }
    if (!strcmp(save, "PROC")) {
      token = strtok(NULL, delims);
      int pos = func_get(token);
      if (pos == -1) {
        printf("Panik!!! No Such Func %s\n", token);
        exit(42);
      }
      curr_func_ptr = &func_list[pos];
      curr_func_ptr->code = (u8*)calloc(1, curr_func_ptr->size);
      ptr = curr_func_ptr->code;
      continue;
    }
    if (!strcmp(save, "ENDP")) {
      continue;
    }
    if (!strcmp(save, "ENTRY")) {
      token = strtok(NULL, delims);
      continue;
    }
    if (strstr(save, ":")) {
      t = mk_tbl;
    }
    else {
      if (!strcmp(save, "CALL")) {
        token = strtok(NULL, delims);
        int pos = func_get(token);
        if (pos == -1) {
          printf("No such call!!!\n");
          exit(4);
        }
        t = mk_call;
        *ptr++ = t->opcode;
        *(u16*)ptr = pos;
        ptr += 2;
        continue;
      }
      else if (!strcmp(save, "RETURN")) {
        t = find_instruction("LDLOC");
        token = strtok(NULL, delims);
        int pos = locals_get(curr_func_ptr, token);
        if (pos == -1) {
          printf("No such return!!!\n");
          exit(4);
        }
        *ptr++ = t->opcode;
        *(u8*)ptr = pos;
        ptr += 1;
        continue;
      }
      else { t = find_instruction(save); }
    }
    *ptr++ = t->opcode;
    printf("%s[%02x] : ", save, t->opcode);
    // size += t->args_size + 1;
    if (t->n_args) {
      token = strtok(NULL, delims);
      if (!strcmp(save, "JMP") || !strcmp(save, "JNZ")) {
        int pos = jmp_table_get(curr_func_ptr, token);
        *(u16*)ptr = pos;
        ptr += 2;
        printf("jmp_to=%d", pos);
      }
      else {
        size_t arg;
        if (!strcmp(save, "RETURN")) {
          arg = (size_t) locals_get(curr_func_ptr, token);
        }
        if (!strcmp(save, "LDLOC") || !strcmp(save, "STLOC") || !strcmp(save, "STLOC_POP")) {
          arg = (size_t) locals_get(curr_func_ptr, token);
        }
        else if (!strcmp(save, "LDARG")) {
          arg = (size_t) argv_get(curr_func_ptr, token);
        }
        else if (!strcmp(save, "CALL")) {
          arg = (size_t)func_get(token);
        }
        else {
          sscanf(token, "%zi", &arg);
        }
        switch (t->args_size) {
        case 4:
          printf("u32=%zx", arg);
          memcpy(ptr, &arg, 4);
          ptr += 4;
          break;
        case 2:
          printf("u16=%zx", arg & 0xffff);
          memcpy(ptr, &arg, 2);
          ptr += 2;
          break;
        case 1:
          printf("u8=%zx", arg & 0xff);
          *ptr++ = arg;
          break;
        default:
          printf("Panik2!!!\n");
          exit(34);
        }
      }
    }
    printf("\n");
  } while (token = strtok(NULL, delims));

  answer.functions = func_list;
  answer.n_functions = n_funcs;

  return answer;
}

struct compiled_code_t load_file(char* filename) {
  // done by copilot
  FILE* f = fopen(filename, "rb");
  if (!f) {
    printf("Panik - can't open file %s\n", filename);
    exit(1337);
  }
  fseek(f, 0, SEEK_END);
  size_t size = ftell(f);
  fseek(f, 0, SEEK_SET);
  char* code = (char*)calloc(1, size+1);
  fread(code, 1, size, f);
  fclose(f);
  return parse_code(code);
}

void dump_hex(u8* buffer, size_t size) {
  size_t i;
  for (i = 0; i < size; i++) {
    printf("%02x ", buffer[i]);
    if (i % 16 == 15) {
      printf("\n");
    }
  }
  printf("\n");
}






int main() {
  struct compiled_code_t prog = load_file("solve.code");
  prog.entry_point_index = 0;
  printf("\n\n\n\n\n");
  for (int i = 0; i < prog.n_functions; i++) {
    printf("%s\n", prog.functions[i].name);
    printf("n_args=%d, n_locals=%d, n_ret=%d\n",
      prog.functions[i].n_args, prog.functions[i].n_locals, prog.functions[i].n_ret_vals
    );
    if (i == prog.entry_point_index) {
      printf("[ENTRY]\n");
    }
    dump_hex(prog.functions[i].code, prog.functions[i].size);
  }

  size_t i;
  size_t file_size = sizeof(struct code_t) +
    (sizeof(struct func_t)) * prog.n_functions +
    prog.r13.size + prog.r14.size;

  for (i = 0; i < prog.n_functions; i++) {
    file_size += prog.functions[i].size;
  }

  u8* file = (u8*)calloc(1, file_size);
  struct code_t* code = (struct code_t*)file;
  code->magic = 0xD6D733270727F;

  code->size_r14 = prog.r14.size;
  code->size_r13 = prog.r13.size;
  
  code->vsize_r13 = 0x2000;//prog.r13.size;
  code->vsize_r14 = prog.r14.size;

  // layout:
  // code_t
  // func[0]
  // func[1]
  // ...
  // func[n_funcs-1]
  // func[0].code
  // func[1].code
  // ...
  // func[n_funcs-1].code
  // r13
  // r14

  code->n_functions = prog.n_functions;
  struct func_t* func_ptr = (struct func_t*)(file + sizeof(struct code_t));

  for (i = 0; i < prog.n_functions; i++) {
    func_ptr[i].n_args = prog.functions[i].n_args;
    func_ptr[i].n_locals = prog.functions[i].n_locals;
    func_ptr[i].n_return_vals = prog.functions[i].n_ret_vals;
    func_ptr[i].size_of_raw = prog.functions[i].size;
  }

  u8* data_ptr = (u8*)(func_ptr + prog.n_functions);
  i = prog.entry_point_index;
  memcpy(data_ptr, prog.functions[i].code, prog.functions[i].size);
  func_ptr[i].ptr_to_raw = data_ptr - file;
  data_ptr += prog.functions[i].size;
    
  for (i = 0; i < prog.n_functions; i++) {
    if (i == prog.entry_point_index) {
      continue;
    }
    memcpy(data_ptr, prog.functions[i].code, prog.functions[i].size);
    func_ptr[i].ptr_to_raw = data_ptr - file;
    data_ptr += prog.functions[i].size;
  }

  memcpy(data_ptr, prog.r13.buf, prog.r13.size);
  code->ptr_to_r13 = data_ptr - file;
  data_ptr += prog.r13.size;
  memcpy(data_ptr, prog.r14.buf, prog.r14.size);
  code->ptr_to_r14 = data_ptr - file;
  data_ptr += prog.r14.size;


  FILE* fuck = fopen("code.bin", "wb");
  fwrite(file, 1, file_size, fuck);
  fclose(fuck);
}
