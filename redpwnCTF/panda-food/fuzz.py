from ptrlib import *
import random

def add(index, name):
    sock.sendlineafter("choice: \n", "0")
    sock.sendlineafter("idx: \n", str(index))
    sock.sendlineafter("name: \n", name)

def select(index):
    sock.sendlineafter("choice: \n", "1")
    sock.sendlineafter("idx: \n", str(index))

def show(index):
    sock.sendlineafter("choice: \n", "2")
    if sock.recv(3) == b'set':
        return
    l = sock.recvlineafter(": ")
    if l != b'A' * len(l):
        print(l)
        exit()

def eat(index):
    sock.sendlineafter("choice: \n", "3")

sock = Process("./chall")

nya = [-0x8000000000000000, -1, 0, 1, 0x8000000000000000, 0xffffffffffffffff, -0xffffffffffffffff]

for i in range(100):
    choice = random_int(0, 3)
    idx = random.choice(nya)
    if choice == 0:
        name = random_str(1, 64, "A")
        print(f"add({idx}, '{name}')")
        add(idx, name)
    elif choice == 1:
        print(f"select({idx})")
        select(idx)
    elif choice == 2:
        print(f"show({idx})")
        show(idx)
    elif choice == 3:
        print(f"eat({idx})")
        eat(idx)

sock.interactive()
