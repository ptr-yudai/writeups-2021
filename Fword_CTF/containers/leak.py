from ptrlib import *
import base64

sock = SSH("bash.fword.tech", 10000,
           username="ctf", password="FwOrDAndKahl4FTW")

sock.sendlineafter("$ ", "cat sealer | base64")
sock.recvline()
b64 = b''
while True:
    l = sock.recvline()
    b64 += l
    if b'=' in l:
        break

output = base64.b64decode(b64)
with open("sealer", "wb") as f:
    f.write(output)

sock.sh()
