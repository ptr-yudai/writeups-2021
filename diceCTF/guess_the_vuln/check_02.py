import re

array = []
with open("ponta.txt", "r") as f:
    rax = 0
    for line in f:
        r = re.findall("mov\s+\[rsp\+818h\+.+\], (.+)", line)
        if r:
            if r[0] == 'rax':
                array.append(rax)
            else:
                array.append(int(r[0][:-1], 16))
        else:
            r = re.findall("mov\s+rax, (.+)h", line)
            rax = int(r[0], 16)

""" Algorithm:
for i in range(length):
    c = request[i]
    array[c] ^= 1 << i

rax = 456
for i in range(length):
    assert array[i] == rax
    rax += 123
"""

print(array)
rax = 456
for i in range(len(array)):
    print(i, hex(array[i] ^ rax))
    rax += 123

