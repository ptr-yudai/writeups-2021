with open('interception', 'rb') as f:
    buf = f.read()

size = len(buf)
for i in range(0, (len(buf) + 7) & ~7, 8):
    rsi = size
    buf = buf[:i+0] + bytes([buf[i+0] ^ (rsi & 0xff)]) + buf[i+1:]
    rsi >>= 8
    buf = buf[:i+1] + bytes([buf[i+1] ^ (rsi & 0xff)]) + buf[i+2:]
    rsi >>= 8
    buf = buf[:i+2] + bytes([buf[i+2] ^ (rsi & 0xff)]) + buf[i+3:]
    rsi >>= 8
    buf = buf[:i+3] + bytes([buf[i+3] ^ (rsi & 0xff)]) + buf[i+4:]
    rsi >>= 8
    buf = buf[:i+4] + bytes([buf[i+4] ^ (rsi & 0xff)]) + buf[i+5:]
    rsi >>= 8
    buf = buf[:i+5] + bytes([buf[i+5] ^ (rsi & 0xff)]) + buf[i+6:]
    rsi >>= 8
    buf = buf[:i+6] + bytes([buf[i+6] ^ (rsi & 0xff)]) + buf[i+7:]
    rsi >>= 8
    buf = buf[:i+7] + bytes([buf[i+7] ^ (rsi & 0xff)]) + buf[i+8:]
    size = size * 0x539 + 0x7a69

with open('decoded.bin', 'wb') as f:
    f.write(buf)

