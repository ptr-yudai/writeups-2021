import re

def parse(x):
    a = 0
    with open(f"circuit_parts/circuit_part_{x}.qasm", "r") as f:
        for line in f:
            if line.startswith("ccx "):
                r = re.findall("ccx q\[\d+\],q\[\d+\],q\[(\d+)\]", line)
                a |= 1 << (int(r[0]) - 384)
            else:
                break
    return a

a = parse(0)
b = parse(1)
c = parse(2)

k1N = a*a - b
k2N = a*a*a*a - c

N = gcd(k1N, k2N)
print(N)
p, q = factor(N)
p = p[0]
q = q[0]

ciphertext = [0 for i in range(14)]
ciphertext[0] = 45749875208794574
ciphertext[1] = 174236903665592715
ciphertext[2] = 92986020244906386
ciphertext[3] = 96036561989153081
ciphertext[4] = 172801515479555174
ciphertext[5] = 44641100244251515
ciphertext[6] = 3734906486773735
ciphertext[7] = 118982417340454121
ciphertext[8] = 174116556965592444
ciphertext[9] = 142609916776911323
ciphertext[10] = 23526131344477686
ciphertext[11] = 133653291044116991
ciphertext[12] = 40709263355256998
ciphertext[13] = 72092056193487302

e = 65537
d = inverse_mod(e, (p-1)*(q-1))
flag = b''
for c in ciphertext:
    m = power_mod(c, d, N)
    flag += int.to_bytes(int(m), 8, 'big')
print(flag.replace(b'\x00', b''))