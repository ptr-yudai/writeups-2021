import string
import random
from ptrlib import *

def add(name):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", name)
def delete():
    sock.sendlineafter("> ", "2")
def edit(index, desc):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", str(index))
    sock.sendlineafter("> ", desc)
def view_stock():
    sock.sendlineafter("> ", "4")
def view_log():
    sock.sendlineafter("> ", "5")

sock = Process(["wasmtime", "./wasmpwn.wasm", "--dir", "./"])

rndstr = lambda n: ''.join([random.choice(string.ascii_letters) for i in range(n)])
index = 10
for j in range(1000):
    choice = random.randrange(0, 5)
    if choice == 0:
        s = rndstr(random.randint(1, 512))
        print(f'add("{s}")')
        add(s)
        index += 1
    elif choice == 1:
        print(f'delete()')
        delete()
        if index > 10:
            index -= 1
    elif choice == 2 and index > 10:
        i = random.randrange(10, index)
        s = rndstr(random.randint(1, 255))
        print(f'edit({i}, "{s}")')
        edit(i, s)
    #elif choice == 3:
    #    print('view_stock()')
    #    view_stock()
    #else:
    #    print('view_log()')
    #    view_log()
        

sock.interactive()
