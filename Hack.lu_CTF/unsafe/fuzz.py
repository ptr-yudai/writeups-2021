from ptrlib import *
import random


def add(username, password):
    assert len(username) < 0x100 and len(password) < 0x100
    sock.send("\x00")
    sock.send(bytes([len(username)]))
    sock.send(username)
    sock.send(bytes([len(password)]))
    sock.send(password)
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def get(username):
    assert len(username) < 0x100
    sock.send("\x01")
    sock.send(bytes([len(username)]))
    sock.send(username)
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")[:-4]
def edit(username, password):
    assert len(username) < 0x100 and len(password) < 0x100
    sock.send("\x02")
    sock.send(bytes([len(username)]))
    sock.send(username)
    sock.send(bytes([len(password)]))
    sock.send(password)
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def push_tasks(vec2d):
    sock.send("\x03")
    sock.send(bytes([len(vec2d)]))
    for vec1d in vec2d:
        sock.send(bytes([len(vec1d)]))
        for s in vec1d:
            sock.send(bytes([len(s)]))
            sock.send(s)
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def pop_task():
    sock.send("\x04")
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def edit_task(index, data):
    sock.send("\x05")
    sock.send(bytes([index]))
    sock.send(bytes([len(data)]))
    sock.send(data)
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def get_task(index):
    sock.send("\x06")
    sock.send(bytes([index]))
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")[:-4]
def add_task(data):
    assert len(data) < 0x100
    sock.send("\x07")
    sock.send(bytes([len(data)]))
    sock.send(data)
    sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def nazo():
    sock.send("\x08")
    sock.recvuntil(b"\x7f\x7f\x7f\x7f")

sock = Socket("localhost", 9999)

PasswordManager = set()

while True:
    choice = random_int(0, 8)
    if choice == 0: # add
        username = random_bytes(0x7, charset=[0x41])
        password = random_bytes(0x7, charset=[0x41])
        print(f"add({username}, {password})")
        add(username, password)
        PasswordManager.add(username)

    elif choice == 1: # get
        if len(PasswordManager):
            username = random.choice(list(PasswordManager))
        else:
            username = random_bytes(0x7, charset=[0x41])
        print(f"get({username})")
        print(get(username))

    elif choice == 2: # edit
        if len(PasswordManager):
            username = random.choice(list(PasswordManager))
        else:
            username = random_bytes(0x7, charset=[0x41])
        password = random_bytes(0x7, charset=[0x41])
        print(f"edit({username}, {password})")
        edit(username, password)

    elif choice == 3: # push tasks
        tasks = random_list(
            2, 0, 0x7, 0.5,
            gen=lambda is_key, depth: random_bytes(0x7, charset=[0x41]) if depth==1 else []
        )
        print(f"push_tasks({tasks})")
        push_tasks(tasks)

    elif choice == 4: # pop_task
        print(f"pop_task()")
        pop_task()

    elif choice == 5: # edit_task
        index = random_int(0x7)
        data = random_bytes(0x7, charset=[0x41])
        print(f"edit_task({index}, {data})")
        edit_task(index, data)

    elif choice == 6: # get_task
        index = random_int(0x7)
        print(f"get_task({index})")
        print(get_task(index))

    elif choice == 7: # add_task
        data = random_bytes(0x7, charset=[0x41])
        print(f"add_task({data})")
        add_task(data)

    nazo()
        
sock.interactive()
