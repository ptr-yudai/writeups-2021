class LameRand:
    def __init__(self, seed):
        self.next = seed
    
    def rand(self):
        self.next = self.next * 0x343FD + 0x269EC3
        self.next = self.next & 0xffffffff
        return (self.next >> 0x10) & 0x7FFF

for seed in range(1, 0x100):
    lm = LameRand(seed)
    rr = [lm.rand() for i in range(0x100)]
    if 1 in rr:
        break

X = 0xbcdb6
username = chr(seed)

v = X
key = ''
while True:
    l = list(filter(lambda x: x < v, rr))
    if l == []:
        break
    n = rr.index(max(l))
    key += f'{n:02x}' * (v // rr[n])
    v = v % rr[n]
    print(v)

n = rr.index(1)
key += f'{n:02x}' * v

print(key)
print(username)
