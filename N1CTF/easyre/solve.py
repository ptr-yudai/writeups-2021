import re

with open("dump.txt", "r") as f:
    ope = []
    reg = {f'XMM{i}':0 for i in range(16)}
    var = {}
    for l in f:
        r = re.findall("([0-9a-f]{9}).+\s\s\s\s\s+?(.+?)\s\s+(.+)", l)
        if r == []:
            continue
        r = r[0]
        if r[2].startswith("RAX,"):
            # RAXを追う
            rax = int(r[2][4:], 16)
            if rax < 0:
                rax = (-rax ^ 0xffffffffffffffff) + 1
            continue

        if r[2].startswith('qword'):
            # local_hoge[0/7]=RAXのエミュレート
            r = re.findall("local\_([0-9a-f]+)\[(\d)\]", r[2])
            if r == []: continue
            r = r[0]
            if r[0] not in var: var[r[0]] = 0
            if r[1] == '7':
                var[r[0]] &= 0x00ffffffffffffff
                var[r[0]] |= rax << (8*7)
            elif r[1] == '0':
                var[r[0]] &= 0xffffffffffffffff0000000000000000
                var[r[0]] |= rax
            else:
                raise Exception("Unreachable")
            continue

        elif r[1] == 'VMOVDQA':
            # xmmA <-- mem[B] / xmmA <-- xmmB / mem[A] <-- xmmBをエミュレート
            operand = r[2].split(",")
            if operand[0][:3] == 'XMM':
                dsttype = 'reg'
                dst = operand[0]
            else:
                dsttype = 'mem'
                r = re.findall("local\_([0-9a-f]+)\[(\d)\]", operand[0])[0]
                assert r[1] == '0'
                dst = r[0]

            if operand[1][:3] == 'XMM':
                srctype = 'reg'
                src = operand[1]
            else:
                srctype = 'mem'
                r = re.findall("local\_([0-9a-f]+)\[(\d)\]", operand[1])[0]
                assert r[1] == '0'
                src = r[0]

            if dsttype == 'mem':
                assert srctype == 'reg'
                var[dst] = reg[src]
            else:
                if srctype == 'reg':
                    reg[dst] = reg[src]
                else:
                    reg[dst] = var[src]

            ope.append(('mov', ((dsttype, dst), (srctype, src))))
            continue

        elif r[1] == 'VAESENC':
            reg1, reg2, reg3 = r[2].split(",")
            assert reg1.startswith("XMM")\
                and reg2.startswith("XMM")\
                and reg3.startswith("XMM")
            ope.append(('enc', (('reg', reg1),
                                ('reg', reg2),
                                ('reg', reg3))))
            continue

        elif r[1] == 'VAESENCL':
            reg1, reg2, reg3 = r[2].split(",")
            assert reg1.startswith("XMM")\
                and reg2.startswith("XMM")\
                and reg3.startswith("XMM")
            ope.append(('enclast', (('reg', reg1),
                                    ('reg', reg2),
                                    ('reg', reg3))))
            continue

        elif r[1] == 'VAESDEC':
            reg1, reg2, reg3 = r[2].split(",")
            assert reg1.startswith("XMM")\
                and reg2.startswith("XMM")\
                and reg3.startswith("XMM")
            ope.append(('dec', (('reg', reg1),
                                ('reg', reg2),
                                ('reg', reg3))))
            continue

        elif r[1] == 'VAESIMC':
            reg1, reg2 = r[2].split(",")
            assert reg1.startswith("XMM")\
                and reg2.startswith("XMM")
            ope.append(('imc', (('reg', reg1),
                                ('reg', reg2))))
            continue

        elif r[1] == 'VAESDECL':
            reg1, reg2, reg3 = r[2].split(",")
            assert reg1.startswith("XMM")\
                and reg2.startswith("XMM")\
                and reg3.startswith("XMM")
            ope.append(('declast', (('reg', reg1),
                                    ('reg', reg2),
                                    ('reg', reg3))))
            continue

        elif r[1] == 'VPXOR':
            reg1, reg2, arg3 = r[2].split(",")
            if arg3[:3] == 'XMM':
                type = 'reg'
            else:
                type = 'mem'
                r = re.findall("local\_([0-9a-f]+)\[(\d)\]", arg3)[0]
                assert r[1] == '0'
                arg3 = r[0]
            assert reg1.startswith("XMM")\
                and reg2.startswith("XMM")
            ope.append(('xor', (('reg', reg1),
                                ('reg', reg2),
                                (type, arg3))))
            continue

        elif r[1] == 'VPADDQ':
            reg1, reg2, arg3 = r[2].split(",")
            if arg3[:3] == 'XMM':
                type = 'reg'
            else:
                type = 'mem'
                r = re.findall("local\_([0-9a-f]+)\[(\d)\]", arg3)[0]
                assert r[1] == '0'
                arg3 = r[0]
            assert reg1.startswith("XMM")\
                and reg2.startswith("XMM")
            ope.append(('add', (('reg', reg1),
                                ('reg', reg2),
                                (type, arg3))))
            continue

        elif r[1] == 'VPSUBQ':
            reg1, reg2, arg3 = r[2].split(",")
            if arg3[:3] == 'XMM':
                type = 'reg'
            else:
                type = 'mem'
                r = re.findall("local\_([0-9a-f]+)\[(\d)\]", arg3)[0]
                assert r[1] == '0'
                arg3 = r[0]
            assert reg1.startswith("XMM")\
                and reg2.startswith("XMM")
            ope.append(('sub', (('reg', reg1),
                                ('reg', reg2),
                                (type, arg3))))
            continue

        elif r[1] == 'VPSHUFD':
            reg1, reg2, imm = r[2].split(",")
            assert reg1.startswith("XMM")\
                and reg2.startswith("XMM")
            ope.append(('shuffle', (reg1, reg2, int(imm, 16))))
            continue

        if r[0] == '100019d98':
            break

print(ope)
