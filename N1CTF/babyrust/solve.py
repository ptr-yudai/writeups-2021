with open("test.txt", "r") as f:
    l = f.readline().strip()

parts = {
    "Never gonna give you up": "pos += 1",
    "Never gonna let you down": "pos -= 1",
    "Never gonna run around and desert you": "reg += 1",
    "Never gonna make you cry": "reg -= 1",
    "Never gonna say goodbye": "reg = flag[pos]",
    "Never gonna tell a lie and hurt you": "flag[pos] = reg"
}

ans = [148, 59, 143, 112, 121, 186, 106, 133, 55, 90, 164, 166, 167, 121, 174, 147, 148, 167, 99, 86, 81, 161, 151, 149, 132, 56, 88, 188, 141, 127, 151, 63]
flag = [0 for c in ans]
pos = 0
reg = 0
code = ""
while l:
    for part in parts:
        if l.startswith(part):
            code += parts[part] + "\n"
            l = l[len(part)+1:]
            break
    else:
        break

exec(code)
o = "n1ctf{"
for a, b in zip(ans, flag):
    o += chr(a - b)
o += "}"
print(o)

