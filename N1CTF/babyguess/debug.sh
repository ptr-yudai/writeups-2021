#!/bin/sh
qemu-system-x86_64 \
    -m 512M \
    -kernel ./bzImage \
    -initrd ./testfs.img \
    --append "loglevel=3 console=ttyS0 root=/dev/ram init=/init nokaslr" \
    -smp cores=2,threads=4 \
    -cpu kvm64,+smep,+smap \
    -monitor /dev/null \
    -nographic \
    -gdb tcp::12345
