#define GPLv2 "GPL v2"
#define ARRSIZE(x) (sizeof(x) / sizeof((x)[0]))

enum _bpf_prog_type {
	xBPF_PROG_TYPE_UNSPEC,
	xBPF_PROG_TYPE_SOCKET_FILTER,
	xBPF_PROG_TYPE_KPROBE,
	xBPF_PROG_TYPE_SCHED_CLS,
	xBPF_PROG_TYPE_SCHED_ACT,
	xBPF_PROG_TYPE_TRACEPOINT,
	xBPF_PROG_TYPE_XDP,
	xBPF_PROG_TYPE_PERF_EVENT,
	xBPF_PROG_TYPE_CGROUP_SKB,
	xBPF_PROG_TYPE_CGROUP_SOCK,
	xBPF_PROG_TYPE_LWT_IN,
	xBPF_PROG_TYPE_LWT_OUT,
	xBPF_PROG_TYPE_LWT_XMIT,
	xBPF_PROG_TYPE_SOCK_OPS,
	xBPF_PROG_TYPE_SK_SKB,
	xBPF_PROG_TYPE_CGROUP_DEVICE,
	xBPF_PROG_TYPE_SK_MSG,
	xBPF_PROG_TYPE_RAW_TRACEPOINT,
	xBPF_PROG_TYPE_CGROUP_SOCK_ADDR,
	xBPF_PROG_TYPE_LWT_SEG6LOCAL,
	xBPF_PROG_TYPE_LIRC_MODE2,
	xBPF_PROG_TYPE_SK_REUSEPORT,
	xBPF_PROG_TYPE_FLOW_DISSECTOR,
	xBPF_PROG_TYPE_CGROUP_SYSCTL,
	xBPF_PROG_TYPE_RAW_TRACEPOINT_WRITABLE,
	xBPF_PROG_TYPE_CGROUP_SOCKOPT,
	xBPF_PROG_TYPE_TRACING,
	xBPF_PROG_TYPE_STRUCT_OPS,
	xBPF_PROG_TYPE_EXT,
	xBPF_PROG_TYPE_LSM,
	xBPF_PROG_TYPE_SK_LOOKUP,
	xBPF_PROG_TYPE_SYSCALL, /* a program that can execute syscalls */
};

enum _bpf_attach_type {
	xBPF_CGROUP_INET_INGRESS,
	xBPF_CGROUP_INET_EGRESS,
	xBPF_CGROUP_INET_SOCK_CREATE,
	xBPF_CGROUP_SOCK_OPS,
	xBPF_SK_SKB_STREAM_PARSER,
	xBPF_SK_SKB_STREAM_VERDICT,
	xBPF_CGROUP_DEVICE,
	xBPF_SK_MSG_VERDICT,
	xBPF_CGROUP_INET4_BIND,
	xBPF_CGROUP_INET6_BIND,
	xBPF_CGROUP_INET4_CONNECT,
	xBPF_CGROUP_INET6_CONNECT,
	xBPF_CGROUP_INET4_POST_BIND,
	xBPF_CGROUP_INET6_POST_BIND,
	xBPF_CGROUP_UDP4_SENDMSG,
	xBPF_CGROUP_UDP6_SENDMSG,
	xBPF_LIRC_MODE2,
	xBPF_FLOW_DISSECTOR,
	xBPF_CGROUP_SYSCTL,
	xBPF_CGROUP_UDP4_RECVMSG,
	xBPF_CGROUP_UDP6_RECVMSG,
	xBPF_CGROUP_GETSOCKOPT,
	xBPF_CGROUP_SETSOCKOPT,
	xBPF_TRACE_RAW_TP,
	xBPF_TRACE_FENTRY,
	xBPF_TRACE_FEXIT,
	xBPF_MODIFY_RETURN,
	xBPF_LSM_MAC,
	xBPF_TRACE_ITER,
	xBPF_CGROUP_INET4_GETPEERNAME,
	xBPF_CGROUP_INET6_GETPEERNAME,
	xBPF_CGROUP_INET4_GETSOCKNAME,
	xBPF_CGROUP_INET6_GETSOCKNAME,
	xBPF_XDP_DEVMAP,
	xBPF_CGROUP_INET_SOCK_RELEASE,
	xBPF_XDP_CPUMAP,
	xBPF_SK_LOOKUP,
	xBPF_XDP,
	xBPF_SK_SKB_VERDICT,
	xBPF_SK_REUSEPORT_SELECT,
	xBPF_SK_REUSEPORT_SELECT_OR_MIGRATE,
	x__MAX_BPF_ATTACH_TYPE
};

struct bpf_load_program_attr {
	enum _bpf_prog_type prog_type;
	enum _bpf_attach_type expected_attach_type;
	const char *name;
	const struct bpf_insn *insns;
	size_t insns_cnt;
	const char *license;
	union {
		__u32 kern_version;
		__u32 attach_prog_fd;
	};
	union {
		__u32 prog_ifindex;
		__u32 attach_btf_id;
	};
	__u32 prog_btf_fd;
	__u32 func_info_rec_size;
	const void *func_info;
	__u32 func_info_cnt;
	__u32 line_info_rec_size;
	const void *line_info;
	__u32 line_info_cnt;
	__u32 log_level;
	__u32 prog_flags;
};

// eebpf
#define BPF_ALSH	0xe0	/* sign extending arithmetic shift left */
#define BPF_ALSH_REG(DST, SRC) BPF_RAW_INSN(BPF_ALU | BPF_ALSH | BPF_X, DST, SRC, 0, 0)
#define BPF_ALSH_IMM(DST, IMM) BPF_RAW_INSN(BPF_ALU | BPF_ALSH | BPF_K, DST, 0, 0, IMM)
#define BPF_ALSH64_REG(DST, SRC) BPF_RAW_INSN(BPF_ALU64 | BPF_ALSH | BPF_X, DST, SRC, 0, 0)
#define BPF_ALSH64_IMM(DST, IMM) BPF_RAW_INSN(BPF_ALU64 | BPF_ALSH | BPF_K, DST, 0, 0, IMM)
 
/* registers */
/* caller-saved: r0..r5 */
#define BPF_REG_ARG1    BPF_REG_1
#define BPF_REG_ARG2    BPF_REG_2
#define BPF_REG_ARG3    BPF_REG_3
#define BPF_REG_ARG4    BPF_REG_4
#define BPF_REG_ARG5    BPF_REG_5
#define BPF_REG_CTX     BPF_REG_6
#define BPF_REG_FP      BPF_REG_10
 
#define BPF_LD_IMM64_RAW(DST, SRC, IMM)         \
  ((struct bpf_insn) {                          \
    .code  = BPF_LD | BPF_DW | BPF_IMM,         \
    .dst_reg = DST,                             \
    .src_reg = SRC,                             \
    .off   = 0,                                 \
    .imm   = (__u32) (IMM) }),                  \
  ((struct bpf_insn) {                          \
    .code  = 0, /* zero is reserved opcode */   \
    .dst_reg = 0,                               \
    .src_reg = 0,                               \
    .off   = 0,                                 \
    .imm   = ((__u64) (IMM)) >> 32 })
#define BPF_LD_MAP_FD(DST, MAP_FD)              \
  BPF_LD_IMM64_RAW(DST, BPF_PSEUDO_MAP_FD, MAP_FD)
#define BPF_LDX_MEM(SIZE, DST, SRC, OFF)        \
  ((struct bpf_insn) {                          \
    .code  = BPF_LDX | BPF_SIZE(SIZE) | BPF_MEM,\
    .dst_reg = DST,                             \
    .src_reg = SRC,                             \
    .off   = OFF,                               \
    .imm   = 0 })
#define BPF_MOV64_REG(DST, SRC)                 \
  ((struct bpf_insn) {                          \
    .code  = BPF_ALU64 | BPF_MOV | BPF_X,       \
    .dst_reg = DST,                             \
    .src_reg = SRC,                             \
    .off   = 0,                                 \
    .imm   = 0 })
#define BPF_ALU64_IMM(OP, DST, IMM)             \
  ((struct bpf_insn) {                          \
    .code  = BPF_ALU64 | BPF_OP(OP) | BPF_K,    \
    .dst_reg = DST,                             \
    .src_reg = 0,                               \
    .off   = 0,                                 \
    .imm   = IMM })
#define BPF_ALU32_IMM(OP, DST, IMM)             \
  ((struct bpf_insn) {                          \
    .code  = BPF_ALU | BPF_OP(OP) | BPF_K,      \
    .dst_reg = DST,                             \
    .src_reg = 0,                               \
    .off   = 0,                                 \
    .imm   = IMM })
#define BPF_STX_MEM(SIZE, DST, SRC, OFF)        \
  ((struct bpf_insn) {                          \
    .code  = BPF_STX | BPF_SIZE(SIZE) | BPF_MEM,\
    .dst_reg = DST,                             \
    .src_reg = SRC,                             \
    .off   = OFF,                               \
    .imm   = 0 })
#define BPF_ST_MEM(SIZE, DST, OFF, IMM)         \
  ((struct bpf_insn) {                          \
    .code  = BPF_ST | BPF_SIZE(SIZE) | BPF_MEM, \
    .dst_reg = DST,                             \
    .src_reg = 0,                               \
    .off   = OFF,                               \
    .imm   = IMM })
#define BPF_EMIT_CALL(FUNC)                     \
  ((struct bpf_insn) {                          \
    .code  = BPF_JMP | BPF_CALL,                \
    .dst_reg = 0,                               \
    .src_reg = 0,                               \
    .off   = 0,                                 \
    .imm   = (FUNC) })
#define BPF_JMP_REG(OP, DST, SRC, OFF)				\
	((struct bpf_insn) {					\
		.code  = BPF_JMP | BPF_OP(OP) | BPF_X,		\
		.dst_reg = DST,					\
		.src_reg = SRC,					\
		.off   = OFF,					\
		.imm   = 0 })
#define BPF_JMP_IMM(OP, DST, IMM, OFF)          \
  ((struct bpf_insn) {                          \
    .code  = BPF_JMP | BPF_OP(OP) | BPF_K,      \
    .dst_reg = DST,                             \
    .src_reg = 0,                               \
    .off   = OFF,                               \
    .imm   = IMM })
#define BPF_EXIT_INSN()                         \
  ((struct bpf_insn) {                          \
    .code  = BPF_JMP | BPF_EXIT,                \
    .dst_reg = 0,                               \
    .src_reg = 0,                               \
    .off   = 0,                                 \
    .imm   = 0 })
#define BPF_LD_ABS(SIZE, IMM)                   \
  ((struct bpf_insn) {                          \
    .code  = BPF_LD | BPF_SIZE(SIZE) | BPF_ABS, \
    .dst_reg = 0,                               \
    .src_reg = 0,                               \
    .off   = 0,                                 \
    .imm   = IMM })
#define BPF_ALU64_REG(OP, DST, SRC)             \
  ((struct bpf_insn) {                          \
    .code  = BPF_ALU64 | BPF_OP(OP) | BPF_X,    \
    .dst_reg = DST,                             \
    .src_reg = SRC,                             \
    .off   = 0,                                 \
    .imm   = 0 })
#define BPF_MOV64_IMM(DST, IMM)                 \
  ((struct bpf_insn) {                          \
    .code  = BPF_ALU64 | BPF_MOV | BPF_K,       \
    .dst_reg = DST,                             \
    .src_reg = 0,                               \
    .off   = 0,                                 \
    .imm   = IMM })
 
int bpf_(int cmd, union bpf_attr *attrs) {
  return syscall(__NR_bpf, cmd, attrs, sizeof(*attrs));
}

int prog_load(struct bpf_insn *insns, size_t insns_count) {
  char verifier_log[100000];
  union bpf_attr create_prog_attrs = {
    .prog_type = BPF_PROG_TYPE_SOCKET_FILTER,
    .insn_cnt = insns_count,
    .insns = (uint64_t)insns,
    .license = (uint64_t)GPLv2,
    .log_level = 2,
    .log_size = sizeof(verifier_log),
    .log_buf = (uint64_t)verifier_log
  };
  int progfd = bpf_(BPF_PROG_LOAD, &create_prog_attrs);
  int errno_ = errno;
  //printf("==========================\n%s==========================\n",verifier_log);
  errno = errno_;
  return progfd;
}
 
long create_filtered_socket_fd(struct bpf_insn *insns, size_t insns_count) {
  int progfd = prog_load(insns, insns_count);
  if (progfd == -1) {
    puts("Oops");
    return -1;
  }
 
  // hook eBPF program up to a socket
  // sendmsg() to the socket will trigger the filter
  // returning 0 in the filter should toss the packet
  int socks[2];
  if (socketpair(AF_UNIX, SOCK_DGRAM, 0, socks))
    err(1, "socketpair");
  if (setsockopt(socks[0], SOL_SOCKET, SO_ATTACH_BPF, &progfd, sizeof(int)))
    err(1, "setsockopt");
  return ((long)socks[0] << 32) | (progfd << 16) | socks[1];
}
 
void trigger_proc(int sockfd) {
  if (write(sockfd, "X", 1) != 1)
    err(1, "write to proc socket failed");
}

