from ptrlib import *

def encode(block):
    encoded = [0] * 8
    for i in range(8):
        c = block[i]
        for j in range(8):
            encoded[j] |= ((c & 1) << i)
            c >>= 1
    return encoded

with open("babyrev", "rb") as f:
    f.seek(0x3020)
    encoded = f.read(0x20)

flag = b''
for block in chunks(encoded, 8):
    flag += bytes(encode(block))

print(flag)
