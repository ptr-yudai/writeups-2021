import ast

INF = 1 << 20

maze = ast.literal_eval(open("message.txt").read())

s = ""
for row in maze:
    ss = ""
    for x in row:
        if x == 0:
            ss += "⬛"
        else:
            ss += "⬜"
    s += ss + "\n"
print(s)

maze = maze[1:-1]
for i in range(len(maze)):
    maze[i] = maze[i][1:-1]

h = len(maze)
w = len(maze[0])


# DFS
# def dfs():
#     dy = [-1, 0, 0, 1]
#     dx = [0, -1, 1, 0]
#     # direction = ["down", "right", "left", "up"]
#     direction = [1, 0, 2, 3]
#     stack = [(0,0, [], set())]
#     while len(stack) > 0:
#         (y, x, path, seen), stack = stack[0], stack[1:]
#         seen.add((y, x))
#         for i in range(4):
#             ny = y + dy[i]
#             nx = x + dx[i]
#             if ny < 0 or h <= ny:
#                 continue
#             if nx < 0 or w <= nx:
#                 continue
#             if maze[ny][nx] == 0:
#                 continue
#             if (ny, nx) in seen:
#                 continue
# 
#             stack = [(ny, nx, path + [direction[i]], set(list(seen)))] + stack
# 
#         if y == h-1 and x == w-1:
#             yield path
# 

# BFS
def bfs():
    dy = [-1, 0, 0, 1]
    dx = [0, -1, 1, 0]
    dist = [[INF for _ in range(w)] for _ in range(h)]
    direction = [1, 0, 2, 3]
    que = [(0,0,0)]
    while len(que) > 0:
        (y, x, cur), que = que[0], que[1:]
        dist[y][x] = cur

        for i in range(4):
            ny = y + dy[i]
            nx = x + dx[i]
            if ny < 0 or h <= ny:
                continue
            if nx < 0 or w <= nx:
                continue
            if maze[ny][nx] == 0:
                continue

            if dist[ny][nx] != INF:
                continue

            que.append((ny, nx, cur + 1))
    return [dist]


# recover route
def recover_route(w, h, dist):
    dy = [-1, 0, 0, 1]
    dx = [0, -1, 1, 0]
    direction = [1, 0, 2, 3]

    stack = [(h-1,w-1,[])]
    while len(stack) > 0:
        (y, x, route), stack = stack[0], stack[1:]
        d = dist[y][x]
        if d == 0:
            return route
        for i in range(4):
            ny = y + dy[i]
            nx = x + dx[i]
            if ny < 0 or h <= ny:
                continue
            if nx < 0 or w <= nx:
                continue
            if dist[ny][nx] != d - 1:
                continue

            stack = [(ny, nx, [direction[i]] + route)] + stack

dist = bfs()[0]
# s = ""
# for row in dist:
#     ss = "|"
#     for x in row:
#         if x == INF:
#             ss += "   "
#         else:
#             ss += "{:3}".format(x)
#         ss += "|"
#     s += ss + "\n"
# print(s)
# quit()
direction_vector = recover_route(h, w, dist)[::2]
dirpath = bytearray()
for i in range(1, len(direction_vector)):
    if direction_vector[i] == direction_vector[i-1]:
        dirpath.append(0)
    elif direction_vector[i] == (direction_vector[i-1]+1)%4:
        dirpath.append(1)
        dirpath.append(1)
    elif direction_vector[i] == (direction_vector[i-1]-1)%4:
        dirpath.append(1)
        dirpath.append(0)

dirpath = bytearray(b'\x01\x00\x01\x01\x00\x01\x00\x00\x01\x00\x00\x01\x01\x01\x01\x01\x00\x01\x00\x01\x01\x00\x01\x01\x00\x01\x00\x01\x00\x00\x01\x01\x01\x01\x01\x00\x00\x01\x01\x01\x00\x01\x01\x01\x01\x01\x00\x00\x01\x01\x01\x00\x01\x01\x01\x01\x01\x00\x01\x01\x01\x00\x00\x00\x01\x00\x01\x00\x00\x01\x01\x00\x01\x01\x01\x01\x01\x00\x00\x00\x01\x00\x00\x01\x00\x01\x00\x01\x01\x01\x01\x00\x01\x01\x00\x01\x00\x01\x01\x01\x01\x01\x00\x01\x01\x01\x01\x01\x00\x00\x00\x01\x01\x00\x01\x00\x01\x00\x01\x01\x01\x00\x01\x00\x01\x01\x01\x00\x01\x01\x01\x00\x01\x00\x01\x01\x01\x01\x01\x00\x00\x00\x00\x00\x01\x00\x01\x01\x00\x00\x01\x00\x01\x00\x00\x01\x01\x00\x01\x01\x01\x01\x01\x00\x01\x00\x00\x00\x00\x01\x01\x01\x01\x00\x00\x01\x00\x01\x00\x01\x01\x01\x01\x01\x00\x00\x01\x00\x01\x01\x01\x00\x00\x01\x01\x01\x00\x00\x00\x01\x01\x00\x01\x00\x01\x00\x01\x01\x00\x01\x00\x01\x00\x00\x01\x01\x01\x00\x00\x01\x01\x01\x01\x00\x01\x00\x01\x01\x01\x00\x00\x01\x01\x01\x01\x01\x00\x01\x01\x01\x00\x01\x01\x01\x00\x01\x00\x01\x01\x01\x01\x01\x00\x00\x01\x01\x01\x01\x00\x01\x00\x01\x01\x01\x00\x01\x00\x00\x00\x01\x01\x01\x00\x01\x00\x01\x01\x00\x01\x00\x01\x00\x01\x01\x01\x00\x01\x01\x01\x01\x01\x00\x00\x00\x00\x01\x01\x01\x00\x01\x00\x00\x01\x01\x00\x01\x01\x01\x01\x01\x00\x00\x01\x00\x01\x01\x00\x01\x01\x01\x00\x01\x01\x01\x01\x01\x00\x01\x00\x00\x00\x01\x01\x01\x01\x01\x00\x01\x01\x00\x00\x01\x01\x01\x00\x00\x01\x00\x01\x00\x01\x01\x01\x01\x01\x00\x00\x01\x00\x01\x01\x00\x00\x01\x00\x01\x00\x00\x01\x01\x01\x00\x01\x00\x01\x01\x00\x01\x01\x00\x00\x00\x01\x01\x01\x01\x00\x01\x00\x01\x00\x00\x00\x01\x01\x01\x00\x01\x00\x00\x01\x01\x01\x01\x00\x00\x00\x01\x01\x01\x01\x01\x00\x01\x00\x01\x01\x01\x00\x01\x00\x00\x01\x01\x00\x00\x00\x00\x01\x00\x01\x01')
key = b''
for i in range(432 // 8):
    c = 0
    for j in range(8):
        #c |= dirpath[i*8+j] << j
        c |= dirpath[i*8+j] << (7 - j)
    key += bytes([c])

from ptrlib import xor
f = open("st6.exe", 'rb')
f.seek(0x2910, 0)
SerialXor = f.read(54)
print(xor(key, SerialXor))
