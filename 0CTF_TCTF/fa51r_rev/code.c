#include <time.h>
#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>

#define MK(n) typedef int##n##_t i##n; typedef uint##n##_t u##n

MK(8);
MK(16);
MK(32);
MK(64);

u8 word_600000040[] =
{
  0x00, 0x00, 0x01, 0x00, 0x10, 0x00, 0x11, 0x00, 0x00, 0x01, 
  0x01, 0x01, 0x10, 0x01, 0x11, 0x01, 0x00, 0x10, 0x01, 0x10, 
  0x10, 0x10, 0x11, 0x10, 0x00, 0x11, 0x01, 0x11, 0x10, 0x11, 
  0x11, 0x11
};

u64 g_Seed;
u32 g_maskCount;
u16 g_Masks[32557];

void print_masks() {
  printf("masks: ");
  for (int i = 0; i < 6; ++i)
    printf("%04x ", g_Masks[i]);
  putchar(10);
}

// > choice:
// 1
// Timestamp: 1625378405 --- initial g_Seed

u32 next_u32() {
  g_Seed = (0x41C64E6D * g_Seed + 0x3039) & 0x7FFFFFFF;
  return g_Seed;
}

u16 get_magic() {
  u32 v1 = next_u32();
  u32 a1 = v1 & 0xF;
  u32 a2 = (v1 >> 4) & 0xf;
  u32 t = (v1 >> 8) & 0xf;
  u32 a3 = t & 3;
  u32 a4 = (t >> 2) & 3;
  u16* fup = (u16*)word_600000040;
  return (fup[a2] << a4) | (u32)(a1 << (4 * a3));
}

u16 magic2(u16 a1) {
  return (2 * a1) & 0xEEEE | (a1 >> 3) & 0x1111u;
}

u16 rotl16(u16 n, u16 k) {
  return (n << k) | (n >> 16 - k);
}

u16 get_mask(u16 magic_w, u16 inp_byt) {
  i32 a = 0;
  i32 b = 0;
  i32 i, j;
  for (i = 0; i <= 3; ++i) {
    if ( (((u32)inp_byt >> (i + 4)) & 1) != 0 ) {
      magic_w = rotl16(magic_w, 4);
      ++a;
    }
  }
  for ( j = 0; j <= 3; ++j ) {
    if ( (((u32)inp_byt >> j) & 1) != 0 ) {
      magic_w = magic2(magic_w);
      ++b;
    }
  }
  return magic_w;
}

u16 propagate_mask(u16 mask)
{
  u16 result;
  int i;

  result = mask;
  for ( i = 0; i <= 3; ++i )
  {
    result = mask & g_Masks[i + 2];
    if ( result )
    {
      result = mask | g_Masks[i + 1];
      g_Masks[i + 1] = result;
      break;
    }
  }
  if ( i == 4 )
  {
    result = mask | g_Masks[5];
    g_Masks[5] = result;
  }
  return result;
}

u16 check_if_lost()
{
  u16 result;
  result = g_Masks[1];
  print_masks();
  if (g_Masks[1])
  {
    printf("You lose!\n");
    exit(0);
  }
  return result;
}

void copy_bitmask(u32 count, u16 mask)
{
  u16 *result;
  i32 i = (i32)count;
  for (; i > 0; --i )
  {
    g_Masks[i + 1] &= ~mask;
    result = &g_Masks[i + 1];
    *result |= mask & g_Masks[i];
  }
}

i32 update_masks()
{
  u16 v0;
  u16 v2;
  unsigned int v3;
  int v4;
  int i;
  int j;

  v3 = 4;
  while ( v3 )
  {
    v4 = 0;
    v2 = g_Masks[v3 + 1];
    for ( i = 0; i <= 3; ++i )
    {
      v0 = 15 << (4 * i);
      if ( v0 == (v2 & v0) )
      {
        v4 = 1;
        ++g_maskCount;
        copy_bitmask(v3, v0);
        v2 = g_Masks[v3 + 1];
      }
    }
    for ( j = 0; j <= 3; ++j )
    {
      if ( (u16)(0x1111 << j) == (v2 & (u16)(0x1111 << j)) )
      {
        v4 = 1;
        ++g_maskCount;
        copy_bitmask(v3, 0x1111 << j);
        v2 = g_Masks[v3 + 1];
      }
    }
    if ( !v4 )
      --v3;
  }
  return 0LL;
}

i32 do_fuck(u8 inp_byte, u16 magic_w) {
  propagate_mask(get_mask(magic_w, inp_byte));
  check_if_lost();
  update_masks();
  return 0;
}

u32 is_valid_login(char* inp, u32 len) {
  for (u32 i = 0; i < len; ++i ) {
    if ( g_maskCount > 0x96 ) {
      break;
    }
    do_fuck(inp[i], get_magic());
  }
  printf("g_MaskCount: %#x\n", g_maskCount);
  for (i32 i = 0; i < 10; ++i) {
    printf("%04x ", g_Masks[i]);
  }
  return g_maskCount > 0x96;
}

int main(int argc, char** argv) {
  if (argc == 2) {
    sscanf(argv[1], "%i", &g_Seed);
  }
  else {
    g_Seed = time(NULL);
  }
  char input[] = "\x00\x07\x11\x110\x00s11\x00\x11q\x11\x13\x00ss\x13\x00\x13\x03\x000q\x00w\x11s\x07\x13\x17\x11\x13w\x00\x03qww\x131\x00\x10p\x11\x00\x00\x07\x170\x073";
  // for (u32 i = 0; i < strlen(input); ++i) {
  //   printf("%04x, ", get_magic());
  // }
  printf("%d\n", is_valid_login(input, sizeof(input)));
  putchar(10);
}
