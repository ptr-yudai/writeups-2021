from z3 import *

table = [
    0x0000, 0x0001, 0x0010, 0x0011, 0x0100,
    0x0101, 0x0110, 0x0111, 0x1000, 0x1001,
    0x1010, 0x1011, 0x1100, 0x1101, 0x1110,
    0x1111
]

def next_u32(update=True):
    global seed
    if update:
        seed = (0x41C64E6D * seed + 0x3039) & 0x7FFFFFFF
        return seed
    else:
        return (0x41C64E6D * seed + 0x3039) & 0x7FFFFFFF

def magic_1(update=True):
    v = next_u32(update)
    c1, c2 = v & 0xff, (v >> 8) & 0xff
    a1 = c1 & 0x0f
    a2 = (c1 & 0xf0) >> 4
    a3 = c2 & 0b0011
    a4 = (c2 & 0b1100) >> 2
    x = (table[a2] << a4) & 0xffff
    y = (a1 << (4*a3)) & 0xffff
    return x | y

def rotl16(n, k):
    return ((n << k) & 0xffff) | ((n >> (16-k)) & 0xffff)

def magic_2(a1):
    v = ((2*a1) & 0xeeee | (a1>>3) & 0x1111) & 0xffff
    return v

def get_mask(w, c):
    for i in range(4):
        if (c >> (i+4)) & 1 != 0:
            w = rotl16(w, 4)
    for i in range(4):
        if (c >> i) & 1 != 0:
            w = magic_2(w)
    return w

def propagate_mask(mask):
    global g_Masks
    for i in range(4):
        if mask & g_Masks[i+2]:
            g_Masks[i+1] = mask | g_Masks[i+1]
            break
    else: # loop ends without break
        g_Masks[5] = mask | g_Masks[5]

def check_if_lost():
    if g_Masks[1]:
        raise Exception("You lose!")

def copy_mask(count, mask):
    global g_Masks
    for i in range(count, 0, -1):
        g_Masks[i+1] &= 0xffff ^ mask
        g_Masks[i+1] |= mask & g_Masks[i]

def update_masks():
    global g_maskCount
    j = 4
    while j:
        flag = False
        for i in range(4):
            mask = (0x000f << 4*i) & 0xffff
            if g_Masks[j+1] & mask == mask:
                flag = True
                g_maskCount += 1
                copy_mask(j, mask)
        for i in range(4):
            mask = (0x1111 << i) & 0xffff
            if g_Masks[j+1] & mask == mask:
                flag = True
                g_maskCount += 1
                copy_mask(j, mask)
        if not flag:
            j -= 1

def do_fuck(c, w):
    propagate_mask(get_mask(w, c))
    check_if_lost()
    update_masks()

def is_valid_login(inp):
    global g_maskCount
    for i, c in enumerate(inp):
        if g_maskCount > 0x96: break
        do_fuck(c, magic_1())
        print(list(map(hex, g_Masks)))
    else:
        return False
    return True

def score(a, b):
    if a & b == 0:
        return 0xffff
    v = a | b
    r = 0
    if v & 0x1111 == 0x1111: r += 1
    if v & 0x2222 == 0x2222: r += 1
    if v & 0x4444 == 0x4444: r += 1
    if v & 0x8888 == 0x8888: r += 1
    if v & 0x000f == 0x000f: r += 1
    if v & 0x00f0 == 0x00f0: r += 1
    if v & 0x0f00 == 0x0f00: r += 1
    if v & 0xf000 == 0xf000: r += 1
    return r

def find_best_move(current, w):
    x, y, z = 0, 0, w
    best_score = score(current, w)
    for i in range(4):
        for j in range(4):
            w = magic_2(w)
            r = score(current, w)
            if r > best_score:
                best_score = r
                x, y, z = i, (j+1)%4, w
        w = rotl16(w, 4)
    return ([0,1,3,7][x] << 4) | [0,1,3,7][y], z

if __name__ == '__main__':
    g_maskCount = 0
    g_Masks = [0, 0, 0, 0, 0, 0]
    seed = 114514810 # GIVEN SEED

    current = 0
    for index in range(10):
        w = magic_1()
        c, best_w = find_best_move(current, w)
        print(bin(c), hex(current), hex(w), hex(best_w))
        propagate_mask(get_mask(w, c))
        check_if_lost()
        update_masks()
        print(list(map(hex, g_Masks)), g_maskCount)
