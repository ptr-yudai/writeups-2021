#include <stdio.h>
#include <inttypes.h>

#define MK(n) typedef int##n##_t i##n; typedef uint##n##_t u##n

MK(8);
MK(16);
MK(32);
MK(64);

u8 word_600000040[] =
{
  0x00, 0x00, 0x01, 0x00, 0x10, 0x00, 0x11, 0x00, 0x00, 0x01, 
  0x01, 0x01, 0x10, 0x01, 0x11, 0x01, 0x00, 0x10, 0x01, 0x10, 
  0x10, 0x10, 0x11, 0x10, 0x00, 0x11, 0x01, 0x11, 0x10, 0x11, 
  0x11, 0x11
};

u64 seed;
u32 g_maskCount;
u16 g_Masks[32557];

// > choice: 1 Timestamp: 1625378405 --- initial seed

u32 next_u32() {
  seed = (0x41C64E6D * seed + 0x3039) & 0x7FFFFFFF;
  return seed;
}

u32 magic() {
  u32 v1 = next_u32();
  u32 a1 = v1 & 0xF;
  u32 a2 = (u8)v1 >> 4;
  u32 a3 = (v1 >> 8 & 0xff) & 3;
  u32 a4 = (((v1 >> 8) & 0xF) >> 2) & 3;
  u16* fup = (u16*)word_600000040;
  return (fup[a2] << a4) | (u32)(a1 << (4 * a3));
}

u16 magic2(u16 a1) {
  return (2 * a1) & 0xEEEE | (a1 >> 3) & 0x1111u;
}

u16 get_mask(u16 magic_w, u16 inp_byt) {
  i32 a = 0;
  i32 b = 0;
  i32 i, j;
  for (i = 0; i <= 3; ++i) {
    if ( (((u32)inp_byt >> (i + 4)) & 1) != 0 ) {
      magic_w = ROL16_4bits(magic_w);
      ++a;
    }
  }
  for ( j = 0; j <= 3; ++j ) {
    if ( (((u32)inp_byt >> j) & 1) != 0 ) {
      magic_w = ROL16_4bits(magic_w);
      ++b;
    }
  }
  return magic_w;
}

u16 propagate_mask(u16 mask)
{
  u16 result;
  int i;

  result = mask;
  for ( i = 0; i <= 3; ++i )
  {
    result = mask & g_Masks[i + 2];
    if ( result )
    {
      result = mask | g_Masks[i + 1];
      g_Masks[i + 1] = result;
      break;
    }
  }
  if ( i == 4 )
  {
    result = mask | g_Masks[5];
    g_Masks[5] = result;
  }
  return result;
}

u16 check_if_lost()
{
  u16 result;
  result = g_Masks[1];
  if ( g_Masks[1] )
  {
    printf("You lose!\n");
    exit(0);
  }
  return result;
}

u16* copy_mask(u32 count, u16 mask)
{
  u16 *result;
  i32 i;

  result = (u16 *)count;
  for ( i = count; i > 0; --i )
  {
    g_Masks[i + 1] &= ~mask;
    result = &g_Masks[i + 1];
    *result |= mask & g_Masks[i];
  }
  return result;
}

i32 update_masks()
{
  int v0;
  u16 v2;
  unsigned int v3;
  int v4;
  int i;
  int j;

  v3 = 4;
  while ( v3 )
  {
    v4 = 0;
    v2 = g_Masks[v3 + 1];
    for ( i = 0; i <= 3; ++i )
    {
      v0 = 15 << (4 * i);
      if ( (u16)v0 == (v2 & (u16)v0) )
      {
        v4 = 1;
        ++g_maskCount;
        copy_bitmask(v3, v0);
        v2 = g_Masks[v3 + 1];
      }
    }
    for ( j = 0; j <= 3; ++j )
    {
      if ( (u16)(0x1111 << j) == (v2 & (u16)(0x1111 << j)) )
      {
        v4 = 1;
        ++g_maskCount;
        copy_bitmask(v3, 0x1111 << j);
        v2 = g_Masks[v3 + 1];
      }
    }
    if ( !v4 )
      --v3;
  }
  return 0LL;
}

i32 do_fuck(u8 inp_byte, u16 magic_w) {
  propagate_mask(get_mask(magic_w, inp_byte));
  check_if_lost();
  update_masks();
  return 0;
}

u32 is_valid_login(char* input, u32 len) {
  for (u32 i = 0; i < len; ++i ) {
    if ( g_maskCount > 0x96 ) {
      break;
    }
    do_fuck(input[i], magic());
  }
  return g_maskCount > 0x96;
}
