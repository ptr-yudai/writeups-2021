from ptrlib import *
import random

seedList = {}
def add(data):
    global seedList
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(len(data)))
    sock.sendafter(": ", data)
    for i in range(10):
        if i not in seedList:
            seedList[i] = data
            break
def mutate(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
def delete(index):
    global seedList
    sock.sendlineafter("> ", "4")
    sock.sendlineafter(": ", str(index))
    del seedList[index]
def set_mutate(mpr):
    sock.sendlineafter("> ", "5")
    sock.sendlineafter(": ", str(mpr))
def run_thread():
    sock.sendlineafter("> ", "6")

sock = Process("./how2mutate")

for rounds in range(0x100):
    choice = random_int(0, 4)
    if choice == 0:
        data = random_bytes(1, 1) * random_int(0, 0x7f00)
        print(f"add({data})")
        add(data)
    elif choice == 1:
        if len(seedList) == 0:
            continue
        index = random.choice(list(seedList.keys()))
        print(f"mutate({index})")
    elif choice == 2:
        if len(seedList) == 0:
            continue
        index = random.choice(list(seedList.keys()))
        print(f"delete({index})")
        delete(index)
    elif choice == 3:
        mpr = random_int(0, 9)
        print(f"set_mutate({mpr})")
        set_mutate(mpr)
    elif choice == 4:
        print(f"run_thread()")
        run_thread()
