import json
import threading
from ptrlib import *

def store(sock, key_size, key, value_size, value):
    sock.sendlineafter("option: ", "1")
    sock.sendlineafter(": ", str(key_size))
    sock.sendafter(": ", key)
    sock.sendlineafter(": ", str(value_size))
    sock.sendafter(": ", value)
def query(sock, key_size, key):
    sock.sendlineafter("option: ", "2")
    sock.sendlineafter(": ", str(key_size))
    sock.sendafter(": ", key)
    l = sock.recvline()
    if l == b'err':
        return None
    r = l.split(b':')
    return int(r[0], 16), bytes.fromhex(r[1].decode())
def delete(sock, key_size, key):
    sock.sendlineafter("option: ", "3")
    sock.sendlineafter(": ", str(key_size))
    sock.sendafter(": ", key)

import timeout_decorator
@timeout_decorator.timeout(1)
def reproduce(opelist):
    sock = process("./mooosl")
    for ope in opelist:
        ope[0](sock, *ope[1])
    sock.close()

opelist = []
with open("crash.txt", "r") as f:
    for line in f:
        ope = json.loads(line)
        f = None
        if ope['func'] == 'store':
            f = store
        elif ope['func'] == 'query':
            f = query
        elif ope['func'] == 'delete':
            f = delete
        args = ope['args']
        opelist.append((f, args))
    
for index in range(len(opelist)-1, -1, -1):
    testlist = opelist[:index] + opelist[index+1:]
    try:
        reproduce(testlist)
    except:
        print(f"[+] reduced {index}")
        opelist = opelist[:index] + opelist[index+1:]

for ope in opelist:
    f = ''
    if ope[0] == store:
        f = 'store'
    elif ope[0] == query:
        f = 'query'
    else:
        f = 'delete'
    print(json.dumps({
        'func': f,
        'args': ope[1]
    }))
