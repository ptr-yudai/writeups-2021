char retBuffer[0x3f0];
char *printCommand, *flag1;

vector<Shape> allShapes;

/**
   type)
     0: triangle
     1: square
     2: circle
     3: polygon

   struct Triangle {
     unsigned short id; // +00h
     int type;          // +04h: == 0
     int numpts;        // +10h
   }
   struct Square {
     unsigned short id; // +00h
     int type;          // +04h: == 1
   }
   struct Circle {
     unsigned short id; // +00h
     int type;          // +04h: == 2
     int size;          // +10h: size of circle
   }
   struct Polygon {
     unsigned short id; // +00h
     int type;          // +04h: == 3
     int *points;       // +08h: list of points
     int numpts;        // +10h: number of points
   }
 */

void CommandReset()
{
  puts("Clearing allShapes");
  for (Shape cur = allShapes.begin(); cur != allShapes.end(); cur++) {
    // do something
  }
  allShapes.clear();
}

Shape *GetShapeFromId(int id)
{
  for (Shape cur = allShapes.begin(); cur != allShapes.end(); cur++) {
    if (cur->id == id) return cur;
  }
  sprintf(retBuffer, "Shape %i not found %i\n", id); // ?
}

Shape *GetShapeFromId(string str_id)
{
  char *s = str_id.c_str();
  char *p = malloc(strlen(s));
  int j = 0;
  for (int i = 0; i < strlen(s); i++) {
    if ((0x30 <= s[i]) && (s[i] <= 0x39)) {
      p[j++] = s[i];
    }
  }
  int id = atoi(p);
  free(p);
  return GetShapeFromId(id);
}

void Polygon::AddPoint(int a, int b) {
  /* [!] vulnerability */
  this->points = realloc((this->numpts + 1) * sizeof(int) * 2);
  this->points[this->numpts+0] = a;
  this->points[this->numpts+1] = b;
  this->numpts += 1;
}

void Polygon::PrintPoint(int a) {
  if (a < 0 || a >= this->numpts) {
    sprintf(retBuffer, "No");
  } else {
    sprintf(retBuffer, "Point &i = &i, &i\n", a, this->points[a], this->points[a+1]);
  }
}

void Polygon::ModPoint(int a, int b, int c) {
  if (a < 0 || a >= this->numpts) {
  } else {
    this->points[a*2+0] = b;
    this->points[a*2+1] = c;
  }
}

void ProcessCommand(char *cmd)
{
  vector<string> args = cmd.split(",");
  int argc = args.size();

  if ((args[0] == "create") && (argc == 2)) {

    CommandCreate(args[1]);

  } else if (args[0] == "print") {

    sprintf(retBuffer, "Executing command: %s\n", printCommand);
    system(printCommand);

  } else if ((args[0] == "addpoint") && (argc == 4)) {

    Shape *shape = GetShapeFromId(args[1]);
    if (shape) {
      if (type != 3) {
        sprintf(retBuffer, "not a polygon!\n");
      } else {
        shape->AddPoint(atoi(args[2].c_str()), atoi(args[3].c_str()));
        sprintf(retBuffer, "Added point, now has %i points\n", shape->numpts);
      }
    }

  } else if ((args[0] == "getpoint") && (argc == 3)) {

    Shape *shape = GetShapeFromId(args[1]);
    if (shape) {
      if (type != 3) {
        sprintf(retBuffer, "not a polygon!\n");
      } else {
        ((Polygon*)shape)->PrintPoint(atoi(args[2].c_str()));
      }
    }

  } else if ((args[0] == "modpoint") && (argc == 5)) {

    Shape *shape = GetShapeFromId(args[1]);
    if (shape) {
      if (type != 3) {
        sprintf(retBuffer, "Not a polygon!\n");
      } else {
        ((Polygon*)shape)->ModPoint(atoi(args[2].c_str()),
                                    atoi(args[3].c_str()),
                                    atoi(args[4].c_str()));
        sprintf(retBuffer, "Point modified\n");
      }
    }

  } else if ((args[0] == "circlesize") && (argc == 3)) {

    Shape *shape = GetShapeFromId(args[1]);
    if (type != 2) {
      sprintf(retBuffer, "Not a circle!\n");
    } else {
      /* [!] vulnerability */
      GetShapeFromId(atoi(args[1].c_str()))->size = atoi(args[2].c_str());
      sprintf(retBuffer, "Circle size set\n");
    }

  }
}

int main()
{
  char cmd[0x100];
  char *ptr1, *ptr2, *ptr3;

  setvbuf(stdin, NULL, _IONBF, 0);
  setvbuf(stdout, NULL, _IONBF, 0);

  printCommand = malloc(0x3c);
  ptr1 = malloc(0x14);
  ptr2 = malloc(0x14);
  ptr3 = malloc(0x14);
  flag1 = malloc(0x14);

  FILE *fp = fopen("flag1.txt", "rb");
  fread(flag1, 1, 0x14, fp);
  fclose(fp);
  strcpy(printCommand, "printer-invoke --printername printer1 --paper a1 --tray 2");
  free(ptr1); // :thinking_face:
  free(ptr2);
  free(ptr3);

  while (1) {
    int ofs = 0;
    memset(cmd, 0, 0xFF);
    if (read(0, cmd, 1) <= 0) {
      puts("Socket closed propbably, closing...");
      CommandReset();
      return 0;
    }
    unsigned char length = cmd[0];
    while (length != 0) {
      int size = read(0, &cmd[ofs], length);
      if (size <= 0) break;
      ofs += size;
      length -= size;
    }

    retBuffer[0] = 0;
    ProcessCommand(cmd);
    if (retBuffer[0]) {
      write(1, retBuffer, strlen(retBuffer));
    }
  }
}
