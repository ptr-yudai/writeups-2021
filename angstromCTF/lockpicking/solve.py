from ptrlib import *

def inc_i(n):
    return 'i'*n
def dec_i(n):
    return 'I'*n
def inc_m(n):
    return 'm'*n
def dec_m(n):
    return 'M'*n
def inc_o(n):
    return 'o'*n
def dec_o(n):
    return 'O'*n
def show_lock(n):
    return '?'*n
def flip_bull():
    return '!'

#sock = Process("./lockpicking")
sock = Socket("nc shell.actf.co 21702")

code = ''
# (6, 37, 13) set var_10 = 1
code += inc_i(6)
code += dec_m(7)
code += inc_o(13)
code += flip_bull()
# (9, 8, 7) set var_0F = 1
code += inc_i(3)
code += inc_m(15)
code += dec_o(6)
code += flip_bull()
# (22, 11, 7) set var_12 = 1 (o cannot be 7)
code += dec_o(1)
code += inc_i(13)
code += inc_m(3)
code += inc_o(1)
# (6, 37, 13) set var_10 = 0, var_11 = 1
code += flip_bull()
code += inc_i(12)
code += dec_m(18)
code += inc_o(6)
# (6, 37, 13) set var_10 = 1
code += flip_bull()
# (0, 0, 0) set var_0E = 1
code += dec_i(6)
code += inc_m(7)
code += dec_o(13)

sock.sendline(code)

sock.interactive()
