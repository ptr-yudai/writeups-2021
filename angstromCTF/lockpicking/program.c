#include <stdio.h>

typedef struct {
  int i; // 00h
  int m; // 04h
  int o; // 08h
  char s[2]; // 0Ah
  char var_0C;
  char var_0D;
  char var_0E;
  char var_0F;
  char var_10;
  char var_11;
  char var_12;
} Lock;

void initialize(Lock *lock) {
  memset(lock, 0, sizeof(Lock));
}

void sub_15a0(Lock *lock) {
  if ((lock->m == 11) && (lock->i == 22) && (lock->o == 7)) {
    lock->var_12 = 1;
  }

  if (lock->o == 7) {

    if ((lock->var_0F == 0) || (lock->var_12 == 0)) {
      if ((lock->m == 8) && (lock->i == 9)) {
        if (lock->var_10) {
          lock->var_0F = lock->var_0D ^ 1;
        } else {
          lock->var_0F = lock->var_10;
        }
      } else {
        lock->var_0F = lock->var_12;
      }
    } else {
      lock->var_0F = lock->var_12;
    }

  } else if ((lock->o == 13) && (lock->m == 0x25) && (lock->i == 6)) {

    if (lock->var_0D == 0) {
      lock->var_10 = 1;
    }

    if (lock->var_0F == 0) {
      lock->var_0E = 0;
      return;
    }

  }


}

char do_action(Lock *lock, char act, int *x) {
  *x++;
  switch (act) {
  case '?':
    show_lock(lock);
    *x--;
    break;
  case '@':
    lock->s[0] ^= 1;
    break;
  case '!':
    lock->s[1] ^= 1;
    break;
  case 'I':
    lock->i = (lock->i + 27) % 28;
    break;
  case 'i':
    lock->i = (lock->i + 1) % 28;
    break;
  case 'M':
    lock->m = (lock->m + 43) % 44;
    break;
  case 'm':
    lock->m = (lock->m + 1) % 44;
    break;
  case 'O':
    lock->m = (lock->m + 55) % 56;
    break;
  case 'o':
    lock->m = (lock->m + 1) % 56;
    break;

  default:
    return 1;
  }

  sub_15a0(lock);
  return 0;
}

int main()
{
  int x;
  char lock[0x12];
  char action[0x400];

  initialize(lock);
  while (1) {
    //
    printf("> ");
    if (fgets(action, 0x400, stdin) == NULL) {
      puts("You walk away from the lock");
      return 0;
    }
    for (char *p = action; *p != '\0' && *p != '\n'; p++) {
      if (do_action(lock, *p, &x)) {
        puts("Your actions prove to be ineffective against the lock");
        return 0;
      }
    }

    if (x > 164) {
      puts("Your hands grow weary from the stiffnes of the lock");
      return 0;
    }

    if ((lock[14] != 0)
        && (lock[15] != 0)
        && (lock[16] != 0)
        && (lock[17] != 0)) {
      break;
    }
  }

  puts("The lock opens.");
  show_flag();

  return 0;
}
