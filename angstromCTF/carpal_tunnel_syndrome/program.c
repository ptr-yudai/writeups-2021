// distribute the source code of pwn tasks !!! :( :( :(

char *texts[] = {"foo", "bar", "", ..., ""};
typedef struct {
  ?;
  Cell *row_next; // +08h
  Cell *col_next; // +10h
  char *text;     // +18h
} Cell;

void setup() {
  Cell *rows[5];
  
  for (int i = 0; i < 5; i++) {
    rows[i] = (Cell*)malloc(sizeof(Cell)); // 0x20
    Cell *cur = rows[i];
    
    cur->text = texts[5*i];
    for (int j = 1; j < 5; j++) {
      Cell *row_next = (Cell*)malloc(sizeof(Cell)); // 0x20
      row_next->text = texts[i*5 + j];
      cur->row_next = row_next;
      cur = row_next;
    }
  }

  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 5; j++) {
      Cell *c1 = rows[i];
      Cell *c2 = rows[i+1];
      for (int k = 0; k < j; k++) {
        c1 = c1->row_next;
        c2 = c2->row_next;
      }
      c1->hoge = c2;
    }
  }

  root = rows[0];
}

