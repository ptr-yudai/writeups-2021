from ptrlib import *
import ctypes

glibc = ctypes.cdll.LoadLibrary('/lib/x86_64-linux-gnu/libc-2.27.so')
glibc.srand(glibc.time(0))
rand = glibc.rand

#sock = Process("./infinity_gauntlet")
sock = Socket("nc shell.actf.co 21700")

def trace():
    if rand() % 2 == 1:
        choice = rand() % 3
        if choice == 0:
            rand()
        elif choice == 1:
            rand()
        else:
            rand()
    else:
        choice = rand() % 4
        if choice == 0:
            rand(), rand()
        elif choice == 1:
            rand(), rand()
        elif choice == 2:
            rand(), rand()
        elif choice == 3:
            rand()

def calc_answer(s):
    if s.startswith("foo"):
        args = s[s.index('(')+1:s.index(')')].split(', ')
        rhs = s[s.index('=')+2:]
        if rhs == '?':
            return int(args[0]) ^ (int(args[1]) + 1) ^ 1337
        elif args[0] == '?':
            return int(rhs) ^ (int(args[1]) + 1) ^ 1337
        elif args[1] == '?':
            return (int(args[0]) ^ int(rhs) ^ 1337) - 1
    else:
        args = s[s.index('(')+1:s.index(')')].split(', ')
        rhs = s[s.index('=')+2:]
        if rhs == '?':
            return int(args[0]) + int(args[1]) * (int(args[2]) + 1)
        elif args[0] == '?':
            return int(rhs) - int(args[1]) * (int(args[2]) + 1)
        elif args[1] == '?':
            return (int(rhs) - int(args[0])) // (int(args[2]) + 1)
        elif args[2] == '?':
            return (int(rhs) - int(args[0])) // int(args[1]) - 1
    raise Exception("Unreachable Path")

for i in range(49):
    print(f"ROUND {i+1}")
    offset = rand()
    answer = rand() % 0x10000
    trace()
    sock.sendlineafter("===\n", str(answer))
    sock.recvline()
    sock.recvline()

flag = '?' * 120
for i in range(114514):
    sock.recvuntil("===\n")
    l = sock.recvline()
    answer = calc_answer(l.decode())
    mod = (answer >> 8) - 50 - i
    x = ((answer & 0xff) ^ (17 * mod)) & 0xff
    flag = flag[:mod] + chr(x) + flag[mod+1:]
    print(mod, x, flag)
    sock.sendline(str(answer))

sock.interactive()
