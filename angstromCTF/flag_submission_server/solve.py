from ptrlib import *
import time
import ctypes
import string
table = string.printable[:-5]
glibc = ctypes.cdll.LoadLibrary('/lib/x86_64-linux-gnu/libc-2.27.so')

def gen_rand(seed):
    glibc.srand(seed ^ 12345678)
    r = 0
    for i in range(8):
        r = r * 10 + (glibc.rand() % 10)
    return r * seed

#sock = Socket("localhost", 21450)
sock = Socket("rev.2021.chall.actf.co", 21450)

# get uuid
key = b"\xde\xad\xbe\xef\xfe\xed\xca\xfe\x13\x37\xab\xcd\xef"
s = b''
while len(s) != 13:
    s += sock.recv()
uuid = xor(s, key)
logger.info("UUID = " + uuid.hex())

# search flag
seed = 0x13371337
flag = ''
for i in range(114514):
    for c in table:
        logger.info(f"attempt: {c}")
        payload  = p64(gen_rand(seed))
        payload += c.encode()
        payload += uuid
        sock.send(payload)
        if sock.recv(1) == b'\r':
            continue
        s = b''
        while len(s) != 4:
            s += sock.recv()
        seed = u32(s, byteorder='big')
        flag += c
        logger.info(flag)
        break
    else:
        print(flag)
        exit()

sock.interactive()

