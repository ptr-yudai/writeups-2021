from ptrlib import *
import ctypes

def reset(field):
    offset = libc.rand() % 0x100
    while True:
        if field[offset] == 0:
            field[offset] = 2
            break
        offset = (offset + 1) % 0x100
    for i in range(50):
        offset = libc.rand() % 0x100
        while True:
            if field[offset] == 0:
                field[offset] = 3
                break
            offset = (offset + 1) % 0x100

def show(field):
    output = ''
    for y in range(16):
        for x in range(16):
            output += ['.', '*', '$', 'x'][field[y*16+x]]
        output += '\n'
    print(output)

with open("masochistic_snake", "rb") as f:
    f.seek(0x3100)
    table = f.read(64)

R, U, L, D = 0, 1, 2, 3
def relative_move(cnt, next):
    nazo = (((0xA1025D352CC2655F>>table[(cnt%32)*2])&1)<<1)|((0xA1025D352CC2655F>>table[(cnt%32)*2+1])&1)
    if (nazo + 0b10) & 0b11 == next:
        return 'a'
    elif nazo == next:
        return 'd'
    elif (nazo + 0b11) & 0b11 == next:
        return 's'
    elif (nazo + 0b01) & 0b11 == next:
        return 'w'
    raise Exception('Unreachable')

libc = ctypes.CDLL('/lib/x86_64-linux-gnu/libc-2.27.so')
"""
sock = Process(["./masochistic_snake", "5"])
"""
sock = SSH("shell.actf.co", 22, "team id", "team password")
logger.info("Login OK")
sock.sendlineafter("$ ", "cd /problems/2021/masochistic_snake")
sock.sendlineafter("$ ", "./masochistic_snake")
#"""

field = [0 for i in range(0x100)]
field[0] = 1
reset(field)

sock.sendlineafter("...", "")
sock.recvline()

fruitnum = 0
nocnt, cnt, pos = 1, 1, 1
moves = [
    [R, R, R, D, D, D, R, R, R, D, D, D],
    [L, L, D, D, D, D, D, D, R],
    [U, R, R, R, R, U],
    [U, U, L, L, L, L, L, L, L, U],
    [U, R, R, D, D, D, D, D],
    [R, R, R, R, U],
    [R, U, U, L, L, D, L, L, L, U],
    [U, R, R, R, U, U, R, U, U, U, U, R],
    [R, R, R, D, D, D, D, D, R, R, D, D, D, L, L, L, L, L, L, D, D, D, D],
    [D, L, L, L, U, U, L, L, L, U, U, L, U, U, R],
    [U, L, L, D, D, D, D, D, D, D, R, R, R, D],
    [R, R, R, R, U, U, U, U, U, U, U, U, U, U, L, L],
    [D, D, D, D, D, D, D, D, R, R, R, R, D],
    [L, L, L, D, R, R, R, R, R, R, R, R, U, L, L, L, L, L, U, U, U, U, L, L, U],
    [L, L, D, D, D, D, D, R, R, D, R, R, R, R, R],
    [R],
    [U, U, L, L, U, L, U, U, U],
    [U, R, R, R, U, U, U, L, U],
    [U, U, L, L, L, L, L, L, L, L, L],
    [D, D, D, D, D, D, D, R, D, D, R],
    [D, L, L, L, L, U, U, U, U, U, U, U, U, U, U, U, U],
    [L, L, D, D, D, D]
]

posinfo = [0 for i in range(0x100)]
step = 0
while True:
    show(field)
    sock.recvuntil("o")
    direction = moves[fruitnum][step]
    if direction == R:
        pos += 1
    elif direction == D:
        pos += 16
    elif direction == U:
        pos -= 16
    elif direction == L:
        pos -= 1
    if field[pos] == 0:
        field[posinfo[nocnt % 0x100]] = 0
        nocnt += 1

    if len(moves[fruitnum]) > step:
        sock.send(relative_move(cnt, moves[fruitnum][step]))
        step += 1
    if len(moves[fruitnum]) == step: # get fruit
        step = 0
        fruitnum += 1
        while 3 in field:
            field[field.index(3)] = 0
        reset(field)
        if fruitnum == len(moves):
            show(field)
            break

    field[pos] = 1
    posinfo[(cnt + 1) % 0x100] = pos
    cnt += 1

sock.interactive()

# break *(0x555555554000 + 0x19c7)
# break *(0x555555554000 + 0x1761)
