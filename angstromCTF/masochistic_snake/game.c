// 0 = quit / 1 = win / 2 = lose
int game(int interval)
{
  unsigned char offset;
  int bomb[50];
  char field[0x100], posinfo[0x100];
  memset(field, 0, 0x100);
  memset(posinfo, 0, 0x100);
  field[0] = 1;

  wtimeout(stdscr, 0);

  // put fruit
  offset = rand() % 0x100;
  while (field[offset] != 0) offset++;
  field[offset] = 2;

  // spray bomb
  for (int i = 0; i < 50; i++) {
    offset = rand() % 0x100;
    while (field[offset] != 0) offset++;
    field[offset] = 3;
    bomb[i] = offset;
  }

  mvprintw(0, 0, "Here's a cheerleader! ...what, did you expect to see a snake?");
  mvprintw(1, 0, "\\o/");

  unsigned char cnt = 0;
  unsigned char nocnt = 0;
  while (1) {
    // show cheerleader
    if (cnt % 2) {
      if (wmove(stdscr, 1, 0) != -1) waddch(stdscr, '/');
      if (wmove(stdscr, 1, 2) != -1) waddch(stdscr, '\\');
    } else {
      if (wmove(stdscr, 1, 0) != -1) waddch(stdscr, '\\');
      if (wmove(stdscr, 1, 2) != -1) waddch(stdscr, '/');
    }
    wrefresh(stdscr);

    // get key
    char key = -1;
    while (key == -1) key = wgetch(stdscr);
    char nazo = (((0xA1025D352CC2655F >> hoge[(cnt % 32)*2]) & 1) << 1)
      | ((0xA1025D352CC2655F >> hoge[(cnt % 32)*2 + 1]) & 1);
    char direction;

    switch(key) {
    case 'w':
      direction = (nazo + 0b10) & 0b11;
      break;
    case 'a':
      direction = nazo;
      break;
    case 's':
      direction = (nazo + 0b11) & 0b11;
      break;
    case 'd':
      direction = (nazo + 0b01) & 0b11;
      break;

    case 'q': // quit game
      return 0;
    }

    pos = posinfo[cnt];
    switch(direction) {
    case 0: // right
      if ((pos & 0xF) == 0) return 2; // wall
      pos += 1;
      break;
    case 1: // up
      if (pos <= 0xF) return 2; // wall
      pos -= 16;
      break;
    case 2: // left
      if ((pos & 0xF) == 0) return 2; // wall
      pos -= 1;
      break;
    case 3: // down
      if (pos >= 0xFF) return 2; // wall
      pos += 16;
      break;
    }

    switch (field[pos]) {
    case 0: // nothing
      field[posinfo[nocnt]] = 0;
      nocnt++;
      break;
    case 1: // snake!
      return 2;
    case 2: // fruit
      for (int i = 0; i < 50; i++) {
        field[bomb[i]] = 0;
      }
      // put fruit
      offset = rand() % 0x100;
      while (field[offset] != 0) offset++;
      field[offset] = 2;
      // spray bomb
      for (int i = 0; i < 50; i++) {
        offset = rand() % 0x100;
        while (field[offset] != 0) offset++;
        field[offset] = 3;
        bomb[i] = offset;
      }
      break;
    case 3: // bomb!
      return 2;
    }

    field[pos] = 1; // put snake
    posinfo[cnt+1] = pos;
    if (nocnt < cnt + 2)
      v14 = cnt + 2 - nocnt;
    else
      v14 = nocnt - (cnt + 2) + 255
    if (v14 > 24) {
      return 1; // win
    }
    cnt++;
  }
}
