from ptrlib import *

id = int(input("id = "))

with open("jailbreak", "rb") as f:
    f.seek(0x3080 + id*4)
    ofs = u32(f.read(4))
    nextOfs = u32(f.read(4))
    size = nextOfs - ofs
    f.seek(0x3100 + ofs)
    data = f.read(size)

key = id
for i in range(size):
    data = data[:i] + bytes([(data[i] ^ key) & 0xff]) + data[i+1:]
    key = 17*id + data[i]*key

print(data.decode())
