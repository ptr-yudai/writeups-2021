void fill_rand_qword(unsigned long *p) {
  unsigned long r;
  for (int i = 0; i < 8; i++) {
    r |= (rand() & 0xff) << (i*8);
  }
  *p = r;
}
void fill_rand(char *buf) {
  for (int i = 0; i < 5; i++) {
    fill_rand_qword(&buf[i*8]);
  }
}

void randomize(int n) {
  // cpuid
  unsigned cpuinfo_eax, cpuinfo_ebx, cpuinfo_ecx, cpuinfo_edx = cpuid(n);
  srand(rand() + cpuinfo_eax);
  srand(rand() + (n == 1 ? 0 : cpuinfo_ebx));
  srand(rand() + cpuinfo_ecx);
  srand(rand() + cpuinfo_edx);
}

void apply_foreach(double *x, double *y, void *func) {
  for (int i = 0; i < 5; i++) {
    x[i] = func(x[i], y[i]);
  }
}
void map_foraech(double *x, void *func) {
  for (int i = 0; i < 5; i++) {
    x[i] = func(x[i]);
  }
}
void subtract_adjacent(char *buf) {
  for (int i = 0; i < 40; i++) {
    buf[i] -= buf[(i+1) % 40];
  }
}

double move(double a, double b) {
  return b;
}
unsigned long ope_xor(unsigned long a, unsigned long b) {
  return a ^ b;
}
unsigned long ope_negate_byte(unsigned long x) {
  // negate each byte
  return ~x & 0x8080808080808080 ^
    (0x8080808080808080 - (x & 0x7f7f7f7f7f7f7f7f));
}
unsigned long ope_mul_byte(unsigned long a, unsigned long b) {
  unsigned char ac[8], bc[8];
  unsigned long x = 0, y = 0;

  *(unsigned long*)ac = a;
  *(unsigned long*)bc = b;
  for (int i = 0; i < 8; i++) {
    x = (x << 8) | (ac[i]*bc[i] & 0xff);
  }
}
unsigned long ope_mul_byte(unsigned long a, unsigned long b) {
  char ac[8], bc[8];
  unsigned long x = 0, y = 0;

  *(unsigned long*)ac = a;
  *(unsigned long*)bc = b;
  for (int i = 0; i < 8; i++) {
    x = (x << 8) | ((ac[i]*ac[i]+1)*bc[i] & 0xff); // maybe
  }
}

int main()
{
  unsigned char rndbuf_1[40], rndbuf_2[40], output[40], input[40];

  /* input flag */
  printf("What to encrypt? ");
  fgets(flag, 0x29, stdin);
  for (int i = 0; i < 40; i++) {
    input[i] = flag[i];
  }

  /* randomize */
  srand(1337);
  for (int i = 0; i < 7; i++) {
    randomize(i);
  }

  /* encrypt flag */
  fill_rand(rndbuf_1);
  fill_rand(rndbuf_2);

  apply_foreach(output, rndbuf_2, ope_move); // enc = rndbuf_2
  subtract_adjacent(rndbuf_1);
  subtract_adjacent(rndbuf_2);
  subtract_adjacent(output);
  subtract_adjacent(input);

  map_foreach(rndbuf_1, ope_negate_byte);
  subtract_adjacent(rndbuf_1);

  apply_foreach(input, rndbuf_1, ope_xor);
  subtract_adjacent(input);

  apply_foreach(rndbuf_2, input, ope_mul_byte);
  subtract_adjacent(rndbuf_2);

  apply_foreach(output, input, ope_mul);
  subtract_adjacent(output);

  /* save encrypted flag */
  FIEL *fp = fopen("flag.enc", "w");
  for (int i = 0; i < 40; i++) {
    fputc(rndbuf_2[i], fp);
    fputc(output[i], fp);
  }
}
