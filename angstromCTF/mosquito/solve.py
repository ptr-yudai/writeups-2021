import ctypes
libc = ctypes.CDLL('/lib/x86_64-linux-gnu/libc-2.27.so')

def sub(arr):
    for i in range(40):
        arr[i] = (arr[i] - arr[(i+1) % 40]) & 0xff
def invsub(arr):
    for i in range(39, -1, -1):
        arr[i] = (arr[i] + arr[(i+1) % 40]) & 0xff
def fill_rand():
    output = []
    for i in range(40):
        output.append(libc.rand() & 0xff)
    return output
def xor(a, b):
    for i in range(40):
        a[i] ^= b[i]
def negate(a):
    for i in range(40):
        a[i] = ((0xff ^ a[i]) & 0x80) ^  (0x80 - (a[i] & 0x7f))
def mul_1(a, b):
    for i in range(40):
        a[i] = (a[i] * b[i]) & 0xff
def mul_2(a, b):
    for i in range(40):
        a[i] = ((a[i] * a[i] + 1) * b[i]) & 0xff
def restore(seed, cipher):
    libc.srand(seed)
    rndbuf1 = fill_rand()
    rndbuf2 = fill_rand()
    sub(rndbuf1)
    sub(rndbuf2)
    negate(rndbuf1)
    sub(rndbuf1)
    r = bytearray(cipher[0::2])
    o = bytearray(cipher[1::2])
    invsub(r)
    invsub(o)
    i = []
    for j in range(40):
        for c in range(0x100):
            if (rndbuf2[j] * c) & 0xff != r[j]:
                continue
            if ((rndbuf2[j] * rndbuf2[j] + 1) * c) & 0xff == o[j]:
                break
        else:
            print("Not found")
            return
        i.append(c)
    invsub(i)
    xor(i, rndbuf1)
    invsub(i)
    flag = bytes(i)
    print(f"Found: {flag}")

with open("flag.enc", "rb") as f:
    prefix = list(f.read(4))
with open("flag.enc", "rb") as f:
    cipher = f.read()

import datetime
for seed in range(0, 0x100000000):
    if seed % 0x100000 == 0:
        print(f"{datetime.datetime.now()}: {hex(seed)}")
    inp = [ord('a'), ord('c'), ord('t'), ord('f'), ord('{')]
    inp += [0x41 for i in range(40 - len(inp))]

    libc.srand(seed)
    rndbuf1 = fill_rand()
    rndbuf2 = fill_rand()
    output = list(rndbuf2)
    sub(rndbuf1)
    sub(rndbuf2)
    sub(output)
    sub(inp)
    negate(rndbuf1)
    sub(rndbuf1)
    xor(inp, rndbuf1)
    sub(inp)
    mul_1(rndbuf2, inp)
    sub(rndbuf2)
    mul_2(output, inp)
    sub(output)

    if [rndbuf2[0], output[0], rndbuf2[1], output[1]] == prefix:
        print(f"[+] Possible seed: {seed}")
        restore(seed, cipher)
