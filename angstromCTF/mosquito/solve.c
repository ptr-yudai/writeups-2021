#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define START 0x100000000 / 4

void sub(unsigned char *a) {
  for (int i = 0; i < 40; i++) {
    a[i] -= a[(i+1)%40];
  }
}
void negate(unsigned char *a) {
  for (int i = 0; i < 40; i++) {
    a[i] = ((0xff^a[i])&0x80) ^ (0x80-(a[i]&0x7f));
  }
}
void xor(unsigned char *a, unsigned char *b) {
  for (int i = 0; i < 40; i++) {
    a[i] ^= b[i];
  }
}
void mul_1(unsigned char *a, unsigned char *b) {
  for (int i = 0; i < 40; i++) {
    a[i] *= b[i];
  }
}
void mul_2(unsigned char *a, unsigned char *b) {
  for (int i = 0; i < 40; i++) {
    a[i] = (a[i]*a[i]+1) * b[i];
  }
}

int main()
{
  FILE *fp = fopen("flag.enc", "rb");
  unsigned char prefix[4];
  if (fread(prefix, 1, 4, fp) != 4) {
    puts("Error");
    return 1;
  }
  fclose(fp);
  unsigned long seed;
  unsigned char input[40], output[40], rndbuf1[40], rndbuf2[40];
  for (seed = START; seed < 0x100000000; seed++) {
    if (seed % 0x1000000 == 0) {
      printf("%lx : %lx\n", time(NULL), seed);
    }
    srand(seed);
    strcpy(input, "actf{");
    for (int i = 0; i < 40; i++)
      rndbuf1[i] = rand();
    for (int i = 0; i < 40; i++)
      rndbuf2[i] = rand();
    sub(rndbuf1);
    sub(rndbuf2);
    memcpy(output, rndbuf2, 40);
    sub(input);
    negate(rndbuf1);
    sub(rndbuf1);
    xor(input, rndbuf1);
    sub(input);
    mul_1(rndbuf2, input);
    sub(rndbuf2);
    if ((rndbuf2[0] != prefix[0]) || (rndbuf2[1] != prefix[2])) continue;
    mul_2(output, input);
    sub(output);
    if ((output[0] == prefix[1]) && (output[1] == prefix[3])) {
      printf("Candidate: 0x%lx\n", seed);
    }
  }
}
