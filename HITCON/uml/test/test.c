#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main() {
  char buf[0x10000];
  memset(buf, 0xcc, 0x10000);

  FILE *fp = fopen("/proc/self/mem", "w+");

  printf("%ld\n", ftell(fp));
  printf("%ld\n", fwrite(buf, 1, 0x400, fp));
  printf("%ld\n", ftell(fp));
  printf("%ld\n", fwrite(buf, 1, 0x400, fp));
  printf("%ld\n", ftell(fp));
  return 0;
}
