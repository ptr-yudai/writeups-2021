#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <crypt.h>
#include <openssl/md5.h>

int main() {
  char buf[0x100] = { 0 };
  for (int a = 101; a < 0x100; a++) {
    printf("a=%d\n", a);
    for (int b = 0; b < 0x100; b++) {
      printf("b=%d\n", b);
      for (int c = 0; c < 0x100; c++) {
        for (int d = 0; d < 0x100; d++) {
          int len = sprintf(buf, "%d.%d.%d.%didの種20211203", a, b, c, d);

          MD5_CTX ctx;
          unsigned char md5[16], hex[33];
          MD5_Init(&ctx);
          MD5_Update(&ctx, buf, len);
          MD5_Final(md5, &ctx);
          for (int i = 0; i < 16; i++) {
            sprintf(&hex[i*2], "%02x", (unsigned int)md5[i]);
          }

          const char *v = crypt(hex, "id");
          if (memcmp(&v[5], "ueyUrcwA", 8) == 0) {
            puts("FOUND!");
            printf("%d.%d.%d.%d\n", a, b, c, d);
            exit(0);
          }
        }
      }
    }
  }
  return 0;
}
