import os
from Crypto.Util.number import *
from hashlib import *
from binascii import unhexlify

LEN = 17
magic = os.urandom(LEN)
print("Magic:", magic.hex())
print('Coud you use it to solve dlog?')

magic_num = bytes_to_long(magic)
print(hex(magic_num))

n = 384 - 17*8
while True:
    b = 1
    for i in range(7):
        b *= getRandomNBitInteger(n//7)
    a = (magic_num << n) // b + 1
    if isPrime(a * b + 1):
        print("Found!")
        print(a)
        print(b)
        print(hex(magic_num << n))
        print(hex(a*b+1))
        break

