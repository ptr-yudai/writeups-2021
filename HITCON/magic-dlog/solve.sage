import os
from Crypto.Util.number import *
from hashlib import *
from binascii import unhexlify

def pohlig_hellman(g, h, p, limit=100000):
    factors = [f[0]**f[1] for f in (p-1).factor(limit=limit) if f[0] <= limit]
    bs = []
    mods = []
    for f in factors:
        k = (p-1)//f
        b = discrete_log(pow(h,k,p), pow(g,k,p), ord=f)
        bs.append(b)
        mods.append(f)
    return CRT(bs, mods)

LEN = 17
magic = os.urandom(LEN)
print("Magic:", magic.hex())
print('Coud you use it to solve dlog?')

#magic_num = bytes_to_long(magic)
magic_num = 0xd88a384ef4e421c710139e15360a08a774
print(hex(magic_num))

n = 384 - 17*8
while True:
    b = 1
    for i in range(8):
        b *= getRandomNBitInteger(n//8)
    a = (magic_num << n) // b + 1
    if isPrime(a * b + 1):
        print("[CANDIDATE]", a, b)
        if any([p > (1<<37) for p, e in factor(a)]):
            continue
        if [p < (1<<32) for p, e in factor(a)].count(False) <= 1:
            print("[FOUND]")
            print(hex(a*b + 1))
            break

P = a*b + 1
F = GF(P)
assert isPrime(P)

print(factor(P-1))
while True:
    data = os.urandom(384//8)
    data2 = sha384(data).digest()
    num1 = bytes_to_long(data)
    num2 = bytes_to_long(data2)
    if num2 >= P:
        continue
    print("[ATTEMPT] New data...")

    try:
        e = pohlig_hellman(num1, num2, P, limit=1<<37)
        print("mumumu...")
        assert pow(num1, e, P) == num2
        print("[SOLVED!]")
        print(P)
        print(e)
        print(data)
        print(data.hex())
        break
    except KeyboardInterrupt:
        break
    except:
        continue
