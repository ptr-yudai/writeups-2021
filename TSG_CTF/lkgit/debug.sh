qemu-system-x86_64 \
  -kernel ./bzImage \
  -initrd ./debugfs.cpio \
  -nographic \
  -monitor /dev/null \
  -cpu kvm64,smep,smap \
  -append "console=ttyS0 nokaslr oops=panic panic=1 quiet" \
  -no-reboot \
  -gdb tcp::12345 \
  -m 256M
