typedef struct {
  char cmd;
  unsigned short length;
  char data[0x200];
} Packet;

void echoflag(void) {
  uint tbLength;
  ssize_t sVar1;
  undefined8 uVar2;
  uint offset;
  uint readLength;
  uint next_base;
  Packet target:
  Packet pkt;

  memset(&target, 0, sizeof(Packet));
  offset = 3;

  do {
    readLength = read(0, &pkt, sizeof(Packet));
    if (readLength < 3)
      return -1;

    if (pkt.cmd == 1) {

      if (0x200 < pkt.length)
        return -1;

      if (pkt.length + 3 <= readLength) {
        print_data(&pkt);
      }
      memcpy(&target, &pkt, readLength);
      offset = readLength;

    } else {

      next_base = (offset + readLength) - 3; // tl=0x0, ofs=0x203 --> nb=0x23+(r-3)
      if (target.length < next_base) {
        readLength = target.length - offset; // rl=-0x203
        next_base = target.length;           // nb=0
      }

      memcpy(&target + (int)offset, pkt.data, readLength - 3);
      offset = next_base;
      if ((int)target.length <= (int)next_base) {
        offset = 3;
        print_data(&target);
      }

    }

  } while(true);
}

