from ptrlib import *

def add(type, name=None):
    sock.sendlineafter(": ", "1")
    sock.sendlineafter(": ", str(type))
    if type != 7:
        sock.sendlineafter(": ", name)
def delete(index):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", str(index))
def show():
    sock.sendlineafter(": ", "3")
def choose(index):
    sock.sendlineafter(": ", "4")
    sock.sendlineafter(": ", str(index))
def set(skill):
    sock.sendlineafter(": ", "5")
    sock.sendlineafter(": ", str(skill))
def use():
    sock.sendlineafter(": ", "6")

sock = Process("./L0stArk")

n = 0
chosen = None
while True:
    choice = random_int(0, 4)
    if choice == 0 or choice == 4:
        t = random.choice([1, 2, 3, 7])
        s = random_str(1, 0x20).replace(" ", "")
        print(f"add({t}, '{s}')")
        add(t, s)
        n += 1
    elif choice == 1 and n > 0:
        i = random_int(0, n-1)
        print(f"delete({i})")
        delete(i)
        if chosen == i:
            chosen = None
        n -= 1
    elif choice == 2 and n > 0:
        i = random_int(0, n-1)
        print(f"choose({i})")
        choose(i)
        chosen = i
    elif choice == 3 and chosen is not None:
        print(f"use()")
        use()

sock.interactive()
