__int64 sub_1762()
{
  __int64 result; // rax
  __int64 v1; // rdx
  __int64 v2; // rdx
  unsigned int v3; // [rsp+Ch] [rbp-B4h]
  int i; // [rsp+10h] [rbp-B0h]
  int j; // [rsp+14h] [rbp-ACh]
  int fd; // [rsp+18h] [rbp-A8h]
  int v7; // [rsp+1Ch] [rbp-A4h]
  size_t size; // [rsp+20h] [rbp-A0h] BYREF
  void *ptr; // [rsp+28h] [rbp-98h]
  _DWORD *v10; // [rsp+30h] [rbp-90h]
  void *v11; // [rsp+38h] [rbp-88h]
  char s1[4]; // [rsp+40h] [rbp-80h] BYREF
  unsigned int n; // [rsp+44h] [rbp-7Ch]
  _QWORD n_4[4]; // [rsp+48h] [rbp-78h] BYREF
  int v15; // [rsp+6Bh] [rbp-55h] BYREF
  __int64 buf[2]; // [rsp+70h] [rbp-50h] BYREF
  int v17; // [rsp+80h] [rbp-40h]
  __int64 v18; // [rsp+90h] [rbp-30h]
  __int64 v19; // [rsp+98h] [rbp-28h]
  __int64 v20; // [rsp+A0h] [rbp-20h]
  __int64 v21; // [rsp+A8h] [rbp-18h]
  __int64 v22; // [rsp+B0h] [rbp-10h]
  unsigned __int64 v23; // [rsp+B8h] [rbp-8h]

  v23 = __readfsqword(0x28u);
  buf[0] = 0LL;
  buf[1] = 0LL;
  v17 = 0;
  v18 = 0LL;
  v19 = 0LL;
  v20 = 0LL;
  v21 = 0LL;
  v22 = 0LL;
  ptr = 0LL;
  v3 = 0;
  printf("->input restore file size : ");
  read(0, buf, 0x14uLL);
  size = atoi((const char *)buf);
  if ( (__int64)size > 0x28 )
  {
    printf("recv size is %d\n", size);
    ptr = malloc(size);
    if ( !ptr )
    {
      puts("malloc error");
      exit(0xFFFFFFFF);
    }
    printf("->input backup file binary contents : ");
    for ( i = 0; i < (__int64)size; ++i )
      read(0, (char *)ptr + i, 1uLL);
    v10 = (_DWORD *)sub_1FEC(ptr, size, &size);
    free(ptr);
    ptr = v10;
    *(_DWORD *)s1 = *v10;
    v15 = v10[1];
    sub_170A(&v15, &size);
    n = size;
    v1 = *((_QWORD *)ptr + 2);
    n_4[0] = *((_QWORD *)ptr + 1);
    n_4[1] = v1;
    v2 = *((_QWORD *)ptr + 4);
    n_4[2] = *((_QWORD *)ptr + 3);
    n_4[3] = v2;
    puts("download restore file info");
    printf("magic : %s\n", s1);
    printf("size : %d\n", n);
    printf("hash: ");
    for ( j = 0; j <= 0x1F; ++j )
      printf("%02x", *((unsigned __int8 *)n_4 + j));
    putchar(0xA);
    if ( !strncmp(s1, "SCTF", 4uLL) )
    {
      fd = open(file, 0x42, 0x180LL);
      write(fd, (char *)ptr + 0x28, n);
      printf("size is %d\n", n);
      close(fd);
    }
    if ( (unsigned int)sub_2D09(file, n_4) )
    {
      v3 = 1;
    }
    else
    {
      puts("check error");
      v7 = open(file, 0x42, 0x180LL);
      v11 = malloc(n);
      read(v7, v11, n);
      write(1, v11, 0x2800uLL);
      free(v11);
      close(v7);
    }
    free(ptr);
    puts("Download Done");
    result = v3;
  }
  else
  {
    puts("error");
    result = 0LL;
  }
  return result;
}