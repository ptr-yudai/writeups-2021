```c
__int64 __fastcall main(int a1, char **a2, char **a3)
{
  size_t v3; // rax
  int *v4; // rax
  char *v5; // rax
  int v7; // [rsp+0h] [rbp-2770h] BYREF
  int i; // [rsp+4h] [rbp-276Ch]
  int v9; // [rsp+8h] [rbp-2768h]
  int v11; // [rsp+10h] [rbp-2760h]
  int v12; // [rsp+14h] [rbp-275Ch]
  unsigned int v13; // [rsp+18h] [rbp-2758h]
  unsigned int v14; // [rsp+1Ch] [rbp-2754h]
  unsigned int v15; // [rsp+20h] [rbp-2750h]
  int fd; // [rsp+24h] [rbp-274Ch]
  time_t timer; // [rsp+28h] [rbp-2748h] BYREF
  struct tm *v19; // [rsp+38h] [rbp-2738h]
  char s[15]; // [rsp+41h] [rbp-272Fh] BYREF
  char file_name[10008]; // [rsp+50h] [rbp-2720h] BYREF
  unsigned __int64 v22; // [rsp+2768h] [rbp-8h]

  v22 = __readfsqword(0x28u);
  setbuf(stdout, 0LL);
  setbuf(stdin, 0LL);
  alarm(0x14u);
  while ( 1 )
  {
    menu();
    __isoc99_scanf("%d", &v7);
    do
      v9 = getchar();
    while ( v9 != 0xA && v9 != 0xFFFFFFFF );
    switch ( v7 )
    {
      case 1:
        timer = time(0LL);
        v19 = localtime(&timer);
        puts("1.write");
        v13 = v19->tm_mday;
        v14 = v19->tm_mon + 1;
        v15 = v19->tm_year + 0x76C;
        sprintf(s, "data/%d-%02d-%02d", v15, v14, v13);
        printf("Write on %s\n", s);
        puts("Please wirte your contents : ");
        fd = open(s, 0x441, 0x180LL);
        fgets(file_name, 0x2710, stdin);
        v3 = strlen(file_name);
        write(fd, file_name, v3);
        close(fd);
        memset(file_name, 0, 0x2710uLL);
        puts("Done");
        break;
      case 2:
        puts("2.veiwing");
        puts("input date(YYYY-MM-DD) : ");
        read(0, s, 0xAuLL);
        do
          v9 = getchar();
        while ( v9 != 0xA && v9 != 0xFFFFFFFF );
        v12 = regcomp(&preg, "[0-9]{4}-[0-9]{2}-[0-9]{2}", 1);
        if ( v12 )
        {
          puts("could not compile regex");
          exit(0xFFFFFFFF);
        }
        v12 = regexec(&preg, s, 0LL, 0LL, 0);
        if ( v12 )
        {
          puts(" not correct format ");
        }
        else
          {
          sprintf(file_name, "data/%s", s);
          if ( access(file_name, 0) )
            puts("there is no history");
          else
            cat_file_to_stdout(file_name);
        }
        regfree(&preg);
        puts("Done");
        break;
      case 3:
        if ( opendir("data") )
        {
          v11 = scandir(
                  "data",
                  (struct dirent ***)&timer,
                  0LL,
                  (int (*)(const struct dirent **, const struct dirent **))&alphasort);
          if ( v11 == 0xFFFFFFFF )
          {
            v4 = __errno_location();
            v5 = strerror(*v4);
            fprintf(stderr, "Directory Scan Error: %s\n", v5);
          }
          else
          {
            for ( i = 2; i < v11; ++i )
            {
              puts((const char *)(*(_QWORD *)(8LL * i + timer) + 0x13LL));
              sprintf(file_name, "data/%s", (const char *)(*(_QWORD *)(8LL * i + timer) + 0x13LL));
              cat_file_to_stdout(file_name);
            }
            for ( i = 0; i < v11; ++i )
              free(*(void **)(8LL * i + timer));
            free((void *)timer);
          }
        }
        break;
      case 4:
        puts("4.backup data");
        libutil_execute("tar cvfP backup.tar ./data/*");
        copy_file(off_204070, off_204078);      // copy backup.tar to backup.bak
        cat_backup_bak_file_as_base64_to_stdout();
        puts("\nDone");
        break;
      case 5:
        puts("5.restore data");
        if ( !(unsigned int)sub_1762() )
          exit(0xFFFFFFFF);
        libutil_execute("tar xvfP restore.bak -C ./data/");
        puts("Done");
        break;
      case 6:
        puts("bye");
        return 0LL;
      default:
        continue;
    }
  }
}
```
