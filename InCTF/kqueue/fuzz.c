#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <time.h>

typedef struct{
    uint32_t max_entries;
    uint16_t data_size;
    uint16_t entry_idx;
    uint16_t queue_idx;
    char* data;
} request_t;

void fatal(const char *msg) {
  perror(msg);
  exit(1);
}

int fd;
int create(int max, int size) {
  request_t r = {.max_entries=max, .data_size=size};
  printf("create(%d, %d);\n", max, size);
  return ioctl(fd, 0xDEADC0DE, &r);
}
int delete(int index) {
  request_t r = {.queue_idx = index};
  printf("delete(%d);\n", index);
  return ioctl(fd, 0xBADDCAFE, &r);
}
int edit(int index, int offset, char *data, int size) {
  request_t r = {.data_size = size, .queue_idx = index,
                 .entry_idx = offset, .data = data};
  printf("edit(%d, %d, %p, %d);\n", index, offset, data, size);
  return ioctl(fd, 0xDAADEEEE, &r);
}
int save(int index, int max, int size) {
  request_t r = {.max_entries=max, .data_size=size,
                 .queue_idx=index};
  printf("save(%d, %d, %d);\n", index, max, size);
  return ioctl(fd, 0xB105BABE, &r);
}

int main() {
  fd = open("/dev/kqueue", O_RDWR);
  if (fd == -1) fatal("/dev/kqueue");

  srand(time(NULL));
  int alive[] = {0, 0, 0, 0};
  while (1) {
    int choice = rand() % 4;
    switch (choice) {
    case 0: {
      create(rand() % 6, rand() % 0x20);
      for (int i = 0; i < 4; i++) {
        if (alive[i] == 0) {
          alive[i] = 1;
          break;
        }
      }
    }
    case 1: {
      for (int j = 0; j < 10; j++) {
        int idx = rand() % 4;
        if (alive[idx] == 1) {
          delete(idx);
          alive[idx] = 0;
          break;
        }
      }
    }
    case 2: {
      for (int j = 0; j < 10; j++) {
        int idx = rand() % 4;
        if (alive[idx] == 1) {
          edit(idx, rand() % 6, "AAAAAAAABBBBBBBBCCCCCCCCDDDDDDDD", rand() % 0x20);
          break;
        }
      }
    }
    case 3: {
      for (int j = 0; j < 10; j++) {
        int idx = rand() % 4;
        if (alive[idx] == 1) {
          save(idx, rand() % 6, rand() % 0x20);
          break;
        }
      }
    }
    }
  }
  return 0;
}
