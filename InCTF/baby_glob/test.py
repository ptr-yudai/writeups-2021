import subprocess
import re

cnt = 0
while True:
    cnt += 1
    r = subprocess.check_output(["cat", "/proc/self/maps"])
    
    x = re.findall(b"[0-9a-f]+\-([0-9a-f]+) ", r.split(b'\n')[4])
    tail = int(x[0], 16)
    x = re.findall(b"([0-9a-f]+)\-[0-9a-f]+ ", r.split(b'\n')[5])
    head = int(x[0], 16)

    ofs = head - tail
    if ofs == 0:
        print(cnt)
        print(r.decode())
        break
