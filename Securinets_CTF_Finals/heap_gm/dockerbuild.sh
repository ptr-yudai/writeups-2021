#!/bin/sh

# run as root.
sudo docker build -t heapgm .
sudo docker run --name heapgm -p 9996:9996 -d heapgm

echo "-----------------------------------------------------------"
echo "Now you can connect to the challenge ex (nc 127.0.0.1 9996)"
echo "-----------------------------------------------------------"
