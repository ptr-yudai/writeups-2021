# b64lib
Author: ptr-yudai

## Title
b64lib

## Description
Hackers always love base64.

## Flag
`ASIS{1mpL1c1T_Typ3_c0nv3rS10n}`

## Deployment
```
docker-compose up
```
Distribute all files under `b64lib/distfiles` to the players.
