from sage.all import *
from Crypto.Util.number import *

with open("output.txt", "r") as f:
    exec(f.read())
wp, wq = 77, 77

for i in range(-4, 0):
    pq_upper = pkey // 10**(2*wq+wp)
    for j in range(-4, 4):
        for k in range(-2, 2):
            pq_lower = pkey % 10**(wq-j)
            pq = (pq_upper + i) * 10**(wq+k) + pq_lower
            hoge = pkey - (10**(wp+wq)+1)*pq
            if hoge < 0:
                continue
            if hoge % 10**wp != 0:
                continue
            p2q2 = hoge // 10**wp
            if ZZ(p2q2 + 2*pq).is_square():
                p_q = sqrt(p2q2 + 2*pq)
                p, q = var('p'), var('q')
                answers = solve([p*q == pq, p+q == p_q], [p,q])
                for ans in answers:
                    p, q = ans[0].right(), ans[1].right()
                    if not is_prime(p) or not is_prime(q):
                        continue
                    P = 10**wq * p + q
                    Q = 10**wp * q + p
                    if not is_prime(P) or not is_prime(Q):
                        continue
                    phi = (P-1)*(Q-1)
                    d = inverse_mod(31337, phi)
                    m = power_mod(enc, d, int(P*Q))
                    print(int.to_bytes(m, 512//8, 'big'))
                    exit()
