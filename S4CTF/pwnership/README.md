# pwnership
Author: ptr-yudai

## Title
pwnership

## Description
You don't have to worry about burning bridges, if you're building your pwn.

## Flag
`ASIS{0wn3rsh1p_1s_s0m3t1m3s_tr0ubl3s0m3}`

## Deployment
```
docker-compose up
```
Distribute all files under `pwnership/distfiles` to the players.

In case the solver in `solution` doesn't work, try
```
$ docker exec -it pwnership /bin/bash
root@XXXX# apt update
root@XXXX# apt upgrade
root@XXXX# md5sum /lib/x86_64-linux-gnu/libc-2.31.so
```
and check if the md5sum is same as that of `pwnership/distfiles/libc.so.6`.
