from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin, AdminIndexView, expose
from flask_admin.contrib.sqla import ModelView

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///login.db'

db = SQLAlchemy(app)

class MyAdmin(AdminIndexView):
    @expose('/')
    def index(self):
        return super(MyAdmin, self).index()

    @expose('/user')
    @expose('/user/')
    def user(self):
        return render_template_string('TODO, need create custom view')

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30),unique=True)
    e_mail = db.Column(db.String(50),unique=True)

#DBのクリエイト宣言
db.create_all()

#DBが空の状態(最初の1回)はtestuserを作成する
user = User.query.filter_by(username='testuser').first()
if user is None:
    testuser = User(username='testuser', e_mail='test@test')
    db.session.add(testuser)
    db.session.commit()

admin = Admin(app)
admin.add_view(ModelView(User, db.session))


@app.route('/')
def index():
    user = User.query.filter_by(username='testuser').first()
    user_name = user.username
    return 'Welcome ' + user_name

if __name__ == '__main__':
    app.run(debug=True)
